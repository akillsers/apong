struct glyphinfo
{
    u32 codepoint;
    f32 index;
    //bounding box
    i32 x0;
    i32 y0;
    i32 x1;    
    i32 y1;
};
