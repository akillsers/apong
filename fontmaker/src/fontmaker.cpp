#include <malloc.h>
#include "../../akela/aks.h"
#include "fontmaker.h"
#define HEADER_SIZE 24
#define PER_GLYPH_SIZE 40

#pragma pack(push, 1)
typedef struct
{
    unsigned short x0,y0,x1,y1; // coordinates of bbox in bitmap
    float xoff,yoff,xadvance;
    float xoff2,yoff2;
} stbtt_packedchar;

typedef struct
{
    unsigned short glyphw, glyphh;
    float xoff,yoff,xadvance;
    float xoff2,yoff2;
    float u, v, s, t;
} akela_packedchar;
#pragma pack(pop, 1)

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"


#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <stdio.h>


//   Improved 3D API (more shippable):
//           #include "stb_rect_pack.h"           -- optional, but you really want it
//           stbtt_PackBegin()
//           stbtt_PackSetOversampling()          -- for improved quality on small fonts
//           stbtt_PackFontRanges()               -- pack and renders
//           stbtt_PackEnd()
//           stbtt_GetPackedQuad()

/*
typedef struct
{
   float x0,y0,s0,t0; // top-left
   float x1,y1,s1,t1; // bottom-right
} stbtt_aligned_quad;

typedef struct
{
   unsigned short x0,y0,x1,y1; // coordinates of bbox in bitmap
   float xoff,yoff,xadvance;
   float xoff2,yoff2;
} stbtt_packedchar;
*/

int main(int argc, char *argv[])
{
    const int FirstChar = 32;
    const int LastChar = 126;
    int TotalChars = LastChar - FirstChar;
    
    if(argc < 3 || str_equal(argv[1], "/?") || str_equal(argv[1], "-help") ||
       str_equal(argv[1], "/help") || str_equal(argv[1], "-?"))
    {
        printf("usage: %s filename pointsize \n", argv[0]);
        return 0;
    }

    f32 pointsize = atof(argv[2]);
    if(!pointsize)
    {
        printf("Invalid point size! \n");
        return 0;
    }
    
    u8* file_noext = (u8 *)argv[1];
    u32 filepath_strlen = str_length(argv[1]);
    u8* file_str = (u8 *)malloc(filepath_strlen + 1);
    str_cpysize(filepath_strlen, file_str, file_noext);
    str_trimextension(file_noext);

    u8 file_bmp[512] = {};
    u8 *p;
    p = str_concat(file_bmp, file_noext);
    p = str_concat(p, (u8 *)".bmp");

    u8  file_aft[512] = {};
    p = str_concat(file_aft, file_noext);
    p = str_concat(p, (u8*)".aft");
    printf("\n");
    
    u8* font_data = (u8*)read_entire_file(file_str); 
    if(font_data)
    {
        stbtt_fontinfo fontinfo;
        if(!stbtt_InitFont(&fontinfo, font_data, 0))
        {
            fprintf(stderr, "Could not init the font. Is the font corrupt??");
            return false;
        }
        /*
          72pt size font is 1 logical inch tall
          1 logical inch is 96 pixels for DPI setting 100% (96 DPI)
        */

        //font height is ascent to descent (not counting leading)
        f32 fontheight_in_logicalinches = (pointsize / 72.0f);
        const f32 DPI = 96;
        f32 fontsize_stbtt = fontheight_in_logicalinches * DPI;
        f32 scalefactor = stbtt_ScaleForPixelHeight(&fontinfo, fontsize_stbtt);

        i32 boundx1 = 0;
        i32 boundy1 = 0;
        i32 boundx2 = 0;
        i32 boundy2 = 0;

        // WHAT does GetFontBoundingBox actually dooo
        // stbtt_GetFontBoundingBox(&fontinfo, &boundx1, &boundy1, &boundx2, &boundy2);
#define LTR 0
#if 0        
        i32 max_w = (boundx2 - boundx1) * scalefactor;
        i32 max_h = (boundy2 - boundy1) * scalefactor;
#else
#if !LTR
        i32 max_w = 4000;
        i32 max_h = 600;
#else
        i32 max_w = 500;
        i32 max_h = 4000;
#endif
#endif
        u8 *pixels = (u8 *)calloc(max_w*max_h, 1);

        i32 glyph_table_size = PER_GLYPH_SIZE * (LastChar-FirstChar);
        u8 *output = (u8 *)malloc(HEADER_SIZE + glyph_table_size);
        u8 *output_cursor = output;

        *((u32*)output_cursor)  = 'TNFA';
        output_cursor += sizeof(u32);
            
        *((u32*)output_cursor) = FirstChar;
        output_cursor += sizeof(u32);

        *((u32*)output_cursor) = TotalChars;
        output_cursor += sizeof(u32);

        *((u32*)output_cursor) = fontsize_stbtt;
        output_cursor += sizeof(u32);

        int ascentwin;
        int descentwin;
        int linegapwin;
        u16 internalleading;

        if(!stbtt_GetFontVMetricsOS2(&fontinfo, &ascentwin, &descentwin, &linegapwin))
        {
            fprintf(stderr, "There is no OS/2 table in the font file! please use an updated version of fontmaker that supports fonts without this table! \n");
            return -1;
        }

        u16 unitsperem = stbtt_GetUnitsPerEm(&fontinfo);
        int internalLeading = ascentwin + descentwin - unitsperem;
        f32 yadvance = (ascentwin + descentwin + linegapwin) * scalefactor;
        printf("yadvance: %0.2f\n", yadvance);
            
        *((f32*)output_cursor) = yadvance;
        output_cursor += sizeof(f32);

        *((f32*)output_cursor) = (ascentwin-internalLeading) * scalefactor;
        output_cursor += sizeof(f32);
        u8 *glyph_table_start = output_cursor;
        
        glyphinfo *glyphs = (glyphinfo *) malloc(sizeof(glyphinfo) *  TotalChars);
        for(int charindex = FirstChar; charindex < LastChar; ++charindex)
        {
            glyphinfo *glyph = glyphs + (charindex - FirstChar);
            glyph->index = stbtt_FindGlyphIndex(&fontinfo, charindex);
            glyph->codepoint = charindex;
            stbtt_GetGlyphBitmapBox(&fontinfo, glyph->index, scalefactor, scalefactor,
                                    &glyph->x0, &glyph->y0, &glyph->x1, &glyph->y1);
        }

        //sorting pass
        for(int i = 0; i < TotalChars-1; i++)
        {
            for(int j = 0; j < TotalChars-1-i; j++)
            {
                glyphinfo *this_glyph = &glyphs[j];
                glyphinfo *next_glyph = &glyphs[j+1];

                int this_glyph_height = this_glyph->y1-this_glyph->y0;
                int next_glyph_height = next_glyph->y1-next_glyph->y0;

                int this_glyph_width = this_glyph->x1-this_glyph->x0;
                int next_glyph_width = next_glyph->x1-next_glyph->x0;

#define SORTBYWIDTH next_glyph_width < this_glyph_width
#define SORTBYHEIGHT next_glyph_height < this_glyph_height
#if LTR
#define SORTFUNC SORTBYHEIGHT
#else
#define SORTFUNC SORTBYWIDTH
#endif
                if(SORTFUNC)
                {
                    glyphinfo temp = *next_glyph;
                    *next_glyph = *this_glyph;
                    *this_glyph = temp;
                }
            }
        }

        int currx = 0;
        int curry = 0;

        u32 max_shrinked_w = 0;
        u32 max_shrinked_h = 0;

#if LTR
        int max_height_in_row = 0;
#else
        int max_width_in_row = 0;
#endif
        const u32 padding = 1;
        
        for(int i = 0; i < TotalChars; i++)
        {
#if 1
            //TODO what is the point of GetGlyphBitmapBox

            // int x0 = 0;
            // int y0 = 0;
            // int x1 = 0;
            // int y1 = 0;
            // stbtt_GetGlyphBox(&fontinfo, glyph_index, &x0, &y0, &x1, &y1);
            // f32 bx0 = (f32)x0 *scalefactor;
            // f32 by0 = (f32)y0 *scalefactor;
            // f32 bx1 = (f32)x1 *scalefactor;
            // f32 by1 = (f32)y1 *scalefactor;
#endif
            glyphinfo *glyph = &glyphs[i];
            int leftSideBearing = 0;
            int xadvance = 0;
            stbtt_GetGlyphHMetrics(&fontinfo, glyph->index, &xadvance, &leftSideBearing);
            i32 required_width = (glyph->x1-glyph->x0);
            i32 required_height = (glyph->y1-glyph->y0);
#if LTR
            if(required_width + currx >= max_w)
            {
                if(currx > max_shrinked_w)
                    max_shrinked_w = currx;

                currx = 0;
                curry += max_height_in_row + padding;
                max_height_in_row = 0;
            }
#else
            if(curry + required_height >= max_h)
            {
                if(curry > max_shrinked_h)
                    max_shrinked_h = curry;

                curry = 0;
                currx += max_width_in_row + padding;
                max_width_in_row = 0;
            }
#endif
            u8 *glyph_loc = pixels + currx  + (max_w * curry);
            stbtt_MakeGlyphBitmap(&fontinfo, glyph_loc, required_width, required_height, max_w, scalefactor, scalefactor, glyph->index);
#if LTR
            if(required_height > max_height_in_row)
                max_height_in_row = required_height;
#else
            if(required_width > max_width_in_row)
                max_width_in_row = required_width;
#endif
            akela_packedchar a;
            a.glyphw = glyph->x1 -glyph->x0;
            a.glyphh = glyph->y1 - glyph->y0;
            a.xoff = leftSideBearing * scalefactor;
            a.yoff = glyph->y0;
            a.xadvance = xadvance * scalefactor;
            a.xoff2 = 0; // we don't use this guy
            a.yoff2 = 0; //we don't use this guy

            a.u = (currx);
            a.v = (f32)(curry);
            a.s = (f32)(currx + required_width);
            a.t = (f32)(curry + required_height);

            *((akela_packedchar *)glyph_table_start + glyph->codepoint - FirstChar) = a;
            // *(akela_packedchar *)output_cursor = a;
            // output_cursor+= sizeof(akela_packedchar);
#if LTR
            currx += required_width + padding;
#else
            curry += required_height + padding;
#endif
        }
#if LTR
        max_shrinked_h = curry + max_height_in_row;
#else
        max_shrinked_w = currx + max_width_in_row;
#endif
        for(int i = 0; i < TotalChars; i++)
        {
            akela_packedchar *packed = (akela_packedchar *) glyph_table_start + i;
            packed->u /= (f32) max_shrinked_w;
            packed->v /= (f32) max_shrinked_h;
            packed->s /= (f32) max_shrinked_w;
            packed->t /= (f32) max_shrinked_h;
        }
        //blit the INFINITELY (totally) sized buffer into an image buffer that is only as big as it needs to be
        //NOTE:: we do this since stbi_write_bmp cannot take a stride in bytes
        u8 *shrinked_pixels = (u8*)calloc(max_shrinked_w * max_shrinked_h, 1);
        u8 *row = pixels;
        u8 *dest = shrinked_pixels;
        for(int i = 0; i < max_shrinked_h; i++)
        {
            u8 *src = row;
            for(int j = 0; j < max_shrinked_w; j++)
            {
                *dest++ = *src++;
            }
            row += max_w;
        }
        
        //TODO get a way to remove the extension of a name and affix .bmp .aft
        //for visual purposes only
        if (stbi_write_bmp((char *)file_bmp, max_shrinked_w, max_shrinked_h, 1, shrinked_pixels))
        {
            printf("Sucessfully wrote %s. \n", file_bmp);
        }
        else
        {
            printf("ERROR: %s could not be written to. \n", file_bmp);
        }
        if(write_file(file_aft, output, HEADER_SIZE + glyph_table_size))
        {
            printf("Sucessfully wrote %s. \n", file_aft);
        }
        else
        {
            printf("ERROR: %s could not be written to. \n", file_aft);
        }

    }
    else
    {
        printf("ERROR: Font file %s could not be read.\n", file_str);
    }
    
    printf("Finished. \n");
    return 0;

}
