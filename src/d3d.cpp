#if INTERNAL
#define D3D_DEBUG_INFO
#endif
#include <d3d9.h>
//TODO POSSIBLE MEMORY LEAKAGE

#if D3D_USE_EX
IDirect3DDevice9Ex *d3ddev;
IDirect3D9Ex *d3d;
#else
LPDIRECT3DDEVICE9 d3ddev;
LPDIRECT3D9 d3d; //pointer to Direct3d interface
#endif
#define VertexFVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)
#define TextureVertexFVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)

struct Vertex
{
    FLOAT x, y, z, rhw;
    DWORD color;
};

struct TextureVertex
{
    FLOAT x, y, z, rhw;
    DWORD color;

    f32 tu, tv;
};

#define MAX_D3D_TEXTURES 20
struct D3DTexture
{
    IDirect3DTexture9 *texture;
    IDirect3DTexture9 *sys_texture;
    u32 width;
    u32 height;
};

struct
{
    LPDIRECT3DVERTEXBUFFER9 v_buffer;
    LPDIRECT3DVERTEXBUFFER9 texturev_buffer;

    u32 viewport_w;
    u32 viewport_h;

    u32 num_textures;
    D3DTexture textures[MAX_D3D_TEXTURES];
    D3DPRESENT_PARAMETERS d3dpp = {};
    D3DDISPLAYMODEEX display_mode;
}d3d_globals;

internal void
d3d_set_states()
{
    HRESULT result;
    //NOTE:: apparently render states reset to default when d3ddev->Reset is called
    result = d3ddev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    assert(result == S_OK);
    result = d3ddev->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
    assert(result == S_OK);
    result = d3ddev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
    assert(result == S_OK);
    result = d3ddev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
    assert(result == S_OK);
    result = d3ddev->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
    assert(result == S_OK);

    d3ddev->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    d3ddev->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    d3ddev->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
    d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
    d3ddev->SetSamplerState(0, D3DSAMP_ADDRESSW, D3DTADDRESS_CLAMP);
}

internal void
d3d_gen_buffers()
{
    HRESULT result;
    result = d3ddev->CreateVertexBuffer(4 * sizeof(Vertex),
                                        D3DUSAGE_DYNAMIC, //usage
                                        VertexFVF,
                                        D3DPOOL_DEFAULT, //D3DPOOL_MANAGED is not valid for ex
                                        &d3d_globals.v_buffer,
                                        0); //reserved
    assert(result == S_OK);

    result = d3ddev->CreateVertexBuffer(4 * sizeof(TextureVertex),
                                        D3DUSAGE_DYNAMIC, //usage
                                        TextureVertexFVF,
                                        D3DPOOL_DEFAULT,
                                        &d3d_globals.texturev_buffer,
                                        0); //reserved
    
    assert(result == S_OK);
}

internal void
d3d_reset_device()
{
    HRESULT result;

    DWORD render_state;

    //TO TAKEAWAY
#if !(D3D_USE_EX)
    //TODO I don't think we need to do this
    IDirect3DSurface9 *default_render_target;
    d3ddev->GetRenderTarget(0, &default_render_target);
    default_render_target->Release();

    for(int i = 0; i < d3d_globals.num_textures; i++)
    {
        if(d3d_globals.textures[i].texture)
        {
            d3d_globals.textures[i].texture->Release();
            d3d_globals.textures[i].texture = {};
            d3d_globals.textures[i].sys_texture->AddDirtyRect(0);
        }
    }
    d3d_globals.v_buffer->Release();
    d3d_globals.texturev_buffer->Release();
#endif
#if D3D_USE_EX


    result = d3ddev->ResetEx(&d3d_globals.d3dpp, d3d_globals.d3dpp.Windowed ? 0 : &d3d_globals.display_mode);
    assert(result == S_OK);
#else
    result = d3ddev->Reset(&d3d_globals.d3dpp);
    assert(result == S_OK);
    d3d_gen_buffers();
#endif

    d3d_set_states();
}

internal void
d3d_begin()
{
    d3ddev->Clear(0, 0, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
    d3ddev->BeginScene();
}

internal void
d3d_end()
{
    d3ddev->EndScene();
    // OLD d3d9 non ex code
#if !(D3D_USE_EX)
   HRESULT cooperative_level = d3ddev->TestCooperativeLevel();
    if (cooperative_level == D3DERR_DEVICENOTRESET)
    {
        d3d_reset_device();
    }
#endif
    RECT rect;
    rect.left = platform.w_w/2 - d3d_globals.viewport_w/2;
    rect.top = platform.w_h/2 - d3d_globals.viewport_h/2;
    rect.right = rect.left + d3d_globals.viewport_w;
    rect.bottom = rect.top + d3d_globals.viewport_h;

    RECT viewport_rect;
    viewport_rect.left = 0;
    viewport_rect.top = 0;
    viewport_rect.right = d3d_globals.viewport_w;
    viewport_rect.bottom = d3d_globals.viewport_h;
#if D3D_USE_EX
    HRESULT present_result = d3ddev->PresentEx(&viewport_rect,
                                               &rect,
                                               0, //hwnd override
                                               0,
                                               0);
    if(present_result != S_OK)
    {
        HRESULT device_state = d3ddev->CheckDeviceState(win32.window);
        
        switch(device_state)
        {
            case D3DERR_DEVICELOST:
                stop("not ok");
                break;
            case S_PRESENT_OCCLUDED:
                //we will present later
                break;
            case S_PRESENT_MODE_CHANGED:
                //lets reset the device to something compatible
                d3d_reset_device();
                break;
            default:
                stop("we do not know how to handle this");
        }
    }
#else
    HRESULT present_result = d3ddev->Present(&viewport_rect, &rect, 0, 0);
    if(present_result != S_OK)
    {
        switch(present_result)
        {
            case D3DERR_DEVICELOST:
                stop("its_removed");
                break;
            
        }
    }
    // d3ddev->Present(0, 0, 0, 0);
#endif

}

    
    // result = texture->LockRect(0, &lockrect, NULL, 0); //null locks the entire reigion
    // assert(result == S_OK);
    // //u8 *srcrow = (u8 *)pixels;
    // u8 *row = (u8 *)lockrect.pBits;

    // //u32 source_pitch = ((width +3)/4)*4;
    // D3DCAPS9 caps;
    // d3ddev->GetDeviceCaps(&caps);
    // for(int i = 0; i < height; i++)
    // {
    //     u8 *pixel = row;
    //     //u8 *srcpix = srcrow;
    //     for(int j = 0; j < width/3; j++)
    //     {
    //         *pixel++ = 255;
    //     }
    //     row += lockrect.Pitch;
    // }
    // texture->UnlockRect(0);

    // d3d_globals.textures[d3d_globals.num_textures] = texture;
internal void
d3d_create_greyscaletexture(D3DTexture *out_D3DTexture, u32 width, u32 height, void *pixels)
{
    //THIS IS TRES DUMB
    HRESULT result;
    result = d3ddev->CreateTexture(width, height, 1, 0, D3DFMT_A8, D3DPOOL_DEFAULT, &out_D3DTexture->texture,0);
    out_D3DTexture->width = width;
    out_D3DTexture->height = height;

    assert(result == S_OK);
    D3DLOCKED_RECT lockrect = {};

    //d3ddev->SetTexture(0, dtexture);
    // IDirect3DBaseTexture9 *base_texture;
    // d3ddev->GetTexture(0, &base_texture);

    //also known as the staging texture
    result = d3ddev->CreateTexture(width, height, 1, 0, D3DFMT_A8, D3DPOOL_SYSTEMMEM, &out_D3DTexture->sys_texture, 0);
    assert(result == S_OK);
    RECT rect_to_lock;
    rect_to_lock.left = 0;
    rect_to_lock.top = 0;
    rect_to_lock.right = width;
    rect_to_lock.bottom = height;
    result = out_D3DTexture->sys_texture->LockRect(0, &lockrect, &rect_to_lock, 0);

    u8 *destrow = (u8 *)lockrect.pBits;
    u8 *srcrow = (u8 *)pixels;

    u32 srcpitch = ((width+3)/4)*4;
    for(int i = 0; i < height; i++)
    {
        u8* destpixel = destrow;
        u8 *srcpixel = srcrow;
        for(int j = 0; j < width; j++)
        {
            *destpixel++ = *srcpixel++;
        }
        destrow += lockrect.Pitch;
        srcrow += srcpitch;
    }
    result = out_D3DTexture->sys_texture->UnlockRect(0);
    assert(result == S_OK);

    result = d3ddev->UpdateTexture(out_D3DTexture->sys_texture, out_D3DTexture->texture);
    assert(result == S_OK);
    //sys_texture->Release();
}

internal u32
d3d_add_greyscaletexture(u32 width, u32 height, void *pixels)
{
    D3DTexture *texture = &d3d_globals.textures[d3d_globals.num_textures];
    d3d_create_greyscaletexture(texture, width, height, pixels);
    return d3d_globals.num_textures++;
}
//TODO don;t 
internal void
d3d_draw_texturemasked_instanced(u32 draw_count,
                                 Rect *transforms, Vec2 *fetchuvs, Vec2 *fetchsts,
                                 Renderer_RGBA *colors, Renderer_Texture1c *tex1c)
{
    HRESULT result;
    if(d3d_globals.textures[tex1c->GPUHandle].texture == 0)
    {
        if(d3d_globals.textures[tex1c->GPUHandle].sys_texture)
        {
            //NOTE:: we know that d3d9ex will never trigger this path
            //recreate the texture
            //TODO On startup this gets called because reset is called on startup which is unnecessary
            d3ddev->CreateTexture(d3d_globals.textures[tex1c->GPUHandle].width,
                                  d3d_globals.textures[tex1c->GPUHandle].height,
                                  1,
                                  0,
                                  D3DFMT_A8,
                                  D3DPOOL_DEFAULT,
                                  &d3d_globals.textures[tex1c->GPUHandle].texture,
                                  0);
            d3ddev->UpdateTexture(d3d_globals.textures[tex1c->GPUHandle].sys_texture, d3d_globals.textures[tex1c->GPUHandle].texture);
        }
        else
        {
            //NOTE:: we are a new device:: recreate the texture even the sys texture
            D3DTexture *texture = &d3d_globals.textures[tex1c->GPUHandle];
            //NOTE:: since this is not a brand new texture we need to add a dirty rect (I think)
            d3d_create_greyscaletexture(texture, tex1c->width, tex1c->height, tex1c->pixels);
        }
    }

    d3ddev->SetTexture(0, d3d_globals.textures[tex1c->GPUHandle].texture);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
    d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
    d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);

    result = d3ddev->SetFVF(TextureVertexFVF);

    //TODO do we need to set this every single time
    result = d3ddev->SetStreamSource(0, d3d_globals.texturev_buffer, 0, sizeof(TextureVertex));


    for(int i = 0; i < draw_count; i++)
    {
        D3DCOLOR d3dcolor = D3DCOLOR_COLORVALUE(colors[i].r, colors[i].g, colors[i].b, colors[i].a);
        TextureVertex vertices[] =
            {
                {transforms[i].x, transforms[i].y + transforms[i].height, 0.0f, 1.0f, d3dcolor, fetchuvs[i].u, fetchsts[i].t},
                {transforms[i].x, transforms[i].y, 0.0f, 1.0f, d3dcolor, fetchuvs[i].u, fetchuvs[i].v},
                {transforms[i].x + transforms[i].width, transforms[i].y + transforms[i].height, 0.0f, 1.0f, d3dcolor, fetchsts[i].s, fetchsts[i].t},
                {transforms[i].x + transforms[i].width, transforms[i].y, 0.0f, 1.0f, d3dcolor, fetchsts[i].s, fetchuvs[i].v},
            };
        VOID *data;
        //TODO no need for a memcpy every single time!!
        //TODO consider setting the world transform instead??
        d3d_globals.texturev_buffer->Lock(0, 0, &data, D3DLOCK_DISCARD);
        Copy(data, vertices, sizeof(vertices));
        result = d3d_globals.texturev_buffer->Unlock();
        assert(result == S_OK);

        result = d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    }
}
    


internal void
d3d_draw_texturemasked(f32 x, f32 y, f32 width, f32 height,
                       f32 u, f32 v, f32 s, f32 t,
                       Renderer_RGB tint, u32 texID)
{
    HRESULT result;
    //TODO slight hackery

    //old d3d9 code
    d3ddev->SetTexture(0, d3d_globals.textures[texID].texture);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
    d3ddev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG1);
    d3ddev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_DIFFUSE);

    D3DCOLOR d3dcolor = D3DCOLOR_COLORVALUE(tint.r, tint.g, tint.b, 1.0f);
    TextureVertex vertices[] =
    {
        {x, y + height, 0.0f, 1.0f, d3dcolor, u, t},
        {x, y, 0.0f, 1.0f, d3dcolor, u, v},
        {x + width, y + height, 0.0f, 1.0f, d3dcolor, s , t},
        {x + width, y, 0.0f, 1.0f, d3dcolor, s, v},
    };
    VOID *data;
    //TODO no need for a memcpy every single time!!
    //TODO consider setting the world transform instead??
    d3d_globals.texturev_buffer->Lock(0, 0, &data, 0);
    Copy(data, vertices, sizeof(vertices));
    result = d3d_globals.texturev_buffer->Unlock();
    assert(result == S_OK);

    
    result = d3ddev->SetFVF(TextureVertexFVF);
    result = d3ddev->SetStreamSource(0, d3d_globals.texturev_buffer, 0, sizeof(TextureVertex));
    result = d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
}

internal void
d3d_draw_rectangle(f32 x, f32 y, f32 width, f32 height, Renderer_RGB color, f32 transparency = 1.0f)
{
    HRESULT result;
    //NOTE WARNING TODO NOW THIS is UNDOCUMENTED set texture 0 may not actually unset the texture
    //RESEARCH CAN we set a null texture!!!!
    d3ddev->SetTexture(0, 0);

    D3DCOLOR d3dcolor = D3DCOLOR_COLORVALUE(color.r, color.g, color.b, transparency);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);
    d3ddev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_DIFFUSE);

    Vertex vertices[] =
    {
        {x, y + height, 0.0f, 1.0f, d3dcolor},
        {x, y, 0.0f, 1.0f, d3dcolor},
        {x + width, y + height, 0.0f, 1.0f, d3dcolor},
        {x + width, y, 0.0f, 1.0f, d3dcolor},
    };
    VOID *data;
    //TODO no need for a memcpy every single time!!
    //TODO consider setting the world transform instead??
    d3d_globals.v_buffer->Lock(0, 0, &data, 0);
    Copy(data, vertices, sizeof(vertices));
    result = d3d_globals.v_buffer->Unlock();
    assert(result == S_OK);

    result = d3ddev->SetFVF(VertexFVF);
    assert(result == S_OK);
    result = d3ddev->SetStreamSource(0, d3d_globals.v_buffer, 0, sizeof(Vertex));
    assert(result == S_OK);
    result = d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    assert(result == S_OK);
}

// internal D3DDISPLAYMODE
// d3d_get_optimal_display_mode(D3DFORMAT format, UINT adapter)
// {
//     u32 pix_count = 0;
//     D3DDISPLAYMODE chosen = {};

//     u32 adapter_mode_count = d3d->GetAdapterModeCount(adapter, format);
//     for(int i = 0; i < adapter_mode_count; i++)
//     {
//         D3DDISPLAYMODE display_mode;
//         HRESULT adapter_result = d3d->EnumAdapterModes(adapter,
//                                                        format,
//                                                        i,
//                                                        &display_mode);
//         if(adapter_result == S_OK)
//         {
//             u32 this_pix_count = display_mode.Width * display_mode.Height;
//             if(this_pix_count > pix_count)
//             {
//                 pix_count = this_pix_count;
//                 chosen = display_mode;
//             }
            
//         }
//     }
//     return chosen;
// }

internal void
d3d_create_device(UINT adapter)
{
    HRESULT result;
    if(d3ddev)
    {

        // //TODO I don't think we need to do this
        // IDirect3DSurface9 *default_render_target;
        // d3ddev->GetRenderTarget(0, &default_render_target);
        // default_render_target->Release();

        //NOTE:: we assume that these exist
        if(d3d_globals.v_buffer)
            d3d_globals.v_buffer->Release();
        if(d3d_globals.texturev_buffer)
            d3d_globals.texturev_buffer->Release();

        for(int i = 0; i < d3d_globals.num_textures; i++)
        {
            if(d3d_globals.textures[i].texture)
            {
                //TODO I think textures are assciate the device so with device gone no more txture???

                ULONG result = d3d_globals.textures[i].texture->Release();
                result = d3d_globals.textures[i].sys_texture->Release();
                d3d_globals.textures[i].texture = {};
                d3d_globals.textures[i].sys_texture = {};
            }
        }
        d3ddev->Release();

    }
#if D3D_USE_EX
    result = d3d->CreateDeviceEx(adapter,
                                 D3DDEVTYPE_HAL,
                                 win32.window,
                                 D3DCREATE_HARDWARE_VERTEXPROCESSING,
                                 &d3d_globals.d3dpp,
                                 (d3d_globals.d3dpp.Windowed) ?
                                 (0) :
                                 (&d3d_globals.display_mode), //FULLScreen display mode
                                 &d3ddev);

    assert(result == S_OK);
#else
    result = d3d->CreateDevice(adapter,
                               D3DDEVTYPE_HAL,
                               win32.window,
                               D3DCREATE_HARDWARE_VERTEXPROCESSING,
                               &d3d_globals.d3dpp,
                               &d3ddev);
    assert(result == S_OK);
#endif
}

internal void
__d3d_init_fullscreen(UINT adapter)
{
    HRESULT result;
    D3DFORMAT format = D3DFMT_X8R8G8B8;
    //D3DDISPLAYMODE display_mode = d3d_get_optimal_display_mode(format, adapter);
    //d3d_globals.d3dpp.BackBufferCount = 4;
    // d3d_globals.d3dpp.BackBufferWidth = 800;
    // d3d_globals.d3dpp.BackBufferHeight = 600;

    // platform.w_w = display_mode.Width;
    // platform.w_h = display_mode.Height;
    d3d_globals.d3dpp.BackBufferFormat = format;

    d3d_globals.display_mode = {sizeof(D3DDISPLAYMODEEX)};
    D3DDISPLAYROTATION rotation = D3DDISPLAYROTATION_IDENTITY;
    d3d->GetAdapterDisplayModeEx(adapter, &d3d_globals.display_mode, &rotation);
    
    d3d_globals.d3dpp.FullScreen_RefreshRateInHz = d3d_globals.display_mode.RefreshRate;
    d3d_globals.d3dpp.Windowed = false;
    d3d_globals.d3dpp.BackBufferWidth = d3d_globals.display_mode.Width;
    d3d_globals.d3dpp.BackBufferHeight = d3d_globals.display_mode.Height;

    platform.w_w = d3d_globals.display_mode.Width;
    platform.w_h = d3d_globals.display_mode.Height;
    
    win32.prefullscreen_window.placement = {sizeof(WINDOWPLACEMENT)};
    BOOL window_result = GetWindowPlacement(win32.window, &win32.prefullscreen_window.placement);
    assert(window_result);
}

internal b32
d3d_find_adapter(HMONITOR target_adapter_monitor, u32 *target_adapter)
{
    u32 acount = d3d->GetAdapterCount();
    for(int i = 0; i < acount; i++)
    {
        HMONITOR this_monitor = d3d->GetAdapterMonitor(i);
        if(this_monitor == target_adapter_monitor)
        {
            target_adapter[0] = i;
            return true;
        }
    }
    return false;
}

internal void
d3d_unset_fullscreen()
{
    d3d_globals.d3dpp.Windowed = true;    //NOTE:: make it not the topmost window
    // SetWindowPos(win32.window, HWND_NOTOPMOST,
    //              0,
    //              0,
    //              0,
    //              0,
    //              SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);
#if 0 
    d3d_globals.d3dpp.BackBufferWidth = 800;
    d3d_globals.d3dpp.BackBufferHeight = 600;

    //NOTE:: hack WM_SIZE isn't triggered when it unenters fullscreen but it need to
    platform.w_w = d3d_globals.d3dpp.BackBufferWidth;
    platform.w_h = d3d_globals.d3dpp.BackBufferHeight;
#endif
    //HRESULT cooperative_level = d3ddev->TestCooperativeLevel();
    d3d_globals.d3dpp.FullScreen_RefreshRateInHz = 0;
    platform.w_w = 800;
    platform.w_h = 600;
    //d3d_reset_device();
    
    // if(win32.prefullscreen_window.placement.showCmd == SW_MAXIMIZE)
    // {
    //     // WINDOWPLACEMENT placement = {sizeof(WINDOWPLACEMENT)};
    //     // GetWindowPlacement(win32.window, &placement);      
    //     // placement.rcNormalPosition = win32.prefullscreen_window.placement.rcNormalPosition;
    //     SetWindowPlacement(win32.window, &win32.prefullscreen_window.placement);
    // }
    // else
#if 0
    {
        //RECT restored_rect = win32.prefullscreen_window.placement.rcNormalPosition;
        RECT restored_rect =
            {
                0,
                0,
                800,
                600
            };
        MoveWindow(win32.window, restored_rect.left, restored_rect.top,
                   restored_rect.right - restored_rect.left,
                   restored_rect.bottom - restored_rect.top, TRUE);
        SetWindowLongPtr(win32.window, GWL_STYLE, win32.prefullscreen_window.style);
        SetWindowLongPtr(win32.window, GWL_EXSTYLE, win32.prefullscreen_window.ex_style);
        // SetWindowPos(win32.window, HWND_NOTOPMOST,
        //              restored_rect.left, restored_rect.top,
        //              restored_rect.right - restored_rect.left,
        //              restored_rect.bottom - restored_rect.top, 0);
    }
#endif
    //TODO can we transform the restored_rect and placement into client rects??
    // RECT crect = {};
    // GetClientRect(win32.window, &crect);
    // platform.w_w = crect.right - crect.left;
    // platform.w_h = crect.bottom - crect.top;
}



internal void
d3d_init(HWND window)
{
#if D3D_USE_EX
//#error "we don't support d3d9ex yet"
    Direct3DCreate9Ex(D3D_SDK_VERSION, &d3d);
#else
    d3d = Direct3DCreate9(D3D_SDK_VERSION);
#endif

#if WINDOW_STARTFULLSCREEN
    d3d_globals.d3dpp.Windowed = FALSE; //fullscreen
#else
    d3d_globals.d3dpp.Windowed = TRUE;
#endif
    d3d_globals.d3dpp.hDeviceWindow = window; //window to be used by d3d
    d3d_globals.d3dpp.SwapEffect = D3DSWAPEFFECT_COPY; //discard old frames

    d3d_globals.d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

    UINT starting_adapter = D3DADAPTER_DEFAULT;
    
    if(!d3d_globals.d3dpp.Windowed)
    {
        // D3DDISPLAYMODE chosen = d3d_get_optimal_display_mode(D3DFMT_X8R8G8B8, starting_adapter);
        // d3d_globals.d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
        // d3d_globals.d3dpp.BackBufferWidth = chosen.Width;
        // d3d_globals.d3dpp.BackBufferHeight = chosen.Height;
        //d3d_set_fullscreen();
        __d3d_init_fullscreen(starting_adapter);
    }

    d3d_create_device(starting_adapter);
    d3d_gen_buffers();
    d3d_set_states();
}

internal void
d3d_recreate_instance()
{
    d3d_globals.v_buffer->Release();
    d3d_globals.texturev_buffer->Release();

    for(int i = 0; i < d3d_globals.num_textures; i++)
    {
        if(d3d_globals.textures[i].texture)
        {
            d3d_globals.textures[i].texture->Release();
            d3d_globals.textures[i].texture = {};
            d3d_globals.textures[i].sys_texture->AddDirtyRect(0);
        }
    }
    d3ddev->Release();
    d3d->Release();
    d3ddev = {};
    
#if D3D_USE_EX
    Direct3DCreate9Ex(D3D_SDK_VERSION, &d3d);
#else
    d3d = Direct3DCreate9(D3D_SDK_VERSION);
#endif

}


internal void
d3d_set_fullscreen()
{
    // SetWindowPos(win32.window, HWND_TOPMOST,
    //              0,
    //              0,
    //              0,
    //              0,
    //              SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE);

    win32.prefullscreen_window.style = GetWindowLong(win32.window, GWL_STYLE);
    win32.prefullscreen_window.ex_style = GetWindowLong(win32.window, GWL_EXSTYLE);

    
    HMONITOR target_monitor = MonitorFromWindow(win32.window, MONITOR_DEFAULTTOPRIMARY);
    D3DDEVICE_CREATION_PARAMETERS cparams = {};
    d3ddev->GetCreationParameters(&cparams);
    HMONITOR current_monitor = d3d->GetAdapterMonitor(cparams.AdapterOrdinal);

    if(current_monitor != target_monitor)
    {
        //we do not have the adapter that yeah so let's recreate the device
        u32 target_adapter;
        if(d3d_find_adapter(target_monitor, &target_adapter))
        {
            __d3d_init_fullscreen(target_adapter);
            d3d_create_device(target_adapter);
            //NOTE:: buffers must be generated after device creation with NON-EX because they are not generated on reset
            d3d_gen_buffers();
            d3d_set_states();
        }
        else
        {
            d3d_recreate_instance();
            if(d3d_find_adapter(target_monitor, &target_adapter))
            {
                __d3d_init_fullscreen(target_adapter);
                d3d_create_device(target_adapter);
                d3d_gen_buffers();
                d3d_set_states();
            }
            else
            {
                stop("adsf");
                goto fullscreen_on_current_adapter;
            }
        }
    }
    else
    {	
    fullscreen_on_current_adapter:
        __d3d_init_fullscreen(cparams.AdapterOrdinal);
        if(d3ddev)
        {
            HRESULT cooperative_level = d3ddev->TestCooperativeLevel();
            d3d_reset_device();            
        }

    }

}

internal void
d3d_toggle_fullscreen()
{
    //toggling fullscreen doesn't send a WM_SIZE

    //TODO another thing we can do is we can handle WM_MOVING and set the move rectangle
    // if(win32.modal_sizemove)
    // {
    //     //TODO we were working herer
    //     //TODO hack
    //     win32.ignore_size = true;
    //     PostMessage(win32.window, WM_CANCELMODE, 0, 0);
    // }

    if(d3d_globals.d3dpp.Windowed)
    {
        d3d_set_fullscreen();
        win32.fullscreen = true;
    }
    else
    {
        d3d_unset_fullscreen();
        win32.fullscreen = false;
    }
}

internal void
d3d_set_viewport_size(u32 width, u32 height)
{

    if(width != d3d_globals.viewport_w || height != d3d_globals.viewport_h)
    {

        //NOTE:: a new device will automatically have the right viewport
        //       and scissor rect (I think)
        // D3DVIEWPORT9 viewport = {};
        // RECT rect = {};
        // d3ddev->GetViewport(&viewport);
        // d3ddev->GetScissorRect(&rect);
        // viewport.X = 0;
        // viewport.Y = 0;
        // viewport.Width = width;
        // viewport.Height = height;

        // rect.left = 0;
        // rect.top = 0;
        // rect.right = width;
        // rect.bottom = height;
        // HRESULT result = d3ddev->SetScissorRect(&rect);

        // result = d3ddev->SetViewport(&viewport);
        // assert(result == S_OK);
        
        d3d_globals.viewport_w = width;
        d3d_globals.viewport_h = height;
        if(d3d_globals.d3dpp.Windowed)
        {
            d3d_globals.d3dpp.BackBufferHeight = 0;
            d3d_globals.d3dpp.BackBufferWidth = 0;
        }
        

        //NOTE:: this is hack but whateves
        if(win32.fullscreen && 0)
        {
            d3d_globals.d3dpp.Windowed = FALSE;

            //NOTE:: There obviously is a better way of finding out the display mode
            d3d_globals.display_mode.Width = win32.fullscreen_width;
            d3d_globals.display_mode.Height = win32.fullscreen_height;

            DEVMODE display_mode = {};
            EnumDisplaySettings(win32.fullscreen_monitor_info.szDevice, ENUM_CURRENT_SETTINGS, &display_mode);

            D3DDISPLAYMODEEX desired_mode = {sizeof(D3DDISPLAYMODEEX)};
            d3d_globals.display_mode.Size = sizeof(D3DDISPLAYMODEEX);
            d3d_globals.display_mode.RefreshRate = display_mode.dmDisplayFrequency;
            d3d_globals.display_mode.Format = D3DFMT_X8R8G8B8;
            d3d_globals.d3dpp.BackBufferWidth = win32.fullscreen_width;
            d3d_globals.d3dpp.BackBufferHeight = win32.fullscreen_height;
            d3d_globals.d3dpp.FullScreen_RefreshRateInHz = display_mode.dmDisplayFrequency;
            

            u32 target_adapter = 0;
            if(!d3d_find_adapter(win32.fullscreen_monitor, &target_adapter))
            {
                d3d_recreate_instance();
                if(!d3d_find_adapter(win32.fullscreen_monitor, &target_adapter))
                {
                    //giveup just use whatever scanline ordering the default monitor has
                     stop("this shouldn't happen");
                }
            }
            
            D3DDISPLAYMODEEX current_mode = {sizeof(D3DDISPLAYMODEEX)};
            d3d->GetAdapterDisplayModeEx(target_adapter, &current_mode, 0);
            d3d_globals.display_mode.ScanLineOrdering = current_mode.ScanLineOrdering;
            d3d_reset_device();
        }
        else
        {
            d3d_globals.d3dpp.Windowed = TRUE;
            d3d_globals.d3dpp.BackBufferWidth = 0;
            d3d_globals.d3dpp.BackBufferHeight = 0;
            d3d_globals.d3dpp.FullScreen_RefreshRateInHz = 0;

            HRESULT cooperative_level = d3ddev->TestCooperativeLevel();
            if(cooperative_level != D3DERR_DEVICELOST)
            {
                d3d_reset_device();
            }
        }
    }
}


//TODO is this really necessary
void cleanD3D()
{
    d3ddev->Release();
    d3ddev->Release();
}
