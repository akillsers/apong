//NOTE:: THIS MUST USE UNIX LINE ENDINGS!!!!
@rectshader
:vert
#version 330 core
layout (location = 0) in vec3 pos;

uniform mat4 model;
uniform mat4 projection;

void main()
{
   gl_Position = projection * model * vec4(pos, 1.0f);
}


:frag
#version 330 core

uniform vec4 color;
out vec4 frag_color;
void main()
{
   frag_color = color; 
}

@texturedrectshader
:vert
#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tex_coords;
layout (location = 2) in vec2 fetchuv;
layout (location = 3) in vec2 fetchst;
layout (location = 4) in vec3 color;
layout (location = 5) in mat4 model_attrib;
//layout (location = 9) in mat4 projection_attrib;

//uniform mat4 model;
//uniform mat4 projection;

out vec2 _tex_coords;
out vec2 _fetchuv;
out vec2 _fetchst;
out vec3 _color;
void main()
{

   gl_Position = model_attrib * vec4(pos, 1.0f); //projection * model * blah
   _tex_coords = tex_coords;

   _fetchuv = fetchuv;
   _fetchst = fetchst;
  // mat4 foo = model;
//   mat4 bar = projection;
   _color = color;
}

:frag
#version 330 core

uniform sampler2D tex;

in vec2 _tex_coords;
in vec2 _fetchuv;
in vec2 _fetchst;
in vec3 _color;
in vec4 _modeprojection;

out vec4 frag_color;


void main()
{

   frag_color = vec4(_color, texture(tex, _tex_coords * (_fetchst-_fetchuv) + _fetchuv).r); 
}


@textureshader
:vert
#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tex_coords;
uniform vec4 tex_rect;

uniform mat4 model;
uniform mat4 projection;

out vec2 _tex_coords;
void main()
{
   gl_Position = projection * model * vec4(pos, 1.0f);
   _tex_coords = tex_coords;
}

:frag
#version 330 core

uniform vec3 color;
uniform sampler2D tex;
uniform vec2 fetchuv;
uniform vec2 fetchst;

out vec4 frag_color;

in vec2 _tex_coords;
void main()
{
   frag_color = vec4(color, 1.0f) * texture(tex, _tex_coords * (fetchst-fetchuv) + fetchuv); 
}
