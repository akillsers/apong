inline b32
verticaledge_touchcircle(Circle circle, f32 xequals, f32 ymin, f32 ymax)
{
    b32 ret;
    //TODO should we use less than or equal or just less than??
    ret = (absolute(xequals - circle.x) < circle.radius &&
           circle.center.y >= ymin && circle.center.y <= ymax);
    return ret;
}

inline b32
horizontaledge_touchcircle(Circle circle, f32 yequals, f32 xmin, f32 xmax)
{
    b32 ret;
    //TODO should we use less than or equal or just less than??
    ret = (distance_frompoint_to_horizontalline(circle.center, yequals) < circle.radius &&
           circle.center.x >= xmin && circle.center.x <= xmax);
    return ret;
}


inline b32
point_intersectAABB(Vec2 point, Rect AABB)
{
    TIMED_BLOCK;
    b32 ret;
    //TODO should we do or equal to comparison or not (inclusive or exclusive)
    ret = (point.x <= AABB.x + AABB.width &&
           point.x >= AABB.x &&
           point.y <= AABB.y + AABB.height &&
           point.y >= AABB.y);
    return ret;
}
/*
DeltaX = CircleX - Max(RectX, Min(CircleX, RectX + RectWidth));
DeltaY = CircleY - Max(RectY, Min(CircleY, RectY + RectHeight));
return (DeltaX * DeltaX + DeltaY * DeltaY) < (CircleRadius * CircleRadius);
*/

inline b32
circle_intersectAABB(Circle circle, Rect AABB)
{
    TIMED_BLOCK;
    b32 ret;

    b32 circleinsideAABB = point_intersectAABB(circle.center, AABB);
    b32 left_edgetest = verticaledge_touchcircle(circle, AABB.x, AABB.y, AABB.y + AABB.height);
    b32 right_edgetest = verticaledge_touchcircle(circle, AABB.x + AABB.width, AABB.y, AABB.y + AABB.height);
    b32 top_edgetest = horizontaledge_touchcircle(circle, AABB.y, AABB.x, AABB.x + AABB.width);
    b32 bottom_edgetest = horizontaledge_touchcircle(circle, AABB.y + AABB.height, AABB.x, AABB.x + AABB.width);

    Vec2 upperleft = v2(AABB.x, AABB.y);
    Vec2 upperright = v2(AABB.x + AABB.width, AABB.y);
    Vec2 lowerleft = v2(AABB.x, AABB.y + AABB.height);
    Vec2 lowerright = v2(AABB.x + AABB.width, AABB.y + AABB.height);
    //TODO get rid of sqrt
    b32 upperleft_cornertest = (distance_frompoint_topoint(circle.center, upperleft) < circle.radius);
    b32 upperright_cornertest = (distance_frompoint_topoint(circle.center, upperright) < circle.radius);
    b32 lowerleft_cornertest = (distance_frompoint_topoint(circle.center, lowerleft) < circle.radius);
    b32 lowerright_cornertest = (distance_frompoint_topoint(circle.center, lowerright) < circle.radius);
        
    ret = (point_intersectAABB(circle.center, AABB) ||
           left_edgetest ||
           right_edgetest ||
           top_edgetest ||
           bottom_edgetest ||
           upperleft_cornertest ||
           upperright_cornertest ||
           lowerleft_cornertest ||
           lowerright_cornertest);

    return ret;
}


inline b32
AABB_intersectAABB(AABB left, AABB right)
{
    TIMED_BLOCK;
    b32 ret;
    //if (RectA.X1 < RectB.X2 && RectA.X2 > RectB.X1 &&
    //    RectA.Y1 > RectB.Y2 && RectA.Y2 < RectB.Y1) 

    ret = ((left.x1 <= right.x2 && left.x2 >= right.x1 &&
            left.y1 <= right.y2 && left.y2 >= right.y1));
    return ret;
}



struct CollisionResult
{
    b32 collided;
    f32 t;
    Vec2 contactpoint; 
};

internal CollisionResult
ray_checkcollisionwith_circle(Vec2 startpoint, Vec2 ray, Circle circle)
{
    TIMED_BLOCK;
    CollisionResult result;
    result.collided = false;

    Vec2 f = startpoint - circle.center; 
    f32 r = circle.radius;

    f32 a = inner(ray, ray);
    f32 b = 2*inner(ray, f);
    f32 c = inner(f, f) - r*r;

    f32 discriminant = b*b - 4*a*c;

    if(discriminant < 0)
        return result;
    else
    {
        discriminant = sqroot(discriminant);

        f32 t1 = ((-b - discriminant)/(2*a)) - 0.000001;
        f32 t2 = (-b + discriminant)/(2*a);


        // char buf[456];
        // sprintf(buf, "Corner case: t1 %f t2 %f\n", t1, t2);
        // OutputDebugString(buf);

        // 3x HIT cases:
        //          -o->             --|-->  |            |  --|->
        // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

        // 3x MISS cases:
        //       ->  o                     o ->              | -> |
        // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)
        if(t1>=0 && t1 <=1)
        {
            Vec2 pos = startpoint + t1*ray;
            //TODO check which side or whatever
            result.collided = true;
            result.t = t1;
            result.contactpoint = pos;
            return result;
        }
        return result;
    }

}

internal CollisionResult
paddle_checkcollisionwith_circle(Vec2 lastpos, Vec2 d, Rect paddle, Circle circle)
{
    TIMED_BLOCK;
    CollisionResult result = {};

    if(d.y == 0)
        return result;
    else
    {
        f32 wy;
        if(d.y > 0)
            wy = circle.center.y - circle.radius - paddle.height;
        else
            wy = circle.center.y + circle.radius;

        f32 t = (wy - lastpos.y)/d.y;
        if(t >= 0 && t <= 1)
        {
            //NOTE:: we hit wally
            Vec2 pos = lastpos + t*d;
            if(pos.x >= circle.center.x - paddle.width && pos.x <= circle.center.x)
            {
                result.collided = true;
                result.t = t;
                result.contactpoint.y = (d.y > 0) ? paddle.y + paddle.height : paddle.y;
                result.contactpoint.x = circle.center.x;
                return result;
            }

        }
        if(d.y > 0)
        {
            {
                Circle checkcircle = circle;
                checkcircle.y -= paddle.height;
                CollisionResult circleresult = ray_checkcollisionwith_circle(lastpos, d, checkcircle);
                if(circleresult.collided)
                {
                    result.collided = true;
                    result.t = circleresult.t;
                    result.contactpoint.y = paddle.y + paddle.height;
                    result.contactpoint.x = circleresult.contactpoint.x;
                    return result;
                }
            }
            {
                Circle checkcircle = circle;
                checkcircle.y -= paddle.height;
                checkcircle.x -= paddle.width;
                CollisionResult circleresult = ray_checkcollisionwith_circle(lastpos, d, checkcircle);

                if(circleresult.collided)
                {
                    result.collided = true;
                    result.t = circleresult.t;
                    result.contactpoint.y = paddle.y + paddle.height;
                    result.contactpoint.x = circleresult.contactpoint.x + paddle.width;
                    return result;
                }
            }
        }
        else if(d.y < 0)
        {
            {
                Circle checkcircle = circle;
                CollisionResult circleresult = ray_checkcollisionwith_circle(lastpos, d, checkcircle);

                if(circleresult.collided)
                {
                    result.collided = true;
                    result.t = circleresult.t;
                    result.contactpoint.y = paddle.y;
                    result.contactpoint.x = circleresult.contactpoint.x;
                    return result;
                }
            }
            {
                Circle checkcircle = circle;
                checkcircle.x -= paddle.width;
                CollisionResult circleresult = ray_checkcollisionwith_circle(lastpos, d, checkcircle);

                if(circleresult.collided)
                {
                    result.collided = true;
                    result.t = circleresult.t;
                    result.contactpoint.y = paddle.y;
                    result.contactpoint.x = circleresult.contactpoint.x + paddle.width;
                    return result;
                }
            }
        }
    
        return result;
    }
}

internal CollisionResult
circle_checkcollisionwith_AABB(Vec2 lastpos, Vec2 d, f32 circle_radius, AABB aabb)
{
    TIMED_BLOCK;
    CollisionResult result = {};

    if(d.x > 0)
    {

        f32 wx = aabb.x1 - circle_radius;
        f32 t = (wx - lastpos.x)/d.x;
        if(t >= 0 && t <= 1)
        {
            //NOTE:: we hit wallx 
            Vec2 pos = lastpos + t*d;

            if(pos.y >= aabb.y1 && pos.y <= aabb.y2)
            {
                result.collided = true;
                result.t = t;
                result.contactpoint.x = aabb.x1;
                result.contactpoint.y = pos.y;
                return result;
                // Vec2 resolvepos = lastpos + (t-0.000001)*d;
                // b_circle->center = resolvepos;
            }
        }
    }

    if(d.x < 0)
    {
        f32 wx = aabb.x2 + circle_radius;
        f32 t = (wx - lastpos.x)/d.x;

        if(t >= 0 && t <= 1)
        {
            //NOTE:: we hit wallx 
            Vec2 pos = lastpos + t*d;
            if(pos.y >= aabb.y1 && pos.y <= aabb.y2)
            {
                result.collided = true;
                result.t = t;
                result.contactpoint.x = aabb.x2;
                result.contactpoint.y = pos.y;
                return result;
                // Vec2 resolvepos = lastpos + (t-0.000001)*d;
                // b_circle->center = resolvepos;
            }
        }
    }
                
    if(d.y > 0)
    {
        f32 wy = aabb.y1 - circle_radius;
        f32 t = (wy - lastpos.y)/d.y;
        if(t >= 0 && t <= 1)
        {
            //NOTE:: we hit wally 
            Vec2 pos = lastpos + t*d;
            if(pos.x >= aabb.x1 && pos.x <= aabb.x2)
            {
                result.collided = true;
                result.t = t;
                result.contactpoint.y = aabb.y1;
                result.contactpoint.x = pos.x;
                return result;

                // Vec2 resolvepos = lastpos + (t-0.000001)*d;
                // b_circle->center = resolvepos;
            }
        }
                    
    }

    if(d.y < 0)
    {
        f32 wy = aabb.y2 + circle_radius;
        f32 t = (wy - lastpos.y)/d.y;

        if(t >= 0 && t <= 1)
        {
            //NOTE:: we hit wally 
            Vec2 pos = lastpos + t*d;
            if(pos.x >= aabb.x1 && pos.x <= aabb.x2)
            {
                result.collided = true;
                result.t = t;
                result.contactpoint.y = aabb.y2;
                result.contactpoint.x = pos.x;
                
                return result;
                
                // Vec2 resolvepos = lastpos + (t-0.000001)*d;
                // b_circle->center = resolvepos;
            }
        }
                    
    }

end_horizontalchecks:;
    Vec2 corner[4];
    corner[0].x = aabb.x1;
    corner[0].y = aabb.y1;

    corner[1].x = aabb.x2;
    corner[1].y = aabb.y1;

    corner[2].x = aabb.x1;
    corner[2].y = aabb.y2;

    corner[3].x = aabb.x2;
    corner[3].y = aabb.y2;

    Vec2 corner_minx_miny[4];
    corner_minx_miny[0].x = corner[0].x - circle_radius;
    corner_minx_miny[0].y = corner[0].y - circle_radius;

    corner_minx_miny[1].x = corner[1].x ;
    corner_minx_miny[1].y = corner[1].y - circle_radius;

    corner_minx_miny[2].x = corner[2].x - circle_radius;
    corner_minx_miny[2].y = corner[2].y;

    corner_minx_miny[3].x = corner[3].x;
    corner_minx_miny[3].y = corner[3].y;

    b32 check_corner[4] = {};

    if(d.x > 0)
    {
        check_corner[0] = true; 
        check_corner[2] = true;
    }
    if(d.x < 0)
    {
        check_corner[1] = true; 
        check_corner[3] = true;
    }
    if(d.y > 0)
    {
        check_corner[0] = true; 
        check_corner[1] = true;
    }
    if(d.y < 0)
    {
        check_corner[2] = true; 
        check_corner[3] = true;
    }

    for(int i = 0; i < arraycount(corner); i++)
    {
        if(!check_corner[i])
            continue;

        Circle corner_circle;
        corner_circle.radius = circle_radius;
        corner_circle.center = corner[i];
        CollisionResult collision = ray_checkcollisionwith_circle(lastpos, d, corner_circle);
        if(collision.collided)
        {
            
            if(collision.contactpoint.x >= corner_minx_miny[i].x &&
               collision.contactpoint.x <= corner_minx_miny[i].x + circle_radius &&
               collision.contactpoint.y >= corner_minx_miny[i].y &&
               collision.contactpoint.y <= corner_minx_miny[i].y + circle_radius)
            {
                result.collided = true;
                result.t = collision.t;
                result.contactpoint = corner[i];
                return result;
            }
            
        }

        
                
    }
    return result;
}
