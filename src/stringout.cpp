#define WINDOWS_LINEENDINGS

struct DestBuf
{
    char *at;
    u32 remaining_chars;
};

inline void
outchar(DestBuf *destbuf, char character)
{
    if(destbuf->remaining_chars)
    {
        destbuf->remaining_chars--;
        *destbuf->at++ = character;
    }
}

inline void
outstring(DestBuf *destbuf, char *string)
{
    while(*string)
    {
        outchar(destbuf, *string);
        string++;
    }
}

inline void
outstringl(DestBuf *destbuf, char *string, u32 strlen)
{
    //TODO optimize this since we do not need to check remaining chars and subtract
    //     each time since we already know the string length
    for(int i = 0; i < strlen; i++)
    {
        outchar(destbuf, string[i]);
    }
}

inline void
outnumber(DestBuf *destbuf, u32 number)
{
    char ASCIIdigits[] = "0123456789";
    u32 base = 10;

    //TODO don't use numlen do that within the loop
    u32 numdigits = numlen(number);

    if(destbuf->remaining_chars < numdigits)
        return;

    destbuf->remaining_chars -= numdigits;

    //we are at the first digit location so take away 1 from the numdigits
    //that where we need to be further along destbuf->at
    destbuf->at += (numdigits-1);
    for(int i = 0; i < numdigits; i++)
    {
        u8 outdigit = number % base;
        char ASCIIoutdigit = ASCIIdigits[outdigit];

        *destbuf->at-- = ASCIIoutdigit;
        number /= base;
    }
    //we will be at before the number so add one plus num digits
    destbuf->at += numdigits+1;
}
