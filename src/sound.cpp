//TODO multithreaded arenas??
//right now we are suballocating a reigion of temp_arena for the sound_arena
//because the sound is on another thread
//e.g. we might push at the same time and not increment the used pointer before the other guy gets there

GAME_OUTPUT_SOUND(game_output_sound)
{
    TIMED_BLOCK;
    GameState *g = (GameState *)memory->permanent_storage;
    arena_reset(&sound_arena);

    u32 write_sample_count = sbuffer->availible_audio_frames * sbuffer->numchannels;

    //NOTE:: outsamples is i32 for easy clamping to i16 max range
    i32 *outsamples = arena_pushcount(&sound_arena, i32, write_sample_count);

    //TODO make this SIMD clear to zero
    for (int i = 0; i < write_sample_count; i++)
    {
        outsamples[i] = 0;
    }

    for(int i = 0; i <= g->mixer.last_playing_sound_index; i++)        
    {
        if(g->mixer.free_playing_sounds[i])
            //this is not a valid sound
            continue;
        volatile PlayingSound *playing_sound = &g->mixer.current_sounds[i];
        volatile Sound *sound = playing_sound->sound;

        u32 currindex = playing_sound->frameindex;
        i32 framestogo = sound->total_frames - currindex;

        // char buf[234];
        
        // sprintf(buf, "Total:: %u index:: %u togo: %i\n", sound->total_frames, currindex, framestogo);
        // OutputDebugString(buf);

        assert(framestogo >= 0)
        if(framestogo <= 0)
        {
            //remove the sound
            if(g->mixer.last_playing_sound_index == i)
            {
                //this is the last playing sound
                g->mixer.last_playing_sound_index--;
//                OutputDebugString("Less \n");
                while(g->mixer.last_playing_sound_index >= 0)
                {
                    if(g->mixer.free_playing_sounds[g->mixer.last_playing_sound_index])
                        g->mixer.last_playing_sound_index--;
                    else break;
//                    OutputDebugString("Less recurse \n");
                }


            }
            else
            {
                //this is not the last playing sound
                g->mixer.free_playing_sounds[i] = true;
//                OutputDebugString("Freelist \n");
            }
                
            continue;
            
        }

        u16 *data = (u16 *)sound->data;

        i16 *source;
        if(sound->num_channels == 2)
        {
            source = (i16 *)&((u32 *)data)[currindex];
        }
        else if(sound->num_channels == 1)
        {
            source = (i16 *)&data[currindex];
        }
            
        //TODO clipping
        //TODO overflow etc.

        i32 *mixed_cursor = outsamples;
        u32 outputmax = minimum(sbuffer->availible_audio_frames, framestogo);
        
        for(int i = 0; i < outputmax; i++)
        {
            //TODO clamp with no branching
            //NOTE:: idea to clamp we can use modulo the max subtract half k
            //Left channel
            if(*mixed_cursor + *source > 0)
                *mixed_cursor = minimum(*mixed_cursor + (source[0]*playing_sound->volume), I16_MAX);
            else if(*mixed_cursor + *source < 0)
                *mixed_cursor = maximum(*mixed_cursor + (source[0]*playing_sound->volume), I16_MIN);
            else
                *mixed_cursor = 0;
            
            mixed_cursor++;
            if(sound->num_channels == 2)
                source++;

            //right channel
            if(*mixed_cursor + *source > 0)
                *mixed_cursor = minimum(*mixed_cursor + (source[0] * playing_sound->volume), I16_MAX);
            else if(*mixed_cursor + *source < 0)
                *mixed_cursor = maximum(*mixed_cursor + (source[0] * playing_sound->volume), I16_MIN);
            else
                *mixed_cursor = 0;
            
            mixed_cursor++;
            source++;

        }
        playing_sound->frameindex += outputmax;
    }

    i16 *destination = (i16 *)sbuffer->data;


    for(int i = 0; i < sbuffer->availible_audio_frames; i++)
    {
        *destination++ = (i16)*outsamples++;
        *destination++ = (i16)*outsamples++;
    }
    
    // if(wav == 0)
    //     return;
    // assert(wav->channel_count == 2);
    // u32 *data = (u32 *)wav->data;

    // i16 *destination = (i16 *)sbuffer->data;
    // i16 *source = (i16 *)&data[g->mixer.frameindex];

    // for(int i = 0; i < sbuffer->availible_audio_frames; i++)
    // {
    //     *destination++ = *source++;
    //     *destination++ = *source++;
    // }
    // g->mixer.frameindex += sbuffer->availible_audio_frames;

    // char buf[256];
    // sprintf(buf, "New frame index: %u (wrote: %u frames)\n", g->mixer.frameindex, sbuffer->availible_audio_frames);
    // OutputDebugString(buf);
}

internal void
play_sound(Mixer *mix, Sound *sound_to_play, f32 volume)
{
    u32 found_playing_sound_index;
    b32 increment_last_playing_sound = false;
    if(mix->last_playing_sound_index + 1 < MAX_PLAYING_SOUNDS)
    {
//        OutputDebugString("MORE \n");
        found_playing_sound_index = mix->last_playing_sound_index + 1;
        increment_last_playing_sound = true;
        goto Found;

    }
    else
    {

        for(int i = 0; i < MAX_PLAYING_SOUNDS; i++)
        {
            if(mix->free_playing_sounds[i])
            {
                //              OutputDebugString("MORE list \n");
                found_playing_sound_index = i;
                increment_last_playing_sound = false;
                goto Found;
            }
        }
        goto Not_Found;
    }
Not_Found:
    // OutputDebugString("Max Playing sounds reached!!!\n");
    return;
Found:
    mix->current_sounds[found_playing_sound_index].sound = sound_to_play;
    mix->current_sounds[found_playing_sound_index].frameindex = 0;
    mix->current_sounds[found_playing_sound_index].volume = volume;

    // NOTE: apong only plays sounds from one thread anyway so we don't have
    //       to worry about any compiler-reordering instructions shenanigans

    mix->free_playing_sounds[found_playing_sound_index] = false;
    if(increment_last_playing_sound)
        mix->last_playing_sound_index++;

}

internal void
play_sound(GameState *g, Sound *sound_to_play, f32 volume)
{
    play_sound(&g->mixer, sound_to_play, volume);
}

internal void
play_sound(GameState *g, char *sound_to_play_name, f32 volume)
{

    Asset *sound_asset = get_asset_by_name(&g->assets, sound_to_play_name, sound);
    play_sound(&g->mixer, sound_asset->sound, volume);

}

internal void
play_sound(Mixer *m, Assets *assets, char *sound_to_play_name, f32 volume)
{

    Asset *sound_asset = get_asset_by_name(assets, sound_to_play_name, sound);
    play_sound(m, sound_asset->sound, volume);

}

internal void
stop_all_sounds(GameState *g)
{
    g->mixer.last_playing_sound_index = -1;
}
