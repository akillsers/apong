#include "debug.h"
#if INTERNAL
inline u32
volume_id_hash(u32 ID)
{
    u32 hash = (ID & 0x1) * 19 + (ID & 0x2) * 7 + (ID & 0x4) * 3 + (ID & 0x8) * 147;
    return hash;
}

inline DebugVolume *
next_volume(DebugSnapshot *snapshot, u32 *index)
{
    *index += 1;
    *index %= MAX_DEBUG_VOLUMES;
    return &snapshot->volume[*index];
}

//TODO anyway to automatically append line??? :)
    
//NOTE:: this function call occurs at the beginning of every frame
//       but the first one
GAME_DEBUG_RECORD_LAST_FRAMEINFO(game_debug_record_last_frame)
{
    if(dbg->snapshot_recording)
    {
        dbg->snapshots[dbg->curr_snapshot_index].frame_time =
            info->last_frame_time;

        dbg->snapshots[dbg->curr_snapshot_index].platform_input_time =
            info->last_input_processing_time;
        dbg->snapshots[dbg->curr_snapshot_index].platform_game_update_time =
            info->last_game_update_time;
        dbg->snapshots[dbg->curr_snapshot_index].platform_clock_stabalizer_time =
            info->last_clock_stablizer_time;
        dbg->snapshots[dbg->curr_snapshot_index].platform_render_time =
            info->last_render_time;

        //NOTE The following statement has to be the last thing that happens in the debug system
        //     every frame

        dbg->curr_snapshot_index = (dbg->curr_snapshot_index + 1) % dbg->num_snapshots;
        //TODO do we need to reset the stuff in each var group or just this is fine??
        dbg->snapshots[dbg->curr_snapshot_index].num_var_groups = 0;
        dbg->snapshots[dbg->curr_snapshot_index].var_groups[0].children_count = 0;
        dbg->snapshots[dbg->curr_snapshot_index].ungrouped_vars.children_count = 0;
        arena_reset(&dbg->snapshots[dbg->curr_snapshot_index].varena);

        //TODO go git style and store a pointer or value of the debug volumes every snapshot
        //     right now we do this clear of every volume before the frame to prepare for the
        //     case the this volume doesn't get updated in this frame
        //TODO related and there is a duplicate TODO somewhere else:: we need to disambiguate the
        //     same line value (the same line value in two files will generate the same ID)
        for(int i = 0; i < dbg->num_volumes; i++)
        {
            u32 hash = volume_id_hash(dbg->volume_ids[i]);
            u32 index = hash % MAX_DEBUG_VOLUMES;

            DebugVolume *potential_volume = &dbg->snapshots[dbg->curr_snapshot_index].volume[index];
            while(!(potential_volume->ID == 0 || potential_volume->ID == dbg->volume_ids[i]))
            {
                potential_volume = next_volume(dbg->snapshots + dbg->curr_snapshot_index, &index);
            }
            if(potential_volume->ID == dbg->volume_ids[i])
            {
                potential_volume->ID = 0;
            }
        }
        
    }
        
}



internal void
dbg_init(GameMemory *memory)
{
    TIMED_BLOCK;
    assert(sizeof(DebugState) <= memory->debug_storage_size);
    dbg = (DebugState *) memory->debug_storage;
    GameState *g = (GameState *)memory->permanent_storage;

    dbg->arena = {};
    dbg->arena.base = (u8 *)memory->debug_storage + sizeof(DebugState);
    dbg->arena.size = memory->debug_storage_size - sizeof(DebugState);

    //TODO make this dynamically allocated and deallocated and not rely on a static thing
    //     or at the very least do a warning check
    dbg->snapshot_recording = true;
    dbg->snapshot_goto_end = true;
    dbg->snapshot_goto_end_slow_mode = true;
    dbg->num_snapshots = MAX_SNAPSHOT_COUNT;
    dbg->snapshots = arena_pushcount(&dbg->arena, DebugSnapshot, dbg->num_snapshots);
    dbg->visited_snapshots = arena_pushcount(&dbg->arena, u32, MAX_VISITED_COUNT);
    dbg->show_recorded_vars = true;
    for(int i = 0; i < dbg->num_snapshots; i++)
    {
        TIMED_BLOCK;
        dbg->snapshots[i].timings = arena_pushcount(&dbg->arena, TimingRecord, num_records);
        dbg->snapshots[i].varena = arena_suballocate(&dbg->arena, kilobytes(100));
        dbg->snapshots[i].vnode_arena = arena_suballocate(&dbg->arena, kilobytes(100));
    }
    dbg->curr_snapshot_index = 0;

    //TEMP::
    //dbg->pixel_storage_arena = arena_suballocate(&dbg->arena, megabytes(950));
    //dbg->view_index = 0;
    //dbg->num_last_frames_captured = 0;


    dbg->rtarget = &g->rtarget;
    dbg->rlist = &g->rlist;
    Asset *font_asset = get_asset_by_name(&g->assets, "hack", font);
    dbg->font = font_asset->font;
}


// #define dbg_markerc(x, y, color)                                       \
//     local_persist u32 dbg_markerID = U32_MAX;                   \
//     dbg_volumef(&dbg_markerID, x, y, DEBUG_MARKER_SIZE, DEBUG_MARKER_SIZE,  color)



//TODO anyway to get the type of this thing automatically??
//TODO why is passing float as varargs getting passed so late???
#include <stdarg.h>

internal void
dbg_consolidate_timing_records()
{
    TIMED_BLOCK;
    if(!dbg->snapshot_recording)
        return;
    for(int i = 0; i < num_records; i++)
    {
        dbg->snapshots[dbg->curr_snapshot_index].timings[i].hitcount = timing_records[i].hitcount;
        dbg->snapshots[dbg->curr_snapshot_index].timings[i].elapsed = timing_records[i].elapsed;

        dbg->snapshots[dbg->curr_snapshot_index].timings[i].file = timing_records[i].file;
        dbg->snapshots[dbg->curr_snapshot_index].timings[i].line = timing_records[i].line;
        dbg->snapshots[dbg->curr_snapshot_index].timings[i].func = timing_records[i].func;
        
        timing_records[i].hitcount = 0;
        timing_records[i].elapsed = 0;
    }
}

GAME_POST_UPDATE_AND_RENDER(game_post_update_and_render)
{
    dbg_consolidate_timing_records();  
};

internal void
dbg_textreset()
{
    dbg->textline_y = 0;
    dbg->textline_x = 0;
}

internal void
dbg_textline(char *str, f32 size = 20.0f)
{
    if(dbg->textline_y + dbg->font->descriptor->yadvance * size/dbg->font->descriptor->fontsize > dbg->rtarget->h)
    {
        dbg->textline_y = 0;
        dbg->textline_x += DEBUG_TEXTLINE_COLUMN_WIDTH;
    }
    dbg->textline_y += dbg->font->descriptor->yadvance * size/dbg->font->descriptor->fontsize;
    
    push_string(dbg->rlist, RENDER_LAYER_DEBUGOVERLAY,
                dbg->textline_x, dbg->textline_y, size, dbg->font, str, ALIGN_HORIZONTAL_LEFT);
}

internal void
dbg_textline_with_x_offset(char *str, f32 x, f32 size = 20.0f)
{
    if(dbg->textline_y + dbg->font->descriptor->yadvance * size/dbg->font->descriptor->fontsize > dbg->rtarget->h)
    {
        dbg->textline_y = 0;
        dbg->textline_x += DEBUG_TEXTLINE_COLUMN_WIDTH;
    }
    dbg->textline_y += dbg->font->descriptor->yadvance * size/dbg->font->descriptor->fontsize;
    
    push_string(dbg->rlist, RENDER_LAYER_DEBUGOVERLAY,
                dbg->textline_x + x, dbg->textline_y, size, dbg->font, str, ALIGN_HORIZONTAL_LEFT);
}

internal void
dbg_textappend(char *str, f32 x, f32 size = 20.0f)
{
    push_string(dbg->rlist, RENDER_LAYER_DEBUGOVERLAY,
                x, dbg->textline_y, size, dbg->font, str, ALIGN_HORIZONTAL_LEFT);
        
}

//NOTE:: retrieves an ungrouped var
internal u8 *
dbg_retrieve_var(char *varname)
{
    u8 *ret = 0;
    DebugSnapshot *viewing_snapshot = &dbg->snapshots[dbg->viewing_snapshot_index];
    for(int i = 0; i < viewing_snapshot->ungrouped_vars.children_count; i++)
    {
        if(string_equal(varname, viewing_snapshot->ungrouped_vars.children[i].name))
            ret = viewing_snapshot->ungrouped_vars.children[i].value;
    }

    return ret;
}

internal void
dbg_format_var(char *buf, ValueType type, char *name, u8 *value)
{
    switch(value_base_types[type])
    {
        case BT_atomic:
        {
            switch(type)
            {
                case T_f32:
                {
                    sprintf(buf, "%s: %0.5f", name, ((f32 *)(value))[0]);
                }break;
                case T_f64:
                {
                    sprintf(buf, "%s: %0.5f", name, ((f64 *)(value))[0]);
                }break;
                case T_u32:
                {
                    sprintf(buf, "%s: %u", name, ((u32 *)(value))[0]);
                }break;
                case T_u64:
                {
                    sprintf(buf, "%s: %I64u", name, ((u64 *)(value))[0]);
                }break;
                case T_i32:
                {
                    sprintf(buf, "%s: %i", name, ((i32 *)(value))[0]);
                }break;
                // case T_i64:
                // {
                //     sprintf(buf, "%s: %I64", name, ((i64 *)(value))[0]);
                // }break;
                case T_b32:
                {
                    sprintf(buf, "%s: %s", name,
                            ((b32 *)(value))[0] == true ? "true" : "false" );
                }break;
                case T_Vec2:
                {
                    f32 *floats = ((f32*)(value));
                    sprintf(buf, "%s: {%0.3f, %0.3f}", name,
                            floats[0], floats[1]);
                }break;
                // case T_Vec3:
                // {
                //     f32 *floats = ((f32*)(value));
                //     sprintf(buf, "%s: {%0.3f, %0.3f, %0.3f}", name,
                //             floats[0], floats[1], floats[2]);
                // }break;
                case T_Circle:
                {
                    f32 *floats = ((f32*)(value));
                    sprintf(buf, "%s: {%0.3f, %0.3f, %0.3f}", name,
                            floats[0], floats[1], floats[2]);
                }break;
                case T_AABB:
                {
                    f32 *floats = ((f32*)(value));
                    sprintf(buf, "%s: {%0.3f, %0.3f, %0.3f, %0.3f}", name,
                            floats[0], floats[1], floats[2], floats[3]);
                }break;
                case T_Rect:
                {
                    f32 *floats = ((f32*)(value));
                    sprintf(buf, "%s: {%0.3f, %0.3f, %0.3f, %0.3f}", name,
                            floats[0], floats[1], floats[2], floats[3]);
                }break;
                
                default:
                    sprintf(buf, "//#711Unknown type of atomic debug variable: %s", name);
            }
        }break;
        case BT_enum:
        {
            switch(type)
            {
                META_GENERATE_ENUM_SWITCHES();
                // case T_GotoBall:
                // {
                //     u32 num = ((u32 *)(value))[0];
                //     sprintf(buf, "%s: %u (%s)", name, num, (num < arraycount(GotoBall_enum_names)) ?
                //             GotoBall_enum_names[num] : "enum out of range");
                // }break;
                // case T_PaddleMoveDirection:
                // {
                //     u32 num = ((u32 *)(value))[0];
                //     sprintf(buf, "%s: %u (%s)", name, num, (num < arraycount(PaddleMoveDirection_enum_names)) ?
                //             PaddleMoveDirection_enum_names[num] : "enum out of range");
                // }break;
                default:
                {
                    u32 num = ((u32 *)(value))[0];
                    sprintf(buf, "%s: %u",name, num);
                }break;
            }break;
        }break;
        default:
            sprintf(buf, "//#711Unknown type of debug variable: %s", name);
    }
}

inline VarNode *
dbg_vargroup_add_node(VarGroup *group, MemoryArena *alloc, VarNodeType type)
{
    VarNode *ret = 0;

    if(group->children_count < group->children_cap)
    {
        //we good
        ret = &group->children[group->children_count];
        group->children_count++;
    }
    else
    {
        //we bad (let's grow the cap)
        if(group->children_cap == 0)
        {
            u32 initial_cap = 10;

            group->children = arena_pushcount(alloc, VarNode, initial_cap);
            group->children_cap = initial_cap;
            ret = &group->children[0];
            group->children_count++;
        }
        else
        {
            //TODO untested
            //stop("unimplemented need to copy")
            u32 desired_cap = group->children_cap*2;

            VarNode *new_location = arena_pushcount(alloc, VarNode, desired_cap);
            for(int i = 0; i < group->children_count; i++)
            {
                new_location[i] = group->children[i];
            }

            group->children = new_location;
            group->children_cap = desired_cap;
            ret = &group->children[group->children_count];
            group->children_count++;
        }
    }

    //NOTE:: make the returned children count 0 but don't change the children cap
    //       this way we do not need to reallocate we just overwrite previous allocated block
    //TODO   does this work??
    ret->children_count = 0; 
    ret->type = type;
    return ret;
}

internal void
dbg_record_var(VarRecord *record, char *name, ValueType type, u8 *ptr)
{
    //TODO right now let's 64bit align these guys but that wastes space
    //     for now this makes it easier since we know nothing will surpass this size

    //TODO idk!! ok simple array
    DebugSnapshot *snapshot = &dbg->snapshots[dbg->curr_snapshot_index];
    //assert(snapshot->var_groups[group_index] < MAX_ORPHAN_VAR_GROUPS);
    record->value_type = type;

    char *at = name;
    char *to = record->name;//snapshot->var_groups[group_index].name;
    while(*at)
    {
        *to++ = *at++;
    }

    to[0] = 0;
    u64 alloc_size = value_sizes[type];

    record->value = (u8 *) arena_pushsize(&snapshot->varena, alloc_size);

    u64 bytes_to_copy = alloc_size;
    u8 *dest = record->value;
    u8 *src = ptr;
    while(bytes_to_copy > 0)
    {
        *dest++ = *src++;
        bytes_to_copy--;
    }

    //snapshot->num_vars++;
}

typedef u64 umm;
internal void
dbg_vargroup(VarGroup *parent, u32 member_count, MemberDefinition *mem_defs, u8 *structptr, char *name,
             MemoryArena *vnode_arena)
{
    char *at = name;
    char *to = parent->name;
    while(*at)
    {
        *to++ = *at++;
    }
    to[0] = 0;
    

    for(int i = 0; i < member_count; i++)
    {
        MemberDefinition *member = &mem_defs[i];

        if(value_base_types[member->type] == BT_vargroup)
        {
            VarGroup *group = dbg_vargroup_add_node(parent, vnode_arena, VarNode_vargroup);
            //we need this members type
            u8 *location = 0;
            if(member->isptr)
            {
                umm address = ((umm *)((u8 *)structptr + member->offset))[0];
                location = (u8 *)address;
            }
            else
            {
                location = (structptr + member->offset);
            }

            //REALLY TODO how do we map the member->name to the member definition
            MemberDefinition *def = type_to_definitions[member->type - vargroup_base];
            dbg_vargroup(group, definition_member_counts[member->type - vargroup_base],
                         def,
                         location, member->name, vnode_arena);
        }
        else
        {
            VarGroup *record = dbg_vargroup_add_node(parent, vnode_arena, VarNode_varrecord);
            dbg_record_var(record, member->name, member->type, (u8 *)(structptr + member->offset));
            
        }
    }
}

internal void
dbg_structf(u32 member_count, MemberDefinition *mem_defs, u8 *structptr, char *name)
{
    DebugSnapshot *snapshot = &dbg->snapshots[dbg->curr_snapshot_index];
    assert(snapshot->num_var_groups < arraycount(snapshot->var_groups));
    VarGroup *group = &snapshot->var_groups[snapshot->num_var_groups];
    group->type = VarNode_vargroup; //TODO set this on init??
    snapshot->num_var_groups++;
    dbg_vargroup(group, member_count, mem_defs, structptr, name,
                 &dbg->snapshots[dbg->curr_snapshot_index].vnode_arena);
}

#define dbg_struct(type, structptr) (dbg_structf(arraycount(type##_members), type##_members, (u8 *)structptr, #type))

internal void
dbg_varf(char *displayname, ValueType type, u8 *ptr)
{
    TIMED_BLOCK;
    DebugSnapshot *snapshot = &dbg->snapshots[dbg->curr_snapshot_index];

    VarRecord *record = dbg_vargroup_add_node(&snapshot->ungrouped_vars, &snapshot->vnode_arena, VarNode_varrecord);
    dbg_record_var(record, displayname, type, ptr);
}



//TODO add support for volumes with conflicting line numbers (we need to take into account)
//     the file or function (something other than line number)
internal void
dbg_volumef(u32 ID, f32 x, f32 y, f32 width, f32 height, Renderer_RGB color, f32 transparency)
{
    u32 index = volume_id_hash(ID) % MAX_DEBUG_VOLUMES;

    DebugVolume *potential_volume = &dbg->snapshots[dbg->curr_snapshot_index].volume[index];
    while(!(potential_volume->ID == 0 || potential_volume->ID == ID))
    {
        index += 1;
        index %= MAX_DEBUG_VOLUMES;

        potential_volume = &dbg->snapshots[dbg->curr_snapshot_index].volume[index];
    }

    assert(potential_volume->ID == 0 || potential_volume->ID == ID);
    //let's fille this slot (or overwrite it)
    potential_volume->x = x;
    potential_volume->y = y;
    potential_volume->width = width;
    potential_volume->height = height;
    potential_volume->color = color;
    potential_volume->transparency = transparency;
    potential_volume->ID = ID;

    //NOTE:: this is dumbly slow
    b32 volume_already_recorded = false;
    for(int i = 0; i < dbg->num_volumes; i++)
    {
        if(dbg->volume_ids[i] == ID)
        {
            volume_already_recorded = true;
            break;
        }
    }
    if(!volume_already_recorded)
    {
        dbg->volume_ids[dbg->num_volumes] = ID;
        dbg->num_volumes++;
    }
}

internal void
dbg_print_vargroup(VarGroup *group, f32 xoffset)
{
    char buf[400];
    buf[0] = 0;

    sprintf(buf, "//#426Group: %s", group->name);
    dbg_textline_with_x_offset(buf, xoffset, 15);
    for(int i = 0; i < group->children_count; i++)
    {
        //TODO this is only one level deep (how to recurse??)
        if(group->children[i].type == VarNode_vargroup)
        {
            f32 indent = 40;
            dbg_print_vargroup(&group->children[i], xoffset + indent);
        }
        else
        {
            dbg_format_var(buf, group->children[i].value_type,
                           group->children[i].name, (u8 *)group->children[i].value);
            dbg_textline_with_x_offset(buf, xoffset, 15);
        }
    }
    
}


internal void
dbg_print_all_vars(DebugSnapshot *snapshot)
{
    for(int i = 0; i < snapshot->num_var_groups; i++)
    {
        dbg_print_vargroup(&snapshot->var_groups[i], 0);
    }
    dbg_print_vargroup(&snapshot->ungrouped_vars, 0);
}

//TODO pass the debug state directly into dbg_update
internal void
dbg_update(GameState *g, RenderList *rlist, RenderTarget *rtarget, Input *input)
{
    TIMED_BLOCK;
    dbg_textreset();

    if(input->key_pressed(KEY_f6))
    {
        dbg->overlay = !dbg->overlay;
		b32 show = dbg->overlay ? true : dbg->cursor_visible;
        dbg->cursor_visible = show;
        rtarget->should_show_cursor = show;
	}
    
    if(input->key_pressed(KEY_f7))
    {
        dbg->show_recorded_vars = !dbg->show_recorded_vars;
        dbg->show_profiler = !dbg->show_profiler;
    }
    
    if(input->key_pressed(KEY_f8))
    {
        dbg->draw_volumes = !dbg->draw_volumes;
    }
    if(input->key_pressed(KEY_f9))
    {
        dbg->vsync_test = ! dbg->vsync_test;
    }
    
    if(dbg->vsync_test)
    {
        push_rectangle(rlist, RENDER_LAYER_DEBUGOVERLAY,
                       0, 0, rtarget->w, rtarget->h, 0.0f, dbg->cyan ? COLOR_AKCYAN : COLOR_AKREALRED);

        dbg->cyan = !dbg->cyan;
    }

    //dbg->fixed_dt_accum += input->fixed_dt;
    dbg->dt_accum += input->dt;

    if(dbg->draw_volumes)
    {
        for(int i = 0; i < dbg->num_volumes; i++)
        {
            u32 volume_id = dbg->volume_ids[i];
            u32 hash = volume_id_hash(volume_id);

            u32 index = hash % MAX_DEBUG_VOLUMES;
            u32 original_snapshot_search_index =
                dbg->snapshot_goto_end ?
                dbg->curr_snapshot_index :
                dbg->viewing_snapshot_index;

            u32 snapshot_search_index = original_snapshot_search_index;
            
            //NOTE:: searches previous snapshots until the volume id is found and draws that
        find_volume:
            DebugVolume *potential_volume = &dbg->snapshots[snapshot_search_index].volume[index];
            while(!(potential_volume->ID == 0 || potential_volume->ID == volume_id))
            {
                potential_volume = next_volume(dbg->snapshots + snapshot_search_index, &index);
            }

            if(potential_volume->ID == 0)
            {
                snapshot_search_index = (snapshot_search_index + MAX_SNAPSHOT_COUNT - 1) % MAX_SNAPSHOT_COUNT;
                index = hash % MAX_DEBUG_VOLUMES;

                //NOTE:: hacky fix but will be rethinked when the whole volume system gets a revamp
                //       and this system moves towards a git like system of pointers
                //       because looping backwards until we find it is SLOW TODO TODO 

                if(snapshot_search_index != original_snapshot_search_index)
                //if we looped back then the volume has been discarded
                //(there are not enough snapshots to contain the last recorded volume that has this ID)
                    goto find_volume;
            }
            else
            {
                push_rectangle(rlist, 0,
                               potential_volume->x, potential_volume->y,
                               potential_volume->width, potential_volume->height, 0.0f, potential_volume->color,
                               potential_volume->transparency);
            }
            

        }
    }


    dbg->snapshots[dbg->curr_snapshot_index].temporary_arena_usage = temp_arena.used;
    dbg->snapshots[dbg->curr_snapshot_index].universe_arena_usage = g->universe_arena.used;

    dbg->snapshots[dbg->curr_snapshot_index].dt_accum = dbg->dt_accum;
    dbg->snapshots[dbg->curr_snapshot_index].fixed_dt_accum = dbg->fixed_dt_accum;
    dbg->snapshots[dbg->curr_snapshot_index].since_startup = input->dt_from_startup;

    if(input->key_pressed(KEY_p) && (input->key[KEY_lCtrl].down || input->key[KEY_rCtrl].down))
    {
        dbg->snapshot_recording = !dbg->snapshot_recording;
        dbg->update_paused = !dbg->update_paused;
    }
    if(dbg->update_paused)
    {
        push_rectangleoutline(rlist, RENDER_LAYER_DEBUGOVERLAY,
                              0, 0, rtarget->w, rtarget->h, 3, COLOR_AKRED);

    }


    if((input->key[KEY_lCtrl].down || input->key[KEY_rCtrl].down) &&
       input->key[KEY_leftArrow].down || input->key_pressed(KEY_leftArrow))
    {
        dbg->snapshot_goto_end = false;
        dbg->viewing_snapshot_index = (dbg->viewing_snapshot_index + dbg->num_snapshots - 1) % dbg->num_snapshots;
    }
    else if ((input->key[KEY_lCtrl].down || input->key[KEY_rCtrl].down) &&
             input->key[KEY_rightArrow].down || input->key_pressed(KEY_rightArrow))
    {
        dbg->snapshot_goto_end = false;
        dbg->viewing_snapshot_index = (dbg->viewing_snapshot_index + 1) % dbg->num_snapshots;
    }

    f32 curry = 0;
    char buf[222];
    //sprintf(buf, "Last frame time: %0.6f ms", dbg->snapshots[last_snapshot].frame_time);
    //list.textline(rlist, &dbg->font, 45, buf);

    if(dbg->snapshot_goto_end)
    {
        if(dbg->snapshot_goto_end_slow_mode)
        {
            if(dbg->update_paused)
            {
                goto granular;
            }
            else
            {
                u32 snapshots_over_quantum;
                snapshots_over_quantum = ((dbg->curr_snapshot_index) % 20) + 1;                    

                dbg->viewing_snapshot_index = (dbg->curr_snapshot_index - snapshots_over_quantum) % dbg->num_snapshots;
            }

        }
        else
        {
        granular:
            dbg->viewing_snapshot_index = dbg->curr_snapshot_index;                
        }

    }

    if(dbg->overlay)
    {
        if((input->key[KEY_lCtrl].down || input->key[KEY_rCtrl].down))
        {
            if(input->key_pressed(KEY_end))
            {
                dbg->snapshot_goto_end = true;
            }
            if(input->key_pressed(KEY_s))
            {
                dbg->snapshot_goto_end_slow_mode = !dbg->snapshot_goto_end_slow_mode;
            }
        }

        

        b32 gotomax = false;


        DebugSnapshot *viewing_snapshot = &dbg->snapshots[dbg->viewing_snapshot_index];
        
    recalc_viewing_snapshot_index:

        TimingRecord *timings_to_show;
        u32 frametime_snapshot_index;
        if(dbg->viewing_snapshot_index == dbg->curr_snapshot_index)
        {
            //show the last frame time and timers
            u32 last_snapshot_index = (dbg->curr_snapshot_index - 1) % dbg->num_snapshots;
            DebugSnapshot *last_snapshot = &dbg->snapshots[last_snapshot_index];

            timings_to_show = last_snapshot->timings;
            frametime_snapshot_index = last_snapshot_index;
            sprintf(buf, "Last frame time:  %0.5f ms ", 
                    dbg->snapshots[frametime_snapshot_index].frame_time);
        }
        else
        {
            //show the viewing frame time and timers
            timings_to_show = viewing_snapshot->timings;
            frametime_snapshot_index = dbg->viewing_snapshot_index;

            sprintf(buf, "Viewing frame time:  %0.5f ms ", 
                    dbg->snapshots[frametime_snapshot_index].frame_time);
        }
        dbg_textline(buf, 20);


        f32 avg = 0;
        f32 min = 3.3e38;
        f32 max = 0;

        u32 num_snapshots_to_calc = 200;
        u32 start = ((frametime_snapshot_index + dbg->num_snapshots - num_snapshots_to_calc)
                     % dbg->num_snapshots);
        u32 at = start;

        for(int i = 0; i < num_snapshots_to_calc; i++)
        {
            f32 frametime = dbg->snapshots[at].frame_time;
            avg += frametime;
            if(frametime > max)
            {
                max = frametime;                
            }
            if(frametime < min)
                min = frametime;
            at = (at + 1) % dbg->num_snapshots;
        }
        avg /= num_snapshots_to_calc;

        sprintf(buf, "(avg:%0.5f; min: %0.5f; max: %0.5f)", 
                avg, min, max);
        dbg_textline(buf, 20);

        sprintf(buf, "input processing: %0.5f", 
                dbg->snapshots[frametime_snapshot_index].platform_input_time);
        dbg_textline(buf, 15);
        sprintf(buf, "render time: %0.5f", 
                dbg->snapshots[frametime_snapshot_index].platform_render_time);
        dbg_textline(buf, 15);
        sprintf(buf, "game update:      %0.5f", 
                dbg->snapshots[frametime_snapshot_index].platform_game_update_time);
        dbg_textline(buf, 15);
        sprintf(buf, "clock stablizer:  %0.5f", 
                dbg->snapshots[frametime_snapshot_index].platform_clock_stabalizer_time);
        dbg_textline(buf, 15);


        sprintf(buf, "Temporary arena usage: %I64u kilobytes (%I64u bytes)", 
                dbg->snapshots[frametime_snapshot_index].temporary_arena_usage / 1024,
                dbg->snapshots[frametime_snapshot_index].temporary_arena_usage);
        dbg_textline(buf, 15);
        sprintf(buf, "Universe arena usage: %I64u kilobytes (%I64u bytes)", 
                dbg->snapshots[frametime_snapshot_index].universe_arena_usage / 1024,
                dbg->snapshots[frametime_snapshot_index].universe_arena_usage);
        dbg_textline(buf, 15);

         if(dbg->viewing_snapshot_index != dbg->curr_snapshot_index)
            sprintf(buf, "//#842Viewing snapshot %u of %u", dbg->viewing_snapshot_index, dbg->num_snapshots);
        else
            sprintf(buf, "Viewing snapshot %u of %u", dbg->viewing_snapshot_index, dbg->num_snapshots);
        dbg_textline(buf, 15);

        
        sprintf(buf, "fixed dt accumulator: %0.5f    dt accumulator: %0.5f    since startup: %0.5f",
                viewing_snapshot->fixed_dt_accum,
                viewing_snapshot->dt_accum,
                viewing_snapshot->since_startup);
        dbg_textline(buf, 15);

        for(int i = 0; i < num_records-1; i++)
        {
            for(int j = 0; j < num_records-1; j++)
            {
                TimingRecord *this_ = &timings_to_show[j];
                TimingRecord *next_ = &timings_to_show[j + 1];

                if(next_->elapsed > this_->elapsed)
                {
                    TimingRecord temp;

                    temp.line = next_->line;
                    temp.file = next_->file;
                    temp.func = next_->func;
                    temp.elapsed = next_->elapsed;
                    temp.hitcount = next_->hitcount;

                    next_->line = this_->line;
                    next_->file = this_->file;
                    next_->func = this_->func;
                    next_->elapsed = this_->elapsed;
                    next_->hitcount = this_->hitcount;

                    this_->line = temp.line;
                    this_->file = temp.file;
                    this_->func = temp.func;
                    this_->elapsed = temp.elapsed;
                    this_->hitcount = temp.hitcount;
                }
            }
                
        }
        if(dbg->show_profiler)
        {
            for(int i = 0; i < num_records; i++)
            {
                if(timings_to_show[i].hitcount == 0 || timings_to_show[i].func == 0)
                {
                    continue;
                }
                sprintf(buf, "%s (%I64u): ",
                        timings_to_show[i].func, timings_to_show[i].line);
                dbg_textline(buf);

                f32 x = 600;
                sprintf(buf, "%uh", timings_to_show[i].hitcount);
                dbg_textappend(buf, x);
                x += 200; 

                sprintf(buf, "%I64ucy", timings_to_show[i].elapsed);
                dbg_textappend(buf, x);
                x += 200; 

                sprintf(buf, "%I64ucy/h", timings_to_show[i].hitcount != 0 ?
                        (timings_to_show[i].elapsed / timings_to_show[i].hitcount) : 0);
                dbg_textappend(buf, x);
            }
        }
        else if (dbg->show_recorded_vars)
        {
            dbg_print_all_vars(viewing_snapshot);
        }

    }
}
#endif

#if INTERNAL && DEBUG_VOLUMES
#define dbg_rectt(x, y, w, h, color, transparency){        \
    local_persist u32 dbg_rectID = __LINE__;                    \
    dbg_volumef(dbg_rectID, x, y, w, h, color, transparency);}

#define dbg_rect(x, y, w, h, color){                            \
        local_persist u32 dbg_rectID = __LINE__;                \
        dbg_volumef(dbg_rectID, x, y, w, h, color, 1.0f);}

#define dbg_marker(x, y)                                                \
    local_persist u32 dbg_markerID = __LINE__;                          \
    dbg_volumef(dbg_markerID, x-DEBUG_MARKER_SIZE/2, y-DEBUG_MARKER_SIZE/2, DEBUG_MARKER_SIZE, DEBUG_MARKER_SIZE, COLOR_AKYELLOW, 1.0f)

#define dbg_markerm(x, y) {                                             \
        local_persist u32 dbg_markerID = __LINE__;                      \
        dbg_volumef(dbg_markerID, MetersToPixels(x)-DEBUG_MARKER_SIZE/2, MetersToPixels(y)-DEBUG_MARKER_SIZE/2, \
                    DEBUG_MARKER_SIZE, DEBUG_MARKER_SIZE, COLOR_AKYELLOW, 1.0f);}

#define dbg_markercm(x, y, color) {                                     \
        local_persist u32 dbg_markerID = __LINE__;                      \
        dbg_volumef(dbg_markerID, MetersToPixels(x)-DEBUG_MARKER_SIZE/2, MetersToPixels(y)-DEBUG_MARKER_SIZE/2, DEBUG_MARKER_SIZE, DEBUG_MARKER_SIZE, color, 1.0f);}

#else
#define dbg_rectt(...)
#define dbg_rect(...)
#define dbg_marker(...)
#define dbg_markerm(...)
#define dbg_markercm(...)

#endif
