struct RandomSeries
{
    u32 num;
    b32 initialized;
};

internal void
random_randomize(RandomSeries *series)
{
    series->num = (__rdtsc() % (U32_MAX - 1)) + 1; //series->num can never be 0
    series->initialized = true;
}

internal u32
random_next(RandomSeries *series)
{
    u32 ret = series->num;
    ret ^= ret << 13;
    ret ^= ret >> 17;
    ret ^= ret << 5;
    series->num = ret;
    return ret;
}

internal u32
random_choice(RandomSeries *series, u32 numchoices)
{
    u32 ret;
    u32 num = random_next(series);
    ret = num % numchoices;
    return ret;
}

internal f32
random_unilateral(RandomSeries *series)
{
    u32 num = random_next(series);
    f32 ret;
    ret = (f32)num / U32_MAX;
    return ret;
}

internal u32
random_rangei(RandomSeries *series, u32 min, u32 max)
{
    u32 num = random_next(series);
    u32 ret;
    ret = min + (num % (max+1 - min));
    return ret;
}

internal f32
random_range(RandomSeries *series, f32 min, f32 max)
{
    u32 num = random_next(series);
    f32 ret;
    ret = min + ((f32)num / U32_MAX)*(max-min);
    return ret;
}

