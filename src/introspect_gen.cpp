//Found 49 source code files(*.cpp, *.h)!
//ai.cpp (3867 bytes)
//apong.cpp (5786 bytes)
//apong_archives.cpp (33173 bytes)
//assets.cpp (7031 bytes)
//collisions.cpp (13055 bytes)
//ctokenizer.cpp (6411 bytes)
//d3d.cpp (940 bytes)
//debug.cpp (26212 bytes)
//debug_timers.cpp (1196 bytes)
//fileio.cpp (1034 bytes)
//hello.cpp (47 bytes)
//introspect_meta - Copy.cpp (42280 bytes)
//introspect_meta.cpp (44560 bytes)
//introspect_meta_template.cpp (3504 bytes)
//level.cpp (68590 bytes)
//level_bkup0.cpp (29503 bytes)
//math.cpp (8847 bytes)
//msvc.cpp (36 bytes)
//opengl.cpp (23526 bytes)
//opengl_error_checking.cpp (20749 bytes)
//opengl_functions.cpp (2417 bytes)
//opengl_meta.cpp (24018 bytes)
//opengl_registry_parser.cpp (31679 bytes)
//opengl_shadergenerated.cpp (4390 bytes)
//putaside.cpp (8635 bytes)
//random.cpp (1141 bytes)
//render.cpp (16871 bytes)
//render_helpers.cpp (5709 bytes)
//simd.cpp (97 bytes)
//sound.cpp (6644 bytes)
//stretchy_akela.cpp (1298 bytes)
//stringout.cpp (1555 bytes)
//stringutils.cpp (6412 bytes)
//text.cpp (676 bytes)
//ui.cpp (18801 bytes)
//win32_opengl.cpp (2 bytes)
//windows - Copy.cpp (28732 bytes)
//windows.cpp (56625 bytes)
//xmltokenizer.cpp (8664 bytes)
//apong.h (9301 bytes)
//apong_platform.h (10194 bytes)
//arena.h (803 bytes)
//assets.h (2392 bytes)
//debug.h (3274 bytes)
//math.h (1242 bytes)
//meta.h (2288 bytes)
//modeinits.h (107 bytes)
//render.h (3189 bytes)
//win32.h (14629 bytes)
META_EXCLUDE ____file
enum ValueType
{
    T_i32,
    T_f32,
    T_u32,
    T_b32,
    T_Vec2,
    T_u64,
    T_char,
    T_void,
    T_GLuint,
    T___m128,
    T_size_t,
    T_UIID,
    T_HWND,
    T_u8,
    T_Rect,
    T_Circle,
    T_GameMode,
    T_Menu,
    T_DifficultyPreset,
    T_AABB,
    T_f64,
    T_u16,
    T_i16,
    T_PlatformFileHandle,
    T_VarGroup,
    T_HANDLE,
    T_RandomSeries,
    T_PaddleAIState,
    T_Paddle,
    T_PaddleAIMode,
    T_GotoBall,
    T_PaddleMoveDirection,
    T_HoningConstraint,
    T_CommandNodeType,
    T_UIDrawType,
    T_BallDir,
    T_Mode,
    T_VarNodeType,
    T_TokenKind,
    T_BaseType,
    T_RenderEntryType,
    T_InputMessageType,
    T_ValueType,
    T_Text,
    T_Count
};


enum ValueBaseType
{
    BT_atomic,
    BT_vargroup,
    BT_enum,
    
    BT_Count
};

    u32 atomic_base = 0;
    u32 vargroup_base = 26;
    u32 enum_base = 29;

ValueBaseType value_base_types[T_Count] = {
    BT_atomic, /*i32*/
    BT_atomic, /*f32*/
    BT_atomic, /*u32*/
    BT_atomic, /*b32*/
    BT_atomic, /*Vec2*/
    BT_atomic, /*u64*/
    BT_atomic, /*char*/
    BT_atomic, /*void*/
    BT_atomic, /*GLuint*/
    BT_atomic, /*__m128*/
    BT_atomic, /*size_t*/
    BT_atomic, /*UIID*/
    BT_atomic, /*HWND*/
    BT_atomic, /*u8*/
    BT_atomic, /*Rect*/
    BT_atomic, /*Circle*/
    BT_atomic, /*GameMode*/
    BT_atomic, /*Menu*/
    BT_atomic, /*DifficultyPreset*/
    BT_atomic, /*AABB*/
    BT_atomic, /*f64*/
    BT_atomic, /*u16*/
    BT_atomic, /*i16*/
    BT_atomic, /*PlatformFileHandle*/
    BT_atomic, /*VarGroup*/
    BT_atomic, /*HANDLE*/
    BT_vargroup, /*RandomSeries*/
    BT_vargroup, /*PaddleAIState*/
    BT_vargroup, /*Paddle*/
    BT_enum, /*PaddleAIMode*/
    BT_enum, /*GotoBall*/
    BT_enum, /*PaddleMoveDirection*/
    BT_enum, /*HoningConstraint*/
    BT_enum, /*CommandNodeType*/
    BT_enum, /*UIDrawType*/
    BT_enum, /*BallDir*/
    BT_enum, /*Mode*/
    BT_enum, /*VarNodeType*/
    BT_enum, /*TokenKind*/
    BT_enum, /*BaseType*/
    BT_enum, /*RenderEntryType*/
    BT_enum, /*InputMessageType*/
    BT_enum /*ValueType*/
};

char *BallDir_enum_names[] =
{
    "BallDir_nowhere",
    "BallDir_left",
    "BallDir_right"
};

char *PaddleMoveDirection_enum_names[] =
{
    "nowhere",
    "up",
    "down"
};

u64 value_sizes[] = 
{
//TODO make these acutal sizes...somehow!!
8,
8,
8,
8,
8,
8,
8,
8,
8,
8,
8,
8,
8,
8,
16,
12,
8,
8,
8,
16,
8,
8,
8,
8,
8,
0,
0,
0,
4,
4,
4,
4,
4,
4,
4,
4,
4,
4,
4,
4,
4,
4,
};


struct MemberDefinition
{
    ValueType type;
    char *name;
    u32 offset;
    b32 isptr;
};


//fixed time step will run at BLAH
//when sampling need to resampe
//TODO worry about unions
//TODO worry about pointers

MemberDefinition Circle_members[] =//math.h
{
    {T_f32, "x",offsetof(Circle, x), false},
    {T_f32, "y",offsetof(Circle, y), false},
    {T_f32, "r",offsetof(Circle, r), false},
    {T_Vec2, "center",offsetof(Circle, center), false},
    {T_f32, "radius",offsetof(Circle, radius), false}
};

MemberDefinition Paddle_members[] =//apong.h
{
    {T_Rect, "bbox",offsetof(Paddle, bbox), false},
    {T_PaddleMoveDirection, "last_movedir",offsetof(Paddle, last_movedir), false},
    {T_Vec2, "vel",offsetof(Paddle, vel), false},
    {T_b32, "last_collided",offsetof(Paddle, last_collided), false},
    {T_PaddleAIState, "ai",offsetof(Paddle, ai), true}
};

MemberDefinition PaddleAIState_members[] =//apong.h
{
    {T_RandomSeries, "random",offsetof(PaddleAIState, random), false},
    {T_f32, "targety",offsetof(PaddleAIState, targety), false},
    {T_f32, "guess_delay",offsetof(PaddleAIState, guess_delay), false},
    {T_b32, "will_miss",offsetof(PaddleAIState, will_miss), false},
    {T_b32, "recovering",offsetof(PaddleAIState, recovering), false},
    {T_b32, "recovery_decision_made",offsetof(PaddleAIState, recovery_decision_made), false},
    {T_f32, "recovering_delay",offsetof(PaddleAIState, recovering_delay), false},
    {T_f32, "fail_recover_targety",offsetof(PaddleAIState, fail_recover_targety), false},
    {T_b32, "last_wonmatch",offsetof(PaddleAIState, last_wonmatch), false},
    {T_BallDir, "last_bdir",offsetof(PaddleAIState, last_bdir), false},
    {T_b32, "doing_rage",offsetof(PaddleAIState, doing_rage), false},
    {T_f32, "rage_frames_until_other_direction",offsetof(PaddleAIState, rage_seconds_until_other_direction), false},
    {T_b32, "rage_up",offsetof(PaddleAIState, rage_up), false}
};

MemberDefinition RandomSeries_members[] =//random.cpp
{
    {T_u32, "num",offsetof(RandomSeries, num), false},
    {T_b32, "initialized",offsetof(RandomSeries, initialized), false}
};

//TODO is there any other way to get a definition from the type at runtime??
MemberDefinition *type_to_definitions[] = 
{
    RandomSeries_members,
    PaddleAIState_members,
    Paddle_members,
};

u32 definition_member_counts[] = 
{
    2,
    13,
    5,
};
#define META_GENERATE_ENUM_SWITCHES(...) {\
case T_BallDir: \
{\
    u32 num = ((u32 *)(value))[0];\
    sprintf(buf, "%s: %u (%s)", name, num, (num < arraycount(BallDir_enum_names)) ? BallDir_enum_names[num] : "enum out of range");\
}break;\
case T_PaddleMoveDirection: \
{\
    u32 num = ((u32 *)(value))[0];\
    sprintf(buf, "%s: %u (%s)", name, num, (num < arraycount(PaddleMoveDirection_enum_names)) ? PaddleMoveDirection_enum_names[num] : "enum out of range");\
}break;\
}
