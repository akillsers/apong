enum ValueType
{
    T_V2,
    T_V3 ,
    T_V4 ,
    T_F32,
    T_F64,
    T_I8 ,
    T_I16,
    T_I32,
    T_I64,
    T_U8 ,
    T_U16,
    T_U32,
    T_U64,
    T_B32,
    T_GotoBall,
    T_PaddleMoveDirection,
    T_PaddleAIMode,
    T_RandomSeries,
    T_RECT,
    T_CIRCLE,
    T_PaddleAIState,
    T_Paddle,
    
    T_Count
};

enum ValueBaseType
{
    BT_atomic,
    BT_vargroup,
    BT_multiplie,
    BT_enum,

    BT_count
};

ValueBaseType value_base_types[T_Count] =
{
    BT_atomic,    // T_V2,
    BT_atomic,   // T_V3 ,
    BT_atomic,// T_V4 ,
    BT_atomic,// T_F32,
    BT_atomic,// T_F64,
    BT_atomic,// T_I8 ,
    BT_atomic,// T_I16,
    BT_atomic,// T_I32,
    BT_atomic,// T_I64,
    BT_atomic,// T_U8 ,
    BT_atomic,// T_U16,
    BT_atomic,// T_U32,
    BT_atomic,// T_U64,
    BT_atomic,// T_B32,
    BT_enum,// T_GotoBall,
    BT_enum,// T_PaddleMoveDirection,
    BT_multiplie,// T_PaddleAIMode,
    BT_multiplie,// T_RandomSeries,
    BT_multiplie,// T_RECT,
    BT_multiplie,// T_CIRCLE,
    BT_vargroup,// T_PaddleAIState,
    BT_vargroup,//paddle
    // T_Count
    
};



//NOTE:: the value types are sorted in this ascending order:
//atomic types, enumerations then vargroup

char *GotoBall_enum_names[] =
{
    "GotoBall_didnotchoose",
    "GotoBall_yes",
    "GotoBall_no"
};

char *PaddleMoveDirection_enum_names[] =
{
    "nowhere",
    "up",
    "down"
  
};

u64 value_sizes[] =
{
    8,// T_V2,
    12,// T_V3 ,
    16, // T_V4 ,
    4, // T_F32,
    8, // T_F64,
    1, // T_I8 ,
    2, // T_I16,
    4, // T_I32,
    8, // T_I64,
    1, // T_U8 ,
    2, // T_U16,
    4, // T_U32,
    8, // T_U64,
    4, // T_B32,
    4, // T_GotoBall,
    4, // T_PaddleMoveDirection,
    4, // T_PaddleAIMode,
    4, // T_RandomSeries,
    16, // T_RECT,
    12, // T_CIRCLE,

    //0 // T_Count
};

struct MemberDefinition
{
    ValueType type;
    char *name;
    u32 offset;
    b32 isptr;
};


MemberDefinition PaddleAIState_members[] =
{
    {T_PaddleAIMode, "mode", offsetof(PaddleAIState, mode), false},
    {T_GotoBall, "gotoball", offsetof(PaddleAIState, gotoball), false},
    {T_RandomSeries, "random", offsetof(PaddleAIState, random), false},
    {T_I32, "steps_until_impact", offsetof(PaddleAIState, steps_until_impact), false},
    {T_I32, "expected_move_steps", offsetof(PaddleAIState, expected_move_steps), false},
    {T_F32, "dist_to_target", offsetof(PaddleAIState, dist_to_target), false},
    {T_F32, "honing_earliness_seconds", offsetof(PaddleAIState, honing_earliness_seconds), false},
    {T_B32, "do_inertia_after_honing", offsetof(PaddleAIState, do_inertia_after_honing), false},
    {T_PaddleMoveDirection, "last_non_nowhere_dir", offsetof(PaddleAIState, last_non_nowhere_dir), false}
}; 

MemberDefinition Paddle_members[] =
{
    {T_RECT, "bbox", offsetof(Paddle, bbox), false},
    {T_PaddleMoveDirection, "last_movedir", offsetof(Paddle, last_movedir), false},
    {T_V2, "vel", offsetof(Paddle, vel), false},
    {T_B32, "last_collided", offsetof(Paddle, last_collided), false},
    {T_PaddleAIState, "ai", offsetof(Paddle, ai), true}
};


//TODO in real meta thing move this up so that no need for subtraction
u32 first_vargroup_T = 20;

MemberDefinition *types_to_definitions[] =
{
    PaddleAIState_members,
    Paddle_members
};

u32 definition_member_counts[] =
{
    9,
    5
};
