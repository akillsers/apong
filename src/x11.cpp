// TODO: Make the platform file handle the same across platforms? How does HandmadeHero handle this, by chance?
typedef int PlatformFileHandle;
#include "apong_platform.h"
#include "render.h"
#include "x11.h"
#include <x86intrin.h>

#include "akgl.h"
#include "opengl.cpp"

#define Font X11Font
#include <GL/glx.h>
#undef Font

#include <stdio.h>
#include <string.h>

#include "render.cpp" // TODO: Can this file just be in apong.cpp?
#include "apong.cpp"

typedef GLXContext (* p_glXCreateContextAttribsARB) (Display *dpy, GLXFBConfig config, GLXContext share_context, Bool direct, const int * attrib_list);
global_variable p_glXCreateContextAttribsARB glXCreateContextAttribsARB;

internal void *
x11_opengl_func_address(char *name)
{
    void *address = 0;
    address = (void *)glXGetProcAddressARB((const GLubyte *)name);
    return address;
}

internal void
x11_exit()
{
    exit(0);
}

internal void
x11_toggle_fullscreen(Display *display, Window window)
{
    Atom _NET_WM_STATE = XInternAtom(display, "_NET_WM_STATE", False);
    Atom _NET_WM_STATE_FULLSCREEN = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);
    
    g_x11.fullscreen = !g_x11.fullscreen;
    // Relevant spec: https://specifications.freedesktop.org/wm-spec/wm-spec-1.3.html
    // See also https://tronche.com/gui/x/xlib/events/client-communication/client-message.html#XClientMessageEvent
    XEvent event = {};
    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = _NET_WM_STATE;
    event.xclient.format = 32;
    event.xclient.data.l[0] = g_x11.fullscreen ? _NET_WM_STATE_ADD : _NET_WM_STATE_REMOVE;
    event.xclient.data.l[1] = _NET_WM_STATE_FULLSCREEN;
    event.xclient.data.l[3] = 1; // "Normal application"
    
    XSendEvent(display, 
               DefaultRootWindow(display),
               0, // TODO: I have no idea what this field means! (Choose false for convenience)
               SubstructureRedirectMask, // TODO: Do we technically need the SubstructureNotifyMask as well?
               &event);
    XFlush(display);
}

internal void
x11_swap_buffers()
{
    glXSwapBuffers(g_x11.display, g_x11.window);
}

internal void
x11_init_opengl(Display *display, Window *window)
{
    GLint glxAttribs[] =
    {
        GLX_X_RENDERABLE, True,
        GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
        GLX_RENDER_TYPE, GLX_RGBA_BIT,
        // GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, // TODO: Does this field matter?
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DOUBLEBUFFER, True, // TODO: Should we enable double buffering?
        None
    };
    
    
    int fb_count;
    GLXFBConfig *config = glXChooseFBConfig(display, DefaultScreen(display), glxAttribs, &fb_count);
    if(config)
    {
        GLXContext context = 0;
        
        int context_attribs[] = 
        {
            GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
            GLX_CONTEXT_MINOR_VERSION_ARB, 3,
            None
        };
        
        
        glXCreateContextAttribsARB = (p_glXCreateContextAttribsARB) x11_opengl_func_address("glXCreateContextAttribsARB");
        
        if(glXCreateContextAttribsARB)
        {
            context = glXCreateContextAttribsARB(display, 
                                                config[0], 
                                                NULL, // share_context
                                                True,
                                                context_attribs);
            XFree(config);
            
            if(context)
            {
                Bool make_current_success = glXMakeCurrent(display, *window, context);
                
                if(make_current_success)
                {
#if 0
                    printf("GL Vendor: %s\n", glGetString(GL_VENDOR));
                    printf("GL Renderer: %s\n", glGetString(GL_RENDERER));
                    printf("GL Version: %s\n", glGetString(GL_VERSION));
                    printf("GL Shading Language: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));    
#endif               
                    opengl_load_functions();

                }
                else
                {
                    fprintf(stderr, "Error: glxMakeCurrent failed.\n");
                }
            }
            else
            {
                fprintf(stderr, "Error: glxCreateContextAttribsARB failed.\n");
            }
        }
        else
        {
            fprintf(stderr, "Error: Could not get glXCreateContextAttribsARB proc address.\n");
        }
        
    }
    else
    {
        // TODO: Logging
        fprintf(stderr, "Error: glXChooseFBConfig failed.\n");
        x11_exit();
    }
}

internal FileError
x11_write_file(PlatformFileHandle file_handle, u64 offset, u32 num_bytes_to_write, u8 *data, b32 truncate = false)
{
    // TODO: Let's not do anything since apong doesn't actually write any files.
    return FileError_success;
}

internal void
x11_get_system_time(PlatformTime *time)
{
    // TODO: Let's not do anything since apong doesn't use the time anyway.
    return;
}

internal u32
x11_get_time_bias_minutes()
{
    // TODO: Let's not do anything since apong doesn't use the time anyway.
    return 0;
}

internal void
x11_update_platformkey(Input *input, u32 keyID, u32 ended_down)
{
    
}

internal void
x11_init_keyboard_map(Display *display)
{
    // Initialize the keyboard mapping
    XkbDescPtr Kbdesc = XkbGetMap(display, 0, XkbUseCoreKbd);
    XkbGetNames(display, XkbKeyNamesMask | XkbKeyAliasesMask, Kbdesc);

    int min_key_code = Kbdesc->min_key_code;
    int max_key_code = Kbdesc->max_key_code;
    
    for(int i = min_key_code; i <= max_key_code; ++i)
    {
        // NOTE: This requires the X11 keyboard extension
        char name[XkbKeyNameLength + 1];
        memcpy(name, Kbdesc->names->keys[i].name, XkbKeyNameLength);
        
        Key key = KEY_unknown;
        
        for(int j = 0; j < arraycount(x11_keymap); ++j)
        {
            if(strncmp(name, x11_keymap[j].name, XkbKeyNameLength) == 0)
            {
                key = x11_keymap[j].key;
                break;
            }
        }
        
        x11_keycode_to_key[i] = key;
    }
}

internal void
x11_process_messages(Display *display, Window window, Input *input)
{
    XEvent event = {};
    while(XPending(display))
    {
        XNextEvent(display, &event);
        
        switch(event.type)
        {
            case KeymapNotify:
            {
                // TODO: I have no idea what we need to do here? When does this get called? When we change the keyboard layout maybe?
            } break;
            
            case MotionNotify:
            {
                input->mousemoved = true;
            } break;
            
            case KeyPress:
            {
                u32 keycode = event.xkey.keycode;
                u32 key = x11_keycode_to_key[keycode];
                
                if(key == KEY_f11)
                {
                    x11_toggle_fullscreen(display, window);
                }
            } break;
            
            case KeyRelease:
            {
                
            } break;
            
            case ConfigureNotify:
            {
                if(event.xconfigure.width != platform.w_w ||
                   event.xconfigure.height != platform.w_h)
                {
                    platform.w_w = event.xconfigure.width;
                    platform.w_h = event.xconfigure.height;
                }
            } break;
            
            default:
            {
                
            } break;
        }
    }
}

internal FileError
x11_open_file_by_path(char *path, PlatformFileHandle *handle, u64 flags)
{
    int oflag = 0;
    if(flags & WRITE_ACCESS)
    {
        oflag |= O_RDWR;
    }
    else
    {
        oflag |= O_RDONLY;
    }
    
    int file_handle = open(path, oflag);
    
    if(file_handle < 0)
    {
        int error_code = errno;
        FileError error = FileError_unknown_error;
        
        switch(error_code)
        {
            case EACCES:
            {
                error = FileError_access_denied;
            } break;
            
            case ENOENT:
            {
                error = FileError_nonexistent;
            } break;
            default:
            {
                error = FileError_unknown_error;
            } break;
        }
        
        return FileError_unknown_error;
    }
    else
    {
        *handle = file_handle;
        return FileError_success;
    }
}

internal FileError 
x11_open_file(FileID file_id, PlatformFileHandle *file_handle)
{
    FileError result = FileError_unknown_error;
    PlatformFileHandle file = 0;
    
    switch(file_id)
    {
        case FileID_GameAssetsFile:
        {
            char *asset_file_path = "data/apong_assets.apa";
            result = x11_open_file_by_path(asset_file_path, &file, 0);
            
            if(result != FileError_success)
            {
                fprintf(stderr, "Error: Could not load data from data/apong_assets.apa. Trying altnerate path (../data/apong_assets.apa)...\n");
                char *alternate_asset_file_path = "../data/apong_assets.apa";
                result = x11_open_file_by_path(alternate_asset_file_path, &file, 0);
            }
            
            if(result != FileError_success)
            {
                fprintf(stderr, "Error: Unable to load game assets.\n");
            }
            
            // TODO: Implement the shenanigans where we pack the assets into the executable
        } break;
        
        default:
        {
            result = FileError_invalid_file_id;
        } break;
    }
    
    *file_handle = file;
    return result;
}

internal void
x11_close_file(PlatformFileHandle file_handle)

{
    int success = close(file_handle);
    if(success < 0)
    {
        fprintf(stderr, "Error: close failed.\n");
        // TODO: Logging;
    }
}

internal FileError
x11_read_file(PlatformFileHandle file_handle, void *out, u64 offset, u32 num_bytes_to_read, b32 block)
{
    // TODO: We don't currently support asynchronous reads
    
    lseek(file_handle, offset, SEEK_SET);
    ssize_t bytes_read = read(file_handle, out, num_bytes_to_read);
    if(bytes_read < 0)
    {
        return FileError_unknown_error;
    }
    else
    {
        return FileError_success;
    }
}

internal u64
x11_get_file_size(PlatformFileHandle handle)
{
    off_t seek_offset = lseek(handle, 0, SEEK_END);
    if(seek_offset < 0)
    {
        fprintf(stderr, "Error: lseek failed\n");
        return 0;
    }
    else
    {
        return seek_offset;
    }
}

// TODO: Is there the equivalent of WinMain on Linux?
int main(int argc, char *argv[])
{
    Display *display = XOpenDisplay(NULL);
    
    if(!display)
    {
        // TODO: logging
    }
    g_x11.display = display;
    
    Screen *screen = XDefaultScreenOfDisplay(display);
    int screenID = XDefaultScreen(display);
    
    
    Window window;

    unsigned long xattrmask = 0;
    
    XSetWindowAttributes xattr = {};
    xattr.override_redirect = True; // TODO: What the heck is this and does it even mattter?
    xattrmask |= CWOverrideRedirect;
    xattr.background_pixel = BlackPixel(display, screenID);
    xattrmask |= CWBackPixel;
    // TODO: Do we need to worry about border pixel and colormap attributes?
    
    Window parent = XDefaultRootWindow(display);
    
    u32 initial_window_width = 600;
    u32 initial_window_height = 480;
    
    //TODO: Is there default options for X, Y, width, height?
    window = XCreateSimpleWindow(display, 
                        parent, 
                        0, // X 
                        0, // Y
                        initial_window_width, // width
                        initial_window_height, // height
                        1, //border_width
                        BlackPixel(display, screenID), // border 
                        WhitePixel(display, screenID)); // background
    g_x11.window = window;
    
    // Hook up platform calls
    platform.opengl_func_address = x11_opengl_func_address;
    platform.open_file = x11_open_file;
    platform.get_file_size = x11_get_file_size;
    platform.read_file = x11_read_file;
    platform.write_file = x11_write_file;
    platform.close_file = x11_close_file;
    platform.get_time = x11_get_system_time;
    platform.get_time_bias_minutes = x11_get_time_bias_minutes;
    platform.shut_off = x11_exit;
    
    // Get OpenGL ready for drawing
    platform.swap_buffers = x11_swap_buffers;
    x11_init_opengl(display, &window);
    opengl_init_draw(initial_window_width, initial_window_height);
    
    x11_init_keyboard_map(display);
    
    // Show the window
    XClearWindow(display, window); // TODO: Is this necessary?
    XMapRaised(display, window);
    
    // Go fullscreen!
    x11_toggle_fullscreen(display, window);
    
    // Subscribe to the relevant input messages
    XSelectInput(display, window, KeyPressMask | KeyReleaseMask | KeymapStateMask | StructureNotifyMask | PointerMotionMask);
    
    // Initialize game memory
    
    void *memory_for_game = mmap(NULL, GAME_STORAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    
    if(memory_for_game == MAP_FAILED)
    {
        fprintf(stderr, "mmap failed to allocate memory for game.\n");
        x11_exit();
    }
    
    GameMemory game_memory = {};
    game_memory.permanent_storage = (u8 *)memory_for_game;
    game_memory.permanent_storage_size = PERMANENT_STORAGE_SIZE;
    
    game_memory.scratch_storage = (u8*)memory_for_game + PERMANENT_STORAGE_SIZE;
    game_memory.scratch_storage_size = SCRATCH_STORAGE_SIZE;
    
#if INTERNAL
    void *memory_for_debug = mmap(NULL, DEBUG_STORAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    
    if(memory_for_debug == MAP_FAILED)
    {
        fprintf(stderr, "mmap failed to allcoate memory for debug.\n");
        x11_exit();
    }
    
    game_memory.debug_storage = (u8 *)memory_for_debug;
    game_memory.debug_storage_size = DEBUG_STORAGE_SIZE;
#endif
    game_init_memory(&game_memory);
    
    bool running = true;
    Input input = {};
    
    input.dt = 0.16;
    platform.w_w = initial_window_width;
    platform.w_h = initial_window_height;
    
    while(running)
    {
        input.mousemoved = false;
        
        x11_process_messages(display, window, &input);
        
        // Grab the mouse position
        Window mouse_root_window;
        Window mouse_child_window;
        int mouse_root_x;
        int mouse_root_y;
        int mouse_win_x;
        int mouse_win_y;
        unsigned int mask_return;
        
        // TODO: Research whether or not XQueryPointer accepts NULL pointers for fields we don't care about
        XQueryPointer(display, window, &mouse_root_window, &mouse_child_window, &mouse_root_x, &mouse_root_y,  &mouse_win_x, &mouse_win_y, &mask_return);
        
        input.mousex = mouse_win_x;
        input.mousey = mouse_win_y;
        
        RenderList *rlist;
        RenderTarget *rtarget;
        game_update_and_render(&input, &game_memory, &rlist, &rtarget);
        
        renderlist_to_screen_opengl(rlist, rtarget);
    }
}
