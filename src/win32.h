/*
  NOTE from <Audioclient.h>
  #define AUDCLNT_ERR(n) MAKE_HRESULT(SEVERITY_ERROR, FACILITY_AUDCLNT, n)
  #define AUDCLNT_SUCCESS(n) MAKE_SCODE(SEVERITY_SUCCESS, FACILITY_AUDCLNT, n)
  #define AUDCLNT_E_NOT_INITIALIZED              AUDCLNT_ERR(0x001)
  #define AUDCLNT_E_ALREADY_INITIALIZED          AUDCLNT_ERR(0x002)
  #define AUDCLNT_E_WRONG_ENDPOINT_TYPE          AUDCLNT_ERR(0x003)
  #define AUDCLNT_E_DEVICE_INVALIDATED           AUDCLNT_ERR(0x004)
  #define AUDCLNT_E_NOT_STOPPED                  AUDCLNT_ERR(0x005)
  #define AUDCLNT_E_BUFFER_TOO_LARGE             AUDCLNT_ERR(0x006)
  #define AUDCLNT_E_OUT_OF_ORDER                 AUDCLNT_ERR(0x007)
  #define AUDCLNT_E_UNSUPPORTED_FORMAT           AUDCLNT_ERR(0x008)
  #define AUDCLNT_E_INVALID_SIZE                 AUDCLNT_ERR(0x009)
  #define AUDCLNT_E_DEVICE_IN_USE                AUDCLNT_ERR(0x00a)
  #define AUDCLNT_E_BUFFER_OPERATION_PENDING     AUDCLNT_ERR(0x00b)
  #define AUDCLNT_E_THREAD_NOT_REGISTERED        AUDCLNT_ERR(0x00c)
  #define AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED   AUDCLNT_ERR(0x00e)
  #define AUDCLNT_E_ENDPOINT_CREATE_FAILED       AUDCLNT_ERR(0x00f)
  #define AUDCLNT_E_SERVICE_NOT_RUNNING          AUDCLNT_ERR(0x010)
  #define AUDCLNT_E_EVENTHANDLE_NOT_EXPECTED     AUDCLNT_ERR(0x011)
  #define AUDCLNT_E_EXCLUSIVE_MODE_ONLY          AUDCLNT_ERR(0x012)
  #define AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL AUDCLNT_ERR(0x013)
  #define AUDCLNT_E_EVENTHANDLE_NOT_SET          AUDCLNT_ERR(0x014)
  #define AUDCLNT_E_INCORRECT_BUFFER_SIZE        AUDCLNT_ERR(0x015)
  #define AUDCLNT_E_BUFFER_SIZE_ERROR            AUDCLNT_ERR(0x016)
  #define AUDCLNT_E_CPUUSAGE_EXCEEDED            AUDCLNT_ERR(0x017)
  #define AUDCLNT_E_BUFFER_ERROR                 AUDCLNT_ERR(0x018)
  #define AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED      AUDCLNT_ERR(0x019)
  #define AUDCLNT_E_INVALID_DEVICE_PERIOD        AUDCLNT_ERR(0x020)
  #define AUDCLNT_E_INVALID_STREAM_FLAG          AUDCLNT_ERR(0x021)
  #define AUDCLNT_E_ENDPOINT_OFFLOAD_NOT_CAPABLE AUDCLNT_ERR(0x022)
  #define AUDCLNT_E_OUT_OF_OFFLOAD_RESOURCES     AUDCLNT_ERR(0x023)
  #define AUDCLNT_E_OFFLOAD_MODE_ONLY            AUDCLNT_ERR(0x024)
  #define AUDCLNT_E_NONOFFLOAD_MODE_ONLY         AUDCLNT_ERR(0x025)
  #define AUDCLNT_E_RESOURCES_INVALIDATED        AUDCLNT_ERR(0x026)
  #define AUDCLNT_E_RAW_MODE_UNSUPPORTED         AUDCLNT_ERR(0x027)
  #define AUDCLNT_E_ENGINE_PERIODICITY_LOCKED    AUDCLNT_ERR(0x028)
  #define AUDCLNT_E_ENGINE_FORMAT_LOCKED         AUDCLNT_ERR(0x029)
  #define AUDCLNT_S_BUFFER_EMPTY                 AUDCLNT_SUCCESS(0x001)
  #define AUDCLNT_S_THREAD_ALREADY_REGISTERED    AUDCLNT_SUCCESS(0x002)
  #define AUDCLNT_S_POSITION_STALLED             AUDCLNT_SUCCESS(0x003)
*/

#include "resource.h"

#define WGL_DRAW_TO_WINDOW_ARB                  0x2001
#define WGL_SUPPORT_OPENGL_ARB                  0x2010
#define WGL_DOUBLE_BUFFER_ARB                   0x2011
#define WGL_COLOR_BITS_ARB                      0x2014
#define WGL_DEPTH_BITS_ARB                      0x2022
#define WGL_STENCIL_BITS_ARB                    0x2023

#define WGL_PIXEL_TYPE_ARB                      0x2013
#define WGL_TYPE_RGBA_ARB                       0x202B

#define WGL_CONTEXT_MAJOR_VERSION_ARB           0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB           0x2092

#define WGL_CONTEXT_PROFILE_MASK_ARB            0x9126
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB        0x00000001
        
typedef BOOL (* p_wglChoosePixelFormatARB) (HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);
p_wglChoosePixelFormatARB wglChoosePixelFormatARB;

typedef HGLRC (* p_wglCreateContextAttribsARB)(HDC hDC, HGLRC hShareContext, const int *attribList);
p_wglCreateContextAttribsARB wglCreateContextAttribsARB;

typedef BOOL (WINAPI * p_wglSwapIntervalEXT) (int interval);
p_wglSwapIntervalEXT wglSwapIntervalEXT;

global_variable volatile b32 global_running;

#define GET_X_LPARAM(lp)                        ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)                        ((int)(short)HIWORD(lp))

#if INTERNAL
//SECTION:: Frame inspection
/*TODO make this something you can set in the game layer to call the platform frame inspeciton mode??*/
//TODO pass input into the game that way we can inspect that too
global_variable b32 frame_inspection_mode;

#endif

#define WIN32_MAX_EXE_NAME_SIZE megabytes(1)
#define WIN32_MAX_USER_PATH_SIZE megabytes(1)

/*
  Derrived from:
  http://www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
*/
u32 win32_scancode_to_key[256] =
{
    KEY_unknown, //Keyboard erro
    KEY_esc,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_0,
    KEY_minus,
    KEY_plus,
    KEY_backspace,
    KEY_tab,
    
    KEY_q,
    KEY_w,
    KEY_e,
    KEY_r,
    KEY_t,
    KEY_y,
    KEY_u,
    KEY_i,
    KEY_o,
    KEY_p,
    KEY_leftsqbrackets,
    KEY_rightsqbrackets,
    KEY_enter,
    KEY_lCtrl,
    KEY_a,
    KEY_s,

    KEY_d,
    KEY_f,
    KEY_g,
    KEY_h,
    KEY_j,
    KEY_k,
    KEY_l,
    KEY_semicolon,
    KEY_singlequotes,
    KEY_graveaccent,
    KEY_lShift,
    KEY_backslash,
    KEY_z,
    KEY_x,
    KEY_c,
    KEY_v,

    KEY_b,
    KEY_n,
    KEY_m,
    KEY_comma,
    KEY_period,
    KEY_forwardslash,
    KEY_rShift,
    KEY_numpadasterisk, /*may be prt scr * on a 83 84 key keyboard*/
    KEY_lAlt,
    KEY_space,
    KEY_capslock,
    KEY_f1,
    KEY_f2,
    KEY_f3,
    KEY_f4,
    KEY_f5,
         
    KEY_f6,
    KEY_f7,
    KEY_f8,
    KEY_f9,
    KEY_f10,
    KEY_numlock,
    KEY_scrolllock,
    KEY_numpad7,
    KEY_numpad8,
    KEY_numpad9,
    KEY_numpadminus,
    KEY_numpad4,
    KEY_numpad5,
    KEY_numpad6,
    KEY_numpadplus,
    KEY_numpad1,

    KEY_numpad2,
    KEY_numpad3,
    KEY_numpad0,
    KEY_numpadperiod,
    KEY_unknown, /**/
    KEY_unknown, /*may be f11 or f12???*/
    KEY_unknown, /*non-US key to the left or right of left ALT key*/
    KEY_f11,
    KEY_f12,

//BEGIN International keys section
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_menu,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,
    KEY_unknown,

};

/*
  Synchronisation
  abstraction
*/

struct sync_request
{
    HANDLE request_send_event;
    HANDLE request_accepted_event;
    HANDLE request_passed_event;
};

internal void
InitRequest(sync_request *request)
{
    request->request_send_event = CreateEvent(0, 0, 0, 0);     //CreateEvent parameters
    request->request_accepted_event = CreateEvent(0, 0, 0, 0); //security attrib, manual reset, initial state, name
    request->request_passed_event = CreateEvent(0, 0, 0, 0);
}

internal void
DeleteRequest(sync_request *request)
{
    CloseHandle(request->request_send_event);
    CloseHandle(request->request_accepted_event);
    CloseHandle(request->request_passed_event);
}

internal void
SendRequestAndWait(sync_request request)
{
    assert(request.request_send_event != INVALID_HANDLE_VALUE && request.request_send_event != 0);
    assert(request.request_accepted_event != INVALID_HANDLE_VALUE && request.request_accepted_event != 0);

    SetEvent(request.request_send_event);
    WaitForSingleObject(request.request_accepted_event, INFINITE);
}

internal void
AcceptRequestIfExists(sync_request request)
{
    assert(request.request_send_event != INVALID_HANDLE_VALUE && request.request_send_event != 0);
    assert(request.request_accepted_event != INVALID_HANDLE_VALUE && request.request_accepted_event != 0);

    if(WaitForSingleObject(request.request_send_event, 0) == WAIT_OBJECT_0)
        SetEvent(request.request_accepted_event);
}

internal void
AcceptRequest(sync_request request)
{
    assert(request.request_send_event != INVALID_HANDLE_VALUE && request.request_send_event != 0);
    assert(request.request_accepted_event != INVALID_HANDLE_VALUE && request.request_accepted_event != 0);

    if(WaitForSingleObject(request.request_send_event, INFINITE) == WAIT_OBJECT_0)
        SetEvent(request.request_accepted_event);
}

//Returns false if didn't exists
//Waits and returns true if exists
internal b32
AcceptWaitRequestIfExists(sync_request request)
{
    assert(request.request_send_event != INVALID_HANDLE_VALUE && request.request_send_event != 0);
    assert(request.request_accepted_event != INVALID_HANDLE_VALUE && request.request_accepted_event != 0);
    assert(request.request_passed_event != INVALID_HANDLE_VALUE && request.request_passed_event != 0);


    if(WaitForSingleObject(request.request_send_event, 0) == WAIT_OBJECT_0)
    {
        SetEvent(request.request_accepted_event);
        WaitForSingleObject(request.request_passed_event, INFINITE);
        return true;
    }
    else
    {
        return false;
    }
}

internal void
NixRequest(sync_request request)
{
    assert(request.request_passed_event != INVALID_HANDLE_VALUE && request.request_passed_event != 0);

    SetEvent(request.request_passed_event);
}
#define INPUTBUFFER_SIZE 128
enum InputMessageType
{
    InputMessage_keyboard,
    InputMessage_mousemove,
    InputMessage_mousebutton,

    InputMessage_count
};

struct InputMessageEntry
{
    InputMessageType type;
    union
    {
        struct
        {
            u32 scancode;
            b32 wasUp;
            b32 e0;
        }keyboardmessage;

        struct
        {
            MButton button;
            b32 wasUp;
        }mousebuttonmessage;
        
    };
};

class WASAPIAudioNotificationClient  : IMMNotificationClient
{
    STDMETHOD_(ULONG, AddRef)() override;
    STDMETHOD_(ULONG, Release)() override;
    STDMETHOD(QueryInterface)(REFIID iid, void **object) override;
    STDMETHOD(OnPropertyValueChanged)(LPCWSTR device_id, const PROPERTYKEY key) override;

    STDMETHOD(OnDeviceAdded)(LPCWSTR device_id) override;
    STDMETHOD(OnDeviceRemoved)(LPCWSTR device_id) override;
    STDMETHOD(OnDeviceStateChanged)(LPCWSTR device_id, DWORD new_state) override;
    STDMETHOD(OnDefaultDeviceChanged)(EDataFlow flow, ERole role, LPCWSTR new_default_device_id) override;
};

STDMETHODIMP_(ULONG) WASAPIAudioNotificationClient::AddRef()
{
    return 1;
}
STDMETHODIMP_(ULONG) WASAPIAudioNotificationClient::Release()
{
    return 1;
}
STDMETHODIMP WASAPIAudioNotificationClient::QueryInterface(REFIID iid, void **object)
{
    return S_OK;
}

#define VSYNC_COUNT_SIZE 200
META_EXCLUDE struct
{
    IAudioRenderClient *audio_render_client;    
    IAudioClient *audio_client;
    IMMDeviceEnumerator *device_enumerator;
    IMMDevice *audio_device;

    WASAPIAudioNotificationClient audio_notification_client;

    u32 buffer_frame_count;

    HANDLE stream_switch;
    HANDLE audio_buffer_ready;
    Input input;
    HGLRC opengl_render_context;

    GameMemory game_memory;
    
    InputMessageEntry inputmessageentries[INPUTBUFFER_SIZE];
    u32 inputmessageentry_index;
    u32 inputmessageentry_nexttodo;
    u32 move_timer;
    b32 minimized;
    b32 has_focus;
    
    void *main_fiber;
    void *message_fiber;

    HANDLE vblank;
    HWND window;
    
    HDC dc;

    //TODO remove??
    u32 stable_vsync_frames;
    u64 last_elapsed_count;
    u32 vsync_count_index;
    u64 vsync_perf_counts[VSYNC_COUNT_SIZE];
    
    struct{
        WINDOWPLACEMENT placement;        
        LONG style;
        LONG ex_style;
    }prefullscreen_window;

    u64 perf_freq;
    struct
    {
        u32 numchannels;
        u32 samplerate;
        u32 bitdepth;
    } audio_output_format;
    
    b32 modal_sizemove;
    b32 fullscreen;
	b32 cursor_visible;
	
    u32 preferred_start_width;
    u32 preferred_start_height;
    //b32 audio_should_stop = true;
    HANDLE audio_threadproc;

    u32 fullscreen_width;
    u32 fullscreen_height;
    HMONITOR fullscreen_monitor;
    MONITORINFOEX fullscreen_monitor_info;
} win32;

DWORD WINAPI win32_audio_threadproc(LPVOID lpParameter);

