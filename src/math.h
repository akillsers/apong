union Mat4
{
    f32 val[4][4];
    __m128 column[4];
};

union Vec3
{
    struct
    {
	f32 x;
	f32 y;
	f32 z;
    };
    struct
    {
	f32 r;
	f32 g;
	f32 b;
    };
};

union Vec4
{
    struct
    {
	f32 x;
	f32 y;
	f32 z;
        f32 w;
    };
    struct
    {
	f32 r;
	f32 g;
	f32 b;
        f32 a;
    };
};

union Vec2
{
    struct
    {
	f32 x;
	f32 y;
    };
    struct
    {
	f32 u;
	f32 v;
    };
    struct
    {
	f32 s;
	f32 t;
    };
};

union Vec2i
{
    struct
    {
	i32 x;
	i32 y;
    };
    struct
    {
	i32 u;
	i32 v;
    };
};

union Vec2u
{
    struct
    {
	u32 x;
	u32 y;
    };
    struct
    {
	u32 u;
	u32 v;
    };
};

union Rect
{
    struct
    {
        f32 x;
        f32 y;
        f32 width;
        f32 height;
    };
    struct
    {
        Vec2 pos;
        Vec2 dimensions;
    };
};

union AABB
{
    struct
    {
        f32 x1;
        f32 y1;
        f32 x2;
        f32 y2;
    };
    struct
    {
        Vec2 upperleft;
        Vec2 lowerright;
    };
};

union Circle
{
    struct
    {
        f32 x;
        f32 y;
        f32 r;
    };
    struct
    {
        Vec2 center;
        f32 radius;
    };
};
