//The error value range starts from 0x0500 
//(e.g. GL_INVALID_ENUM is 0x0500, then GL_INVALID_VALUE is 0x501)
char *error_names[] = {"GL_INVALID_ENUM", 
                       "GL_INVALID_VALUE",
                       "GL_INVALID_OPERATION",
                       "GL_STACK_OVERFLOW",
                       "GL_STACK_UNDERFLOW",
                       "GL_OUT_OF_MEMORY",
                       "GL_INVALID_FRAMEBUFFER_OPERATION",
                       "GL_CONTEXT_LOST"};

#define OPENGLERR(error_enum) error_names[error_enum-0x0500] 

GLenum glGetError_errcheck(u32 line, const char *function)
{
    GLenum ret = glGetError();
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetError has returned an error: %s", error_name);
        platform.display_error(buf);
    }
    return ret; 
}
#define glGetError() glGetError_errcheck(__LINE__, __FUNCTION__)
void glReadBuffer_errcheck(GLenum arg0, u32 line, const char *function)
{
    glReadBuffer(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glReadBuffer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glReadBuffer(arg0) glReadBuffer_errcheck(arg0, __LINE__, __FUNCTION__)
void glBlitFramebuffer_errcheck(GLint arg0, GLint arg1, GLint arg2, GLint arg3, GLint arg4, GLint arg5, GLint arg6, GLint arg7, GLbitfield arg8, GLenum arg9, u32 line, const char *function)
{
    glBlitFramebuffer(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBlitFramebuffer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBlitFramebuffer(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) glBlitFramebuffer_errcheck(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, __LINE__, __FUNCTION__)
GLenum glCheckFramebufferStatus_errcheck(GLenum arg0, u32 line, const char *function)
{
    GLenum ret = glCheckFramebufferStatus(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glCheckFramebufferStatus has returned an error: %s", error_name);
        platform.display_error(buf);
    }
    return ret; 
}
#define glCheckFramebufferStatus(arg0) glCheckFramebufferStatus_errcheck(arg0, __LINE__, __FUNCTION__)
GLint glGetUniformLocation_errcheck(GLuint arg0, const GLchar* arg1, u32 line, const char *function)
{
    GLint ret = glGetUniformLocation(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetUniformLocation has returned an error: %s", error_name);
        platform.display_error(buf);
    }
    return ret; 
}
#define glGetUniformLocation(arg0, arg1) glGetUniformLocation_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glFinish_errcheck(u32 line, const char *function)
{
    glFinish();
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glFinish has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glFinish() glFinish_errcheck(__LINE__, __FUNCTION__)
void glFlush_errcheck(u32 line, const char *function)
{
    glFlush();
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glFlush has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glFlush() glFlush_errcheck(__LINE__, __FUNCTION__)
GLuint glCreateShader_errcheck(GLenum arg0, u32 line, const char *function)
{
    GLuint ret = glCreateShader(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glCreateShader has returned an error: %s", error_name);
        platform.display_error(buf);
    }
    return ret; 
}
#define glCreateShader(arg0) glCreateShader_errcheck(arg0, __LINE__, __FUNCTION__)
void glShaderSource_errcheck(GLuint arg0, GLsizei arg1, const GLchar*const* arg2, const GLint* arg3, u32 line, const char *function)
{
    glShaderSource(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glShaderSource has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glShaderSource(arg0, arg1, arg2, arg3) glShaderSource_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glCompileShader_errcheck(GLuint arg0, u32 line, const char *function)
{
    glCompileShader(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glCompileShader has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glCompileShader(arg0) glCompileShader_errcheck(arg0, __LINE__, __FUNCTION__)
void glGetShaderiv_errcheck(GLuint arg0, GLenum arg1, GLint* arg2, u32 line, const char *function)
{
    glGetShaderiv(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetShaderiv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGetShaderiv(arg0, arg1, arg2) glGetShaderiv_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glGetShaderInfoLog_errcheck(GLuint arg0, GLsizei arg1, GLsizei* arg2, GLchar* arg3, u32 line, const char *function)
{
    glGetShaderInfoLog(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetShaderInfoLog has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGetShaderInfoLog(arg0, arg1, arg2, arg3) glGetShaderInfoLog_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
GLuint glCreateProgram_errcheck(u32 line, const char *function)
{
    GLuint ret = glCreateProgram();
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glCreateProgram has returned an error: %s", error_name);
        platform.display_error(buf);
    }
    return ret; 
}
#define glCreateProgram() glCreateProgram_errcheck(__LINE__, __FUNCTION__)
void glAttachShader_errcheck(GLuint arg0, GLuint arg1, u32 line, const char *function)
{
    glAttachShader(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glAttachShader has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glAttachShader(arg0, arg1) glAttachShader_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glLinkProgram_errcheck(GLuint arg0, u32 line, const char *function)
{
    glLinkProgram(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glLinkProgram has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glLinkProgram(arg0) glLinkProgram_errcheck(arg0, __LINE__, __FUNCTION__)
void glGetProgramiv_errcheck(GLuint arg0, GLenum arg1, GLint* arg2, u32 line, const char *function)
{
    glGetProgramiv(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetProgramiv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGetProgramiv(arg0, arg1, arg2) glGetProgramiv_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glGetProgramInfoLog_errcheck(GLuint arg0, GLsizei arg1, GLsizei* arg2, GLchar* arg3, u32 line, const char *function)
{
    glGetProgramInfoLog(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetProgramInfoLog has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGetProgramInfoLog(arg0, arg1, arg2, arg3) glGetProgramInfoLog_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glDeleteShader_errcheck(GLuint arg0, u32 line, const char *function)
{
    glDeleteShader(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glDeleteShader has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glDeleteShader(arg0) glDeleteShader_errcheck(arg0, __LINE__, __FUNCTION__)
void glBindTexture_errcheck(GLenum arg0, GLuint arg1, u32 line, const char *function)
{
    glBindTexture(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBindTexture has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBindTexture(arg0, arg1) glBindTexture_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glTexImage2D_errcheck(GLenum arg0, GLint arg1, GLint arg2, GLsizei arg3, GLsizei arg4, GLint arg5, GLenum arg6, GLenum arg7, const void * arg8, u32 line, const char *function)
{
    glTexImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glTexImage2D has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glTexImage2D(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) glTexImage2D_errcheck(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, __LINE__, __FUNCTION__)
void glGenFramebuffers_errcheck(GLsizei arg0, GLuint* arg1, u32 line, const char *function)
{
    glGenFramebuffers(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGenFramebuffers has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGenFramebuffers(arg0, arg1) glGenFramebuffers_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glBindFramebuffer_errcheck(GLenum arg0, GLuint arg1, u32 line, const char *function)
{
    glBindFramebuffer(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBindFramebuffer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBindFramebuffer(arg0, arg1) glBindFramebuffer_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glGenTextures_errcheck(GLsizei arg0, GLuint* arg1, u32 line, const char *function)
{
    glGenTextures(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGenTextures has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGenTextures(arg0, arg1) glGenTextures_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glTexParameteri_errcheck(GLenum arg0, GLenum arg1, GLint arg2, u32 line, const char *function)
{
    glTexParameteri(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glTexParameteri has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glTexParameteri(arg0, arg1, arg2) glTexParameteri_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glFramebufferTexture2D_errcheck(GLenum arg0, GLenum arg1, GLenum arg2, GLuint arg3, GLint arg4, u32 line, const char *function)
{
    glFramebufferTexture2D(arg0, arg1, arg2, arg3, arg4);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glFramebufferTexture2D has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glFramebufferTexture2D(arg0, arg1, arg2, arg3, arg4) glFramebufferTexture2D_errcheck(arg0, arg1, arg2, arg3, arg4, __LINE__, __FUNCTION__)
void glGenVertexArrays_errcheck(GLsizei arg0, GLuint* arg1, u32 line, const char *function)
{
    glGenVertexArrays(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGenVertexArrays has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGenVertexArrays(arg0, arg1) glGenVertexArrays_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glGenBuffers_errcheck(GLsizei arg0, GLuint* arg1, u32 line, const char *function)
{
    glGenBuffers(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGenBuffers has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGenBuffers(arg0, arg1) glGenBuffers_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glBindVertexArray_errcheck(GLuint arg0, u32 line, const char *function)
{
    glBindVertexArray(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBindVertexArray has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBindVertexArray(arg0) glBindVertexArray_errcheck(arg0, __LINE__, __FUNCTION__)
void glBindBuffer_errcheck(GLenum arg0, GLuint arg1, u32 line, const char *function)
{
    glBindBuffer(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBindBuffer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBindBuffer(arg0, arg1) glBindBuffer_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glBufferData_errcheck(GLenum arg0, GLsizeiptr arg1, const void * arg2, GLenum arg3, u32 line, const char *function)
{
    glBufferData(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBufferData has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBufferData(arg0, arg1, arg2, arg3) glBufferData_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glVertexAttribPointer_errcheck(GLuint arg0, GLint arg1, GLenum arg2, GLboolean arg3, GLsizei arg4, const void * arg5, u32 line, const char *function)
{
    glVertexAttribPointer(arg0, arg1, arg2, arg3, arg4, arg5);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glVertexAttribPointer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glVertexAttribPointer(arg0, arg1, arg2, arg3, arg4, arg5) glVertexAttribPointer_errcheck(arg0, arg1, arg2, arg3, arg4, arg5, __LINE__, __FUNCTION__)
void glEnableVertexAttribArray_errcheck(GLuint arg0, u32 line, const char *function)
{
    glEnableVertexAttribArray(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glEnableVertexAttribArray has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glEnableVertexAttribArray(arg0) glEnableVertexAttribArray_errcheck(arg0, __LINE__, __FUNCTION__)
void glVertexAttribDivisor_errcheck(GLuint arg0, GLuint arg1, u32 line, const char *function)
{
    glVertexAttribDivisor(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glVertexAttribDivisor has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glVertexAttribDivisor(arg0, arg1) glVertexAttribDivisor_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glDisable_errcheck(GLenum arg0, u32 line, const char *function)
{
    glDisable(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glDisable has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glDisable(arg0) glDisable_errcheck(arg0, __LINE__, __FUNCTION__)
void glEnable_errcheck(GLenum arg0, u32 line, const char *function)
{
    glEnable(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glEnable has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glEnable(arg0) glEnable_errcheck(arg0, __LINE__, __FUNCTION__)
void glBlendFunc_errcheck(GLenum arg0, GLenum arg1, u32 line, const char *function)
{
    glBlendFunc(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBlendFunc has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBlendFunc(arg0, arg1) glBlendFunc_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glUseProgram_errcheck(GLuint arg0, u32 line, const char *function)
{
    glUseProgram(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUseProgram has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUseProgram(arg0) glUseProgram_errcheck(arg0, __LINE__, __FUNCTION__)
void glUniformMatrix4fv_errcheck(GLint arg0, GLsizei arg1, GLboolean arg2, const GLfloat* arg3, u32 line, const char *function)
{
    glUniformMatrix4fv(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUniformMatrix4fv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUniformMatrix4fv(arg0, arg1, arg2, arg3) glUniformMatrix4fv_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glUniform4fv_errcheck(GLint arg0, GLsizei arg1, const GLfloat* arg2, u32 line, const char *function)
{
    glUniform4fv(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUniform4fv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUniform4fv(arg0, arg1, arg2) glUniform4fv_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glDrawArrays_errcheck(GLenum arg0, GLint arg1, GLsizei arg2, u32 line, const char *function)
{
    glDrawArrays(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glDrawArrays has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glDrawArrays(arg0, arg1, arg2) glDrawArrays_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glPixelStorei_errcheck(GLenum arg0, GLint arg1, u32 line, const char *function)
{
    glPixelStorei(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glPixelStorei has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glPixelStorei(arg0, arg1) glPixelStorei_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glGetIntegerv_errcheck(GLenum arg0, GLint* arg1, u32 line, const char *function)
{
    glGetIntegerv(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glGetIntegerv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glGetIntegerv(arg0, arg1) glGetIntegerv_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glBufferSubData_errcheck(GLenum arg0, GLintptr arg1, GLsizeiptr arg2, const void * arg3, u32 line, const char *function)
{
    glBufferSubData(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glBufferSubData has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glBufferSubData(arg0, arg1, arg2, arg3) glBufferSubData_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glActiveTexture_errcheck(GLenum arg0, u32 line, const char *function)
{
    glActiveTexture(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glActiveTexture has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glActiveTexture(arg0) glActiveTexture_errcheck(arg0, __LINE__, __FUNCTION__)
void glUniform1i_errcheck(GLint arg0, GLint arg1, u32 line, const char *function)
{
    glUniform1i(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUniform1i has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUniform1i(arg0, arg1) glUniform1i_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
void glDrawArraysInstanced_errcheck(GLenum arg0, GLint arg1, GLsizei arg2, GLsizei arg3, u32 line, const char *function)
{
    glDrawArraysInstanced(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glDrawArraysInstanced has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glDrawArraysInstanced(arg0, arg1, arg2, arg3) glDrawArraysInstanced_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glUniform3fv_errcheck(GLint arg0, GLsizei arg1, const GLfloat* arg2, u32 line, const char *function)
{
    glUniform3fv(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUniform3fv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUniform3fv(arg0, arg1, arg2) glUniform3fv_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glUniform2fv_errcheck(GLint arg0, GLsizei arg1, const GLfloat* arg2, u32 line, const char *function)
{
    glUniform2fv(arg0, arg1, arg2);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glUniform2fv has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glUniform2fv(arg0, arg1, arg2) glUniform2fv_errcheck(arg0, arg1, arg2, __LINE__, __FUNCTION__)
void glViewport_errcheck(GLint arg0, GLint arg1, GLsizei arg2, GLsizei arg3, u32 line, const char *function)
{
    glViewport(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glViewport has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glViewport(arg0, arg1, arg2, arg3) glViewport_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glClearColor_errcheck(GLfloat arg0, GLfloat arg1, GLfloat arg2, GLfloat arg3, u32 line, const char *function)
{
    glClearColor(arg0, arg1, arg2, arg3);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glClearColor has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glClearColor(arg0, arg1, arg2, arg3) glClearColor_errcheck(arg0, arg1, arg2, arg3, __LINE__, __FUNCTION__)
void glClear_errcheck(GLbitfield arg0, u32 line, const char *function)
{
    glClear(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glClear has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glClear(arg0) glClear_errcheck(arg0, __LINE__, __FUNCTION__)
void glReadPixels_errcheck(GLint arg0, GLint arg1, GLsizei arg2, GLsizei arg3, GLenum arg4, GLenum arg5, void * arg6, u32 line, const char *function)
{
    glReadPixels(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glReadPixels has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glReadPixels(arg0, arg1, arg2, arg3, arg4, arg5, arg6) glReadPixels_errcheck(arg0, arg1, arg2, arg3, arg4, arg5, arg6, __LINE__, __FUNCTION__)
void glDeleteTextures_errcheck(GLsizei arg0, const GLuint* arg1, u32 line, const char *function)
{
    glDeleteTextures(arg0, arg1);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glDeleteTextures has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glDeleteTextures(arg0, arg1) glDeleteTextures_errcheck(arg0, arg1, __LINE__, __FUNCTION__)
