#define RENDER_MAX_POST_EFFECTS 2
#define RENDER_LAYER_DEBUGOVERLAY   3.0f
#define RENDER_LAYER_MENU           2.0f
#define RENDER_LAYER_MENUBACK       1.9f
#define RENDER_LAYER_GAME_FRONT     1.2f
#define RENDER_LAYER_GAME           1.0f
#define RENDER_LAYER_BACK           0.0f

#define COLOR_AKRED       rgb(0.75f, 0.1f, 0.1f)
#define COLOR_AKGREEN     rgb(0.1f, 0.9f, 0.1f)
#define COLOR_AKBLUE      rgb(0.1f, 0.1f, 0.9f)
#define COLOR_AKWHITE     rgb(1.0f, 1.0f, 1.0f)
#define COLOR_AKYELLOW    rgb(1.0f, 1.0f, 0.2f)
#define COLOR_AKGOLDEN    rgb(0.972f, 0.714f, 0.322f)
#define COLOR_AKBLACK     rgb(0.0f, 0.0f, 0.0f)
#define COLOR_AKGRAY      rgb(0.5f, 0.5f, 0.5f)
#define COLOR_AKDARKGRAY  rgb(0.3f, 0.3f, 0.3f)
#define COLOR_AKVIOLET    rgb(0.38f, 0.26f, 0.93f)
#define COLOR_AKAQUA      rgb(0.15f, 0.91f, 0.80f)
#define COLOR_AKCYAN      rgb(0.0f, 1.0f, 1.0f)
#define COLOR_AKREALRED   rgb(1.0f, 0.0f, 0.0f)


//The stuff here is probably better to put in the asset.cpp file when we make one
// struct Texture1c
// {
//     u32 OpenGLHandle;
//     u32 width;
//     u32 height;

//     void *red_pixels;

//     //opengl specific::TODO move away????
//     //HOW do we leverage this
//     //should we keep this handle here or what???

//     //if 0 -> not valid texture
 
// };

struct Renderer_Texture3c
{
    u32 width;
    u32 height;

    void *pixels;

    //if 0 -> not valid texture
    u32 GPUHandle;
};

struct Renderer_Texture1c
{
    u32 GPUHandle;
    u32 width;
    u32 height;

    u8 pixels[1];
};

enum RenderEntryType
{
    Render_rectangle,
    Render_texture_masked,
    Render_texture
};


//TODO handle transparency
union Renderer_Mat4
{
    f32 val[4][4];
    __m128 column[4];
};

union Renderer_Vec2
{
    struct
    {
	f32 x;
	f32 y;
    };
    struct
    {
	f32 u;
	f32 v;
    };
};

struct Renderer_RGB
{
    f32 r, g, b;
};

struct Renderer_RGBA
{
    f32 r, g, b, a;
};

Renderer_RGB rgb(f32 r, f32 g, f32 b)
{
    Renderer_RGB ret;
    ret.r = r;
    ret.g = g;
    ret.b = b;
    return ret;
}

struct RenderEntry
{
    RenderEntryType type;
    f32 z;
    f32 x, y, w, h;
    f32 orientation;
    Renderer_RGB color;
    f32 transparency;
        
    f32 u, v, s, t; // Render_texture_masked and Render_texture only
    
    // TODO: We could save space here if we wanted (using just one pointer)
    Renderer_Texture1c *mask; // Render_texture_masked only
    Renderer_Texture3c *tex; // Render_texture only
};

struct RenderTarget
{
    u32 w;
    u32 h;

    b32 should_show_cursor;
};

enum RenderPostEffectType
{
    RenderPostEffect_Blur,
};

struct RenderPostEffect
{
    RenderPostEffectType type;
};

struct RenderList
{
    u32 num_entries;
    u32 max_entries;
    //NOTE:: entries array must be pre allocated
    RenderEntry *entries;
    MemoryArena temp_arena;
    
    RenderPostEffect post_effects[RENDER_MAX_POST_EFFECTS];
    u32 num_post_effects;
};

inline void
render_add_posteffect(RenderList *list, RenderPostEffectType type)
{
    assert(list->num_post_effects < RENDER_MAX_POST_EFFECTS);
    list->post_effects[list->num_post_effects++].type = type;
}
