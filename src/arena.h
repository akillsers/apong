struct MemoryArena
{
    u8 *base;
    u64 used;
    u64 size;
};


#define arena_pushtype(memory_arena, type) (type *)arena_pushsize(memory_arena, sizeof(type)) 
#define arena_pushcount(memory_arena, type, count) (type *)arena_pushsize(memory_arena, sizeof(type) * count) 

internal void *
arena_pushsize(MemoryArena *arena, u64 size)
{
    assert(arena->used + size <= arena->size);
    
    u8 *ret = arena->base + arena->used;
    arena->used += size;
    return ret;
}

internal void
arena_reset(MemoryArena *arena)
{
    arena->used = 0;
    return;
}

internal MemoryArena
arena_suballocate(MemoryArena *srcarena, u64 size)
{
    MemoryArena ret;
    ret.base = (u8 *)arena_pushsize(srcarena, size);
    ret.size = size;
    ret.used = 0;
    return ret;
}

