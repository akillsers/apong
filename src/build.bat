@echo off
IF NOT EXIST ..\build mkdir "..\build"
pushd "..\build"

set CompilerFlags=-nologo -EHa- -Zi -fp:fast -fp:except- -GR- -Gm- -GS- -F4000000
set LinkerFlags=-incremental:no

if [%1]==[release] (
	set CompilerFlags=%CompilerFlags% -DINTERNAL=0 -DSLOW=0 -O2
	echo Building release executable.
) else (
    set CompilerFlags=%CompilerFlags% -DINTERNAL=1 -DSLOW=1 -Od 
	echo Building debug executable.
)

set "registryparser="
if defined registryparser (
    cl %CompilerFlags% -Fo:glregparse -Fe:glregparse "..\src\opengl_registry_parser.cpp" -I "..\headers" /link %LinkerFlags%
)

set "meta="
if defined meta (
  cl %CompilerFlags% -Fe:glmeta "..\src\opengl_meta.cpp" -I "..\headers" /link %LinkerFlags%
  pushd "..\src"
  ..\build\glmeta.exe opengl_error_checking.cpp opengl_shadergenerated.cpp > opengl_functions.cpp 
  popd
) 

set "introspectmeta="
if defined introspectmeta (
   cl %CompilerFlags% -Fe:dumpMeta.exe "..\src\introspect_meta.cpp" -I"..\headers" /link %LinkerFlags%
   pushd "..\src"        
   ..\build\dumpMeta.exe > introspect_gen.cpp
   popd
)

set DefaultLib= ^
    "ucrt.lib" ^
    "vcruntime.lib" 

REM cl -c -Bt+  %CompilerFlags% "..\src\windows.cpp" -I"..\headers" -I"..\headers\fdlibm"
REM link -time+ windows.obj -nodefaultlib -debug -pdb:windows.pdb %DefaultLib% "user32.lib" "kernel32.lib" "opengl32.lib" "Winmm.lib" "gdi32.lib"

REM if RENDERING with D3D or OPENGL you do not need the other lib

REM We could make resource compilation part of this build.bat file!

cl  %CompilerFlags% -Fd:apong.pdb -Fe:apong.exe -DRENDER_OPENGL=1 -DD3D_USE_EX=1 -DRENDER_D3D=0 -DUSE_EXCLUSIVE_FULLSCREEN=1 -DDEBUG_VOLUMES=1  "..\src\windows.cpp" -I"..\data\res file" -I"..\headers" -I"..\headers\fdlibm" /link -debug -pdb:apong.pdb  "user32.lib" "kernel32.lib" "shell32.lib" "opengl32.lib" "Winmm.lib" "gdi32.lib" "ole32.lib" "d3d9.lib" "../data/res file/resource.res"  -incremental:no

copy /b apong.exe+"..\data\apong_assets.apa" apong.exe 1>nul

popd "..\build"
