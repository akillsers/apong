#define MAX_UI_DRAWABLES 200
#define MAX_UI_FOCUSABLES 200
/*
  UIUNITS

  1280 UIW = 1 whole width  of output image buffer
  720  UIH = 1 whole height of output image buffer
  
  x   UIF =  (n ptsize) * output_buffer_width / 1280 
  (ptsize is ascender to descender)
*/

//UIUnits to output buffer pixel coordinates
#define _UIW(uiw, output_buffer_width) (uiw*output_buffer_width/1280) 
#define _UIH(uih, output_buffer_height) (uih*output_buffer_height/720)
#define _UIF(uif, output_buffer_width) (uif*output_buffer_width/1280)


//NOTE:: These macros use ui_state.rtarget as the output_buffer_width and output_buffer_height
#define UIW(uiw) _UIW(uiw, ui_state.rtarget->w)
#define UIH(uih) _UIH(uih, ui_state.rtarget->h)
#define UIF(uif) _UIF(uif, ui_state.rtarget->w)

/*
  params x and y
  width and height are in UIW and UIH units respectively
*/

#define ui_focusup_key (KEY_upArrow)
#define ui_focusdown_key (KEY_downArrow)
#define ui_focusnext_key (KEY_tab)
#define ui_activate_mbutton (MButton_left)

//TODO make this cleaner
#define ui_activate_key (KEY_enter)
#define ui_activate_key2 (KEY_space)

#define ui_reset_focus ui_state.focus_group->focus_index = 0

#define GenUIID __LINE__ //ID SHOULD NEVER BE 0!!!!
#define GenUIIDa(append) __LINE__##append

typedef u32 UIID;

struct UIFocusableItem
{
    UIID id;
    f32 x; //TODO why do we have x, y, w, h as members of UIFocusableItem??
    f32 y;
    f32 w;
    f32 h;
};

struct UIFocusGroup
{
    u32 focus_index;
    u32 active_id;

    //NOTE TODO this is hackery
    //b32 focus_is_active;
};

enum UIDrawType
{
    text,
    spacey_button,
    traditional_button,
    rectangle,
    image,
    
    count
};

struct UIDrawableItem
{
    UIDrawType type;
    UIID id;
    union
    {
        struct
        {
            f32 x, y, width, height;
            f32 u, v, s, t;
            Renderer_RGB color;
            Asset *asset;
        }image;
        struct //text
        {
            f32 x, w, baseliney;
            Renderer_RGB color;
            f32 transparency;
            char *str;
            f32 fontsize;
            Font *font;
            u32 alignment;
        }text;
        struct //rectangle
        {
            f32 x, y, w, h;
            f32 orientation;
            Renderer_RGB color;
            f32 transparency;
            f32 zorder;
        }rectangle;

        struct //spacey_button
        {
            f32 x, y, width, height;
            Renderer_RGB color;
            Renderer_RGB selected_color;
            char *str;
            f32 fontsize;
            Font *font;
        }spacey_button;

        struct //traditional_button
        {
            f32 x, y, width, height;
            char *str;
            f32 fontsize;
            Font *font;
        }traditional_button;
        
    };
};

enum class ActivationMethod
{
    no_activate,
    mouse,
    keyboard,
};

enum class FocusMethod
{
    no_focus,
    mouse,
    keyboard,
};


struct
{
    //these are init by the game (apong.cpp)
    RenderList *rlist;
    RenderTarget *rtarget;

    //NOTE:: buffer our draw calls for a few reasons:
    //a) the mouse may move the focus down so something that was drawn previously with the focus
    //   no longer has the focus this frame (we need to change the render entry so that it is drawn with no
    //   focus)
    //b) we may click something this frame that goes outside the menu (e.g. another menu), in this frame,
    //   the contents of the current menu must be discarded

    //   another solution is to buffer this into ui renderable things
    //   this may make it easier to say change a button to its unfocused state easily
    UIFocusableItem *focusables;
    u32 num_focusables;

    b32 cycling;
    b32 no_mouse_focusing;
    
    f32 arrow_y;

    UIFocusGroup *focus_group;
    Assets *assets;

    //TODO don't allocate this on the heap please
    UIDrawableItem *drawables;
    u32 num_drawables;
#define TEXT_BUFFER_SIZE kilobytes(12)
    char *text_buffer;
    u32 text_buffer_used;
    Mixer *mixer;

    Key activate_release_key;

    ActivationMethod activate_method;
    FocusMethod focus_method;

    b32 already_keyboard_focused;
}ui_state;

internal void
ui_use_focus_group(UIFocusGroup *group)
{
    ui_state.focus_group = group;
}


internal void
ui_allocate_storage(MemoryArena *alloc)
{
    ui_state.focusables = arena_pushcount(alloc, UIFocusableItem, MAX_UI_FOCUSABLES);
    //handle input
    ui_state.drawables = arena_pushcount(alloc, UIDrawableItem, MAX_UI_DRAWABLES);
    ui_state.text_buffer = arena_pushcount(alloc, char, TEXT_BUFFER_SIZE);
    ui_state.already_keyboard_focused = false;
}

internal void
ui_reset()
{
    ui_state.num_focusables = 0;
    ui_state.num_drawables = 0;
    ui_state.text_buffer_used = 0;
    ui_state.activate_method = ActivationMethod::no_activate;
    ui_state.focus_method = FocusMethod::no_focus;
}

//TODO is it more efficient to pass by value or reference??
internal void
ui_push_drawable_item(UIDrawableItem *item)
{
    assert(ui_state.drawables && "please allocate ui storage first")
    ui_state.drawables[ui_state.num_drawables] = item[0];
    ui_state.num_drawables++;
}


internal void
ui_draw_items()
{
    for(int i = 0; i < ui_state.num_drawables; i++)
    {
        UIDrawableItem *draw_item = &ui_state.drawables[i];

        switch(draw_item->type)
        {
            case UIDrawType::rectangle:
            {
                push_rectangle(ui_state.rlist, draw_item->rectangle.zorder,
                               draw_item->rectangle.x, draw_item->rectangle.y,
                               draw_item->rectangle.w, draw_item->rectangle.h,
                               draw_item->rectangle.orientation,
                               draw_item->rectangle.color, draw_item->rectangle.transparency);

            }break;
            case UIDrawType::text:
            {
                push_string(ui_state.rlist, RENDER_LAYER_MENU,
                            draw_item->text.x,
                            draw_item->text.baseliney, draw_item->text.w,
                            draw_item->text.fontsize,
                            draw_item->text.font,
                            draw_item->text.str,
                            draw_item->text.alignment,
                            draw_item->text.color,
                            draw_item->text.transparency);

            }break;
            case UIDrawType::spacey_button:
            {
                b32 focus = ui_state.focusables[ui_state.focus_group->focus_index].id == draw_item->id;
                b32 active = ui_state.focus_group->active_id == draw_item->id;
                
                push_string(ui_state.rlist, RENDER_LAYER_MENU,
                            draw_item->spacey_button.x,
                            draw_item->spacey_button.y + draw_item->spacey_button.height/2,
                            draw_item->spacey_button.fontsize,
                            draw_item->spacey_button.font,
                            draw_item->spacey_button.str,
                            (u32)(ALIGN_HORIZONTAL_LEFT | ALIGN_VERTICAL_CENTER),
                            active ?
                            draw_item->spacey_button.selected_color :
                            draw_item->spacey_button.color);
                
            }break;
            case UIDrawType::image:
            {
                push_texture_masked(ui_state.rlist, RENDER_LAYER_MENU,
                                    draw_item->image.x, draw_item->image.y,
                                    draw_item->image.width, draw_item->image.height,
                                    draw_item->image.u, draw_item->image.v, draw_item->image.s, draw_item->image.t,
                                    draw_item->image.color, draw_item->image.asset);
            }break;
            case UIDrawType::traditional_button:
            {
                b32 focus = ui_state.focusables[ui_state.focus_group->focus_index].id == draw_item->id;
                b32 active = ui_state.focus_group->active_id == draw_item->id;

                f32 back_transparency;
                Renderer_RGB back_color;
                Renderer_RGB text_color;
                
                if (focus)
                {
                    if(active && !ui_state.cycling)
                        //focus and active
                        goto active;

                    back_color = COLOR_AKWHITE;
                    text_color = COLOR_AKBLACK;
                    back_transparency = 1.0f;
                }
                else if(active)
                {
                active:
                    back_color = COLOR_AKGRAY;
                    text_color = COLOR_AKBLACK;
                    back_transparency = 1.0f;

                }
                else
                {
                    back_color = COLOR_AKBLACK;
                    text_color = COLOR_AKWHITE;
                    back_transparency = 0.0f;
                }
                
                push_rectangle(ui_state.rlist, RENDER_LAYER_MENU,
                               draw_item->traditional_button.x,
                               draw_item->traditional_button.y,
                               draw_item->traditional_button.width,
                               draw_item->traditional_button.height,
                               0.0f, //orientation
                               back_color,
                               back_transparency);
                push_string(ui_state.rlist, RENDER_LAYER_MENU,
                            draw_item->traditional_button.x,
                            draw_item->traditional_button.y + draw_item->traditional_button.height/2,
                            draw_item->traditional_button.width,
                            draw_item->traditional_button.fontsize,
                            draw_item->traditional_button.font,
                            draw_item->traditional_button.str,
                            ALIGN_HORIZONTAL_CENTER | ALIGN_VERTICAL_CENTER_BYFULL,
                            text_color);
            }break;
            default:
                stop("invalid default case");
        }
    }
}

internal void
ui_focus_arrow(f32 x, Renderer_RGB color)
{
    x = UIW(x);
    Asset *asset = get_asset_by_name(ui_state.assets, "focus_arw", tex1c);
    f32 startx = x - UIW(asset->tex1c->width);
    f32 scale = (x - startx)/asset->tex1c->width;
    
    f32 targety = ui_state.focusables[ui_state.focus_group->focus_index].y +
        ui_state.focusables[ui_state.focus_group->focus_index].h/2 -
        asset->tex1c->height/2;
    f32 t = 0.3f;

    if(ui_state.arrow_y == 0)
    {
        //init the arrow y
        //TODO should we put this here???
        ui_state.arrow_y = targety;
    }
    else
    {
        ui_state.arrow_y = ui_state.arrow_y + t*(targety - ui_state.arrow_y);        
    }
    push_texture_masked(ui_state.rlist, RENDER_LAYER_MENU,
                        startx, UIH(ui_state.arrow_y), scale, color, asset);
}

internal void
ui_title(UIID id,
         f32 x, f32 w, f32 baseliney, const char *str, f32 fontsize, Font *font, Renderer_RGB color)
{
    x = UIW(x);
    w = UIW(w);
    baseliney = UIH(baseliney);
    fontsize = UIF(fontsize);

    char *stored_str = ui_state.text_buffer + ui_state.text_buffer_used;

    char *at = stored_str;
    while(str[0])
    {
        *at++ = *str++;
    }
    *at++ = 0;
    ui_state.text_buffer_used += at - stored_str;
    assert(ui_state.text_buffer_used < TEXT_BUFFER_SIZE);

    UIDrawableItem item = {};
    item.type = UIDrawType::text;
    item.id = id;
    item.text.x = 0;
    item.text.w = w;
    item.text.baseliney = baseliney;
    item.text.str = stored_str;
    item.text.fontsize = fontsize;
    item.text.font = font;
    item.text.color = color;
    item.text.transparency = 1.0f;
    item.text.alignment = ALIGN_HORIZONTAL_CENTER;
    
    ui_push_drawable_item(&item);
}

internal void
ui_rectangle(UIID id, f32 zlayer, f32 x, f32 y, f32 w, f32 h, f32 orientation, Renderer_RGB color, f32 transparency)
{
    UIDrawableItem item = {};
    item.type = UIDrawType::rectangle;
    item.id = id;
    item.rectangle.x = x;
    item.rectangle.y = y;
    item.rectangle.w = w;
    item.rectangle.h = h;
    item.rectangle.orientation = orientation;
    item.rectangle.color = color;
    item.rectangle.transparency = transparency;
    item.rectangle.zorder = zlayer;
    
    ui_push_drawable_item(&item);
}




internal void
ui_text(UIID id,
        f32 x, f32 w, f32 baseliney, char *str, f32 fontsize, Font *font, Renderer_RGBA color,
        u32 alignment = ALIGN_HORIZONTAL_CENTER)
{
    x = UIW(x);
    w = UIW(w);
    baseliney = UIH(baseliney);
    fontsize = UIF(fontsize);

    char *stored_str = ui_state.text_buffer + ui_state.text_buffer_used;

    char *at = stored_str;
    while(str[0])
    {
        *at++ = *str++;
    }
    *at++ = 0;
    ui_state.text_buffer_used += at - stored_str;
    assert(ui_state.text_buffer_used < TEXT_BUFFER_SIZE);
    
    UIDrawableItem item = {};
    item.type = UIDrawType::text;
    item.id = id;
    item.text.x = x;
    item.text.w = w;
    item.text.baseliney = baseliney;
    item.text.str = stored_str;
    item.text.fontsize = fontsize;
    item.text.font = font;
    item.text.color = {color.r, color.g, color.b};
    item.text.transparency = color.a;
    item.text.alignment = alignment;

    ui_push_drawable_item(&item);
}

inline void
ui_text(UIID id,
        f32 x, f32 baseliney, char *str, f32 fontsize, Font *font, Renderer_RGB color,
        u32 alignment = ALIGN_HORIZONTAL_CENTER_TO_LEFT)
{
    f32 w = 1280; //whole screen
    ui_text(id,
            x, w, baseliney, str, fontsize, font, {color.r, color.g, color.b, 1.0f},
            alignment);
}

inline b32
ui_mouse_in_bounds(Input *input, f32 x, f32 y, f32 width, f32 height)
{
    Vec2 mousepos = v2(input->mousex, input->mousey);
    if(mousepos.x >= x && mousepos.x <= x + width &&
       mousepos.y >= y && mousepos.y <= y + height)
        return true;
    return false;
}

//TODO is there anyway to calculate max focus automagically probs use metaprogramming
internal void
ui_focus_with_keyboard(Input *input, u32 minfocus, u32 maxfocus)
{
    //TODO slight hackery to make sure we only focus with the keyboard once for when we call goto restart_menu
    //this boolean value gets reset at ui_allocate_render_storage
    if(ui_state.already_keyboard_focused)
        return;

    if(input->key[ui_focusup_key].down && input->has_focus)
    {
        u32 repeat_count = input->key[ui_focusup_key].down_repeats;
        if(ui_state.focus_group->focus_index >= minfocus + repeat_count)
        {
            ui_state.focus_group->focus_index -= repeat_count;
        }
        else
        {
            ui_state.focus_group->focus_index = minfocus;
        }
        ui_state.cycling = true;
    }
    else if (input->key[ui_focusdown_key].down && input->has_focus)
    {
        u32 repeat_count = input->key[ui_focusdown_key].down_repeats;
        if(ui_state.focus_group->focus_index + repeat_count <= maxfocus)
        {
            ui_state.focus_group->focus_index += repeat_count;
        }
        else
        {
            ui_state.focus_group->focus_index = maxfocus;
        }

        ui_state.cycling = true;
    }
    else if (input->key[ui_focusnext_key].down && input->has_focus)
    {
        ui_state.focus_group->focus_index = (ui_state.focus_group->focus_index +
                                             input->key[ui_focusnext_key].down_repeats) % ((maxfocus-minfocus)+1);
        ui_state.cycling = true;
    }

    ui_state.already_keyboard_focused = true;
}

internal b32
ui_button_traditional(UIID id, Input *input,
                      f32 x, f32 y, f32 width, f32 height,
                      Renderer_RGB color, char *str, f32 fontsize, Font *font)
{
    x = UIW(x);
    y = UIH(y);
    width = UIW(width);
    height = UIH(height);
    fontsize = UIF(fontsize);
    
    assert(ui_state.rlist);
    ui_state.focusables[ui_state.num_focusables].id = id;
    ui_state.focusables[ui_state.num_focusables].x = x;
    ui_state.focusables[ui_state.num_focusables].y = y;
    
    b32 focus = ui_state.focusables[ui_state.focus_group->focus_index].id == id;
    b32 active = ui_state.focus_group->active_id == id;
    b32 ret = false;
    b32 mouse_in_bounds = ui_mouse_in_bounds(input, x, y, width, height);
    f32 back_transparency;

    Renderer_RGB back_color;
    Renderer_RGB text_color;

    if(mouse_in_bounds)
    {
        if(input->key_pressed(ui_activate_key) || input->key_pressed(ui_activate_key2) ||
           input->button_pressed(ui_activate_mbutton))
        {
            active = true;
            ui_state.focus_group->active_id = id;
            ui_state.cycling = false;
        }
            
        if(input->mousemoved && ui_state.focus_group->active_id == 0 && !focus)
        {
            ui_state.focus_group->focus_index = ui_state.num_focusables;
            focus = true;
        }
        
    }
    if(active && input->button_released(ui_activate_mbutton))
    {
        //TODO move
        ui_state.focus_group->active_id = 0;
        ret = mouse_in_bounds && input->has_focus;

        active = false;
        
        if(!focus)
        {
            focus = true;
            ui_state.focus_group->focus_index = ui_state.num_focusables;
        }

    }
          
    if(focus)
    {
        if(input->key_pressed(ui_activate_key) || input->key_pressed(ui_activate_key2))
        {
            ui_state.focus_group->active_id = 0;
            active = false;
            ret = true;
            input->sticky_press = input->key_pressed(ui_activate_key) ? ui_activate_key : ui_activate_key2;
        }

        if(active && !ui_state.cycling)
        {
            goto active;
        }   
        else
        {
            back_color = COLOR_AKWHITE;
            text_color = COLOR_AKBLACK;
            back_transparency = 1.0f;
        }


    }
    else if (active)
    {
    active:
        back_color = COLOR_AKGRAY;
        text_color = COLOR_AKBLACK;
        back_transparency = 1.0f;
    }
    else
    {
        back_color = COLOR_AKBLACK;
        text_color = COLOR_AKWHITE;
        back_transparency = 0.0f;
    }
    
    // push_rectangle(ui_state.rlist, RENDER_LAYER_MENU,
    //                x, y, width, height, 0.0f, back_color, back_transparency);
    // push_string(ui_state.rlist, RENDER_LAYER_MENU,
    //             x+width/2, y+height/2, fontsize, font, str, ALIGN_HORIZONTAL_CENTER | ALIGN_VERTICAL_CENTER_BYFULL,
    //             text_color);

    UIDrawableItem item = {};
    item.type = UIDrawType::traditional_button;
    item.id = id;
    item.traditional_button.x = x;
    item.traditional_button.y = y;
    item.traditional_button.width = width;
    item.traditional_button.height = height;
    item.traditional_button.str = str;
    item.traditional_button.fontsize = fontsize;
    item.traditional_button.font = font;
    
    ui_push_drawable_item(&item);
    ui_state.num_focusables++;
    return ret;
}

internal void
ui_reset_sticky_active(UIFocusGroup *group)
{
    group->active_id = 0;
}

internal b32
ui_button_spacey(UIID id, Input *input,
                 f32 x, f32 y, f32 width, f32 height,
                 Renderer_RGB color, Renderer_RGB selected_color, char *str,
                 f32 fontsize, Font *font, b32 sticky_active = true)
{
    ui_state.focusables[ui_state.num_focusables].id = id;
    ui_state.focusables[ui_state.num_focusables].x = x;

    ui_state.focusables[ui_state.num_focusables].y = y;
    ui_state.focusables[ui_state.num_focusables].w = width;
    ui_state.focusables[ui_state.num_focusables].h = height;

    x = UIW(x);
    y = UIH(y);
    height = UIH(height);
    fontsize = UIF(fontsize);
    width = get_string_render_width(str, font, fontsize / font->descriptor->fontsize);    

    b32 ret = false;
    
    b32 focus = ui_state.focusables[ui_state.focus_group->focus_index].id == id ? true : false;
    b32 active = ui_state.focus_group->active_id == id;
    b32 mouse_in_bounds = ui_mouse_in_bounds(input, x, y, width, height);

    assert(ui_state.rlist);


    //DEBUG::
    // push_rectangle(ui_state.rlist, RENDER_LAYER_MENU,
    //                x, y, width, height, 0.0f, COLOR_AKBLUE);
    
    if(mouse_in_bounds)
    {
        if(input->button_pressed(ui_activate_mbutton))
        {
            active = true;
            ui_state.focus_group->active_id = id;

            ui_state.cycling = false;
        }

        if(input->mousemoved && ui_state.focus_group->active_id == 0 && !ui_state.no_mouse_focusing)
        {
            ui_state.focus_group->focus_index = ui_state.num_focusables;
            focus = true;
            ui_state.focus_method = FocusMethod::mouse;
        }
    }
    
    if(active)
    {
        if(input->key_released(ui_state.activate_release_key) && !sticky_active)
        {
            ui_state.activate_release_key = KEY_invalid;
            active = false;
            ui_state.focus_group->active_id = 0;
        }

        if(input->button_released(ui_activate_mbutton))
        {
            ret = mouse_in_bounds && input->has_focus;
            if(ret)
            {
                ui_state.activate_method = ActivationMethod::mouse;
            }
            if(!ret || !sticky_active)
            {
                ui_state.focus_group->active_id = 0;
                active = false;
            }
            if(!focus)
            {
                focus = true;
                ui_state.focus_group->focus_index = ui_state.num_focusables;
            }
        }
    }

    if(focus)
    {
        if(input->key_pressed(ui_activate_key) || input->key_pressed(ui_activate_key2))
        {
            ui_state.activate_method = ActivationMethod::keyboard;
            ui_state.focus_group->active_id = id;
            active = true;
            ui_state.cycling = false;
            ret = true;
            ui_state.activate_release_key = input->key_pressed(ui_activate_key) ? ui_activate_key : ui_activate_key2;
        }
    }
    ui_state.num_focusables++;
draw:
    UIDrawableItem item = {};
    item.type = UIDrawType::spacey_button;
    item.id = id;
    item.spacey_button.x = x;
    item.spacey_button.y = y;
    item.spacey_button.selected_color = selected_color;
    item.spacey_button.color = color;
    item.spacey_button.width = width;
    item.spacey_button.height = height;
    item.spacey_button.str = str;
    item.spacey_button.fontsize = fontsize;
    item.spacey_button.font = font;

    ui_push_drawable_item(&item);

    return ret;
}

internal void
ui_texture_masked(UIID id, f32 x, f32 y, f32 width, f32 height, Asset *asset, Renderer_RGB color)
{
    x = UIW(x);
    y = UIH(y);
    width = UIW(width);
    height = UIW(height);
    
    UIDrawableItem item = {};
    item.type = UIDrawType::image;
    item.id = id;
    item.image.x = x;
    item.image.y = y;
    item.image.width = width;
    item.image.height = height;
    item.image.u = 0;
    item.image.v = 0;
    item.image.s = 1;
    item.image.t = 1;
    item.image.color = color;
    item.image.asset = asset;
    ui_push_drawable_item(&item);
}
internal b32
ui_button_spacey(UIID id, Input *input,
                 f32 x, f32 y, f32 width, f32 height,
                 Renderer_RGB color, char *str, f32 fontsize, Font *font)
{

    return ui_button_spacey(id, input,
                            x, y, width, height,
                            color, color, str, fontsize, font);

}
