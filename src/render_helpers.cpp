internal void
push_texture_masked(RenderList *rlist, f32 z,
                    f32 x, f32 y, f32 width, f32 height,
                    f32 u, f32 v, f32 s, f32 t,
                    Renderer_RGB color, Asset *asset)
{
    Renderer_RGB rgb;
    rgb.r = color.r;
    rgb.g = color.g;
    rgb.b = color.b;
    push_texture_masked(rlist, z,
                        x, y, width, height,
                        u, v, s, t,
                        rgb, (Renderer_Texture1c *)asset->tex1c);
    
}


internal void
push_texture_masked(RenderList *rlist, f32 z,
                    f32 x, f32 y, f32 scale,
                    Renderer_RGB color, Asset *mask)
{
    push_texture_masked(rlist, z,
                        x, y, mask->tex1c->width * scale, mask->tex1c->height * scale,
                        0, 0, 1, 1,
                        color, mask);
}


internal void
push_stringline(RenderList *rlist, f32 z,
                f32 x, f32 baseline_y, f32 scale, Font *fnt, char *str, Renderer_RGB color, f32 transparency)
{
    f32 currx = 0;
    Renderer_RGB original_color = color;
    for(;;)
    {
        TIMED_BLOCK;
        if(!*str)
            break;
        //TODO maybe draw a character not supported string??
        if(!(*str >= fnt->descriptor->first_unicode_codepoint &&
             *str <= fnt->descriptor->first_unicode_codepoint + fnt->descriptor->numglyphs))
        {
            str++;
            continue;
        }
        if(str[0] == '/' &&
           str[1] == '/')
        {
            if(str[2] == '#' &&
               str[3] &&
               str[4] &&
               str[5])
            {
                if(str[3] == 'p' && str[4] == 'o' && str[5] == 'p')
                {
                    color = original_color;
                }
                else
                {
                    color.r = (f32)(str[3] - 0x30) / 9;
                    color.g = (f32)(str[4] - 0x30) / 9;
                    color.b = (f32)(str[5] - 0x30) / 9; 
                }
                str += 6;
            }
            // else if (str[2] == '^' &&
            //          str[3] &&
            //          str[4])
            // {
            //     f32 size = (f32)(str[3] - 0x30) * 10 + (f32)(str[4] - 0x30);
            //     scale = size / fnt->fontsize;
            //     str += 5;
            // }
        }

        FontGlyph *glyph = &fnt->descriptor->glyphs[*str - fnt->descriptor->first_unicode_codepoint];
        if(*str != ' ')
        {
            
            
            push_texture_masked(rlist, z,
                                x + currx + glyph->xoff*scale, baseline_y + glyph->yoff*scale,
                                glyph->w*scale, glyph->h*scale,
                                (f32)glyph->u, (f32)glyph->v,
                                (f32)glyph->s, (f32)glyph->t,
                                ((Renderer_RGB*)&color)[0], (Renderer_Texture1c *)fnt->tex1c, transparency);
            

        }
        currx += glyph->xadvance*scale;
        str++;
    }
}

internal f32
get_string_render_width(char *str, Font *fnt, f32 scale)
{
    TIMED_BLOCK;
    f32 ret = 0;

    while(*str)
    {
        //TODO maybe draw a character not supported string??
        if(!(*str >= fnt->descriptor->first_unicode_codepoint &&
             *str <= fnt->descriptor->first_unicode_codepoint + fnt->descriptor->numglyphs))
        {
            str++;
            continue;
        }
        if(str[0] == '/' && str[1] == '/' && str[2] == '#' &&
           str[3] &&
           str[4] &&
           str[5])
        {
            str += 6;
            continue;
        }

        FontGlyph *glyph = &fnt->descriptor->glyphs[*str - fnt->descriptor->first_unicode_codepoint];
        f32 advance = (glyph->xadvance * scale);
        ret += advance;
        str++;
    }

    return ret;
}

//TODO proper definition for INFINITE
//INSTEAD DO AKWHOLESCREEN
#define AKINFINITE 99999
//TODO make push string take a textbox style kindof thing (with width and word wrapping)
//TODO make the "textbox" take a height so that we can base our vertical centering by that instead of by y
internal void
push_string(RenderList *rlist, f32 z,
            f32 x, f32 y, f32 w, f32 size, Font *fnt, char *str, u32 alignment, Renderer_RGB color = COLOR_AKWHITE,
            f32 transparency = 1.0f)
{
    TIMED_BLOCK;

    f32 scale = size / fnt->descriptor->fontsize;

    
    f32 baseline_y;
    f32 startx;

    if(alignment & ALIGN_HORIZONTAL_CENTER)
    {
        f32 string_w = get_string_render_width(str, fnt, scale);
        // // if(x == AKINFINITE)
        //     startx = x - string_w / 2;
        // // else
        startx = (x + w/2) - string_w / 2;
    }
    else if (alignment & ALIGN_HORIZONTAL_CENTER_TO_LEFT)
    {
        f32 string_w = get_string_render_width(str, fnt, scale);
        startx = x - string_w/2;
    }
    else if (alignment & ALIGN_HORIZONTAL_LEFT)
    {
        startx = x;
    }
    else if (alignment & ALIGN_HORIZONTAL_RIGHT)
    {
        f32 string_w = get_string_render_width(str, fnt, scale);
        startx = x - string_w;
    }

    if(alignment & ALIGN_VERTICAL_CENTER)
    {
#if 1
        baseline_y = y + (fnt->descriptor->ascent*scale)/2;
#elif 0
        f32 descent = size - fnt->descriptor->ascent*scale; 
        baseline_y = y + descent;
#else
        f32 descent = size - fnt->descriptor->ascent*scale; 
        baseline_y = y - descent + size/2;
#endif
    }
    else if (alignment & ALIGN_VERTICAL_CENTER_BYFULL)
    {
        f32 descent = size - fnt->descriptor->ascent*scale; 
        baseline_y = y - descent + size/2;
        
    }
    else
    {
        baseline_y = y;
    }

    push_stringline(rlist, z,
                    startx, baseline_y, scale, fnt, str, color, transparency);
}



internal void
push_string(RenderList *rlist, f32 z,
            f32 x, f32 y, f32 size, Font *fnt, char *str, u32 alignment, Renderer_RGB color = COLOR_AKWHITE)
{
    push_string(rlist, z,
                x, y, AKINFINITE, size, fnt, str, alignment, color);
    
}
