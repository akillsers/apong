internal u32
days_in_month(u32 month, b32 leapyear)
{
    if(month % 2)
    {
        //month is odd (for example jan)
        if(month <= 7) //month is july or earlier
        {
            return 31;
        }
        else
        {
            return 30;
        }
    }
    else
    {
        //month is even
        if(month == 2)
        {
            //for feburary
            if(leapyear)
                return 29;
            else
                return 28;
        }
        if(month <= 6)//june or earlier
        {
            return 30;
        }
        else
        {
            return 31;
        }
    }
}

internal u32
complete_month_to_days(u32 month, b32 leapyear)
{
    i32 index = month;
    u32 total = 0;
    for(int i = 0; i < month; i++)
    {
        total += days_in_month(index, leapyear);
        index--;
    }
    return total;
}

internal u64
ComputeSecondsSince2000(PlatformTime *time)
{
    u64 ret = 0;
    //seconds in a year (365 days): 31,536,000
    //seconds in a year (366 days): 31,622,400 (leap year)
    u32 total_years = time->year - 2000;
    u32 num_leap_years = (total_years / 4) + 1;
    u32 num_non_leap_years = total_years - num_leap_years;

    ret += num_non_leap_years * 31536000;
    ret += num_leap_years * 31622400; 
    
    u32 days_since_start_of_year = 0;
    b32 leapyear = false;
    if(time->year % 4 == 0)
        leapyear = true;
    days_since_start_of_year = complete_month_to_days(time->month-1, leapyear) + (time->day - 1);
    ret += days_since_start_of_year * 86400;
    
    ret += time->hour * 3600;
    ret += time->minute * 60;
    ret += time->second;
    return ret;
}

internal u64
GetSecondsSince2000()
{
    PlatformTime time;
    platform.get_time(&time);

    return ComputeSecondsSince2000(&time);
}

internal u32
days_to_complete_years_from2000(u32 *days)
{
    //we know how many days will be in a 4 year period from 2000 (1461)
#define DAYS_IN_4_YEARS 1461
    u32 ret = 0;
    u32 num_complete_4_year_periods = days[0] / DAYS_IN_4_YEARS;
    ret += num_complete_4_year_periods * 4;

    days[0] -= DAYS_IN_4_YEARS * num_complete_4_year_periods;

    //NOTE:: not greater or equal to 365 days because the first year is a leap year
    if(days[0] >= 366)  
    {
        //more than one complete year has passed
        //account for the first leap year so we divide accurately
        u32 num_complete_years = ((days[0]-1) / 365);
        ret += num_complete_years;

        days[0] -= 366;
        if(num_complete_years > 1)
            days[0] -= 365 * (num_complete_years-1);
    }
    
    return ret;

}

internal u32
days_to_month(u32 *days, b32 leapyear)
{
    //TODO this is a pretty slow way of doing this
    u32 days_in_month[] =
        {31, leapyear ? (u32)29 : (u32)28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    u32 mindex = 0;
check_next_month:
    if(days[0] < days_in_month[mindex])
        return mindex + 1;
    else
        days[0] -= days_in_month[mindex];
    mindex++;
    if(mindex < 12)
        goto check_next_month;
    return false;
}

internal void
SecondsFrom2000ToDateTime(u64 seconds, u32 *outsecond,
                          u32 *outminute, u32 *outhour,
                          u32 *outdays, u32 *outmonth, u32 *outyear)
{
    //this truncates to the full days since 2000
    u32 days_from_2000 = seconds / 86400;


    u32 days_from_start_of_year = days_from_2000;
    u32 years_from2000 = days_to_complete_years_from2000(&days_from_start_of_year); 

    u32 days_from_start_of_month = days_from_start_of_year;
    b32 leapyear = (years_from2000 % 4 == 0);
    u32 months = days_to_month(&days_from_start_of_month, leapyear);

    *outdays = days_from_start_of_month + 1; //1 is the first day but its 0
    *outmonth = months;
    *outyear = years_from2000;

    u32 minutes_from_2000 = seconds / 60;
    u32 hours_from_2000 = minutes_from_2000 / 60;

    //TODO OPTIMIZE THIS FUNCTION ESPECIALLY THIS PART
    *outhour = hours_from_2000 - (days_from_2000 * 24);    
    *outminute = minutes_from_2000 - (days_from_2000*1440 + outhour[0]*60);
    *outsecond = seconds - (minutes_from_2000 * 60);
}
