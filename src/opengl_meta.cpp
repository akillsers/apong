#include <aks.h>

#include "stringutils.cpp"
#include "stringout.cpp"
#include "ctokenizer.cpp"

#include "fileio.cpp"
//@header

struct GLFunction
{
    char *name;
    u32 namelen;

    u32 num_args;
};

global_variable void *function_info;
global_variable u32 num_functions_inreg;

#define MAX_SHADERS 40
#define MAX_UNIORMS_PER_SHADER 50
struct Shader
{
    Text name;

    Text vertex_source;
    Text fragment_source;

    u32 num_uniforms;
    Text uniform[MAX_UNIORMS_PER_SHADER];
};

//TODO is there a performance increase from initializing to null
//     by putting the variable here vs using = {}??

//NOTE:: the parser expects these guys to be initialized to ZERO!!!
global_variable Shader shaders[MAX_SHADERS];
global_variable u32 num_shaders;    


//@implementation

//MEGA NOTE AND WARNING!! this assumes the strings being compared are in the same case
internal b32
is_glfunc_alphabetically_before(char *string, u32 stringlen, char *comparestring)
{
    u32 letterindex = 0;
    while(letterindex < stringlen && comparestring[letterindex])
    {
        //both string have not reached the end
        if(string[letterindex] < comparestring[letterindex])
        {
            //string is before comparestring
            return true;
        }
        else if(string[letterindex] > comparestring[letterindex])
        {
            //string is after comparestring
            return false;
        }
        else
        {
            //the letters are equal:: GOTO THE NEXT ONE!!
            letterindex++;
        }
    }

    if(letterindex < stringlen)
    {
        //only the compare string reached its end
        return false; 
    }
    else if (comparestring[letterindex])
    {
        //only the first string reached its end 
        return true;
    }        
    else
    {
        //otherwise...if both string reached their end
        // then the strings are equal so logically return false
        return false;
        
    }
}

//WARNING NOTE TODO::WE DON"T ACTUALLY DO ANY BUFFER SIZE CHECKING!!
internal void
gl_function_tostr(char* buf, int bufsize, GLFunction gl_func)
{

    gl_func.name +=2;
    gl_func.namelen -=2;
    buf  = string_cat(buf, "opengl_function(");

    char uppercase_function[256];
    to_upperl(uppercase_function, sizeof(uppercase_function), gl_func.name, gl_func.namelen);
    
    buf = string_catl(buf, uppercase_function, gl_func.namelen);
    buf = string_cat(buf, ", ");

    buf = string_catl(buf, gl_func.name, gl_func.namelen);
    buf  = string_cat(buf, ")\n");

    *buf = 0;
}


internal bool
identifier_is_gl_function(Token identifier)
{
    if(identifier.text[0] == 'g' && identifier.text[1] == 'l')
    {
        if(is_upper(identifier.text[2]))
        {
            return true;
        }
    }
     
    return false;
}

global_variable GLFunction gl_functions[1024];
u32 num_opengl_functions_parsed;

inline bool
glfunction_already_inarray(GLFunction test_func, GLFunction *gl_funcarray, int arraysize)
{
    for(int i = 0; i < arraysize; i++)
    {
        if(gl_funcarray[i].namelen == test_func.namelen)
            if(string_equall(gl_funcarray[i].name, test_func.name, test_func.namelen))
                if(gl_funcarray[i].num_args == gl_funcarray[i].num_args)
                return true;
    }
    return false;
}

internal char *
find_glfunc_in_reg(char *funcname, u32 funcname_len)
{
    //Binary search for function in the function info file
    u32 *start_of_functionoffsets = ((u32 *)function_info + 1);
    
    i32 current_start = 0;
    i32 current_end = num_functions_inreg - 1;

search_nextiteration:
    u32 current_search_index = (current_start + current_end) / 2; //10 -> 5 11 -> 5 12->6 
    
    char *function_nameinreg = ((char *) (function_info)) + start_of_functionoffsets[current_search_index];
    if(string_equall(funcname, funcname_len, function_nameinreg))
    {
        //we found the function
        return function_nameinreg;
    }
    else
    {
        if(current_start > current_end)
        {
            return 0;
        }

        if(is_glfunc_alphabetically_before(funcname, funcname_len, function_nameinreg))
        {
            //our function is alphabetically before the current function in registry
            //we need to look on the left side of this

            current_end = current_search_index - 1;
            goto search_nextiteration;
        }
        else
        {
            //the function in the registry is before what we are looking for
            //we need to look on the right side of this
            current_start = current_search_index + 1;
            goto search_nextiteration;
        }
    }

}

internal u32
generate_errorchecking_for_function(char *buf, u32 bufsize, GLFunction function)
{
    //WHAT we are trying to produce!!
#if 0
void glReadBuffer_errcheck(GLenum arg0, u32 line, const char *function)
{
    glReadBuffer(arg0);
    GLenum error = glGetError();
    if(error)
    {
        char *error_name = OPENGLERR(error);
        char buf[1024];
        snprintf(buf, 1024, "glReadBuffer has returned an error: %s", error_name);
        platform.display_error(buf);
    }
}
#define glReadBuffer(arg0) glReadBuffer_errcheck(arg0, __LINE__, __FUNCTION__)
#endif

    DestBuf dest = {};
    dest.at = buf;
    dest.remaining_chars = bufsize;

    char *glfunc_inreg = find_glfunc_in_reg(function.name, function.namelen);

    if(!glfunc_inreg)
    {
        fprintf(stderr, "Error: could not find function %.*s in gl.func! Check the gl.func file.\n", function.namelen, function.name);
        return 0;
    }

    char *at = glfunc_inreg;

    while(at[0])
        at++;
    at++;
    
    Text return_type = {};
    return_type.from = at;
    while(at[0])
        at++;

    return_type.len = at - return_type.from;

    b32 returntype_isvoid = string_equall(return_type.from, return_type.len, "void");
    
    at++;

    u32 num_params = string_tonum(at);
    while(at[0])
        at++;

    Text errcheck_function = {};

    outstringl(&dest, return_type.from, return_type.len);
    outchar(&dest, ' ');
    errcheck_function.from = dest.at;
    outstringl(&dest, function.name, function.namelen);
    outstring(&dest, "_errcheck");
    errcheck_function.len = dest.at - errcheck_function.from;


    outchar(&dest, '(');
    for(int i = 0; i < num_params; i++)
    {
        char *param = at;

        at++;
        param = at;
        while(at[0])
            at++;

        u32 param_len = at - param;
        outstringl(&dest, param, param_len);
        outstring(&dest, " arg");
        outnumber(&dest, i);
        outstring(&dest, ", ");
    }
    
    outstring(&dest, "u32 line, const char *function)\n{\n    ");


    if(!returntype_isvoid)
    {
        outstringl(&dest, return_type.from, return_type.len);
        outstring(&dest, " ret = ");
    }

    
    outstringl(&dest, function.name, function.namelen);

    Text argument_list = {};
    argument_list.from = dest.at;

    outchar(&dest, '(');
    for(int i = 0; i < num_params; i++)
    {
        outstring(&dest, "arg");
        outnumber(&dest, i);
        if(i + 1 != num_params)
            outstring(&dest, ", ");
    }
    outchar(&dest, ')');
    argument_list.len = dest.at - argument_list.from;
    
    outstring(&dest, ";\n    GLenum error = glGetError();\n");
    
    outstring(&dest, "    if(error)\n    {\n");
    outstring(&dest, "        char *error_name = OPENGLERR(error);\n");
    outstring(&dest, "        char buf[1024];\n");
    outstring(&dest, "        snprintf(buf, 1024, \"");
    outstringl(&dest, function.name, function.namelen);
    outstring(&dest, " has returned an error: %s\", error_name);\n");
    outstring(&dest, "        platform.display_error(buf);\n");
    outstring(&dest, "    }\n");


    if(!returntype_isvoid)
        outstring(&dest, "    return ret; \n");

    outstring(&dest, "}\n");
    outstring(&dest, "#define ");
    outstringl(&dest, function.name, function.namelen);
    outstringl(&dest, argument_list.from, argument_list.len);

    outchar(&dest, ' ');
    outstringl(&dest, errcheck_function.from, errcheck_function.len);
    outstringl(&dest, argument_list.from, argument_list.len-1);

    //if there is more than 2 characters in the arglist then there are argument
    //the min number of chars is 2 because of the opening and closing parens
    if(argument_list.len > 2) 
        outstring(&dest, ", ");
    outstring(&dest,"__LINE__, __FUNCTION__)\n");


#if 0
    outstring(&dest, "#define ");

    Text function_call = {};
    function_call.from = dest.at;

    outstring(&dest, "gl");
    outstringl(&dest, function.name, function.namelen);

    outchar(&dest, '(');
    for(int i = 0; i < function.num_args; i++)
    {
        outstring(&dest, "arg");
        outnumber(&dest, i);
        if(i + 1 != function.num_args)
            outchar(&dest, ',');
    }
    outchar(&dest, ')');
    function_call.len = dest.at - function_call.from;

    outstring(&dest, " \\\n");

    outstring(&dest, "    ");
    outstringl(&dest, function_call.from, function_call.len);
    outchar(&dest, ';');

    
    outstring(&dest, " \\\n");

    outstring(&dest, "    opengl_check_error(\"");
    outstring(&dest, "gl");
    outstringl(&dest, function.name, function.namelen);
    outstring(&dest, "\", __LINE__, __FUNCTION__);");

    outchar(&dest, '\n');

    //outchar(&dest, '\0');
#endif
    return dest.at - buf;    
}

char opengl_check_error_lookup[] =
R"delimiter(//The error value range starts from 0x0500 
//(e.g. GL_INVALID_ENUM is 0x0500, then GL_INVALID_VALUE is 0x501)
char *error_names[] = {"GL_INVALID_ENUM", 
                       "GL_INVALID_VALUE",
                       "GL_INVALID_OPERATION",
                       "GL_STACK_OVERFLOW",
                       "GL_STACK_UNDERFLOW",
                       "GL_OUT_OF_MEMORY",
                       "GL_INVALID_FRAMEBUFFER_OPERATION",
                       "GL_CONTEXT_LOST"};

#define OPENGLERR(error_enum) error_names[error_enum-0x0500] 

)delimiter";

internal void
ParseGLFunctionsAndErrorChecking(char *errorchecking_filepath)
{
    FILE *errorcheckingfile = 0;
    if(errorchecking_filepath)
    {
        errorcheckingfile = fopen(errorchecking_filepath,"w");

        if(!errorcheckingfile)
            fprintf(stderr, "Could not write to the error checking file specified");
        fwrite(opengl_check_error_lookup, 1, sizeof(opengl_check_error_lookup)-1, errorcheckingfile);        
    }

    char *filetoprocess_path = "..\\src\\opengl.cpp";
    char *filetoprocess = (char *)read_entire_file_and_nullterminate(filetoprocess_path);
    if(!filetoprocess)
    {
        fprintf(stderr, "ERROR: Could not find %s!\n", filetoprocess_path);
        return;
    }

    Tokenizer tokenizer = {};
    tokenizer.at = filetoprocess;
    
    b32 parsing = true;
    while(parsing)
    {
        Token token = get_nexttoken(&tokenizer);
        switch(token.type)
        {

            case Token_endofstream:
                parsing = false;
                break;
            case Token_identifier:
                    if(identifier_is_gl_function(token))
                    {
                        Token glFunctionIdentifier = token;

                        int argCount;
                        token = get_nexttoken(&tokenizer);
                        if(token.type == Token_openparen)
                        {
                            int inInnerParen = 1;
                            token = get_nexttoken(&tokenizer);
                            if(token.type != Token_closeparen)
                            {
                                argCount = 1;
                                while(!((token.type == Token_closeparen && inInnerParen == 0) ||
                                        token.type == Token_endofstream))
                                {
                                    token = get_nexttoken(&tokenizer);
                                    if(token.type == Token_comma)
                                        argCount++;
                                    else if (token.type == Token_openparen)
                                        inInnerParen++;
                                    else if (token.type == Token_closeparen)
                                        inInnerParen--;
                                }
                            }else
                                argCount = 0;
                        }
                        else break; //NOTE:: isn't an opengl function after all

                        GLFunction gl_func = {};

                        gl_func.name = glFunctionIdentifier.text;     //NOTE:: REMOVE GL
                        gl_func.namelen = glFunctionIdentifier.text_len;
                        gl_func.num_args = argCount;

                        char glLine[512];
                        if(!glfunction_already_inarray(gl_func, gl_functions, num_opengl_functions_parsed))
                        {
                            gl_function_tostr(glLine, sizeof(glLine), gl_func);                            
                            gl_functions[num_opengl_functions_parsed] = gl_func;
                            num_opengl_functions_parsed++;

                            printf(glLine);

                            if(errorcheckingfile)
                            {
                                char function_errcheck[2048];
                                u32 num_chars_written =
                                    generate_errorchecking_for_function(function_errcheck,
                                                                        sizeof(function_errcheck), gl_func);
                                if(num_chars_written > 0)
                                {
                                    fwrite(function_errcheck, 1, num_chars_written, errorcheckingfile);
                                }
                            }

                        }
                }break;

#if 0 
            case Token_number:
            {
                printf("%d: %.*s [%.*s]\n", token.type, token.text_len, token.text, 40, token.text);
            }break;
#endif
        }
    }

}

inline void
end_all_active_shader_reading(char *endlocation)
{
    if(shaders[num_shaders - 1].vertex_source.from &&
       !shaders[num_shaders - 1].vertex_source.len)
    {
        shaders[num_shaders - 1].vertex_source.len = endlocation - shaders[num_shaders - 1].vertex_source.from;
    }
    else if (shaders[num_shaders - 1].fragment_source.from &&
             !shaders[num_shaders - 1].fragment_source.len)
    {
        shaders[num_shaders - 1].fragment_source.len = endlocation - shaders[num_shaders - 1].fragment_source.from;
    }
   
}

char shadergenerated_header[] = "//TODO:: FIX LINE ENDINGS and weirdness!!\ninternal GLuint\nopengl_load_shader(char *vert_shadersource, char *frag_shadersource);\n";

internal b32
ParseGLShaders(char *shadergenerated_filepath)
{
    FILE *shadergenerated_file = 0;
    if(shadergenerated_filepath)
    {
        shadergenerated_file = fopen(shadergenerated_filepath,"wb");

        if(!shadergenerated_file)
        {
            fprintf(stderr, "Could not open the shader file specified for writing.\n");
            return false;
        }
        fwrite(shadergenerated_header, 1, arraycount(shadergenerated_header), shadergenerated_file);
        
    }
    else
    {
        return false;
    }

    char *filetoprocess_path = "..\\src\\opengl_shaders.glsl";
    char *filetoprocess = (char *)read_entire_file_and_nullterminate(filetoprocess_path);    
    if(!filetoprocess)
    {
        fprintf(stderr, "ERROR: Could not find %s!\n", filetoprocess_path);
        return false;
    }
    
    char *at = filetoprocess;
    char *last_nonwhitespace;
    for(;;)
    {

        switch(at[0])
        {
            case '@':
            {
                if(num_shaders > 0)
                    end_all_active_shader_reading(last_nonwhitespace + 1);

                at++;
                shaders[num_shaders].name.from = at;
                while(!is_endofline(at[0]) && !is_whitespace(at[0]))
                    at++;
                shaders[num_shaders].name.len = at - shaders[num_shaders].name.from;
                num_shaders++;
            }break;
            case '\0':
            {
                end_all_active_shader_reading(last_nonwhitespace + 1);
                goto done_parsing;
            }break;
            case ':':
            {
                if(at[1] == 'f' && at[2] == 'r' && at[3] == 'a' && at[4] == 'g')
                {
                    end_all_active_shader_reading(last_nonwhitespace + 1);
                    //start recording frag shader
                    at += 5;
                    while(is_endofline(at[0]) || is_whitespace(at[0])) //skip whitespaces/endoflines
                        at++;
                    shaders[num_shaders - 1].fragment_source.from = at;
                }
                else if (at[1] == 'v' && at[2] == 'e' && at[3] == 'r' && at[4] == 't')
                {
                    end_all_active_shader_reading(last_nonwhitespace + 1);
                    //start recording vert shader
                    at += 5;
                    while(is_endofline(at[0]) || is_whitespace(at[0])) //skip whitespaces/endoflines
                        at++;
                    shaders[num_shaders - 1].vertex_source.from = at;
                }
            }
            default:
            {
                if(string_equall(at, "uniform"))
                {
                    at += 8;
                    Text temp_identifier = {};
                    for(;;)
                    {
                        if(is_alpha(at[0]) || at[0] == '_')
                        {
                            temp_identifier.from = at;
                            while(is_alpha(at[0]) || is_numeric(at[0]) || at[0] == '_')
                                at++;

                            if(at[0] == ';')
                            {
                                temp_identifier.len = at - temp_identifier.from;
                                break;
                            }
                        }
                        at++;
                    }

                    shaders[num_shaders - 1].uniform[shaders[num_shaders - 1].num_uniforms].from =
                        temp_identifier.from;
                    shaders[num_shaders - 1].uniform[shaders[num_shaders - 1].num_uniforms].len =
                        temp_identifier.len;
                    shaders[num_shaders - 1].num_uniforms++;
                }
            }break;
        }
        if(!is_whitespace(at[0]))
            last_nonwhitespace = at;

        at++;
    }
done_parsing:    
    char structsbuf[4096];
    char sourcebuf[4096];
    char functionbuf[4096];

    DestBuf structs = {};
    structs.at = structsbuf;
    structs.remaining_chars = arraycount(structsbuf);

    DestBuf source = {};
    source.at = sourcebuf;
    source.remaining_chars = arraycount(sourcebuf);

    DestBuf function = {};
    function.at = functionbuf;
    function.remaining_chars = arraycount(functionbuf);

    outstring(&function, "internal void\nload_allshaders()\n{\n");
    
    for(int i = 0; i < num_shaders; i++)
    {
        //structs buf
        outstring(&structs, "global_variable\n");
        outstring(&structs, "struct\n");
        outstring(&structs, "{\n");
        outstring(&structs, "    GLuint ID;\n\n");

        //source buf
        outstring(&source, "char *");

        Text vertex_shadersource_identifier;
        vertex_shadersource_identifier.from = source.at;

        outstringl(&source, shaders[i].name.from, shaders[i].name.len);
        outstring(&source, "_vertexsource");
        vertex_shadersource_identifier.len = source.at - vertex_shadersource_identifier.from;
                
        outstring(&source, " = \nR\"delimiter(");
        outstringl(&source, shaders[i].vertex_source.from, shaders[i].vertex_source.len);
        outstring(&source, "\n)delimiter\";\n\n");

        outstring(&source, "char *");

        Text fragment_shadersource_identifier;
        fragment_shadersource_identifier.from = source.at;

        outstringl(&source, shaders[i].name.from, shaders[i].name.len);
        outstring(&source, "_fragmentsource");
        fragment_shadersource_identifier.len = source.at - fragment_shadersource_identifier.from;

        outstring(&source, " = \nR\"delimiter(");
        outstringl(&source, shaders[i].fragment_source.from, shaders[i].fragment_source.len);
        outstring(&source, "\n)delimiter\";\n\n");
        
        //functions buf
        outstring(&function, "    ");

        Text shader_identifier;
        shader_identifier.from = function.at;

        outstring(&function, "opengl_");
        outstringl(&function, shaders[i].name.from, shaders[i].name.len);
        shader_identifier.len = function.at - shader_identifier.from;

        outstring(&function, ".ID = opengl_load_shader(");
        outstringl(&function, vertex_shadersource_identifier.from, vertex_shadersource_identifier.len);
        outstring(&function, ", ");
        outstringl(&function, fragment_shadersource_identifier.from, fragment_shadersource_identifier.len);
        outstring(&function, ");\n");

        for(int j = 0; j < shaders[i].num_uniforms; j++)
        {
            outstring(&structs, "    GLint ");
            outstringl(&structs, shaders[i].uniform[j].from, shaders[i].uniform[j].len);
            outstring(&structs, "_loc;\n");

            outstring(&function, "    ");
            outstringl(&function, shader_identifier.from, shader_identifier.len);
            outchar(&function, '.');
            outstringl(&function, shaders[i].uniform[j].from, shaders[i].uniform[j].len);
            outstring(&function, "_loc = glGetUniformLocation(");
            outstringl(&function, shader_identifier.from, shader_identifier.len);
            outstring(&function, ".ID, \"");
            outstringl(&function, shaders[i].uniform[j].from, shaders[i].uniform[j].len);
            outstring(&function, "\");\n");
            
        }

        outchar(&structs, '}');
        outstringl(&structs, shader_identifier.from, shader_identifier.len);
        outstring(&structs, ";\n\n");
        outchar(&function, '\n');
    }

    outchar(&function, '}');

    u32 num_structs_chars_written = structs.at - structsbuf;
    u32 num_source_chars_written = source.at - sourcebuf;
    u32 num_function_chars_written = function.at - functionbuf;

    fwrite(structsbuf, 1, num_structs_chars_written, shadergenerated_file);
    fwrite(sourcebuf, 1, num_source_chars_written, shadergenerated_file);
    fwrite(functionbuf, 1, num_function_chars_written, shadergenerated_file);
    return true;
}

int main(int argc, char *argv[])
{
    function_info = read_entire_file_and_nullterminate("gl.func");

    if(function_info)
        num_functions_inreg = *(u32*)function_info;        
    else
    {
        fprintf(stderr, "No gl.func file found in the working directory. Generate this file with the glregparse utility using the mode 'function_info' and name it the file gl.func.\n");
    }

#if 0
    char *result = find_glfunc_in_reg(argv[1], string_length(argv[1]));
    if(result)
    {
        printf("Found it!");
    }
    else
    {
        printf("Could not find that function!");
    }
#endif
    ParseGLFunctionsAndErrorChecking(argv[1]);
    ParseGLShaders(argv[2]);
    return 0;
}
