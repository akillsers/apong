enum PaddleMoveDirection
{
    nowhere,
    up,
    down,
};

enum PaddleAIMode
{
    PaddleAI_idle,
    PaddleAI_wandering,
    PaddleAI_honing,
    PaddleAI_inertia
//    PaddleAI_goingtoball,
//    PaddleAI_missball
};

enum GotoBall
{
    GotoBall_didnotchoose,
    GotoBall_yes,
    GotoBall_no
};

enum HoningConstraint
{
    HoningConstraint_no_constraints,
    HoningConstraint_must_be_lower,
    HoningConstraint_must_be_higher
};


enum BallDir
{
    BallDir_nowhere, BallDir_left, BallDir_right
};

//forward declare gamemodelevel
META_EXCLUDE struct GameModeLevel;
META_EXCLUDE struct PaddleAIState;
META_EXCLUDE struct Paddle;

struct PaddleAISettings
{
    b32 (*calculate_underestimate_anyway_func)(GameModeLevel *l, Paddle *paddle);
    b32 (*calculate_underestimate_normal_func)(GameModeLevel *l, Paddle *paddle);
    b32 (*calculate_overestimate_normal_func) (GameModeLevel *l, Paddle *paddle);
};

struct PaddleAIState
{
    RandomSeries random;
    f32 targety;
    f32 guess_delay;

    b32 will_miss;
    b32 recovering;
    b32 recovery_decision_made;
    f32 recovering_delay;
    f32 fail_recover_targety;
    
    b32 last_wonmatch;
    BallDir last_bdir;

    b32 doing_rage;
    f32 rage_seconds_until_other_direction;
    f32 seconds_of_rage;
    f32 rage_seconds_until_downtime;
    b32 rage_up;
    
    META_EXCLUDE PaddleAISettings ai_settings;
};

char *record_set_to_name[]= 
{
    "Normal",
    "Nightmare",
    "Multiplayer"
};

//@savegame
/*
  u32 major_format
  u32 minor_format;

  b32 isEasyLocked?
  b32 isNightmareLocked?
  b32 isMultiplayerLocked?

  b32 isReservedLocked[12]

  THE HIGH SCORE CHUNK:
  b32 beat[2]                  //for easy and nightmare only
  u16 high_score_left_score[2] //for easy and nightmare only

  THE SCORE RECORD CHUNK:
  u32 num_record_sets //for version 0.0 this is always 3
  u32 offset_in_file[num_record_sets];
  u32 num_stats_entry[num_record_sets]

  offset_in_file:
  u64 miliseconds since 2000 Jan 1 12:00 A:M in UTC
  u32 left_score
  u32 right_score 

  char 'e' //always 'e'(check character)
*/
//1 read how many historic scores are in the save game and copy them to a temp buffer
//then we will add the historic score that we did not add yet by comparing the num_played value
//with the game memory's num_played value

#define NUM_RECORD_SETS Mode_count

#define HEADER_SIZE 8
#define LOCK_CHUNK_SIZE 60
#define HIGH_CHUNK_SIZE 12
#define SCORE_CHUNK_HEADER_SIZE 4 + 8*NUM_RECORD_SETS
#define FOOTER_SIZE 1

#define SCORE_CHUNK (HEADER_SIZE + LOCK_CHUNK_SIZE + HIGH_CHUNK_SIZE)
#define FIRST_RECORD_SET_OFFSET (HEADER_SIZE + LOCK_CHUNK_SIZE + HIGH_CHUNK_SIZE + SCORE_CHUNK_HEADER_SIZE)
#define SAVE_GAME_TOTAL_FIXED_SIZE (HEADER_SIZE + LOCK_CHUNK_SIZE + HIGH_CHUNK_SIZE + SCORE_CHUNK_HEADER_SIZE + FOOTER_SIZE)

#define SAVE_GAME_PER_RECORD_SIZE 16

#define EASY_LOCKED_OFFSET 8
#define FIRST_LOCKED_OFFSET EASY_LOCKED_OFFSET
#define NIGHTMARE_LOCKED_OFFSET 20
#define MULTI_LOCKED_OFFSET 24

#pragma pack(push, 1)
struct ScoreRecord
{
    u64 time;
    u32 left_score;
    u32 right_score;
};
#pragma pack(pop)

struct SaveGameInfo
{
    u64 offset_in_file;
    u32 bytes_to_write;
    u8 *data;
};

struct Paddle
{
    Rect bbox;
    PaddleMoveDirection last_movedir = nowhere;
    Vec2 vel;
    b32 last_collided;
    
    PaddleAIState *ai;
};

struct Ball
{
    Circle b_circle;
    Vec2 vel;
};

struct Player
{
    Paddle *player_paddle;
    u32 pts;
    b32 isAI;
};

enum GameMode
{
    GameMode_level,

    GameMode_count
};

enum Menu
{
    Menu_MainMenu,
    Menu_Settings,
    Menu_NewGame,
    Menu_About,
    Menu_Quit,

    Menu_Count
};

//TODO something special for NIGHTMARE

// struct PaddleAISettings
// {
//     //First, the ball speed difficulty starts increasing from 0 starting at this speed... 
//     f32 min_challenging_ballspeed;

//     //the ball speed difficulty then starts rising quadratically
//     //the ball speed difficulty reaches 1 in this many ballspeed units away from min_challenging_ballspeed

//     f32 challenging_ballspeed_pickup; 
//     //(the higher challenging_ballspeed_pickup is... the slower the difficulty ramps up to 1)
        
//     //Second, the slope difficulty is calculated by
//     //mapping the slope from min_slope and max_slope to 0 and 1 (clamped on the zero side)
//     f32 min_slope;
//     f32 max_slope;

//     //Finally, the distance difficulty is cacluated by mapping the distance away from the ball
//     //from 0 to max distance possible linearly to 0 and max_distance_difficulty
//     f32 max_distance_difficulty;

//     //The final difficulty rating is calculated by multiplying the sum
// };

META_EXCLUDE struct DifficultyPreset
{
    f32 ball_initial_speed;
    f32 ball_initial_spawndelay;
    f32 ballspawn_decrement;
    f32 ballspeed_increment;

    PaddleAISettings ai_settings;
};


enum Mode
{
    Mode_normal,
    Mode_multiplayer,

    Mode_count
};

inline b32 calculate_underestimate_anyway_easy(GameModeLevel *, Paddle *);
inline b32 calculate_underestimate_normal_easy(GameModeLevel *, Paddle *);
inline b32 calculate_overestimate_normal_easy(GameModeLevel *, Paddle *);
inline b32 calculate_underestimate_anyway_nightmare(GameModeLevel *, Paddle *);
inline b32 calculate_underestimate_normal_nightmare(GameModeLevel *, Paddle *);
inline b32 calculate_overestimate_normal_nightmare(GameModeLevel *, Paddle *);

// TODO: Make the spawn delay longer when the ball ended up going faster in the last game? (for optimal recovery purposes and to make the game more interesting when death happens fast?)
DifficultyPreset diff_presets[Mode_count] =
{
    //normal
    {5.5f,    //ball initial speed
     2.3,    //initial spawn delay
     0.14,  //spawn delay decrement
     0.2, //speed increment
     calculate_underestimate_anyway_easy,
     calculate_underestimate_normal_easy,
     calculate_overestimate_normal_easy},  

    //multiplayer
    diff_presets[Mode_normal]
};
    
// char *difficulty_desc[] = {"The easiest difficulty of apong.",
//                            "If you're a normal kind of person, consider playing this mode.",
//                            "This is where the fun begins, they say.",
//                            "Ready to rage? Play on.",
//                            "Challenge your friends to a duel of reflexes."};


// enum DifficultyInfoEnum
// {
//     DifficultyInfo_easy,
//     DifficultyInfo_normal,
//     DifficultyInfo_hard,
//     DifficultyInfo_nightmare,
//     DifficultyInfo_multiplayer,

//     DifficultyInfo_count
// };

// //TODO init from file??
// global_variable DifficultyInfo diff_info[DifficultyInfo_count] =
// {
//     //easy
//     {
//         difficulty_desc[0],
//         arraycount(difficulty_desc[0])
//     },
//     //normal
//     {
//         difficulty_desc[1],
//         arraycount(difficulty_desc[1])
//     },
//     //hard
//     {
//         difficulty_desc[2],
//         arraycount(difficulty_desc[2])
//     },
//     //nightmare
//     {
//         difficulty_desc[3],
//         arraycount(difficulty_desc[3])
//     },
//     //multiplayer
//     {
//         difficulty_desc[4],
//         arraycount(difficulty_desc[4])
//     }
// };
#define MAX_MEM_HISTORIC_SCORES 2
char *normal = "normal";

char *multiplayer_tex = "sword";
char *multiplayer = "multiplayer";

struct ModeInfo
{
    char *desc;
    u32 desc_len;

    //FOR now since we don't have tag based asset retrieval
    char *texture_name;
    char *name;
    b32 unlocked;
    DifficultyPreset *preset;

    //TODO Note applicable for multiplayer
    b32 beat;
    u16 highscore_left_score;

    //TODO WIN 5 in a row!!
    //TODO make this AKINFINITE
    ScoreRecord historic_scores[MAX_MEM_HISTORIC_SCORES];
    u32 historic_scores_index;
    u32 num_played;
};

//NOTE:: This is really NEW_MENU state so we can allocate and deallocate this on the fly
struct MenuState
{
    //TODO put other things in here into the menu state like showmenu
    //     and current menu
    b32 explain_lock;
    //NOTE:: for Menu_NewGame
    b32 flashing;
    f32 flashing_t;
    Mode starting_mode;
    u32 flashing_button;
    b32 can_esc_inout;

    f32 yoffset;
    f32 target_yoffset;

    f32 stats_transparency;
    f32 target_stats_transparency;
    
    b32 showing_info;
};


enum RandomSpeedNiche
{
    way_slow,
    slight_increment,
    way_fast,
};

typedef f32 (*RandomSpeedFunc) (GameModeLevel *l);

struct GameModeLevel
{
    AABB court_bounds;
    
    Player left_player;
    Player right_player;

    RandomSeries ballserve_random;
    Ball *ball;

    b32 unlocked_new;
    b32 wongame;
    b32 endless;
    u32 endless_original_points;
    f32 wonmatch;
    f32 ballspeed;
    f32 ballspawn_delay;
    f64 win_game_time;

    f32 ballspawn_timer;
    b32 serve_to_right;

    Menu current_menu;
    u32 menu_about_page;


    RandomSpeedFunc random_speed;
    f32 ball_targety;
    f32 ball_lefty;
    f32 last_speed;

    Texture1c ball_texture;
    
    b32 showmenu;
    MenuState menu;

    RandomSeries random;
    Mode current_mode;
    //TODOmove to another struct??
    ModeInfo mode_info[Mode_count];
};

b32 calculate_underestimate_anway_easy(GameModeLevel *l, PaddleAIState *ai);
b32 calculate_underestimate_anway_easy(GameModeLevel *l, PaddleAIState *ai);
b32 calculate_underestimate_anway_easy(GameModeLevel *l, PaddleAIState *ai);
b32 calculate_underestimate_anway_easy(GameModeLevel *l, PaddleAIState *ai);


#define PixelsToMeters(pixels) ((pixels) * (1.0f/g->pixelspermeter))
#define MetersToPixels(meters) ((meters) * g->pixelspermeter)
/*
universe arena (for the game state)
mode specific arena 
temp arena (gets booted every frame)
*/


#define MAX_PLAYING_SOUNDS 20
struct PlayingSound
{
    f32 volume;
    Sound *sound;
    u32 frameindex;
};
struct Mixer
{
    //TODO we probs want this to be another data structure
    //     like linked list??

    //TODO is this thread safe??
    volatile i32 last_playing_sound_index;
    volatile b32 free_playing_sounds[MAX_PLAYING_SOUNDS];

    volatile PlayingSound current_sounds[MAX_PLAYING_SOUNDS];
};


struct GameState
{
    Mixer mixer;
    
    RenderList rlist;
    RenderTarget rtarget;
    f32 pixelspermeter;

    b32 cursor_showing;
    Font *gamefont;

    MemoryArena universe_arena;

    GameMode mode;
    union
    {
        GameModeLevel *level_data;
    };

    Assets assets;
};

global_variable MemoryArena temp_arena;
global_variable MemoryArena sound_arena;
