//stretchy buffers (?) invented by Sean Barrett??

#include <aks.h>
#include <stddef.h>
struct BufHeader
{
    size_t len;
    size_t cap;
    char buf[0];
};

#define buf__header(b) ((BufHeader *) ((char *)b - offsetof(BufHeader, buf)))
#define buf__fits(b, n) (buf_len(b) + n <= buf_cap(b))
#define buf__fit(b, n) (buf__fits(b, n) ? 0 : (*(void**)&(b) = buf__grow((void*)(b), buf_len(b) + (n), sizeof(*(b))))) 

#define buf_len(b) ((b) ? buf__header(b)->len : 0)
#define buf_cap(b) ((b) ? buf__header(b)->cap : 0)
#define buf_push(b, x) (buf__fit(b, 1), b[buf_len(b)] = (x), buf__header(b)->len++)
#define buf_last(b) ((b)[buf_len(b) - 1])
#define buf_free(b) ((b) ? free(buf__hdr(b) : 0)


void *buf__grow(const void *buf, size_t new_len, size_t elem_size)
{
    //amortized constant time push
    size_t newcap = max(2*buf_cap(buf), new_len);
    assert(new_len <= newcap);
    size_t newsize = newcap * elem_size;
    
    BufHeader *new_header;
    if(buf)
    {
        new_header = (BufHeader *)realloc(buf__header(buf), newsize + sizeof(size_t) * 2);        
    }
    else
    {
        new_header = (BufHeader *)malloc(newsize + sizeof(size_t) * 2);
        new_header->len = 0;
    }
    new_header->cap = newcap;
    return new_header->buf;
}
