#if INTERNAL
#define DEBUG_MARKER_SIZE 20

// TODO: We should probably make this relative to the font size (how many characters, etc.)
#define DEBUG_TEXTLINE_COLUMN_WIDTH 550
#define MAX_DEBUG_VOLUMES 71
#define MAX_DEBUG_TEXTLINES 60

// TODO: Sholdn't these two things be always the same?
#define MAX_SNAPSHOT_COUNT 2000
#define MAX_VISITED_COUNT 2000

#define MAX_ORPHAN_VAR_GROUPS 50
#define MAX_VAR_NODE_CHILDREN 50;
//NOTE:: This is allowed since debug update comes before game update
//       this matters because debug update is when this is reset
//dbg_persistent_textlines.textline(&g->rlist, &g->gamefont, 21, buf);

//TODO outputdebugstring replacement

//#include "introspect_generated.cpp"
//#include "introspect_meta_template.cpp"
#include "introspect_gen.cpp"

struct Marker
{
    f32 x;
    f32 y;
    Renderer_RGB color;
};


//NOTE:: FOR NOW:: a debug volume is really a debug rect
struct DebugVolume
{
    u32 ID;
    f32 x;
    f32 y;
    f32 width;
    f32 height;
    f32 transparency;
    Renderer_RGB color;
};

        
struct VarRecord
{
    char name[255]; //TODO should we allcoate this dynamically??
    ValueType type; 
    u8 *value;
};

enum VarNodeType
{
    VarNode_vargroup, //has no data (children are records)
    VarNode_varrecord //has data    (has no childrens)
};
/*
    Paddle(var group) -> u32 var      (var record)
                      -> v2  velocity
                      -> v3  position
                      -> AI  ai (var group) -> u32 movespeed  (var recodrd)
                                            -> u32 isIdle     (record);
*/
struct VarNode
{
    VarNodeType type;
    union
    {
        struct //VarGroup
        {
            char name[255];

            u32 children_count;
            u32 children_cap;
            VarNode *children;
        };
        struct //VarRecord
        {
            char name[255];
            ValueType value_type;
            u8 *value;
        };
    };
};

#define VarGroup VarNode
#define VarRecord VarNode


//var record types:
//vargroup has more varrecords
//var enum enumeration

struct DebugSnapshot
{
    //Texture3c last_frame;
    f32 frame_time;
    f32 platform_render_time;
    f32 platform_input_time;
    f32 platform_game_update_time;
    f32 platform_clock_stabalizer_time;

    u64 temporary_arena_usage;
    u64 universe_arena_usage;
    
    f64 dt_accum;
    f64 fixed_dt_accum;
    f64 since_startup; 
    
    TimingRecord *timings;

    u32 num_var_groups;
    VarGroup var_groups[MAX_ORPHAN_VAR_GROUPS]; 

    VarGroup ungrouped_vars;
    MemoryArena varena;
    MemoryArena vnode_arena;

    DebugVolume volume[MAX_DEBUG_VOLUMES];
};

    
//TODO this struct is getting clunky    
struct DebugState
{
    b32 update_paused;

    b32 draw_volumes;
    u32 num_volumes;
    u32 volume_ids[MAX_DEBUG_VOLUMES];
    // DebugTextline textline[MAX_DEBUG_TEXTLINES];
    // u32 num_textlines;
    
    MemoryArena arena;

    b32 cyan;
    b32 vsync_test;
    Font *font;

    //Last snapshots playback mode
    b32 overlay;
    b32 cursor_visible;
    b32 show_profiler;
    b32 show_recorded_vars;

    b32 snapshot_recording;
    b32 snapshot_goto_end;
    b32 snapshot_goto_end_slow_mode;
    u32 viewing_snapshot_index;
    u32 *visited_snapshots;
    u32 visited_snapshot_index;

    
    u32 num_snapshots; //number of snapshots
    DebugSnapshot *snapshots;
    u32 curr_snapshot_index;

    f64 fixed_dt_accum;
    f64 dt_accum;
    f64 dt_from_secondframe;

    RenderList *rlist;
    RenderTarget *rtarget;
    f32 textline_y;
    f32 textline_x;
};

global_variable DebugState *dbg;

void dbg_varf(char *displayname, ValueType type, u8 *ptr);
#define dbg_var(display, type, var) dbg_varf(display, type, (u8 *)&(var))
#else
#define dbg_var(display, type, var)
#define dbg_struct(...)
#endif
