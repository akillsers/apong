#define GAME_NAME "apong"
#define SAVE_FILE_PATH "\\apong\\apong_game_save.ags"
#define ASSETS_FILE_NAME "apong_assets.apa"
/*
    Apong.CPP is THE GAME LAYER!!!
    code in this file and those that are #included
    here is the game that is abstracted from the platform

    In addition to the #includes in this file,
    this file (apong.cpp) also has access to "apong_platform.h" which is header
    file that connects the platform independent and platform layer
*/
/*
TODO
frame rate locking + visual seeing it
*/

#if INTERNAL
char dbuf[9000];
#define AkelaDebugString(...) \
    sprintf(dbuf, __VA_ARGS__);       \
    OutputDebugString(dbuf);
#endif

#define introspect

//#include "debug_timers.cpp"
#include "assets.h"
#include "random.cpp"

#include "apong.h"

#include "assets.cpp"

#include "time.cpp"
#include "render_helpers.cpp"
#include "debug.cpp"
#include "sound.cpp"
#include "ui.cpp"

#include "ai.cpp"
#include "collisions.cpp"
#include "text.cpp"

#include "modeinits.h"
internal void
switch_gamemode(GameState *state, GameMode mode, GameMemory *memory)
{
    MemoryArena mode_arena = {};
    mode_arena.base = (u8 *)memory->permanent_storage + sizeof(GameState) + state->universe_arena.size;
    mode_arena.size = memory->permanent_storage_size - sizeof(GameState) - state->universe_arena.size;

    switch(mode)
    {
        case GameMode_level:
            state->level_data = init_level(state, &mode_arena);
            break;
        default:
            stop("unknown game mode");
            break;
    }


    state->mode = mode;
}

#include "level.cpp"

internal Vec2u
get_maxdimensions(u32 max_width, u32 max_height, f32 aspectratio)
{
    Vec2u ret;
    f32 height_from_maxwidth = (f32) max_width / aspectratio;
    if(height_from_maxwidth > max_height)
    {
        f32 width_from_maxheight = (f32) max_height * aspectratio;
        ret.x = width_from_maxheight;
        ret.y = max_height;
    }
    else
    {
        ret.x = max_width;
        ret.y = height_from_maxwidth;
    }
    return ret;
}

struct CLParseContext
{
    char *at;
};

internal Text
command_line_get_next_text(CLParseContext *context)
{
    Text ret;
    while(context->at[0] == ' ')
        context->at++;
    ret.from = context->at;
    ret.len = 0;
    while(context->at[0])
    {
        context->at++;
        if(context->at[0] == ' ' || context->at[0] == '\0')
        {
            ret.len = context->at - ret.from;
            context->at++;
            return ret;
        }
    }
    return ret;
}

struct DestBuf
{
    char *at;
    u32 remaining;
};

inline void
outstring(DestBuf *destbuf, char *string)
{
    while(*string)
    {
        *destbuf->at++ = string[0];
        destbuf->remaining--;
         string++;
    }
}

GAME_PROCESS_COMMAND_LINE(game_process_command_line)
{
	GameState *g = (GameState *)memory->permanent_storage;
	
    CLParseContext context;
    context.at = command_line;
    while(context.at[0])
    {
        Text text = command_line_get_next_text(&context);
        //TODO remove
#if ENABLE_SCOREDUMPING
        if(string_equall(text.from, text.len, "scoredump"))
        {
            PlatformFileHandle fhandle;
            FileError ferror = platform.open_file(g->save_path, &fhandle, 0);
            if(!ferror)
            {
                u64 savegame_filesize = platform.get_file_size(fhandle);
                void *savegame_file = arena_pushsize(&temp_arena, savegame_filesize);
                platform.read_file(fhandle, savegame_file, 0, savegame_filesize, true);

#define pull(type)                             \
    *(type *)at;                               \
    at += sizeof(type);                        

                u8 *at = (u8 *)savegame_file + SCORE_CHUNK;
                //TODO cannot create a dump score file bigger than 1 megabyte
                u32 num_record_sets = pull(u32);

                u32 *file_offset = arena_pushcount(&temp_arena, u32, num_record_sets);
                for(int i = 0; i < num_record_sets; i++)
                {
                    file_offset[i] = pull(u32);
                }

                u32 *num_played = arena_pushcount(&temp_arena, u32, num_record_sets);
                for(int i = 0; i < num_record_sets; i++)
                {
                    num_played[i] = pull(u32);
                }
                
                u32 alloc_size = megabytes(1);
                char *output = (char *)arena_pushsize(&temp_arena, alloc_size);

                DestBuf dbuf;
                dbuf.at = output;
                dbuf.remaining = alloc_size;

                outstring(&dbuf, "Mode, Time, Left Score, Right Score \n");
                for(int i = 0; i < num_record_sets; i++)
                {
                    u64 time;
                    u32 left;
                    u32 right;
                    char *record_set_name = record_set_to_name[i];
                    for(int j = 0; j < num_played[i]; j++)
                    {
                        time = pull(u64);
                        left = pull(u32);
                        right = pull(u32);

                        u32 second = 0;
                        u32 minute = 0;
                        u32 hour = 0;
                        u32 day = 0;
                        u32 month = 0;
                        u32 year = 0;

                        u32 bias = platform.get_time_bias_minutes();
                        time = time - bias*60;

                        SecondsFrom2000ToDateTime(time, &second, &minute, &hour,
                                                  &day, &month, &year);

                        char buf[512];
                        sprintf(buf, "%s, %02u:%02u:%02u %u/%u/%u, %u, %u\n", record_set_name, hour, minute, second,
                                day, month, year, left, right);
                        outstring(&dbuf, buf);
                    }
                }

                PlatformFileHandle score_file_handle;
                ferror = platform.open_file("apong_score_dump.csv", &score_file_handle, CREATE_IF_NONEXISTENT | WRITE_ACCESS);
                if(ferror == FileError_nonexistent)
                {
                    platform.write_file(score_file_handle, 0, dbuf.at - output, (u8 *)output, false);
                    platform.close_file(score_file_handle);
                }
                else if(!ferror)
                {
                    b32 overwrite =  platform.display_yesno("File already exists", "Overwrite apong_score_dump.csv?");
                    if(overwrite)
                    {
                        platform.write_file(score_file_handle, 0, dbuf.at - output, (u8 *)output, true);
                   
                    }
                    platform.close_file(score_file_handle);
                }
                else
                {
                    platform.display_error("Unable to save score dump in csv file",
                                           "APONG could not access the file apong score dump.csv. Is another process using it?");
                }

                platform.close_file(fhandle);
                platform.shut_off();
            }
            else
            {
                platform.display_error("Unable to open data/apong_game_save.ags",
                                       "Is the file deleted or used by another process??");
                    
            }
        }
#endif
    }
#undef pull
}

GAME_UPDATE_AND_RENDER(game_update_and_render)
{
    TIMED_BLOCK;
    arena_reset(&temp_arena);
    GameState *g = (GameState *)memory->permanent_storage;

    f32 aspectratio = 16.0f/9;
    f32 screen_width_in_meters = 16;
    Vec2u desired_dimensions = get_maxdimensions(platform.w_w, platform.w_h, aspectratio);
    g->rtarget.w = desired_dimensions.x;
    g->rtarget.h = desired_dimensions.y;
    g->rlist = allocate_render_storage(&temp_arena, megabytes(5));

    //TODO should we do this mouse position transformation somewhere else??
    //TODO how should we handle floating point precision
    //     right now sometimes we get 0.5s, do we trucatethat?? how do we round that
    
    //NOTE:: map the input cursor position to the framebuffer reigion
    //       (we know the framebuffer will be in the middle of the window)
    f32 framebuffer_xoffset = (f32)(platform.w_w - desired_dimensions.x)/2;
    f32 framebuffer_yoffset = (f32)(platform.w_h - desired_dimensions.y)/2;;
    input->mousex -= framebuffer_xoffset;
    input->mousey -= framebuffer_yoffset;

    // TODO we are only resetting the enter key since that's the only key we need
    // input->already_pressed[KEY_enter] = false;
    // input->already_pressed[KEY_space] = false;
    //TODO do this in platform layer
    input->sticky_press = KEY_invalid;
    // for(int i = 0; i < KEY_count; i++)
    // {
    //     input->already_pressed[i] = false;
    // }
    
    // char buf[234];
    // sprintf(buf ,"x: %f, y:%f\n", input->mousex, input->mousey);
    // OutputDebugString(buf);

    g->pixelspermeter = g->rtarget.w / screen_width_in_meters;
    
    switch(g->mode)
    {
        case GameMode_level:
            level_updateandrender(g, input, memory);
            break;
        default:
            stop("Unknown game mode!");
    }

#if INTERNAL
    dbg_update(g, &g->rlist, &g->rtarget, input);    
#endif

    rlist[0] = &g->rlist;
    rtarget[0] = &g->rtarget;

}

GAME_INIT_MEMORY(game_init_memory)
{
    TIMED_BLOCK;
    assert(sizeof(GameState) <= memory->permanent_storage_size);
    GameState *g = (GameState *)memory->permanent_storage;

    f32 aspectratio = 16.0f/9;
    Vec2u framebuffer_initial_size = get_maxdimensions(platform.w_w, platform.w_h, aspectratio);
    framebuffer_initial_size = {1,1};

    
    MemoryArena scratch_arena = {};
    scratch_arena.base = (u8 *)memory->scratch_storage;
    scratch_arena.size = memory->scratch_storage_size;

    temp_arena = arena_suballocate(&scratch_arena, scratch_arena.size*2/3);
    sound_arena = arena_suballocate(&scratch_arena, scratch_arena.size/3);

    g->universe_arena = {};
    g->universe_arena.base = (u8 *)memory->permanent_storage + sizeof(GameState);
    g->universe_arena.size = megabytes(6);

    // dbg_loadfont(&g->universe_arena, &temp_arena, "..//data//hack.bmp", "..//data//hack.aft", g->gamefont);
		
    g->assets.memory_arena = arena_suballocate(&g->universe_arena, megabytes(2));
    load_asset_file(&g->assets);

    g->mixer.last_playing_sound_index = -1;
#if INTERNAL
    dbg_init(memory);
#endif    
    ui_state.rlist = &g->rlist;
    ui_state.rtarget = &g->rtarget;
    ui_state.assets = &g->assets;
    ui_state.mixer = &g->mixer;
    
    Asset *font_asset = get_asset_by_name(&g->assets, "hack", font);
    g->gamefont = font_asset->font;
    switch_gamemode(g, GameMode_level, memory);
}

#if INTERNAL
//TODO CPP RANT we can't extern const and make that array size
const u32 num_records = __COUNTER__;
TimingRecord timing_records[num_records]; //TODO what if we use __COUNTER__ for something else
                                          //now we would allocate this to be too big
#endif
