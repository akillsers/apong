META_EXCLUDE ____file
enum TokenType
{
    Token_invalid,
    Token_unknown,

    Token_startelement,
    Token_attribname,
    Token_attribvalue,
    Token_endelement,
    Token_text,

    Token_endofstream,

    Token_count
};

char * tokennames[Token_count] =
{
    "Unknown",
    "start element",
    "attribute name",
    "attribute value",
    "end element",
    "text",
    "end of stream"
};

struct Token
{
    TokenType type;

    i32 text_len;
    char *text;
};

struct Tokenizer
{
    char *at;
    char *prevtokenat;
};

internal void
eat_all_whitespace_and_comments(Tokenizer *tokenizer)
{
    for(;;)
    {
        if(is_whitespace(tokenizer->at[0]))
        {
            tokenizer->at++;
        }
        else if (tokenizer->at[0] == '<' &&
                 tokenizer->at[1] == '!' &&
                 tokenizer->at[2] == '-' &&
                 tokenizer->at[3] == '-')
        {
            while(!(tokenizer->at[0] == '-' &&
                  tokenizer->at[1] == '-' &&
                    tokenizer->at[2] == '>'))
                tokenizer->at++;
        }
        else
            break;
    }
}

internal void
backup_tokenizer(Tokenizer *tokenizer)
{
    if(tokenizer->prevtokenat != 0)
    {
        tokenizer->at = tokenizer->prevtokenat;
        tokenizer->prevtokenat = 0;
    }
}

internal Token
get_nexttoken(Tokenizer *tokenizer)
{
    Token ret = {};
    
    eat_all_whitespace_and_comments(tokenizer);
    tokenizer->prevtokenat = tokenizer->at;

    char c = tokenizer->at[0];
    tokenizer->at++;
    switch(c)
    {
        case '\0':{ret.type = Token_endofstream;} break;
        case '/':
        {
            if(tokenizer->at[0] == '>')
            {
                tokenizer->at++;
                ret.type = Token_endelement;
            }
            else
            {
                goto unknown;
            }
        }break;

        case '\"':
        {
            ret.text = tokenizer->at;
            ret.type = Token_attribvalue; 
            do{
                tokenizer->at++;
            }while(tokenizer->at[0] != '\"');
            ret.text_len = tokenizer->at - ret.text;

            tokenizer->at++;
            if(tokenizer->at[0] == '>')
                tokenizer->at++;

        }break;

        case '<':
        {
            if((is_alpha(tokenizer->at[0]) ||
                tokenizer->at[0] == '_'))
            {
                ret.text = tokenizer->at;
                ret.type = Token_startelement;

                do{
                    tokenizer->at++;
                }while(is_alpha(tokenizer->at[0]) ||
                       is_numeric(tokenizer->at[0]) ||
                       tokenizer->at[0] == '_' ||
                       tokenizer->at[0] == '-');
                ret.text_len = tokenizer->at - ret.text;

                if(tokenizer->at[0] == '>')
                    tokenizer->at++;

            }
            else if(tokenizer->at[0] == '/')
            {
                tokenizer->at++;
                if((is_alpha(tokenizer->at[0]) ||
                    tokenizer->at[0] == '_'))
                {
                    ret.type = Token_endelement;                    

                    do{
                        tokenizer->at++;
                    }while(is_alpha(tokenizer->at[0]) ||
                           is_numeric(tokenizer->at[0]) ||
                           tokenizer->at[0] == '_' ||
                           tokenizer->at[0] == '-');
                    if(tokenizer->at[0] == '>')
                        tokenizer->at++;

                }
                else
                    goto unknown;
            }
            else
                goto unknown;
        }break;


        default:
        {
            ret.text = tokenizer->at-1;
            while(tokenizer->at[0])
            {
                if(tokenizer->at[0] == '<')
                {
                    ret.type = Token_text;
                    ret.text_len = tokenizer->at - ret.text;
                    break;
                }
                else if (tokenizer->at[0] == '=')
                {
                    ret.type = Token_attribname;
                    ret.text_len = tokenizer->at - ret.text;
                    tokenizer->at++;
                    break;
                }
                tokenizer->at++;
            }
        }break;
        unknown:
            ret.type = Token_unknown;
            
    }

    
    return ret;
}


Token get_nextelementrelatedtoken(Tokenizer *tokenizer, i32 *level)
{
    Token ret;
    do
    {
        
        ret = get_nexttoken(tokenizer);
        if(ret.type == Token_endelement) {--*level; break;};
        if(ret.type == Token_startelement){++*level; break;};
    }while (ret.type != Token_endofstream);

    return ret;
}

Token get_nextelementtoken(Tokenizer *tokenizer, i32 *level)
{
    Token ret;
    do
    {
        
        ret = get_nexttoken(tokenizer);
        if(ret.type == Token_endelement) --*level;
        if(ret.type == Token_startelement){++*level; break;};
    }while (ret.type != Token_endofstream);

    return ret;
}


struct AttribValuePair
{
    char *attrib;
    u32 attrib_len;

    char *value;
    u32 value_len;
};


//NOTE:: this function returns the element if found or 0 if it found a attribvaluepair
Token get_nextelementtoken_or_attribvalue_pair(Tokenizer *tokenizer, AttribValuePair *outpair, i32 *level)
{
    Token token;
    do
    {
        
        token = get_nexttoken(tokenizer);
        if(token.type == Token_attribname)
        {
            outpair->attrib = token.text;
            outpair->attrib_len = token.text_len;

            token = get_nexttoken(tokenizer);
            if(token.type == Token_attribvalue)
            {
                outpair->value = token.text;
                outpair->value_len = token.text_len;
            }
            token.type = Token_invalid;
            return token;
        }
        if(token.type == Token_endelement) --*level;
        if(token.type == Token_startelement){++*level; break;};
    }while (token.type != Token_endofstream);

    return token;
}


//NOTE:: this function returns 1 if it gets  the next attrib value pair
//       or null if it hits something else
b32 get_next_attribvalue_pair(Tokenizer *tokenizer, AttribValuePair *outpair)
{
    Token token;
    do
    {
        
        token = get_nexttoken(tokenizer);
        if(token.type == Token_attribname)
        {
            outpair->attrib = token.text;
            outpair->attrib_len = token.text_len;

            token = get_nexttoken(tokenizer);
            if(token.type == Token_attribvalue)
            {
                outpair->value = token.text;
                outpair->value_len = token.text_len;
            }
            return 1;
        }
        else
        {
            //we hit something else
            //maybe its the end of the attrib list for this element?
            backup_tokenizer(tokenizer);
            return 0;
        }
    }while (token.type != Token_endofstream);

    return 0;
}

Token get_nextelementortexttoken(Tokenizer *tokenizer, i32 *level)
{
    Token ret;
    do
    {
        
        ret = get_nexttoken(tokenizer);
        if(ret.type == Token_text){ break;};
        if(ret.type == Token_endelement) --*level;
        if(ret.type == Token_startelement){++*level; break;};
    }while (ret.type != Token_endofstream);

    return ret;
}

Token get_nextelementtoken_onlevel(Tokenizer *tokenizer, i32 *level, i32 desiredlevel)
{
    Token ret;

    do
    {
        ret = get_nexttoken(tokenizer);
        if(ret.type == Token_endelement) --*level;
        if(ret.type == Token_startelement)
        {
            ++*level;
            if(*level == desiredlevel)
                break;
        }

    }while (ret.type != Token_endofstream);

    return ret;
}

Token get_nexttoken(Tokenizer *tokenizer, i32 *level)
{
    Token ret;
    ret = get_nexttoken(tokenizer);
    if(ret.type == Token_endelement) --*level;
    if(ret.type == Token_startelement) ++*level;
    return ret;
}


Token get_nextelementtoken(Tokenizer *tokenizer)
{
    Token ret;
    do
    {
        
        ret = get_nexttoken(tokenizer);
        if(ret.type == Token_startelement){break;};
    }while (ret.type != Token_endofstream);

    return ret;
}

