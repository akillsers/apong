#include <stdio.h> // for sprintf, snprintf
#include "stringutils.cpp"
void Copy(void *dest, void *src, size_t size)
{
    u8 *src_at = (u8 *)src;
    u8 *dest_at = (u8 *)dest;
    while(size--)
    {
        *dest_at++ = *src_at++;
    }
}
