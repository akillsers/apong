#include <stdio.h>
#include <malloc.h>

internal void*
read_entire_file_and_nullterminate(char* path)
{
    if(path)
    {
	FILE * the_file = fopen((char *)path, "rb");
	char *raw_file_data;
	if(the_file)
	{
	    fseek(the_file, 0, SEEK_END);
	    size_t file_size = ftell(the_file);
	    rewind(the_file);

	    raw_file_data = (char *) malloc(file_size +1);
	    u32 bytesread = fread(raw_file_data, 1, file_size, the_file);
            fclose(the_file);
	    raw_file_data[bytesread] = 0;
	    return (void *) raw_file_data;
	}
	else
	{
	    return 0;
	}
    }
    return 0;
}

internal b32
write_entirefile(u8* path, void* writecontent, u64 writecontent_size)
{
    if(path)
    {
	FILE * the_file = fopen((char *)path, "wb");
	if(the_file)
	{
            if(fwrite ( (void *) writecontent , 1, writecontent_size, the_file ))
            {
                fclose(the_file);
                return true;
            }
	}
	else
	{
	    return false;
	}
    }
    return false;
}

