#define MAX_APA_NAME_LENGTH 10
#define TEX1C_HEADER_SIZE 8
#pragma pack(push, 1)
struct APAAssetFileHeader
{
    u8 magic[4];
    u16 version;
    u32 num_assets;
};

struct APAAssetHeader
{
    u16 asset_type;
    u32 data_offset_in_bytes;
    u32 size_of_data_in_bytes;
    u32 name_offset_in_bytes;
};


#if 1
enum AssetType
{
    unknown,
    tex1c,
    texture3c,
    sound,
    font,
    invalid,

    AssetType_count
};

#endif

struct Texture1c
{
    u32 OpenGLHandle; 


    u32 width;
    u32 height;

    //TODO way of accessing this (see bitwise ep 1)
    u8 pixels[1]; //width * height
};

struct FontGlyph
{
    u16 w;
    u16 h;
    f32 xoff;
    f32 yoff;
    f32 xadvance;
    f32 xoff2;
    f32 yoff2;
    f32 u, v, s, t;
};

struct FontDescriptor
{
    u32 first_unicode_codepoint;
    u32 numglyphs;
    u32 fontsize;
    f32 yadvance;
    f32 ascent;
    
    FontGlyph glyphs[0];
};

struct Font
{
    Texture1c *tex1c;
    FontDescriptor *descriptor;
};

struct Sound
{
    u16 num_channels;
    u32 total_frames;
    i16 data[1];
};

#pragma pack(pop)

//these bytes are put on top of the raw loaded data
u64 additional_required_bytes[] =
{
    0,
    4,              //texture1c for OpenGLHandle
    0,              //tex3c
    0,              //sound
    sizeof(Font) + additional_required_bytes[AssetType::tex1c],   //font
};

u32 null_asset_bytes_needed[] =
{
    0,
    sizeof(Texture1c),
    0, //tex3c
    sizeof(Sound), //sound
    sizeof(Font),
};

struct Asset
{
    //TODO or use a UUID to verify that we are the write asset?
    char name[MAX_APA_NAME_LENGTH];

    union
    {
        Texture1c *tex1c;
        Font *font;
        void *data_start;
        Sound *sound;
    };
    void *loaded_raw_data;
};

//NOTE::a prime number for better hashing!!
#define MAX_IN_MEMORY_ASSETS 2069 
struct Assets
{
    MemoryArena memory_arena;

    PlatformFileHandle file;
    u64 file_offset;
    
    //FOR NOW::the asset file headers and namechunks are loaded in mem permanently
    APAAssetHeader *headers; 
    u32 num_assets;
	//we need a hash table representation of our file
    char *name_chunk;

    Asset assets[MAX_IN_MEMORY_ASSETS];

    //TODO cleanup
    void *null_memory;
    Font *empty_font_memory;
};

//char *null_image;

