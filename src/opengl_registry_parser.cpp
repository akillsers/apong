#include "aks.h"

#include "fileio.cpp"
#include "stringutils.cpp"
#include "stringout.cpp"
#include "xmltokenizer.cpp"

struct GLCommand
{
    char *name;
    u32 namelen;

    char *returntype;
    u32 returntype_len;
    
    u32 num_params;
    char *paramtypes[256];
    u32 paramtypes_len[256];
};

global_variable GLCommand commands[4096];
global_variable u32 commandcount;

global_variable char *needed_command_name[4096];
global_variable u32 needed_command_name_len[4096];
global_variable u32 needed_command_count;

global_variable u32 required_version_major;
global_variable u32 required_version_minor;
global_variable b32 use_core_profile = false;
global_variable char *outputfilepath;
global_variable FILE *outputfile;

#define MAX_CHILD_NODES 4096
struct Node
{
    void *data;
    Node *parent_node;
    Node *child_nodes[MAX_CHILD_NODES];
    u32 num_child_nodes;
};

enum CommandNodeType
{
    CommandNodeType_unknown,
    CommandNodeType_command,
    CommandNodeType_proto,
    CommandNodeType_element,
    CommandNodeType_plaintext
};

struct CommandNodeData
{
    CommandNodeType nodetype;
    char *textcontents;
    u32 text_len;
};

inline void
node_addchild(Node *node, Node *childtoadd)
{
    assert(node->num_child_nodes < MAX_CHILD_NODES)
        node->child_nodes[node->num_child_nodes] = childtoadd;
    node->num_child_nodes++;
}

inline Node *
makenew_commandnode(Node *parentnode)
{
    Node *node = (Node *)calloc(1, sizeof(Node));
    node->data = (void*)calloc(1,sizeof(CommandNodeData));
    node_addchild(parentnode, node);
    return node;
}
inline Node *
makenew_commandnode_of_child_and_type(Node *parentnode, CommandNodeType type)
{
    Node *node = (Node *)calloc(1, sizeof(Node));
    node->data = (void*)calloc(1,sizeof(CommandNodeData));
    node_addchild(parentnode, node);
    CommandNodeData *nodedata = (CommandNodeData *)node->data;                    
    nodedata->nodetype = type;
    return node;
}

//NOTE:: this function expects outmajor and outminor to already be cleared to 0
internal b32
get_version_fromstr(char *str, u32 strlen, u32 *outmajor, u32 *outminor)
{
    u32 index = 0;
    while(index < strlen)
    {
        if(str[index] >= '0' && str[index] <= '9') //the pointer is at a number
        {
            *outmajor = *outmajor * 10 + str[index] - '0';
            index++;
        }
        else if (str[index] == '.')
        {
            index++;
            break;
        }
        else
            return false;

    }
#define ARBITUARY_MAX_PARAMS 38 //whatever! just a random number that can fit any number of possible params
    while(index < strlen)
    {
        if(str[index] >= '0' && str[index] <= '9') //the pointer is at a number
            *outminor = *outminor * 10 + str[index] - '0';
        else
            return false;
        index++;
    }
    return true;
}


internal void
parseglrequired(Tokenizer *tokenizer)
{
//NOTE:: start on level above feature
    i32 levelunderfeature = -1;
    Token token = {};
    
next_feature_notcurrentone:
    token = get_nextelementtoken(tokenizer, &levelunderfeature);   
next_feature:
    //jump to next feature
    for(;;)
    {
        if(string_equall(token.text, token.text_len, "feature"))
        {
            break; //we found it!!
        }
        else if (levelunderfeature <= 0 && !string_equall(token.text, token.text_len, "feature"))
        {
            goto done;
        }
        token = get_nextelementtoken(tokenizer, &levelunderfeature);   
    }

    //parse all attribs of this feature
    for(;;)
    {
        AttribValuePair pair;
        b32 nextpairexists = get_next_attribvalue_pair(tokenizer, &pair);

        if(!nextpairexists)
            //we haven't prematurely went to next_feature_notcurrentone and tested all attribs: allow this feature!!
            break; 

        if(string_equall(pair.attrib, pair.attrib_len, "api"))
        {
            //check for api name (we only care about gl api)
            if(!string_equall(pair.value, pair.value_len, "gl"))
            {
                goto next_feature_notcurrentone;
            }
        }
            
        if(string_equall(pair.attrib, pair.attrib_len, "number"))
        {
            //Check for version number                    

            u32 version_major = 0;
            u32 version_minor = 0;

            get_version_fromstr(pair.value, pair.value_len, &version_major, &version_minor);

            if(version_major > required_version_major)
            {
                goto next_feature_notcurrentone;
            }
            else if (version_major == required_version_major)
            {
                if(version_minor > required_version_minor)
                    goto next_feature_notcurrentone;
            }
        }
    }

    //printf("%.*s\n", token.text_len, token.text); //debug
    //we are now in a feature...now parse command info of this feature
    token = get_nextelementtoken(tokenizer, &levelunderfeature);
    for(;;)
    {


        if(string_equall(token.text, token.text_len, "require"))
        {
            int y =0;
            for(;;)
            {
                //get all the commands (and maybe later enums) that are required
                token = get_nextelementtoken(tokenizer, &levelunderfeature);

                if(levelunderfeature < 2)
                     break; //done getting everything under this require block
                if(string_equall(token.text, token.text_len, "command"))
                {
                    AttribValuePair command_attribpair = {};
                    get_next_attribvalue_pair(tokenizer, &command_attribpair);

                    //NOTE:: we expect the attrib name for the attrib value pair
                    //       to always be "name"
                    needed_command_name[needed_command_count] = command_attribpair.value;
                    needed_command_name_len[needed_command_count] = command_attribpair.value_len;
                    needed_command_count++;
                        
                    printf("Required command: %.*s \n", command_attribpair.value_len, command_attribpair.value);
                }
            }
                
        }
        else if (string_equall(token.text, token.text_len, "remove") && use_core_profile)
        {
            for(;;)
            {
                //get all the api features that need to be removed because of core profile
                //as of writing this code the only reason why there is a remove tag at all
                //in the registry is because of core/compatability profile
                token = get_nextelementtoken(tokenizer, &levelunderfeature);

                if(levelunderfeature < 2)
                    break; //done getting everything under this require block
                if(string_equall(token.text, token.text_len, "command"))
                {
                    AttribValuePair command_attribpair = {};
                    get_next_attribvalue_pair(tokenizer, &command_attribpair);

                    //NOTE:: we expect the attrib name for the attrib value pair
                    //       to always be "name"
                    for(int i = 0; i < needed_command_count; i++)
                    {
                        if(string_equall(command_attribpair.value, command_attribpair.value_len,
                                         needed_command_name[i], needed_command_name_len[i]))
                        {
                            //we found the command in the needed list
                            needed_command_name[i] = 0;
                            needed_command_name_len[i] = 0;
                        }
                    }
                    printf("Removed command: %.*s \n", command_attribpair.value_len, command_attribpair.value);

                }
            }
        }

            
        if(levelunderfeature <= 0)
            goto next_feature; //we are out of this feature
    }
#if 0
    if(token.type == Token_invalid) 
    {
        //Handle attrib
        if(string_equall(currentelement.text, currentelement.text_len, "feature"))
        {
        }

        //Debug
        printf("%.*s:   %.*s : %.*s\n", currentelement.text_len, currentelement.text,
               pair.attrib_len, pair.attrib, pair.value_len, pair.value);
    }
    else
    {
        //Handle element
        currentelement = token;
        if(levelunderfeature == 0)
        {
            if(string_equall(token.text, token.text_len, "feature"))
            {
                
            }
            else
            {
                //done parsing features
                break;
            }
        }
        else if(levelunderfeature == 1)
        {
                
        }
    }
#endif

done:;
    
}

internal void
parseglcommands(Tokenizer *tokenizer)
{
    //NOTE:: start on level above command
    i32 levelundercommand = -1;
    Token token;

    Node commands_root = {};
    Node *command_node = {};
    // Node *sub_command_node;
    // Node *sub_sub_command_node;
    // Node *sub_sub_sub_command_node;
#define MAX_LEVEL_UNDER_COMMAND 5
    Node *command_node_subs[MAX_LEVEL_UNDER_COMMAND] = {};

    token = get_nextelementtoken(tokenizer, &levelundercommand);    
    for(;;)
    {


        if((string_equall(token.text, token.text_len, "command") && levelundercommand == 0))
        {
            command_node = makenew_commandnode_of_child_and_type(&commands_root, CommandNodeType_command);            
        }

        do
        {
            token = get_nextelementortexttoken(tokenizer, &levelundercommand);
            
            if(token.type == Token_text)
                levelundercommand++;

            if(levelundercommand > 0 && levelundercommand <= MAX_LEVEL_UNDER_COMMAND)
            {
                Node *parent_node = levelundercommand == 1 ? command_node : command_node_subs[levelundercommand-2];
                Node *this_node;

                this_node = makenew_commandnode(parent_node);
                command_node_subs[levelundercommand-1] = this_node;

                CommandNodeData *node_data = (CommandNodeData *)this_node->data;                    

                if(token.type == Token_startelement)
                    node_data->nodetype = CommandNodeType_element;
                else if(token.type == Token_text)
                    node_data->nodetype = CommandNodeType_plaintext;
                else
                    node_data->nodetype = CommandNodeType_unknown;
                node_data->textcontents = token.text;
                node_data->text_len = token.text_len;

            }
            if(token.type == Token_text)
                levelundercommand--;


        }while(levelundercommand > 0);

#if 0
        //Debug
        for(int k = 0; k < command_node->num_child_nodes; k++)
        {
            Node *subnode = command_node->child_nodes[k];
            CommandNodeData *sub_command_data = (CommandNodeData *)subnode->data;
            printf("%d :sub command node: %.*s\n", sub_command_data->nodetype,
                   sub_command_data->text_len, sub_command_data->textcontents);            

            for(int i = 0; i < subnode->num_child_nodes; i++)
            {
                Node *subsubnode;
                subsubnode = subnode->child_nodes[i];
                CommandNodeData *sub_sub_command_data = (CommandNodeData *)subsubnode->data;
                printf("%d :sub sub command node: %.*s\n", sub_sub_command_data->nodetype,
                       sub_sub_command_data->text_len, sub_sub_command_data->textcontents);
                for(int j = 0; j < subsubnode->num_child_nodes; j++)
                {
                    Node *subsubsubnode = subsubnode->child_nodes[j];
                    CommandNodeData *sub_sub_sub_command_data = (CommandNodeData *)subsubsubnode->data;
                    printf("%d :sub sub sub command node: %.*s\n", sub_sub_sub_command_data->nodetype,
                           sub_sub_sub_command_data->text_len, sub_sub_sub_command_data->textcontents);
                }
            }
        }
#endif
        
        for(int s1 = 0; s1 < command_node->num_child_nodes; s1++)
        {
            Node *sub1_node = command_node->child_nodes[s1];
            CommandNodeData *sub1_nodedata = (CommandNodeData *)sub1_node->data;

            if(sub1_nodedata->nodetype == CommandNodeType_element)
            {
                if(string_equall(sub1_nodedata->textcontents, sub1_nodedata->text_len, "proto"))
                {
                    //Handle proto
                    for(int s2 = 0; s2 < sub1_node->num_child_nodes; s2++)
                    {
                        Node *sub2_node = sub1_node->child_nodes[s2];
                        CommandNodeData *sub2_nodedata = (CommandNodeData *)sub2_node->data;
                        
                        if(sub2_nodedata->nodetype == CommandNodeType_element &&
                           string_equall(sub2_nodedata->textcontents, sub2_nodedata->text_len, "name"))
                        {
                            //NOTE:: we expect only one sub3 node with text...so no for loop here
                            Node *sub3_node = sub2_node->child_nodes[0];
                            CommandNodeData *sub3_nodedata = (CommandNodeData *)sub3_node->data;
                            
                            if(sub3_node &&
                               sub3_nodedata->nodetype == CommandNodeType_plaintext)
                            {

                                if(string_inarray(sub3_nodedata->textcontents, sub3_nodedata->text_len,
                                                  needed_command_name, needed_command_name_len,
                                                  needed_command_count)){
                                    commands[commandcount].name = sub3_nodedata->textcontents;
                                    commands[commandcount].namelen = sub3_nodedata->text_len;
                                }
                                else
                                {
                                    //This is not a command we care about
                                    
                                    commands[commandcount].returntype = 0;
                                    goto nextcommand;
                                }
                            }
                            else
                            {
                                fprintf(stderr, "Invalid syntax:: expected text under <name> node\n");

                                commands[commandcount].returntype = 0;
                                goto nextcommand;
                            }
                        }
                        else if (sub2_nodedata->nodetype == CommandNodeType_element &&
                                 string_equall(sub2_nodedata->textcontents, sub2_nodedata->text_len, "ptype"))
                        {
                            //NOTE:: we expect only one sub3 node with text...so no for loop here
                            Node *sub3_node = sub2_node->child_nodes[0];
                            CommandNodeData *sub3_nodedata = (CommandNodeData *)sub3_node->data;


                            
                            if(sub3_node &&
                               sub3_nodedata->nodetype == CommandNodeType_plaintext)
                            {
                                if(commands[commandcount].returntype)
                                {
                                    //there is already a returntype do some concatenation

                                    //overwrite the text from the end of the last place with this
                                    //text (we can overwrite because we do not care about the text in between)
                                    //example
                                    //<proto group="String">const <ptype>GLubyte</ptype> *<name>glGetString</name></proto>
                                    char *dest = commands[commandcount].returntype + commands[commandcount].returntype_len;
                                    string_catl(dest, sub3_nodedata->textcontents, sub3_nodedata->text_len);
                                    commands[commandcount].returntype_len += sub3_nodedata->text_len;
                                }
                                else
                                {
                                    commands[commandcount].returntype = sub3_nodedata->textcontents;
                                    commands[commandcount].returntype_len = sub3_nodedata->text_len;
                                }

                            }
                            
                        }
                        else if (sub2_nodedata->nodetype == CommandNodeType_plaintext)
                        {
                            //NOTE:: the only possibility if a node under proto is plainttext is...
                            //       that this is the void return type

                                if(commands[commandcount].returntype)
                                {
                                    //same as above else if clause
                                    char *dest = commands[commandcount].returntype + commands[commandcount].returntype_len;
                                    string_catl(dest, sub2_nodedata->textcontents, sub2_nodedata->text_len);
                                    commands[commandcount].returntype_len += sub2_nodedata->text_len;
                                }
                                else
                                {
                                    commands[commandcount].returntype = sub2_nodedata->textcontents;
                                    commands[commandcount].returntype_len = sub2_nodedata->text_len;
                                }

                        }

                    }
                    
                }
                else if (string_equall(sub1_nodedata->textcontents, sub1_nodedata->text_len, "param"))
                {
                    //TODO reallocate or something to make this not a fixed size (WE WASTE SPACE)
                    //This needs a stretchy buffer type thing
                    char *concatenated_paramtype = (char *)malloc(128);
                    u32 concatenated_paramtype_len = 0;
                    
                    for(int s2 = 0; s2 < sub1_node->num_child_nodes; s2++)
                    {
                        Node *sub2_node = sub1_node->child_nodes[s2];
                        CommandNodeData *sub2_nodedata = (CommandNodeData *)sub2_node->data;

                        if(sub2_nodedata->nodetype == CommandNodeType_element &&
                           string_equall(sub2_nodedata->textcontents, sub2_nodedata->text_len, "name"))
                            continue; //NOTE:: discard the name (we only care about the parameter TYPE!!!!

                        Node *paramtype_textnode;

                        if(sub2_nodedata->nodetype == CommandNodeType_element &&
                           string_equall(sub2_nodedata->textcontents, sub2_nodedata->text_len, "ptype"))
                        {
                            paramtype_textnode = sub2_node->child_nodes[0];
                        }
                        else if (sub2_nodedata->nodetype == CommandNodeType_plaintext)
                        {
                            paramtype_textnode = sub2_node;
                        }
                        CommandNodeData *paramtype_textnodedata = (CommandNodeData *)paramtype_textnode->data;

                        string_catl(&concatenated_paramtype[concatenated_paramtype_len],
                                  paramtype_textnodedata->textcontents, paramtype_textnodedata->text_len);

                        concatenated_paramtype_len += paramtype_textnodedata->text_len;
                    }
                    commands[commandcount].paramtypes[commands[commandcount].num_params] = concatenated_paramtype;
                    commands[commandcount].paramtypes_len[commands[commandcount].num_params]
                        = concatenated_paramtype_len;
                    commands[commandcount].num_params++;
                }
            }
        }
        
        //NOTE:: check if the last character is a whitespace if so delete it
        if(commands[commandcount].returntype[commands[commandcount].returntype_len-1] == ' ')
            commands[commandcount].returntype_len--;

        commandcount++;
    nextcommand:
        if(levelundercommand < 0)
        {

            //NOTE:: done parsing commands
            break;
        }
    }
}

internal void
MakeSortedOpenglTypedefs(char *path)
{
    char *filetoprocess = (char *)read_entire_file_and_nullterminate(path);

    
    Tokenizer tokenizer = {};
    tokenizer.at = filetoprocess;

    b32 parsing = true;
    char *commands_loc = 0;
    while(parsing)
    {
        Token token = get_nexttoken(&tokenizer);
        switch(token.type)
        {
            case Token_startelement:
            {
                if(string_equall(token.text, token.text_len, "commands"))
                {
                    if(needed_command_count == 0)
                        commands_loc = tokenizer.at;
                    else
                        parseglcommands(&tokenizer);

                }
                else if (string_equall(token.text, token.text_len, "feature"))
                {
                    backup_tokenizer(&tokenizer); //Move the tokenizer back
                    parseglrequired(&tokenizer);
                    if(commands_loc != 0)
                    {
                        tokenizer.at = commands_loc;
                        parseglcommands(&tokenizer);
                        goto output_commands;
                    }
                }
            }break;
#if 0
            default:
            {
                printf("%s: %.*s\n", tokennames[token.type], token.text_len, token.text);
            }break;
#endif
            case Token_endofstream:
                parsing = false;
        }
    }

output_commands:
    u32 outputfile_headersize = sizeof(u32) * commandcount + 1 * sizeof(u32);
    u32 *commandoffsets = (u32 *)malloc(sizeof(u32) * commandcount);

    
    u32 current_offset = outputfile_headersize;
    //seek to where the actual info will go (not the header)
    fseek(outputfile, outputfile_headersize, 0);  
    for(int i = 0; i < commandcount; i++)
    {
        commandoffsets[i] = current_offset;        
        
        char buf[256] = {};
        DestBuf dest = {};
        dest.at = buf;
        dest.remaining_chars = arraycount(buf);

        outstringl(&dest, commands[i].name, commands[i].namelen);
        outchar(&dest, '\0');
        outstringl(&dest, commands[i].returntype, commands[i].ggreturntype_len);
        outchar(&dest, '\0');
        outnumber(&dest, commands[i].num_params);
        outchar(&dest, '\0');

        for(int j = 0; j < commands[i].num_params; j++)
        {
            outstringl(&dest, commands[i].paramtypes[j], commands[i].paramtypes_len[j]);
            outchar(&dest, '\0');
        }

        u32 chars_used = dest.at - buf;
        //str_copyl(&command_info_buffer[current_offset], buf, chars_used);
        
        fwrite(buf, sizeof(char), chars_used, outputfile);
        current_offset += chars_used * sizeof(char);
    }

    //rewind to the location of the header
    rewind(outputfile);
    fwrite(&commandcount, 1, sizeof(u32), outputfile);
    fwrite(commandoffsets, sizeof(u32), commandcount, outputfile);
    fclose(outputfile);
}
enum RegParseMode
{
    RegParseMode_nonespecified,
    RegParseMode_funcinfo,
    RegParseMode_typedefs,
    RegParseMode_header
};

int main(int argc, char *argv[])
{
    RegParseMode mode = RegParseMode_nonespecified;
    for (int i = 1; i < argc; i++)
    {
        if(argv[i][0] == '-' || argv[i][0] == '/')
        {
            //parse switch
            char *at = &argv[i][1];
            if(!is_whitespace(at[0]))
            {
                while(at[0])
                {
                    char *switch_text_start = at;
                    u32 switch_text_length = 0;
                    while(!(at[0] == '\0' || at[0] == ' ' || at[0] == ':'))
                        at++;
                    switch_text_length = at - switch_text_start;
                    if(string_equall(switch_text_start, switch_text_length, "help") ||
                       string_equall(switch_text_start, switch_text_length, "?"))
                    {
                        printf("%s is a utility for generating typedefs, etc. from the OpenGL registry. \n\n", argv[0]);
                        printf("    /mode:(function_info | typedefs | header)   what to generate\n");
                        printf("    /coreprofile                                use core OpenGL profile (%s uses compatability by default )\n", argv[0]);
                        printf("    /version:(version to use)                   OpenGL version to use (format is majorversion.minorversion)");
                        printf("    /output:(output file)                       output to this file\n");
                        return 0;
                    }
                    else if (string_equall(switch_text_start, switch_text_length, "coreprofile"))
                    {
                        use_core_profile = true;
                    }
                    else if (string_equall(switch_text_start, switch_text_length, "output"))
                    {
                        if(at[0] == ':' && at[1] && !is_whitespace(at[1]))
                        {
                            at++;
                            char *value_text_start = at;

                            outputfilepath = value_text_start;
                            outputfile = fopen((char *)outputfilepath, "wb");

                            if(!outputfile)
                            {
                                fprintf(stderr, "Error: The output file specified '%s' could not be opened for writing. Use /? for help. \n", value_text_start);
                                return -1;
                                
                            }
                            break;
                        }
                        else
                        {
                            fprintf(stderr, "No output path specified. Use /? for help. \n");
                            return -1;
                        }

                    }
                    else if (string_equall(switch_text_start, switch_text_length, "version"))
                    {
                        if(at[0] == ':' && at[1] && !is_whitespace(at[1]))
                        {
                            at++;
                            char *value_text_start = at;
                            u32 value_text_length = 0;
                            while(!(at[0] == '\0' || at[0] == ' '))
                                at++;
                            value_text_length = at - value_text_start;

                            b32 version_parseresult = get_version_fromstr(value_text_start, value_text_length,
                                                                          &required_version_major, &required_version_minor);

                            if(!version_parseresult)
                            {
                                fprintf(stderr, "'%.*s': Unknown version format. Use /? for help. \n", value_text_length, value_text_start);
                                return -1;
                            }
                        }
                        else
                        {
                            fprintf(stderr, "No version specified. Specify a version\n"); 
                            fprintf(stderr, "by putting the version after '/version:'. \n");
                            fprintf(stderr, "For more help use /?. \n");
                            return -1;
                        }
                    }
                    else if (string_equall(switch_text_start, switch_text_length, "mode"))
                    {
                        if(at[0] == ':' && at[1] && !is_whitespace(at[1]))
                        {
                            at++;
                            char *value_text_start = at;
                            u32 value_text_length = 0;
                            while(!(at[0] == '\0' || at[0] == ' '))
                                at++;
                            value_text_length = at - value_text_start;
                            if(string_equall(value_text_start, value_text_length, "function_info"))
                            {
                                mode = RegParseMode_funcinfo;
                            }
                            else if (string_equall(value_text_start, value_text_length, "typedefs"))
                            {
                                mode = RegParseMode_typedefs;
                            }
                            else if (string_equall(value_text_start, value_text_length, "header"))
                            {
                                mode = RegParseMode_header;
                            }
                            else
                            {
                                fprintf(stderr, "Unknown mode '%.*s '. Use /? for help. \n", value_text_length, value_text_start);
                                return -1;
                            }
                        }
                        else
                        {
                            goto nomode;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Unknown switch '%.*s'. Use /? for help. \n", switch_text_length, switch_text_start);
                        return -1;
                    }
                }
            }
           

                    
        }
    }

	switch (mode)
	{
	case RegParseMode_nonespecified:
	{
        nomode:
		fprintf(stderr, "No parsing mode specified. Specify a mode by putting the mode after '/mode:'\n");
		fprintf(stderr, "For more help use /?. \n");

		return -1;
	}break;
	case RegParseMode_funcinfo:
	{

            if(outputfilepath)
            {
                MakeSortedOpenglTypedefs("..\\headers\\opengl_registry.xml");                
            }
            else
            {
                fprintf(stderr, "No output path specified. Use /? for help. \n");
                return -1;
            }

	}break;
	case RegParseMode_header:
	{
		fprintf(stderr, "header mode is currently not supported.\n");
		return -1;

	}break;
	case RegParseMode_typedefs:
	{
		fprintf(stderr, "typedefs mode is currently not supported.\n");
		return -1;
	}break;
	}

    return 0;
}
