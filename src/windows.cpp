#if RENDER_OPENGL
#if RENDER_D3D
#error Please only specify one render target
#else
//#include opengl.cpp
#endif
#else
#if RENDER_D3D
//#include 
#else
#error Please specify a render target
#endif
#endif

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <timeapi.h>
//#include <Strsafe.h>
#include <Objbase.h>
#include <Audioclient.h>
#include <mmdeviceapi.h>
#include <dwmapi.h>
#include <versionhelpers.h>
#include <DbgHelp.h>
#include <shlobj.h>

#include <WInternl.h>
//#include <d3dkmthk.h>
#include "akgl.h"
#include <intrin.h>

#include "msvc.cpp"

typedef HANDLE PlatformFileHandle;
#include "apong_platform.h"
#include "render.h" // TODO: Can this file just be in apong.h?

#include "shared_funcs.cpp"
#include "debug_timers.cpp"
#include "win32.h"
#if RENDER_OPENGL
#include "opengl.cpp"
#elif RENDER_D3D
#include "d3d.cpp"   
#endif

#include "render.cpp" // TODO: Can this file just be in apong.cpp?
#include "apong.cpp"

internal void
win32_error_box(char *title, char *message)
{
    MessageBox(win32.window, message, title, MB_ICONERROR);
}

internal void
win32_close_window()
{
    ExitProcess(0);
}

internal void
win32_fatal_error(char *message)
{
    win32_error_box("Fatal Error", message);
    win32_close_window();
}

internal void
win32_display_error(char *message)
{
    int result = MessageBox(win32.window, message, "Error", MB_ICONERROR | MB_RETRYCANCEL);
    if(result == IDCANCEL)
    {
        win32_close_window();
    }
        
    //win32_error_box("Error", message);
}

internal int
win32_wchar_to_utf8(char *dest, u32 dest_size, WCHAR *src_text)
{
    int bytes_written =  WideCharToMultiByte(CP_UTF8,
                                             0, // dwFlags
                                             src_text,
                                             -1, 
                                             (char *) dest,
                                             dest_size,
                                             NULL, // lpDefaultChar
                                             NULL); // lpUsedDefaultChar
    assert(bytes_written);
    return bytes_written;
}

// TODO: This may be useful when we implement save game (win/loss record)
internal char *
win32_get_user_data_path()
{
	PWSTR known_folder_wide = 0;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, NULL, &known_folder_wide);
	assert(SUCCEEDED(hr));
    
	int str_len = lstrlenW(known_folder_wide);
	u64 alloc_size = str_len * 2;
	char *result = (char *)VirtualAlloc(0, alloc_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	int bytes_written = win32_wchar_to_utf8(result, alloc_size, known_folder_wide);
    
	assert(bytes_written);
	CoTaskMemFree(known_folder_wide);
    
	return result;
}

internal u32
win32_get_time_bias_minutes()
{
    TIME_ZONE_INFORMATION info;
    DWORD time_zone_id = GetTimeZoneInformation(&info);
    if(time_zone_id == TIME_ZONE_ID_UNKNOWN)
    {
        return info.Bias;
    }
    else if (time_zone_id == TIME_ZONE_ID_STANDARD)
    {
        return info.Bias + info.StandardBias;
    }
    else if (time_zone_id == TIME_ZONE_ID_DAYLIGHT)
    {
        return info.Bias + info.DaylightBias;
    }
    else
    {
        return false;
    }
}

internal b32
win32_update_cursor(b32 should_show)
{
    int current_counter = ShowCursor(should_show);
    int old_show = current_counter >= 0;
	
    if(should_show)
    {
        while (current_counter < 0)
        {
            current_counter = ShowCursor(true);
        }
    }
    else
    {
        while(current_counter >= 0)
        {
            current_counter = ShowCursor(false);
        }
    }

    win32.cursor_visible = should_show;

    return old_show;
}

internal void
win32_get_system_time(PlatformTime *time)
{
    SYSTEMTIME systime;
    GetSystemTime(&systime);
    
    time->year = systime.wYear;
    time->month = systime.wMonth;
    time->day = systime.wDay;
    time->hour = systime.wHour;
    time->minute = systime.wMinute;
    time->second = systime.wSecond;
    time->milliseconds = systime.wMilliseconds;
}

internal u64
win32_get_file_size(PlatformFileHandle handle)
{
    LARGE_INTEGER file_size_li;
    BOOL result = GetFileSizeEx(handle, &file_size_li);
    if(result == FALSE)
    {
        DWORD last_error = GetLastError();
        if(last_error == ERROR_INVALID_HANDLE)
        {
            stop("Invalid handle");
        }
        else
        {
            // TODO handle this
        }
    }
    
    return file_size_li.QuadPart;
}

internal FileError
win32_open_file_by_path(char *path, PlatformFileHandle *handle, u64 flags)
{
	DWORD dwDesiredAccess = GENERIC_READ;
	if(flags & WRITE_ACCESS)
	{
        dwDesiredAccess |= GENERIC_WRITE;
	}
    
    //TODO make all win32_read_file requests overlapped
    HANDLE file_handle = CreateFile(path,
                                    dwDesiredAccess,
                                    0, // no file sharing
                                    0,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL, 
                                    0);
    int last_error = GetLastError();
    if(file_handle == INVALID_HANDLE_VALUE)
    {
        if(last_error == ERROR_SHARING_VIOLATION)
        {
            return FileError_sharing_access_violation;
        }
        else if(last_error == ERROR_FILE_NOT_FOUND)
        {
            return FileError_nonexistent;
        }
        else if (last_error == ERROR_PATH_NOT_FOUND)
        {
            return FileError_path_not_found;
        }
        else
        {
            return FileError_unknown_error;
        }
    }
    else
    {
        handle[0] = file_handle;
        return FileError_success;
    }
    
}

internal FileError
win32_open_file(FileID file_id, PlatformFileHandle *file_handle)
{
    FileError result = FileError_unknown_error;
    PlatformFileHandle file = 0;

    switch(file_id)
    {
        case FileID_GameAssetsFile:
        {   
            char *asset_file_path = "data/apong_assets.apa";
            
            result = win32_open_file_by_path(asset_file_path, &file, 0);

            if(result != FileError_success)
            {
                char *exe_name = (char *)VirtualAlloc(0, WIN32_MAX_EXE_NAME_SIZE, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
                DWORD length = GetModuleFileNameA(0, exe_name, WIN32_MAX_EXE_NAME_SIZE);
                result = win32_open_file_by_path(exe_name, &file, 0);
                VirtualFree(exe_name, 0, MEM_RELEASE);
            }

            if(result != FileError_success)
            {
                // TODO: Logging.
                break;
            }
        } break;

        default:
        {
            result = FileError_invalid_file_id;
        } break;
    }

    *file_handle = file;
    return result;
}

internal void
win32_close_file(PlatformFileHandle handle)
{
    CloseHandle(handle);
}

//TODO open the file one time and read and write to it as we wish
internal FileError
win32_write_file(PlatformFileHandle file_handle, u64 offset, u32 num_bytes_to_write, u8 *data, b32 truncate = false)
{
    LARGE_INTEGER offset_li;
    offset_li.QuadPart = offset;
    SetFilePointerEx(file_handle, offset_li, 0, FILE_BEGIN);
    
    DWORD bytes_written;
    if(WriteFile(file_handle,
                 data,
                 num_bytes_to_write,
                 &bytes_written,
                 0))
    {
        if(truncate)
            SetEndOfFile(file_handle);
        return FileError_success;
    }
    else
    {
        DWORD last_error = GetLastError();
        if(last_error == ERROR_INVALID_HANDLE)
            return FileError_invalid_handle;
        else
            return FileError_unknown_error;
    }
}


internal FileError
win32_read_file(PlatformFileHandle file_handle, void *out, u64 offset, u32 num_bytes_to_read, b32 block = true)
{
    OVERLAPPED overlapped = {};
    //Are these offsets correct
    overlapped.Offset = offset & 0xffffffff;
    overlapped.OffsetHigh = (offset >> 32) & 0xffffffff;
    if (ReadFile(file_handle,
                 out,
                 num_bytes_to_read,
                 0, &overlapped))//numbytesread, overlapped struc
    {
        return FileError_success;
    }
    else
    {
        DWORD last_error = GetLastError();
        if(last_error == ERROR_IO_PENDING)
        {
            return FileError_success;
        }
        else if(last_error == ERROR_INVALID_HANDLE)
            return FileError_invalid_handle;
        else
            return FileError_unknown_error;
    }
}

/*
  keyboard  -> tagged keyboard keypad
  mice      -> tagged mouse    pointer
  joysticks -> tagged joystick gamepad 

  from
  http://www.usb.org/developers/hidpage/Hut1_12v2.pdf
*/
internal void
win32_init_rawinput(HWND window)
{
    RAWINPUTDEVICE Rid[2];
    
    Rid[0].usUsagePage = 0x01; 
    Rid[0].usUsage = 0x06; 
    Rid[0].dwFlags = RIDEV_INPUTSINK;   // adds HID keyboard and also ignores legacy mouse messages
    Rid[0].hwndTarget = window;
    
    Rid[1].usUsagePage = 0x01; 
    Rid[1].usUsage = 0x02; 
    Rid[1].dwFlags = RIDEV_INPUTSINK;   // adds HID mouse and also ignores legacy mouse messages
    Rid[1].hwndTarget = window;
    
    if (!RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])))
    {
        //registration failed. Call GetLastError for the cause of the error
        win32_fatal_error("Could not register raw input devices.");
    }
}

internal void *
win32_opengl_func_address(char *name)
{
    void *p = (void *)wglGetProcAddress(name);
    if(p == 0 ||
       (p == (void*)0x1) || (p == (void*)0x2) || (p == (void*)0x3) ||
       (p == (void*)-1) )
    {
        HMODULE module = LoadLibraryA("opengl32.dll");
        p = (void *)GetProcAddress(module, name);
        return p;
    }
    
    return p;
}



#if RENDER_OPENGL
internal void
win32_swap_buffers()
{
    
    SwapBuffers(win32.dc);
}


internal void
win32_init_opengl(HWND window)
{
    PIXELFORMATDESCRIPTOR pfd = {};
    
    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    
    //should we support compositin TODO alt tab is stuttery
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    
    pfd.cColorBits = 32; //color depth of framebuffer
    pfd.cDepthBits = 24;
    pfd.cStencilBits = 8;
    
    HDC dc = GetDC(window);
    
    int format = ChoosePixelFormat(dc, &pfd);
    PIXELFORMATDESCRIPTOR matched_pfd;
    DescribePixelFormat(dc, format, sizeof(PIXELFORMATDESCRIPTOR), &matched_pfd);
    //TODO check if matched_pfd is usable (almost unecessary)
    
    SetPixelFormat(dc, format, &pfd);
    
    HGLRC false_context = wglCreateContext(dc);
    
    wglMakeCurrent(dc, false_context);
    wglChoosePixelFormatARB = (p_wglChoosePixelFormatARB) win32_opengl_func_address("wglChoosePixelFormatARB");
    wglSwapIntervalEXT = (p_wglSwapIntervalEXT) win32_opengl_func_address("wglSwapIntervalEXT");
    
    if(wglChoosePixelFormatARB)
    {
        int format_attriblist[] = {WGL_DRAW_TO_WINDOW_ARB, 1,
            WGL_SUPPORT_OPENGL_ARB, 1,
            WGL_DOUBLE_BUFFER_ARB, 1,
            WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
            WGL_COLOR_BITS_ARB, 32,
            WGL_DEPTH_BITS_ARB, 24,
            WGL_STENCIL_BITS_ARB, 8,
            0};
        i32 returned_format;
        u32 returned_count = 0;
        wglChoosePixelFormatARB(dc,
                                format_attriblist,
                                0,
                                1,
                                &returned_format,
                                &returned_count);
        if(returned_count > 0)
        {
            wglCreateContextAttribsARB = (p_wglCreateContextAttribsARB) win32_opengl_func_address("wglCreateContextAttribsARB");
            if(wglCreateContextAttribsARB)
            {
                int context_attriblist[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
                    WGL_CONTEXT_MINOR_VERSION_ARB, 3,
                    WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
                    0};
                win32.opengl_render_context = wglCreateContextAttribsARB(dc, 0, context_attriblist);
                
                if(win32.opengl_render_context)
                {
                    wglMakeCurrent(dc, 0);
                    wglDeleteContext(false_context);
                    wglMakeCurrent(dc,win32.opengl_render_context);
                    //TODO manual vsync
                    wglSwapIntervalEXT(1);
                    opengl_load_functions();
                }
                else
                {
                    win32_fatal_error("Could not acquire an OpenGL context.");
                }
            }
            else
            {
                win32_fatal_error("Could not find wglCreateContextAttribsARB.");
            }
        }
        else
        {
            win32_fatal_error("Could not get a compatible pixel format during OpenGL context creation.");
        }
    }
    else
    {
        win32_fatal_error("Could not locate wglChoosePixelFormatARB.");
    }
}
#endif

internal void
win32_move_cursor(i32 x, i32 y)
{
    POINT cursorpt = {};
    GetCursorPos(&cursorpt);
    cursorpt.x += x;
    cursorpt.y += y;
    
    SetCursorPos(cursorpt.x, cursorpt.y);
}

internal void
win32_update_platformkey(Input *input, u32 keyID, u32 endeddown)
{
    if(input->key[keyID].down != endeddown)
    {
        input->key[keyID].down = endeddown;
        input->key[keyID].transitioncount++;
        input->key[keyID].down_repeats++;
    }
    else
    {
        //assert(input->key[keyID].down);
        //TODO is there any way to intercept the alt tab message???
        if(endeddown)
            input->key[keyID].down_repeats++;
    }
    
#if 0
    char buf[256];
    wsprintf(buf, "WM_INPUT: %s  down: %s transitioncount: %u \n",
             keyname[keyID], endeddown ? "True" : "False",  input->key[keyID].transitioncount);
    
    OutputDebugString(buf);
#endif
}

internal void
win32_update_platform_mousebutton(Input *input, MButton button, u32 endeddown)
{
    if(input->mouse[button].down != endeddown)
    {
        input->mouse[button].down = endeddown;
        input->mouse[button].transitioncount++;
    }

#if 0
    char buf[256];
    wsprintf(buf, "WM_INPUT: %s  down: %s transitioncount: %u \n",
             mbutton_name[button], endeddown ? "True" : "False",  input->mouse[button].transitioncount);
    
    OutputDebugString(buf);
#endif
}

internal void
push_inputmessageentry(InputMessageEntry entry)
{
    assert(win32.inputmessageentry_index < arraycount(win32.inputmessageentries));
    
    win32.inputmessageentries[win32.inputmessageentry_index] = entry;
    
    _WriteBarrier();
    _mm_sfence();
    
    win32.inputmessageentry_index = (win32.inputmessageentry_index + 1) % INPUTBUFFER_SIZE;
}

LRESULT CALLBACK window_proc(HWND   window,
                             UINT   message,
                             WPARAM wparam,
                             LPARAM lparam)
{
    LRESULT ret = 0;
    
    switch(message)
    {
        case WM_SYSKEYDOWN:
        {
            if(wparam == VK_F10 || wparam == VK_MENU)
            {
                
            }
            else
            {
                goto process_by_windows;
            }
        }break;
        
        case WM_DEVICECHANGE:
        {
            //d3d_recreate_if_needed();
        }break;
        case WM_SIZE:
        OutputDebugString("Size \n");
#if RENDER_D3D && 0
        //The REct needs to be invalidated
        InvalidateRect(win32.window, 0, TRUE);
#endif
        if(wparam == SIZE_MINIMIZED)
        {
            win32.minimized = true;
            OutputDebugString("min \n");
        }
        else
        {
            win32.minimized = false;
            platform.w_w = LOWORD(lparam);
            platform.w_h = HIWORD(lparam);

            char buf[1024];
            wsprintf(buf, "Dim: %d, %d\n", platform.w_w, platform.w_h);
            OutputDebugString(buf);
        }
        break;

        case WM_GETMINMAXINFO:
        {
            MINMAXINFO *minmax_info = (MINMAXINFO *) lparam;
            minmax_info->ptMinTrackSize.x = 640;
            minmax_info->ptMinTrackSize.y = 360;
        } break;

        case WM_MOVING:
        {
            if(win32.fullscreen)
            {
                //DON'T MOVE IF WE ARE FULLSCREEN
                RECT before_rect = {};
                GetWindowRect(win32.window, &before_rect);
                
                RECT *rect = (RECT *)lparam;
                rect[0] = before_rect;
            }
        }break;
#if 0		
		case WM_SETCURSOR:
		{
            
            WORD ht = LOWORD(lparam);
            if(ht == HTCLIENT)
            {
                OutputDebugString("We hit the client area!\n");
                win32_update_cursor(win32.cursor_visible);
                //ret = DefWindowProc(window, message, wparam, lparam);
            }
            else
            {
                ret = DefWindowProc(window, message, wparam, lparam);
            }
#if 0
            WORD ht = LOWORD(lparam);
            if(ht == HTCLIENT)
            {
                win32_update_cursor(win32.cursor_visible);
                ret = TRUE;
            }
            else
            {

                
            }

			if (ht == HTCLIENT)
			{
                win32_update_cursor(win32.cursor_visible);
			}
			else
			{
				win32_update_cursor(true);
			}
#endif
		} break;
#endif	
        case WM_MOUSEMOVE:    
        InputMessageEntry entry;
        entry.type = InputMessage_mousemove;
        push_inputmessageentry(entry);
        break;
        // case WM_LBUTTONDOWN:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_left;
        //     entry.mousebuttonmessage.wasUp = false;
        //     push_inputmessageentry(entry);
        // }break;
        // case WM_MBUTTONDOWN:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_middle;
        //     entry.mousebuttonmessage.wasUp = false;
        //     push_inputmessageentry(entry);
        // }break;
        // case WM_RBUTTONDOWN:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_right;
        //     entry.mousebuttonmessage.wasUp = false;
        //     push_inputmessageentry(entry);
        // }break;
        // case WM_LBUTTONUP:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_left;
        //     entry.mousebuttonmessage.wasUp = true;
        //     push_inputmessageentry(entry);
        // }break;
        // case WM_MBUTTONUP:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_middle;
        //     entry.mousebuttonmessage.wasUp = true;
        //     push_inputmessageentry(entry);
        // }break;
        // case WM_RBUTTONUP:
        // {
        //     InputMessageEntry entry;
        //     entry.type = InputMessage_mousebutton;
        //     entry.mousebuttonmessage.button = MButton_right;
        //     entry.mousebuttonmessage.wasUp = true;
        //     push_inputmessageentry(entry);
        // }break;
        
        case WM_SETFOCUS:
        {
            win32.has_focus = true;
        }break;
        case WM_ACTIVATE:
        {
			if(wparam == WA_INACTIVE)
			{
				win32.has_focus = false;
			}
        }break;
        
        case WM_INPUT:
        {
            u8 data[sizeof(RAWINPUT)] = {};
            u32 size = sizeof(RAWINPUT);
            i32 returnedbytes = GetRawInputData((HRAWINPUT) lparam, RID_INPUT,
                                                data, &size, sizeof(RAWINPUTHEADER));
            
            if(returnedbytes <= 0)
            {
                assert("GetRawInputData error");
            }
            
            RAWINPUT *raw = (RAWINPUT *)data;
            if(raw->header.dwType == RIM_TYPEKEYBOARD)
            {
                RAWKEYBOARD *rawkeyboard = (RAWKEYBOARD *)&raw->data.keyboard;
                
                u16 vKey = rawkeyboard->VKey;
                if(vKey == 255)
                {
                    //NOTE discard fake keys which are part of escape messages
                    break;
                }
                
                u16 scancode = rawkeyboard->MakeCode;
                
                u16 flags = rawkeyboard->Flags;
                b32 wasUp = (flags & RI_KEY_BREAK);
                b32 e0 = (flags & RI_KEY_E0);
                
                InputMessageEntry entry;
                entry.type = InputMessage_keyboard;
                entry.keyboardmessage.wasUp = wasUp;
                entry.keyboardmessage.e0 = e0;
                entry.keyboardmessage.scancode = scancode;
                
                push_inputmessageentry(entry);
            }
            else if(raw->header.dwType == RIM_TYPEMOUSE)
            {
                // OutputDebugString("WM_INPUT message!\n");

                RAWMOUSE *rawmouse = (RAWMOUSE *)&raw->data.mouse;
                
                InputMessageEntry entry;
                entry.type = InputMessage_mousebutton;
                //NOTE:: no overlapping bits (each flag is a bit)
                //therefore no need to do (flags & mask == mask)
#define HasFlag(mask) ((rawmouse->usButtonFlags & mask)) 
                if(HasFlag(RI_MOUSE_LEFT_BUTTON_DOWN))
                {
                    entry.mousebuttonmessage.button = MButton_left;
                    entry.mousebuttonmessage.wasUp = false;
                    push_inputmessageentry(entry);
                    // OutputDebugString("down");
                }
                if (HasFlag(RI_MOUSE_LEFT_BUTTON_UP))
                {
                    entry.mousebuttonmessage.button = MButton_left;
                    entry.mousebuttonmessage.wasUp = true;
                    push_inputmessageentry(entry);
                    // OutputDebugString("up");
                }
                
                if (HasFlag(RI_MOUSE_RIGHT_BUTTON_DOWN))
                {
                    entry.mousebuttonmessage.button = MButton_right;
                    entry.mousebuttonmessage.wasUp = false;
                    push_inputmessageentry(entry);
                }
                if (HasFlag(RI_MOUSE_RIGHT_BUTTON_UP))
                {
                    entry.mousebuttonmessage.button = MButton_right;
                    entry.mousebuttonmessage.wasUp = true;
                    push_inputmessageentry(entry);
                }
                
                if (HasFlag(RI_MOUSE_MIDDLE_BUTTON_DOWN))
                {
                    entry.mousebuttonmessage.button = MButton_middle;
                    entry.mousebuttonmessage.wasUp = false;
                    push_inputmessageentry(entry);
                }
                if (HasFlag(RI_MOUSE_MIDDLE_BUTTON_UP))
                {
                    entry.mousebuttonmessage.button = MButton_middle;
                    entry.mousebuttonmessage.wasUp = true;
                    push_inputmessageentry(entry);
                }
                
#undef HasFlag
            }
        }break;
        // case WM_ERASEBKGND:
        // {
        
        // }break;
        //POSSIBLE WAYS TO FIX THIS
        //GODOT/os_windows.cpp
        //https://gist.github.com/pervognsen/6a67966c5dc4247a0021b95c8d0a7b72
        case WM_ENTERSIZEMOVE:
        {
            
            win32.modal_sizemove = true;
            win32.move_timer = SetTimer(window, 1, USER_TIMER_MINIMUM, (TIMERPROC)0);
        }break;
        case WM_EXITSIZEMOVE:
        {
            win32.modal_sizemove = false;
            KillTimer(window, win32.move_timer);
        }break;
        case WM_TIMER:
        {
            SwitchToFiber(win32.main_fiber);
        }break;
#if 0
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC dc; 
            dc = BeginPaint(window, &ps);
            if(wglMakeCurrent(dc, opengl_render_context))
            {
                int y = 0;
            }
            SwapBuffers(dc);
            wglMakeCurrent(dc, 0);
            EndPaint(window, &ps);
        }break;
#endif
        case WM_CLOSE:
        global_running = false;
        break;
        process_by_windows:
        default:
        ret = DefWindowProc(window, message, wparam, lparam);
    }
    return ret;
}

LONG WINAPI
win32_exception_handler( PEXCEPTION_POINTERS exceptioninfo)
{
    HMODULE dbg_help = 0;
    HANDLE file = 0;
    
    b32 minidump_success = false;
    dbg_help = LoadLibrary("dbghelp.dll");
    if(!dbg_help)
    {
        goto done;
    }
    
    file = CreateFile("apong crashdump.dmp", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    if(file == INVALID_HANDLE_VALUE)
        goto done;
    
    MINIDUMP_EXCEPTION_INFORMATION mei;
    mei.ThreadId = GetCurrentThreadId();
    mei.ExceptionPointers = exceptioninfo;
    mei.ClientPointers = FALSE;
    // mei.CallbackRoutine = NULL;
    // mei.CallbackParam = 0;
    
    typedef BOOL (WINAPI *LPMINIDUMPWRITEDUMP)
        (HANDLE                            hProcess,
         DWORD                             ProcessId,
         HANDLE                            hFile,
         MINIDUMP_TYPE                     DumpType,
         PMINIDUMP_EXCEPTION_INFORMATION   ExceptionParam,
         PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
         PMINIDUMP_CALLBACK_INFORMATION    CallbackParam);
    
    LPMINIDUMPWRITEDUMP MiniDumpWriteDump = (LPMINIDUMPWRITEDUMP) GetProcAddress(dbg_help, "MiniDumpWriteDump");
    HANDLE hProcess = GetCurrentProcess();
    DWORD dwProcessId = GetCurrentProcessId();
    
    BOOL dump_written = MiniDumpWriteDump(hProcess, dwProcessId, file, MiniDumpNormal, &mei, 0, 0);
    if(dump_written)
        minidump_success = true;
    done:
    ShowWindow(win32.window, SW_HIDE);
    if(minidump_success)
    {
        MessageBox(0, "apong has crashed. A minidump has been saved in the executable's working directory. To help improve apong, please contact the developer about the crash with the minidump file.", "Error", MB_OK);
    }
    else
    {
        MessageBox(0, "apong has crashed. However, saving a minidump file has failed. To help improve apong, please contact the developer about the crash.", "Error", MB_OK);
        
    }
    return EXCEPTION_EXECUTE_HANDLER;
}

internal void
win32_process_inputmessages(Input *input, InputMessageEntry entry)
{
    switch(entry.type)
    {
        case InputMessage_mousemove:
        {
            //TODO handle woah it moved messages
            //input->mousex = entry.mousemessage.mousex;
            //input->mousey = entry.mousemessagef.mousey;
            input->mousemoved = true;
        }break;
        
        case InputMessage_mousebutton:
        {
            win32_update_platform_mousebutton(input, entry.mousebuttonmessage.button, !entry.mousebuttonmessage.wasUp);
        }break;
        case InputMessage_keyboard:
        {
            
            u16 scancode = entry.keyboardmessage.scancode;
            b32 wasUp = entry.keyboardmessage.wasUp;
            b32 e0 = entry.keyboardmessage.e0;
            
            u32 platformkey = win32_scancode_to_key[scancode];
            
            //TODO implement media key support
            if(e0)
            {
                //TODO Make this not control flow and into a lookup table??
                if(platformkey == KEY_enter)
                    platformkey = KEY_numpadenter;
                else if(platformkey == KEY_lCtrl)
                    platformkey = KEY_rCtrl;
                //else if(platformkey == KEY_lShift)
                //lfakeshift
                //else if(platformkey == KEY_rShift)
                //rfakeshift
                else if(platformkey == KEY_forwardslash)
                    platformkey = KEY_numpadslash;
                else if(platformkey == KEY_numpad7)
                    platformkey = KEY_home;
                else if(platformkey == KEY_numpad8)
                    platformkey = KEY_upArrow;
                else if(platformkey == KEY_numpad9)
                    platformkey = KEY_pageup;
                else if(platformkey == KEY_numpad4)
                    platformkey = KEY_leftArrow;
                else if(platformkey == KEY_numpad6)
                    platformkey = KEY_rightArrow;
                else if(platformkey == KEY_numpad1)
                    platformkey = KEY_end;
                else if(platformkey == KEY_numpad2)
                    platformkey = KEY_downArrow;
                else if(platformkey == KEY_numpad3)
                    platformkey = KEY_pagedown;
                else if(platformkey == KEY_numpad0)
                    platformkey = KEY_insert;
                else if(platformkey == KEY_numpadperiod)
                    platformkey = KEY_delete;
                else if(platformkey == KEY_numpadasterisk)
                    platformkey = KEY_printscreen;
                //*/printscr to ctrl printscr
                //scrollock to ctrl break
            }
            
            //if no focus only list for up keys so that we don't trigger anything important
            if(win32.has_focus || wasUp)
            {
                win32_update_platformkey(input, platformkey, !wasUp);                
            }
            
#if 0
            char buf[256];
            wsprintf(buf, "Key pressed: %s down: %s\n", keyname[platformkey], wasUp ? "False" : "True");
            OutputDebugString(buf);
#endif
#if 0
            //NOTE:: set flag keys
            if(e0)
                win32_update_platformkey(debge0, true);
            else
                win32_update_platformkey(platformkey, !wasUp);
            if(e1)
                win32_update_platformkey(platformkey, !wasUp);
            else
                win32_update_platformkey(platformkey, !wasUp);
#endif
            
            
            //     }
            //     else
            //     {
            //         int errnumber = GetLastError();
            //         int y = 0;
            //     }
            // }
            // else
            // {
            //     stop("TODO handle this scenario. Use memory arena???");
            // }
            //            }
        }break;
        default:
        stop("Unkown input message");
        break;
    }
}


inline u64
win32_query_perfcount()
{
    LARGE_INTEGER perfcountli;
    QueryPerformanceCounter(&perfcountli);
    return perfcountli.QuadPart;
}

struct
GameLoopThreadInfo
{
    Input *input;
    HWND window;
};

internal void
win32_toggle_fullscreen(HWND window)
{
#if 0 && RENDER_D3D && USE_EXCLUSIVE_FULLSCREEN
    //DEBUG::
    d3d_toggle_fullscreen();
#else
    //NOTE:: the following toggles into "fake fullscreen" not exclusive fullscreen (faster altabbing)
    //       we use if for RENDER_OPENGL
    DWORD window_style = GetWindowLong(window, GWL_STYLE);
    if (win32.fullscreen == false)
    {
        win32.fullscreen = true;
        win32.fullscreen_monitor_info.cbSize = sizeof(MONITORINFOEX);
        
        GetWindowPlacement(window, &win32.prefullscreen_window.placement);

        win32.prefullscreen_window.style = GetWindowLong(window, GWL_STYLE);
        win32.prefullscreen_window.ex_style = GetWindowLong(window, GWL_EXSTYLE);
        
        //TODO test if we should use default to nearest or primary
        //TODO FIDDLE WITH making the flickering disappear
        HMONITOR monitor = MonitorFromWindow(window, MONITOR_DEFAULTTONEAREST);
        win32.fullscreen_monitor = monitor;
        if(GetMonitorInfo(monitor, &win32.fullscreen_monitor_info)) {
            RECT rcClient;
            GetClientRect(window, &rcClient);
            
            win32.fullscreen_width = win32.fullscreen_monitor_info.rcMonitor.right - win32.fullscreen_monitor_info.rcMonitor.left;
            win32.fullscreen_height = win32.fullscreen_monitor_info.rcMonitor.bottom - win32.fullscreen_monitor_info.rcMonitor.top;

            platform.w_w = win32.fullscreen_width;
            platform.w_h = win32.fullscreen_height;

#if 0
            SetWindowPos(window, HWND_TOPMOST,
                         0, 0,
                         rcClient.right - rcClient.left,
                         rcClient.bottom - rcClient.top,
                         0);
#endif       
            // MoveWindow(window,
            //            win32.fullscreen_monitor_info.rcMonitor.left, win32.fullscreen_monitor_info.rcMonitor.top,
            //            win32.fullscreen_monitor_info.rcMonitor.right - win32.fullscreen_monitor_info.rcMonitor.left,
            //            win32.fullscreen_monitor_info.rcMonitor.bottom - win32.fullscreen_monitor_info.rcMonitor.top,
            //            TRUE);
            
            //SetWindowLongPtr(window, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
            //SetWindowLongPtr(window, GWL_EXSTYLE, WS_EX_TOOLWINDOW);
            //SetWindowLongPtr(window, GWL_STYLE, WS_VISIBLE | WS_POPUP);
            SetWindowLong(window, GWL_STYLE, win32.prefullscreen_window.style &~(WS_CAPTION | WS_THICKFRAME));
            SetWindowLong(window, GWL_EXSTYLE, win32.prefullscreen_window.ex_style &~(WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE | WS_EX_DLGMODALFRAME | WS_EX_STATICEDGE));
            // SetWindowPos(window, HWND_NOTOPMOST, 0, 0, 0, 0,
            //              SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
            SetWindowPos(window, HWND_NOTOPMOST, win32.fullscreen_monitor_info.rcMonitor.left, win32.fullscreen_monitor_info.rcMonitor.top,
                         win32.fullscreen_width,
                         win32.fullscreen_height,
                         SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
            
        }
    }
    else
    {
        win32.fullscreen = false;
        RECT restored_rect = win32.prefullscreen_window.placement.rcNormalPosition;
        if(!(win32.prefullscreen_window.placement.showCmd == SW_MAXIMIZE))
        {
            MoveWindow(window, restored_rect.left, restored_rect.top,
                       restored_rect.right - restored_rect.left,
                       restored_rect.bottom - restored_rect.top, TRUE);
        }
        
        //TODO going back needs some work
        // SetWindowPlacement(window, &win32.prefullscreen_window.placement);
        SetWindowLongPtr(window, GWL_STYLE, win32.prefullscreen_window.style | WS_VISIBLE);
        SetWindowLongPtr(window, GWL_EXSTYLE, win32.prefullscreen_window.ex_style);
        
        if(win32.prefullscreen_window.placement.showCmd == SW_MAXIMIZE)
        {
            WINDOWPLACEMENT placement;
            GetWindowPlacement(window, &placement);      
            placement.rcNormalPosition = win32.prefullscreen_window.placement.rcNormalPosition;
            SetWindowPlacement(window, &placement);
        }
        
        //TODO manually send SWP_framechanged???
        SetWindowPos(window, HWND_NOTOPMOST, 0, 0,
                     restored_rect.right - restored_rect.left,
                     restored_rect.bottom - restored_rect.top,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE|SWP_FRAMECHANGED);
        
        //SendMessage(window, WM_NCCALCSIZE, FALSE, (LPARAM)&restored_rect);
    }
#endif
}    


//TODO remove
struct AudioThreadInfo
{
    GameMemory *gamememory;
};

STDMETHODIMP WASAPIAudioNotificationClient::OnPropertyValueChanged(LPCWSTR device_id, const PROPERTYKEY key)
{
    return S_OK;
}

STDMETHODIMP WASAPIAudioNotificationClient::OnDeviceAdded(LPCWSTR device_id)
{
    return S_OK;
}

STDMETHODIMP WASAPIAudioNotificationClient::OnDeviceRemoved(LPCWSTR device_id)
{
    return S_OK;
}

STDMETHODIMP WASAPIAudioNotificationClient::OnDeviceStateChanged(LPCWSTR device_id, DWORD new_state)
{
    return S_OK;
}

internal void
win32_init_wasapi()
{
    HRESULT result;
    
    result = CoCreateInstance(__uuidof(MMDeviceEnumerator), 0,
                              CLSCTX_ALL,
                              __uuidof(IMMDeviceEnumerator), (void **) &win32.device_enumerator);
    
    win32.device_enumerator->RegisterEndpointNotificationCallback((IMMNotificationClient *)&win32.audio_notification_client);
    if(result != S_OK)
    {
        win32_error_box("Error", "Could not initialize WASAPI.");
    }
    win32.stream_switch = CreateEvent(0,0,0,0);
}

internal b32
win32_wasapi_setup_default_device()
{
    HRESULT result;
    
    //TODO how much faster is coinit_speed_over_memory
    //TODO why mu.cpp use coinitbase_multithreaded
    //TODO what is CLSCTX_ALL vs inproc server
    //TODO what is IID_PPV_ARGS(&device_enumerator) even for
    //TODO i think this will always be unresolved::
    //     WTF is MMdevice vs IMMdevice and just what
    //IMMDevice *audio_device = 0;
    
    win32.device_enumerator->GetDefaultAudioEndpoint(eRender, eConsole, &win32.audio_device);
    if(!win32.audio_device)
    {
        win32_error_box("Error", "Call to win32.device_enumerator->GetDefaultAudioEndpoint() failed.");
        return false;
    }
    
    
    /*
    TODO Some possible return values 
    HANDLE THEM ALL or not all
    E_NOINTERFACE 
    E_POINTER 
    E_INVALIDARG 
    E_OUTOFMEMORY 
    AUDCLNT_E_DEVICE_INVALIDATED 
    */
    
    IAudioClient *audio_client = 0;
    result = win32.audio_device->Activate(__uuidof(IAudioClient),
                                          CLSCTX_INPROC_SERVER, 0,
                                          (void **)&audio_client);
    if(result != S_OK)
    {
        win32_error_box("Error", "Call to win32.audio_device->Activate() failed.");
        return false;
    }
    //TODO some usefule stream flags (second param) include
    //AUDCLNT_STREAMFLAGS_EVENTCALLBACK, AUDCLNT_STREAMFLAGS_RATEADJUST and AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM 
    //TODO it says the buffer duration is expressed in 100 nanosecond units
    //     is that what the * 10 is for?? how much is 100 nanoseconds 
    
    
    WAVEFORMATEX audio_format;
    audio_format.wFormatTag = WAVE_FORMAT_PCM;
    audio_format.nChannels = 2; //test for supported nchannels
    audio_format.nSamplesPerSec = 44100; //test for support samples per sec
    audio_format.wBitsPerSample = 16; //test for supported bits per sample
    audio_format.nBlockAlign = (audio_format.nChannels * audio_format.wBitsPerSample) / 8;
    audio_format.nAvgBytesPerSec = audio_format.nSamplesPerSec * audio_format.nBlockAlign;
    audio_format.cbSize = 0;
    
    /*
      TODO
      is it worth it to call 
      HRESULT GetMixFormat(
          [out] WAVEFORMATEX **ppDeviceFormat
      );

      or after initialize::
      HRESULT GetBufferSize(
         [out] UINT32 *pNumBufferFrames
      );
     */
    //DEBUG
#if INTERNAL
    WORD testformat1 = WAVE_FORMAT_EXTENSIBLE;
    WORD testformat2 = WAVE_FORMAT_MPEG;
    WORD testformat3 = WAVE_FORMAT_MPEGLAYER3;
#endif
    
    
#define WASAPI_USE_AUTOCONVERT_PCM
    DWORD flags = AUDCLNT_STREAMFLAGS_EVENTCALLBACK;
#if !defined(WASAPI_USE_AUTOCONVERT_PCM)
    flags |= 0;
    WAVEFORMATEX *suggested_format;
    result = audio_client->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED, &audio_format, &suggested_format);
    if (result == S_FALSE)
    {
        if(audio_format.nSamplesPerSec != suggested_format->nSamplesPerSec)
        {
            //we need to convert pcm sample rate!!!!
        }
        else if (audio_format.nChannels != suggested_format->nChannels)
        {
            //we need to accomodate this scenario
        }
        audio_format = *suggested_format;
        audio_format.wFormatTag = WAVE_FORMAT_PCM; //we need PCM
        audio_format.nChannels = 2;                //right now we only support //TODO does this work with 5.1??
        audio_format.cbSize = 0;                   //...PCM has no extra data
        audio_format.wBitsPerSample = 16;          //we only support 16 bits per sample
        audio_format.nBlockAlign = (audio_format.nChannels * audio_format.wBitsPerSample) / 8;
        audio_format.nAvgBytesPerSec = audio_format.nSamplesPerSec * audio_format.nBlockAlign;
        
        result = audio_client->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED, &audio_format, &suggested_format);
        if(result != S_OK)
        {
            win32_error_box("Error", "Could not find a suitable WASAPI audio output format!");
        }
    }
    else if (result == S_OK)
    {
        
    }
    else
    {
        //AUDCLNT_E_DEVICE_INVALIDATED
        //AUDCLNT_E_SERVICE_NOT_RUNNING
        win32_error_box("Error", "Call to audio_client->IsFormatSupported() failed");
    }
    
    CoTaskMemFree(suggested_format);
#else
    flags |= AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM | AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY;
#endif
    win32.audio_output_format.samplerate = audio_format.nSamplesPerSec;
    win32.audio_output_format.bitdepth = audio_format.wBitsPerSample;
    win32.audio_output_format.numchannels = audio_format.nChannels;
    
    REFERENCE_TIME audio_buffer_duration = 100 * 1000 * 10; //100 milliseconds
    
    result = audio_client->Initialize(AUDCLNT_SHAREMODE_SHARED, flags,
                                      audio_buffer_duration,
                                      0, //non zero only in exclusive mode
                                      &audio_format,
                                      0);//seission guid
    if(result != S_OK)
    {
        win32_error_box("Error", "Call to win32.audio_client->Initialize() failed.");
        return false;
    }

    if(!win32.audio_buffer_ready)
        win32.audio_buffer_ready = CreateEvent(0, 0, 0, 0);

    result = audio_client->SetEventHandle(win32.audio_buffer_ready);
    
    //DEBUG::
    //HRESULT test = AUDCLNT_E_NOT_INITIALIZED;
    if(result != S_OK)
    {
        win32_error_box("Error", "Call to win32.audio_client->SetEventHandle() failed.");
        return false;
    }
    
    IAudioRenderClient *audio_render_client;
    result = audio_client->GetService(IID_PPV_ARGS(&audio_render_client));
    if(result != S_OK)
    {
        win32_error_box("Error", "Call to win32.audio_client->GetService() failed.");
        return false;
    }
    
    result = audio_client->GetBufferSize(&win32.buffer_frame_count);
    if(result != S_OK)
    {
        win32_error_box("Error", "Call to win32.audio_client->GetBufferSize() failed.");
        return false;
    }
    
    /*NOTE:: To avoid start-up glitches with rendering streams, clients should not call Start until the audio engine has been initially loaded with data by calling the IAudioRenderClient::GetBuffer and IAudioRenderClient::ReleaseBuffer methods on the rendering interface.*/
    win32.audio_render_client = audio_render_client;
    win32.audio_client = audio_client;
    
    //device_enumerator->Release();
    //win32.audio_device->Release();
    //win32.audio_should_stop = false;
    if(win32.audio_threadproc)
    {
        CloseHandle(win32.audio_threadproc);
    }
        
    win32.audio_threadproc = CreateThread(0, //security
                                          0, //stack size
                                          win32_audio_threadproc,
                                          0, //params
                                          0, //create flags
                                          0); //threadid
    
    return true;
}


internal void
win32_audio_reset()
{
    win32.audio_client->Stop();
    win32.audio_render_client->Release();
    win32.audio_client->Release();
    win32.audio_device->Release();
    win32_wasapi_setup_default_device();
}

DWORD WINAPI
win32_audio_threadproc(LPVOID lpParameter)
{
    HRESULT result;
    AudioThreadInfo *threadinfo = (AudioThreadInfo *)lpParameter;
    
    //NOTE:: Fill it with zero first to avoid start up glitches
    BYTE *data = 0;
    result = win32.audio_render_client->GetBuffer(win32.buffer_frame_count, &data);
    if(result == AUDCLNT_E_DEVICE_INVALIDATED)
    {
        win32_audio_reset();
        goto done;
    }
    else if (result != S_OK)
    {
        //TODO better logging
        MessageBox(win32.window, "Call to win32.audio_render_client->GetBuffer() failed.", "Error", MB_ICONERROR);
    }

    win32.audio_render_client->ReleaseBuffer(win32.buffer_frame_count, AUDCLNT_BUFFERFLAGS_SILENT);
    
    result = win32.audio_client->Start();
    while(global_running)
    {
        wait: 
        HANDLE wait_array[] = {win32.audio_buffer_ready, win32.stream_switch};
        switch(WaitForMultipleObjects(2, wait_array, FALSE, INFINITE))
        {
            case WAIT_OBJECT_0:
            {
                //the audio buffer is ready
            }break;
            case( WAIT_OBJECT_0 + 1): //we need to switch our stream
            {
                win32_audio_reset();
                goto done;
            }break;
            default:
                stop("wait for single object has failed");
            break;
        }
        //TODO check if buffer has been overwritten???
        u32 padding_frames;
        win32.audio_client->GetCurrentPadding(&padding_frames);
        u32 availible_frames = win32.buffer_frame_count - padding_frames;
        
        SoundBuffer sbuf;
        sbuf.numchannels = win32.audio_output_format.numchannels;
        sbuf.samplerate= win32.audio_output_format.samplerate;
        sbuf.bitdepth = win32.audio_output_format.bitdepth;
        sbuf.availible_audio_frames = availible_frames;
        
        BYTE *data = 0;
        
        HRESULT result = win32.audio_render_client->GetBuffer(availible_frames, &data);
        if(result != S_OK)
        {
            switch(result)
            {
                case AUDCLNT_E_DEVICE_INVALIDATED:
                {
                    win32_audio_reset();
                    goto done; //this thread will be called again
                    break;
                }
                    
                case AUDCLNT_E_BUFFER_ERROR:
                {
                    //TODO comfirm this continues out of the for statement
                    //TODO do we need to release the buffer here??
                    OutputDebugString("Buffer internal error!");
                    continue; 
                    break;
                }

                case AUDCLNT_E_OUT_OF_ORDER:
                {
                    win32_error_box("Error", "win32.audio_render_client->GetBuffer returned AUDCLNT_E_OUT_OF_ORDER");
                } break;

                default:
                {
                    stop("Handle this");
                } break;
            }
        }
        sbuf.data = data;
        game_output_sound(&sbuf, &win32.game_memory);
        win32.audio_render_client->ReleaseBuffer(availible_frames, 0);
    }
    
    done:
    return 0;
}


STDMETHODIMP WASAPIAudioNotificationClient::OnDefaultDeviceChanged(EDataFlow flow, ERole role, LPCWSTR new_default_device_id)
{
    if(flow == eRender && role == eConsole)
    {
        //init the new device
#define CHECK_NEW 0
#if CHECK_NEW
        HRESULT result;
        LPWSTR current_device_id = {};
        result = win32.audio_device->GetId(&current_device_id);
        if(!(current_device_id == new_default_device_id && result == S_OK))
        {
#endif
            SetEvent(win32.stream_switch);
            // win32.audio_should_stop = true;
            // WaitForSingleObject(win32.audio_threadproc, INFINITE);
            // OutputDebugString("over the wat\n");
            // win32.audio_render_client->Release();
            // win32.audio_client->Release();
            // win32.audio_device->Release();
            
            // win32_wasapi_setup_default_device();
            // win32_start_audio_thread();
#if CHECK_NEW
        }
#endif
    }
    
    return S_OK;
}

internal void
win32_process_messages()
{
    for(;;)
    {
        MSG message;
        //TODO replace with getmessage
        while(PeekMessage(&message, 0, WM_PAINT - 1, 0, PM_REMOVE))
        {
            TranslateMessage(&message);
            DispatchMessage(&message);
        }
        while(PeekMessage(&message, 0, WM_PAINT + 1, 0, PM_REMOVE))
        {
            TranslateMessage(&message);
            DispatchMessage(&message);
        }
        SwitchToFiber(win32.main_fiber);
        
    }
}

#if 0
internal void 
win32_error(PlatformError type, char *message)
{
    UINT icon = MB_ICONWARNING;
    char *caption = "apong Warning";
    
    if(type == PlatformError_fatal)
    {
        caption = "apong Fatal Error";
        icon = MB_ICONSTOP;
    }
    
    MessageBox(win32.window, message, caption, icon);
    
    if(type == PlatformError_fatal)
    {
        ExitProcess(-1);
    }
}
#endif
int CALLBACK WinMain(HINSTANCE instance,
                     HINSTANCE,
                     LPSTR,
                     int)
{
    if(!IsWindowsVistaOrGreater())
    {
        win32_fatal_error("apong requires Windows Vista or greater.");
    }
    
    SetUnhandledExceptionFilter(win32_exception_handler);
    
	HRESULT coinit_result = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	assert(coinit_result == S_OK);
	
    //TODO how does notepad++ get away with not using com components???
    
    WNDCLASS window_class = {};
    window_class.style = CS_OWNDC;
    window_class.lpfnWndProc = window_proc;
    window_class.hInstance = instance;
    window_class.hCursor = LoadCursor(0, IDC_ARROW);
    window_class.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    window_class.lpszClassName = "apong Window Class";
    
    if(RegisterClass(&window_class))
    {
        RECT window_rect = {};
        HWND window = CreateWindow(window_class.lpszClassName,
                                   "apong",
                                   WS_OVERLAPPEDWINDOW,
                                   CW_USEDEFAULT,
                                   CW_USEDEFAULT,
                                   CW_USEDEFAULT,
                                   CW_USEDEFAULT,
                                   0, 0,
                                   instance,
                                   0);
        SetMenu(window, 0);
        win32.dc = GetDC(window);
        
        if(window)
        {
            win32.window = window;
            
            global_running = true;

            //Hook up platform service calls
            platform.opengl_func_address = win32_opengl_func_address;
            platform.open_file = win32_open_file;
            platform.get_file_size = win32_get_file_size;
            platform.close_file = win32_close_file;
            platform.read_file = win32_read_file;
            platform.write_file = win32_write_file;
            platform.get_time = win32_get_system_time;
            platform.get_time_bias_minutes = win32_get_time_bias_minutes;
            platform.shut_off = win32_close_window;
            platform.display_error = win32_display_error;            
#if RENDER_OPENGL
            platform.swap_buffers = win32_swap_buffers;
            
            // TODO: Why the heck are we using this as our initial framebuffer size?
            //       Can't we avoid recreating framebuffers if we use the actual size of the window
            //       as the initial size?
            Vec2u framebuffer_initial_size = {1,1};
            win32_init_opengl(window);
            opengl_init_draw(framebuffer_initial_size.x, framebuffer_initial_size.y);
#elif RENDER_D3D
            d3d_init(window);
#endif
#if (INTERNAL == 0)
            win32_toggle_fullscreen(window);
#endif

            HICON hicon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1));

            if(hicon)
            {
                SendMessage(window, WM_SETICON, ICON_SMALL, (LPARAM)hicon);
                SendMessage(window, WM_SETICON, ICON_BIG, (LPARAM) hicon);
                DestroyIcon(hicon);
            }
            
            ShowWindow(win32.window, SW_SHOW);
            win32_init_rawinput(window);
            
            //Initialize game memory
            i32 error = timeBeginPeriod(1);
            if(error)
            {
                // TODO: Actually call get time dev caps...
                stop("we need to call get time dev caps to ensure this is possible");
            }
                
            
            void *memory_for_game = VirtualAlloc(0, GAME_STORAGE_SIZE, MEM_COMMIT, PAGE_READWRITE);
            
            if(!memory_for_game)
            {
                i32 error = GetLastError();
                win32_fatal_error("Could not allocate enough RAM for the game!");
            }

            win32.game_memory.permanent_storage = (u8 *)memory_for_game;
            win32.game_memory.permanent_storage_size = PERMANENT_STORAGE_SIZE;
            
            win32.game_memory.scratch_storage = (u8 *)win32.game_memory.permanent_storage + PERMANENT_STORAGE_SIZE;
            win32.game_memory.scratch_storage_size = SCRATCH_STORAGE_SIZE;
            
#if INTERNAL
            void *memory_for_debug = VirtualAlloc(0, 
                                                     DEBUG_STORAGE_SIZE, MEM_COMMIT, PAGE_READWRITE);
            
            if(!memory_for_debug)
            {
                stop("Couldn't allocate memory for debug!");
            }
            
            win32.game_memory.debug_storage = (u8 *)memory_for_debug;
            win32.game_memory.debug_storage_size = DEBUG_STORAGE_SIZE;
#endif
            game_init_memory(&win32.game_memory);
            
            win32_init_wasapi();
            win32_wasapi_setup_default_device();
            
            char *command_line = GetCommandLine();
            game_process_command_line(command_line, &win32.game_memory);
            
            AudioThreadInfo athreadinfo;
            athreadinfo.gamememory = &win32.game_memory;
            //win32_start_audio_thread();
            // win32.audio_threadproc = CreateThread(0, //security
            //                                       0, //stack size
            //                                       win32_audio_threadproc,
            //                                       &athreadinfo, //params
            //                                       0, //create flags
            //                                       0); //threadid
            // HANDLE vsync_threadproc = CreateThread(0, //security
            //                                        0, //stack size
            //                                        win32_vsync_threadproc,
            //                                        0, //params
            //                                        0, //create flags
            //                                        0); //threadid
            
            win32.main_fiber = ConvertThreadToFiber(0);
            assert(win32.main_fiber);
            win32.message_fiber = CreateFiber(0, (LPFIBER_START_ROUTINE)win32_process_messages, 0);
            assert(win32.message_fiber);
            
            u32 targetFPS = 1000;
            
            LARGE_INTEGER perf_freqli;
            QueryPerformanceFrequency(&perf_freqli);
            win32.perf_freq = perf_freqli.QuadPart; //Performance counter frequency (counts per second)
            f64 perf_freqms = (f64)win32.perf_freq / 1000;    //Performance counter frequency (counts per millisecond)
            
            u64 perf_startcount = win32_query_perfcount();
            
            u64 perf_targetcount = win32.perf_freq / targetFPS;
            f64 real_targetms = (f64)perf_targetcount / win32.perf_freq;
            u64 perf_countsbehind = 0;
            u64 perf_frametimecount = 0;
            u64 perf_startup = perf_startcount;
            // win32.dc = GetDC(window);
            
            win32.cursor_visible = true;

#if INTERNAL
#define PERF_START(name) u64 perf_##name = win32_query_perfcount();
#define PERF_END(name) perf_##name = win32_query_perfcount() - perf_##name;
#else
#define PERF_START(name)
#define PERF_END(name)
#endif
            f64 dt_from_startup;
            while(global_running)
            {
                PERF_START(input);
                // if(win32.input.key_pressed(KEY_space))
                //     framemode = !framemode;
                // if(framemode)
                // {
                //     win32.input.dt = 0;
                //     //win32.input.fixed_dt = 0;
                //     if(win32.input.key_pressed(KEY_leftArrow))
                //     {
                //         win32.input.dt = -((f32)1 / targetFPS);
                //         //win32.input.fixed_dt = -((f32)1 / targetFPS);
                //     }
                
                //     else if(win32.input.key_pressed(KEY_rightArrow))
                //     {
                //         win32.input.dt = ((f32)1 / targetFPS);
                //         //win32.input.fixed_dt = ((f32)1 / targetFPS);
                //     }
                
                // }
                for(int i = 0; i < KEY_count; i++)
                {
                    //TODO make it so that it only clear keys that need to be cleared
                    //time this and see
                    win32.input.key[i].transitioncount = 0;
                    win32.input.key[i].down_repeats = 0;
                }
                for(int i = 0; i < MButton_count; i++)
                {
                    win32.input.mouse[i].transitioncount = 0;
                }
                win32.input.mousemoved = false;
                
                //win32_process_messages();
                SwitchToFiber(win32.message_fiber);
                
                //Section:: HANDLE INPUT
                //TODO if we don't remap this to framebuffer we can just do this when the mouse moves
                //     with process input messages
                POINT cursor_point;
                GetCursorPos(&cursor_point);
                ScreenToClient(window, &cursor_point);
                win32.input.mousex = (f32)cursor_point.x;
                win32.input.mousey = (f32)cursor_point.y;
                
                while(win32.inputmessageentry_nexttodo != win32.inputmessageentry_index)
                {
                    win32_process_inputmessages(&win32.input,
                                                win32.inputmessageentries[win32.inputmessageentry_nexttodo]);
                    win32.inputmessageentry_nexttodo = (win32.inputmessageentry_nexttodo + 1) % INPUTBUFFER_SIZE;
                }
                win32.input.has_focus = win32.has_focus;
                
                if((win32.input.key[KEY_lAlt].down || win32.input.key[KEY_rAlt].down) &&
                   win32.input.key_pressed(KEY_f4))
                {
                    global_running = false;
                    break;
                }
                
                if (win32.input.key_pressed(KEY_printscreen))
                {
                    keybd_event(VK_SNAPSHOT, 0x37, KEYEVENTF_EXTENDEDKEY, 0);
                }
                
                if(win32.input.key_pressed(KEY_f11))
                {
                    win32_toggle_fullscreen(window);
                }
                
                PERF_END(input);
                PERF_START(game_update);
                
                RenderList *rlist;
                RenderTarget *rtarget;
                game_update_and_render(&win32.input, &win32.game_memory, &rlist, &rtarget);
                
                PERF_END(game_update);

                if(!rtarget->should_show_cursor && win32.cursor_visible && win32.fullscreen)
                {
                    OutputDebugString("Unshowing cursor!\n");
                    win32_update_cursor(false);
                }
                else if(!win32.cursor_visible && (rtarget->should_show_cursor || !win32.fullscreen))
                {
                    OutputDebugString("Showing cursor!\n");
                    win32_update_cursor(true);
                }

                PERF_START(render);

                if(!win32.minimized)
                {
#if RENDER_OPENGL
                    renderlist_to_screen_opengl(rlist, rtarget);
#elif RENDER_D3D
                    renderlist_to_screen_d3d(rlist, rtarget);
#endif
                }
                PERF_END(render);
                PERF_START(clock_stablizer);
                
#if INTERNAL
                game_post_update_and_render(&win32.game_memory);
#endif
                
                //win32_blitframebuffer_toscreen();
                
                //UpdateWindow(window);
                u64 adjustedtargetcount = (perf_targetcount >= perf_countsbehind)
                    ? perf_targetcount-perf_countsbehind : 0;  
                
                u64 perf_thiscount = win32_query_perfcount();
                perf_frametimecount = perf_thiscount - perf_startcount;
                
#if ALLOW_SLEEP
                if(perf_frametimecount + (win32.perf_freq/1000)< adjustedtargetcount)
                {
                    // More than a millisecond is availible to sleep
                    u32 sleeptime = ((adjustedtargetcount - perf_frametimecount) / perf_freqms);
                    Sleep(sleeptime-1);
                }
                else
    #endif                
                
                if(perf_frametimecount > adjustedtargetcount)
                {
                    //TODO::: can't keep up!!!
                }
                
                while(perf_frametimecount < adjustedtargetcount)
                {
                    //we are running ahead...so spinlock
                    perf_thiscount = win32_query_perfcount();
                    perf_frametimecount = perf_thiscount - perf_startcount;
                }
                
                /*This is the frame boundary ^prev frame vthis frame*/
                perf_startcount = perf_thiscount;
                //NOTE::calculate counts debt from frametime information of previous frame
                if(perf_frametimecount > perf_targetcount)
                {
                    u64 missed = perf_frametimecount - perf_targetcount;
                    perf_countsbehind += missed;
                }
                
                //we were behind and we accounted for that
                if(perf_countsbehind > 0 && perf_frametimecount < perf_targetcount) 
                    perf_countsbehind -= perf_targetcount - perf_frametimecount;
                
                dt_from_startup = (f64)(perf_startcount - perf_startup) / win32.perf_freq;
                
                
                f64 frametime;
                frametime = (f64) perf_frametimecount / win32.perf_freq;
                
                win32.input.dt = (f64) frametime;
                //win32.input.fixed_dt = real_targetms;
                win32.input.dt_from_startup = dt_from_startup;
                PERF_END(clock_stablizer);
                
#if INTERNAL
                f32 input_time = (f32) perf_input / win32.perf_freq;
                f32 game_update_time = (f32) perf_game_update / win32.perf_freq;
                f32 render_time = (f32) perf_render / win32.perf_freq;
                f32 clock_stabalizer_time = (f32) perf_clock_stablizer / win32.perf_freq;
                
                
                PlatformLastFrameInformation info;
                info.last_render_time = (f32)render_time * 1000;
                info.last_frame_time = (f32)frametime * 1000;
                info.last_input_processing_time = input_time * 1000;
                info.last_game_update_time = game_update_time * 1000;
                info.last_clock_stablizer_time = clock_stabalizer_time * 1000;
                game_debug_record_last_frame(&info, &win32.game_memory);
#endif
            }
            WaitForSingleObject(win32.audio_threadproc, INFINITE);
        }
    }
    
    return 0;
}

void __stdcall WinMainCRTStartup()
{
    int result = WinMain(GetModuleHandle(0), 0, 0, 0);
    ExitProcess(result);
}
