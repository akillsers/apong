
//TODO REMOVE MATH.H

//TODO make this function acutally not crap (no branching allowed)
inline f32
absolute(f32 val)
{
    f32 ret = val >= 0 ? val : -val;  
    return ret;
}
inline f64
absolute_f64(f64 val)
{
    f32 ret = val >= 0 ? val : -val;  
    return ret;
}

#if 0
#include "fdlibm.h"

static const double
huge   = 1.0e+300,
tiny   = 1.0e-300;

static const double
one =  1.0,
zero =  0.00000000000000000000e+00, /* 0x00000000, 0x00000000 */
half =  5.00000000000000000000e-01, /* 0x3FE00000, 0x00000000 */
two24 =  1.67772160000000000000e+07, /* 0x41700000, 0x00000000 */
twon24  =  5.96046447753906250000e-08, /* 0x3E700000, 0x00000000 */
invpio2 =  6.36619772367581382433e-01, /* 0x3FE45F30, 0x6DC9C883 */
pio2_1  =  1.57079632673412561417e+00, /* 0x3FF921FB, 0x54400000 */
pio2_1t =  6.07710050650619224932e-11, /* 0x3DD0B461, 0x1A626331 */
pio2_2  =  6.07710050630396597660e-11, /* 0x3DD0B461, 0x1A600000 */
pio2_2t =  2.02226624879595063154e-21, /* 0x3BA3198A, 0x2E037073 */
pio2_3  =  2.02226624871116645580e-21, /* 0x3BA3198A, 0x2E000000 */
pio2_3t =  8.47842766036889956997e-32; /* 0x397B839A, 0x252049C1 */

#include "s_copysign.c"
#include "s_scalbn.c"
#include "s_floor.c"
#include "k_rem_pio2.c"
#include "e_rem_pio2.c"
#include "k_sin.c"
#include "k_cos.c"
#include "s_sin.c"
#else
#include <math.h>
#endif
//#include "x86intrin.h"
//#include <xmmintrin.h>

//NOTE:: we are not using msvc crt for sine and cosine
//       so we might be missing out a little bit
//       because msvc crt uses some nice _libm_sse2_sincosf
//       it does it together and may be faster
//#include <math.b>
inline f32
sine(f32 val)
{
    f32 ret = (f32)sin(val);
    return ret;
}

inline f32
cosine(f32 val)
{
#if 0
    return (f32)cos(val);
#else
    return 1.0f;
#endif
    
}

inline Vec4
v4 (f32 x, f32 y, f32 z, f32 w)
{
    Vec4 ret;
    ret.x = x;
    ret.y = y;
    ret.z = z;
    ret.w = w;
    return ret;
}
    
inline Vec3
v3(f32 x, f32 y, f32 z)
{
    Vec3 ret;
    ret.x = x;
    ret.y = y;
    ret.z = z;
    return ret;
}

inline Vec2
v2(f32 x, f32 y)
{
    Vec2 ret;
    ret.x = x;
    ret.y = y;
    return ret;
}

inline Mat4
ortho_mat4(f32 l, f32 r, f32 b, f32 t, f32 n, f32 f)
{
    Mat4 ret = {};
    ret.val[0][0] = 2 / (r - l);
    ret.val[1][1] = 2 / (t - b);
    ret.val[2][2] = 2 / n - f;

    ret.val[3][0] = (l + r) / (l - r);
    ret.val[3][1] = (b + t) / (b -t);
    ret.val[3][2] = (n + f) / (n - f);     //TODO not super sure about this one
    ret.val[3][3] = 1;

    return ret;
}
inline Mat4
rotate_mat4(f32 theta)
{
    Mat4 ret = {};
    ret.val[0][0] = cosine(theta);
    ret.val[0][1] = sine(theta);
    
    ret.val[1][0] = -sine(theta);
    ret.val[1][1] = cosine(theta);
    ret.val[2][2] = 1;
    ret.val[3][3] = 1;
    return ret;
}

inline Mat4
scale_mat4(Vec3 scale)
{
    Mat4 ret = {};
    ret.val[0][0] = scale.x;
    ret.val[1][1] = scale.y;
    ret.val[2][2] = scale.z;
    ret.val[3][3] = 1;

    return ret;
}

inline f32
sign(f32 value)
{
    f32 ret;
    ret = value / absolute(value);
    return ret;
}

inline Mat4
translate_mat4(Vec3 trans)
{
    Mat4 ret = {};

    ret.val[3][0] = trans.x;
    ret.val[3][1] = trans.y;
    ret.val[3][2] = trans.z;

    ret.val[0][0] = 1;
    ret.val[1][1] = 1;
    ret.val[2][2] = 1;

    ret.val[3][3] = 1;

    return ret;
}

//this code is from rygorous (gist.github.com/rygorous/4172889)
//linear combination::
//a[0] * B.row[0] + a[1] * B.row[1] + a[2] * B.row[2] + a[3] * B.row[3]


inline __m128
lincomb_SSE(__m128 &a, Mat4 &b)
{
    __m128 ret;

    __m128 shuffled = _mm_shuffle_ps(a, a, 0x00); 
    ret = _mm_mul_ps(shuffled, b.column[0]);
    ret = _mm_add_ps(ret, _mm_mul_ps(_mm_shuffle_ps(a, a, 0x55), b.column[1]));
    ret = _mm_add_ps(ret, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xaa), b.column[2]));
    ret = _mm_add_ps(ret, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xff), b.column[3]));
    return ret;
}
/*
  NOTE:: matrix multiplication is the do product of rows and columns
  so row * column
*/
#define USE_SSE 1
internal Mat4
operator * (Mat4 left, Mat4 right)
{
    Mat4 ret;

#if USE_SSE
    __m128 outcolumn0 = lincomb_SSE(right.column[0], left);
    __m128 outcolumn1 = lincomb_SSE(right.column[1], left);
    __m128 outcolumn2 = lincomb_SSE(right.column[2], left);
    __m128 outcolumn3 = lincomb_SSE(right.column[3], left);

    ret.column[0] = outcolumn0;
    ret.column[1] = outcolumn1;
    ret.column[2] = outcolumn2;
    ret.column[3] = outcolumn3;
#else

    for(int col = 0; col < 4; col++)
    {
        
        for(int row = 0; row < 4; row++)
        {
            f32 sum = 0;
            for(int i = 0; i < 4; i++)
            {
                sum += left.val[i][row] * right.val[col][i];
            }

            ret.val[col][row] = sum;
        }
    }


#endif
    return ret;
}


Vec2
operator + (Vec2 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left.x + right.x;
    ret.y = left.y + right.y;
    return ret;
}

Vec2
operator - (Vec2 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

Vec2
operator * (Vec2 left, f32 right)
{
    Vec2 ret;
    ret.x = left.x * right;
    ret.y = left.y * right;
    return ret;
}

Vec2
operator * (f32 left, Vec2 right)
{
    Vec2 ret;
    ret.x = left * right.x;
    ret.y = left * right.y;
    return ret;
}

//TODO is there anyway to not use references here??
Vec2
operator += (Vec2 &left, Vec2 right)
{
    left = left + right; 
    return left;
}

Vec2
operator -= (Vec2 &left, Vec2 right)
{
    left = left - right; 
    return left;
}
Vec2
operator *= (Vec2 &left, f32 right)
{
    left = left * right; 
    return left;
}

b32
operator == (Vec2 left, Vec2 right)
{
    b32 ret;
    ret = (left.x == right.x && left.y == right.y);
    return ret;
}

b32
operator != (Vec2 left, Vec2 right)
{
    b32 ret;
    ret = (left.x != right.x || left.y != right.y);
    return ret;
}

inline i32
round_f32toi32(f32 val)
{
    i32 ret = (i32)(val + 0.5f);
    return ret;
}


inline f32
square(f32 val)
{
    f32 ret;
    ret = val * val;
    return ret;
}

inline f32
sqroot(f32 val)
{
    f32 ret;
    F32_4x val4x;
    val4x.d = _mm_set1_ps(val);;

    //TODO check hh can we just access f32_4x directly without .whatever??
    F32_4x ret4x;
    ret4x.d = _mm_sqrt_ps(val4x.d);
    ret =  ret4x.val[0];
    return ret;
}

inline f32
inner(Vec2 left, Vec2 right)
{
    f32 ret;
    ret = left.x*right.x + left.y*right.y;
    return ret;
}

inline f32
v2_length(Vec2 arg)
{
    f32 ret;
    ret = sqroot(square(arg.x) + square(arg.y));
    return ret;
}

inline Vec2
normalize(Vec2 arg)
{
    Vec2 ret = {};

    f32 len = v2_length(arg);

    if(len != 0)
    {
        ret.x = arg.x / len;
        ret.y = arg.y / len;
    }
    return ret;
}

inline Vec2
operator -(Vec2 vector)
{
    Vec2 ret;
    ret.x = -vector.x;
    ret.y = -vector.y;
    return ret;
}

inline b32
operator !(Vec2 vector)
{
    if(vector.x != 0 || vector.y  != 0)
        return false;
    return true;
}

inline f32
distance_frompoint_topoint(Vec2 point1, Vec2 point2)
{
    f32 ret;
    f32 retsquared;

    retsquared = square(point2.y - point1.y) + square(point2.x - point1.x);
    ret = sqroot(retsquared);
    return ret;
}

inline f32
distance_frompoint_to_verticalline(Vec2 point, f32 xequals)
{
    f32 ret;
    ret = absolute(xequals - point.x);
    return ret;
}

inline f32
distance_frompoint_to_horizontalline(Vec2 point, f32 yequals)
{
    f32 ret;
    ret = absolute(yequals - point.y);
    return ret;
}

inline Rect
operator * (Rect rect, f32 scalar)
{
    Rect ret;
    ret.x = rect.x * scalar;
    ret.y = rect.y * scalar;
    ret.width = rect.width * scalar;
    ret.height = rect.height * scalar;
    return ret;
}

inline Vec2
rect_center(Rect rect)
{
    Vec2 ret;
    ret.x = rect.x + (rect.width - rect.x)/2;
    ret.y = rect.y + (rect.height - rect.y)/2;
    return ret;
}
inline Vec2
AABB_center(AABB aabb)
{
    Vec2 ret;
    ret.x = (aabb.x1 + aabb.x2)/2;
    ret.y = (aabb.y1 + aabb.y2)/2;
    return ret;
}

inline AABB
rect_toAABB(Rect rect)
{
    AABB ret;
    ret.x1 = rect.x;
    ret.y1 = rect.y;
    ret.x2 = rect.x + rect.width;
    ret.y2 = rect.y + rect.height;
    return ret;
}

internal Vec2
vec2_lerp(Vec2 a, Vec2 b, f32 t)
{
    Vec2 ret;
    ret.x = (1-t)*a.x + t*b.x;
    ret.y = (1-t)*a.y + t*b.y;
    return ret;
}

