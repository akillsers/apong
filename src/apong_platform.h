#include "aks.h"
#include "arena.h"
#include "simd.cpp"
#include "math.h"
#include "math.cpp"

#define DEBUG_STORAGE_SIZE megabytes(512)

// TODO: We might as well remove this?
#if INTERNAL && 0
#define PERMANENT_STORAGE_SIZE gigabytes(1)
#define SCRATCH_STORAGE_SIZE megabytes(128)
#define DEBUG_STORAGE_SIZE gigabytes(1)
#else
#define PERMANENT_STORAGE_SIZE megabytes(8)
#define SCRATCH_STORAGE_SIZE megabytes(8)
#endif

#define GAME_STORAGE_SIZE (PERMANENT_STORAGE_SIZE + SCRATCH_STORAGE_SIZE)
enum MButton
{
    MButton_left,
    MButton_middle,
    MButton_right,
    MButton_count
};
const char *mbutton_name[] =
{
    "Left Mouse Button",
    "Middle Mouse Button",
    "Right Mouse Button"
};

//TODO Replace upArrow with uparrow
enum Key
{
    KEY_invalid,
    KEY_tab,
    KEY_a,   
    KEY_b,
    KEY_c,
    KEY_d,
    KEY_e,
    KEY_f,
    KEY_g,
    KEY_h,
    KEY_i,
    KEY_j,
    KEY_k,
    KEY_l,
    KEY_m,
    KEY_n,
    KEY_o,
    KEY_p,
    KEY_q,
    KEY_r,
    KEY_s,
    KEY_t,
    KEY_u,
    KEY_v,
    KEY_w,
    KEY_x,
    KEY_y,
    KEY_z,

    KEY_comma,
    KEY_period,
    KEY_forwardslash,
    KEY_semicolon,
    KEY_singlequotes,
    KEY_leftsqbrackets,
    KEY_rightsqbrackets,
    
    KEY_graveaccent,
    KEY_space,
    KEY_capslock,
    KEY_numlock,
    KEY_scrolllock,
    
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,

    KEY_minus,
    KEY_plus,
    KEY_backslash,
    
    KEY_upArrow,
    KEY_downArrow,
    KEY_leftArrow,
    KEY_rightArrow,

    KEY_enter,
    KEY_backspace,
    
    KEY_lShift,
    KEY_rShift,
    KEY_lCtrl,
    KEY_rCtrl,
    KEY_lAlt,
    KEY_rAlt,
    KEY_lMenu,
    KEY_rMenu,
    KEY_lContext,
    KEY_rContext,
    KEY_menu,
    
    KEY_numpad0,
    KEY_numpad1,
    KEY_numpad2,
    KEY_numpad3,
    KEY_numpad4,
    KEY_numpad5,
    KEY_numpad6,
    KEY_numpad7,
    KEY_numpad8,
    KEY_numpad9,
    KEY_numpadslash,
    KEY_numpadasterisk,
    KEY_numpadminus,
    KEY_numpadplus,
    KEY_numpadenter,
    KEY_numpadperiod,

    KEY_esc,
    KEY_f1,
    KEY_f2,
    KEY_f3,
    KEY_f4,
    KEY_f5,
    KEY_f6,
    KEY_f7,
    KEY_f8,
    KEY_f9,
    KEY_f10,
    KEY_f11,
    KEY_f12,

    KEY_printscreen,
    KEY_scroll,
    KEY_break,
    KEY_insert,
    KEY_home,
    KEY_end,
    KEY_pageup,
    KEY_pagedown,
    KEY_delete,

    KEY_mediahome,
    KEY_mediaemail,
    KEY_mediaplay,
    KEY_mediastop,
    KEY_mediaprev,
    KEY_medianext,
    KEY_medialouder,
    KEY_mediasofter,
    KEY_mediamute,

#if INTERNAL
    KEY_dbge0, //TODO fake dbg key
    KEY_dbge1, //TODO fake dbg key
#endif
    KEY_unknown,
    KEY_count
};

//These are keynames for US keyboards
//TODO support international keys
//TODO create keynames for non-us keyboards
const char *keyname[256]=
{
    "TAB",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",

    ",",
    ".",
    "/",
    ";",
    "'",
    "[",
    "]",

    "`",
    "Space",
    "Caps Lock",
    "Num Lock",
    "Scroll Lock",
    
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",

    "-",
    "+/=",
    "/",
    
    "Up Arrow",
    "Down Arrow",
    "Left Arrow",
    "Right Arrow",

    "Enter",
    "Backspace",
    
    "Left Shift",
    "Right Shift",
    "Left Control",
    "Right Control",
    "Left Alt",
    "Right Alt",
    "Left Menu",
    "Right Menu",
    "Left Context",
    "Right Context",
    "Menu",
    
    "Numpad 0",
    "Numpad 1",
    "Numpad 2",
    "Numpad 3",
    "Numpad 4",
    "Numpad 5",
    "Numpad 6",
    "Numpad 7",
    "Numpad 8",
    "Numpad 9",
    "Numpad /",
    "Numpad *",
    "Numpad -",
    "Numpad +",
    "Numpad Enter",
    "Numpad .",

    "Escape",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",

    "Print Screen",
    "Scroll Lock",
    "Break",
    "Insert",
    "Home",
    "End",
    "Page Up",
    "Page Down",
    "Delete",

    "Media Home",
    "Media Volume Up",
    "Media Volume Down",
    "Media Mute",

#if INTERNAL
    "Debug E0",
    "Debug E1",
#endif
    "Unsupported key"
};


struct GameMemory
{
    void *permanent_storage;
    u64 permanent_storage_size;
    void *scratch_storage;
    u64 scratch_storage_size;
#if INTERNAL
    void *debug_storage;
    u64 debug_storage_size;
#endif
};

struct SoundBuffer
{
    u32 samplerate;
    u32 bitdepth;
    u32 numchannels;
    
    u32 availible_audio_frames;
    void *data;
};

//TODO half transition count
struct KeyState
{
    u32 transitioncount;
    u32 down_repeats;
    b32 down; //ended down
};

u32 
keystate_pressed(KeyState keystate)
{
    u32 ret;
    ret = (keystate.transitioncount / 2);
    if(keystate.down && (keystate.transitioncount % 2))
        ret += 1;
    return ret;
}
    
b32 
keystate_pressedb(KeyState keystate)
{
    b32 ret;
    ret = (keystate.transitioncount > 1 ||
           (keystate.down && keystate.transitioncount == 1));

    return ret;
}

b32
keystate_lastkey_down(KeyState keystate)
{
    b32 ret;
    ret = ((keystate.transitioncount % 2) != keystate.down);
    return ret;
}

b32
keystate_released(KeyState keystate)
{
    b32 ret;
    ret = (keystate.transitioncount > 1 ||
           (!keystate.down && keystate.transitioncount == 1));

    return ret;
}

#define buttonstate_pressed(keystate) keystate_pressed(keystate)
#define buttonstate_pressedb(keystate) keystate_pressed(keystate)
#define buttonstate_lastkey_down(keystate) keystate_lastkey_down(keystate)
struct Input
{
    //Input
    f64 dt; //NOTE:: the game updates the game as if dt time in seconds has passed since last frame 

    //TODO change to monitor refresh rate??
    //f64 fixed_dt;
    f64 dt_from_startup; //from the second frame

    KeyState key[KEY_count];
    KeyState mouse[MButton_count];

    //NOTE:: for non multi-window platforms: this is always true
    b32 has_focus;

    f32 mousex;
    f32 mousey;

    b32 mousemoved;
    //Key helper functions
       //Key helper functions
        //NOTE:: this returns the number of times the key has been pressed in the time between 
//       the last frame and the current one
    //b32 already_pressed[KEY_count];
    Key sticky_press;

    u32 
    key_pressed(u32 keyID)
    {
        return keystate_pressed(this->key[keyID]) && sticky_press != keyID;
    }

    b32 
    key_pressedb(u32 keyID)
    {
        return keystate_pressedb(this->key[keyID]) && sticky_press != keyID;
    }
    
    b32
    lastkey_down(u32 keyID)
    {
        return keystate_lastkey_down(this->key[keyID]);
    }

    // b32 
    // key_pressed_once(u32 keyID)
    // {
    //     b32 ret = key_pressed(keyID) && !already_pressed[keyID];
    //     already_pressed[keyID] = true;
    //     return ret;
    // }
    
    b32
    key_released(u32 keyID)
    {
        return keystate_released(key[keyID]);
    }
    
    u32 
    button_pressed(u32 button)
    {
        return buttonstate_pressed(this->mouse[button]);
    }

    b32 
    button_pressedb(u32 button)
    {
        return buttonstate_pressedb(this->mouse[button]);
    }

    b32
    lastbutton_down(u32 button)
    {
        return buttonstate_lastkey_down(this->mouse[button]);
    }
    b32
    button_released(u32 button)
    {
        return keystate_released(mouse[button]);
    }

};

struct PlatformWarning
{
    char *message;
    char *desc;
};

META_EXCLUDE struct RenderList;
META_EXCLUDE struct RenderTarget;
#define GAME_UPDATE_AND_RENDER(name) internal void name(Input *input, GameMemory *memory, RenderList **rlist, RenderTarget **rtarget)
GAME_UPDATE_AND_RENDER(game_update_and_render);

#define GAME_INIT_MEMORY(name) internal void name(GameMemory *memory)
GAME_INIT_MEMORY(game_init_memory);

#define GAME_PROCESS_COMMAND_LINE(name) internal void name(char *command_line, GameMemory *memory)
GAME_PROCESS_COMMAND_LINE(game_process_command_line);

#define GAME_OUTPUT_SOUND(name) internal void name(SoundBuffer *sbuffer, GameMemory *memory)
GAME_OUTPUT_SOUND(game_output_sound);

#if INTERNAL
struct PlatformLastFrameInformation
{
    f32 last_render_time;
    f32 last_frame_time; //this is how long the last frame (not this frame) took
    f32 last_game_update_time;
    f32 last_input_processing_time;
    f32 last_clock_stablizer_time;
};

#define GAME_DEBUG_RECORD_LAST_FRAMEINFO(name) internal void name(PlatformLastFrameInformation *info, GameMemory *memory)
GAME_DEBUG_RECORD_LAST_FRAMEINFO(game_debug_record_last_frameinfo);

#define GAME_POST_UPDATE_AND_RENDER(name) internal void name(GameMemory *memory)
GAME_POST_UPDATE_AND_RENDER(game_post_update_and_render);

#endif
enum FileError
{
    FileError_success,
    FileError_nonexistent,
    FileError_invalid_handle,
    FileError_access_denied,
    FileError_sharing_access_violation,
    FileError_path_not_found,
    FileError_path_could_not_create,
    
    FileError_invalid_file_id,
    FileError_unknown_error,

    FileError_count
};

#define HasFileError(file_err) (file_err != FileError_success)

enum PlatformError
{
    PlatformError_fatal,
    PlatformError_non_fatal,
};

/*
  leap years and total year delta
  2001 1 1
  2002 1 2
  2003 1 3
  2004 1 4
  2005 2 5
  2006 2
  2007 2
  2008 2
  2009 3



  337 (days without feburary)
  365 - 366 days
  jan 31
  feb 28 (29 on leap year)
  mar 31
  apr 30
  may 31
  jun 30
  july 31
  aug 31
  sept 30
  oct 31
  nov 30
  dec 31
*/

struct PlatformTime
{
    u32 year;
    u32 month; //1 is jan...etc.
    u32 day;
    u32 hour;
    u32 minute;
    u32 second;
    u32 milliseconds;
};

enum OpenFileFlags
{
    WRITE_ACCESS = 0x2,
};

enum FileID
{
    FileID_GameAssetsFile
};

META_EXCLUDE struct Platform
{
    //Info about the window
    volatile u32 w_h;
    volatile u32 w_w;

    void *(*opengl_func_address)(char *name);

    FileError (*open_file) (FileID file_id, PlatformFileHandle *handle);
    void (*display_error) (char *message);
    
    u64 (*get_file_size) (PlatformFileHandle file_handle);
    //default argument list??
    FileError (*read_file) (PlatformFileHandle file_handle, void *out, u64 offset, u32 num_bytes_to_read, b32 block); 
    FileError (*write_file)(PlatformFileHandle file_handle, u64 offset, u32 num_bytes_to_write, u8 *data, b32 truncate);
    void (*close_file) (PlatformFileHandle file_handle);
    //NOTE create if nonexistent also creates the path

    void (*swap_buffers) ();

    //time is in UTC 
    void (*get_time) (PlatformTime *time);
    u32 (*get_time_bias_minutes) ();

    void(*shut_off)();
};


static Platform platform;
