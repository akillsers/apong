META_EXCLUDE ____file
enum TokenType
{
    Token_unknown,

    Token_identifier,
    Token_openparen,
    Token_closeparen,
    Token_colon,
    Token_string,
    Token_semicolon,
    Token_asterisk,
    Token_openbracket,
    Token_closebracket,
    Token_openbraces,
    Token_closebraces,
    Token_pound,
    Token_comma,
    Token_number, //NOTE:: integer or floating point

    Token_endofstream
};

struct Token
{
    TokenType type;

    i32 text_len;
    char *text;
};

struct Tokenizer
{
    char *at;
};


internal void
eat_all_whitespace_and_comments(Tokenizer *tokenizer)
{
    for(;;)
    {
        if(is_whitespace(tokenizer->at[0]))
        {
            tokenizer->at++;
        }
        else if (tokenizer->at[0] == '/' && tokenizer->at[1] == '/')
        {
            while(!is_endofline(tokenizer->at[0]))
            {
                tokenizer->at++;
            }
        }
        else if(tokenizer->at[0] == '/' && tokenizer->at[1] == '*')
        {
            while(!(tokenizer->at[0] == '*' && tokenizer->at[1] == '/'))
            {
                tokenizer->at++;
            }
        }
        else
            break;
    }
}

internal void
eat_all_decimaldigits(Tokenizer *tokenizer)
{
    while(tokenizer->at[0] && is_numeric(tokenizer->at[0]))
    {
        tokenizer->at++;
    }
}

internal void
eat_all_octaldigits(Tokenizer *tokenizer)
{
    while(tokenizer->at[0] && is_octal(tokenizer->at[0]))
    {
        tokenizer->at++;
    }
}

inline bool
is_floating_suffix(char character)
{
    if(character == 'f' || character == 'F' || character == 'l' || character == 'L')
        return true;
    return false;
}

internal Token
get_nexttoken(Tokenizer *tokenizer)
{
    Token ret = {};
    
    eat_all_whitespace_and_comments(tokenizer);

    ret.text = tokenizer->at;
    char c = tokenizer->at[0];
    tokenizer->at++;
    switch(c)
    {
        case '\0':{ret.type = Token_endofstream;} break;
        
        case '(': {ret.type = Token_openparen;} break;
        case ')': {ret.type = Token_closeparen;} break;
        case '[': {ret.type = Token_openbracket;} break;
        case ']': {ret.type = Token_closebracket;} break;
        case '{': {ret.type = Token_openbraces;} break;
        case '}': {ret.type = Token_closebraces;} break;
        case ':': {ret.type = Token_colon;} break;
        case ';': {ret.type = Token_semicolon;} break;
        case '*': {ret.type = Token_asterisk;} break;
        case '#': {ret.type = Token_pound;} break;
        case ',': {ret.type = Token_comma;} break;
            
        case '"':
        {
            ret.type = Token_string;
            ret.text = tokenizer->at;

            while(tokenizer->at[0] &&tokenizer->at[0] != '"')
            {
                if(tokenizer->at[0] == '\\' &&
                    tokenizer->at[1]) //escape characters NEXT!!!
                    tokenizer->at++;
                tokenizer->at++;
            }
            ret.text_len = tokenizer->at - ret.text;
            if(tokenizer->at[0] == '"')
                tokenizer->at++;
        } break;
        default:
        {
            if(is_alpha(c))
            {
                ret.type = Token_identifier;
                while(is_alpha(tokenizer->at[0]) ||
                      is_numeric(tokenizer->at[0]) ||
                      tokenizer->at[0] == '_')
                {
                    tokenizer->at++;
                }
                ret.text_len = tokenizer->at - ret.text;
            }
            else if (is_numeric(c))
            {
                ret.type = Token_number;
                if(c == '0')
                {
                    if((tokenizer->at[0] == 'x' || tokenizer->at[0] == 'X'))
                    {
                        //hexadecimal
                        //can't be float
                        tokenizer->at++;        
                        while(is_hexdigit(tokenizer->at[0]))
                        {
                            tokenizer->at++;
                        }
                        
                    }
                    else
                    {
                        //octal or float
                        //if it is octal -> doesn't matter since yeah
                        goto handle_float;
                    }
                }
                else
                {
                handle_float:
                    //decimal
                    //or float
                    eat_all_decimaldigits(tokenizer);
                    if(tokenizer->at[0] == '.')
                    {
                        tokenizer->at++;
                        eat_all_decimaldigits(tokenizer);
                        if(tokenizer->at[0] == 'E' || tokenizer->at[0] == 'e')
                        {
                            tokenizer->at++;
                            if(tokenizer->at[0] == '-')
                                tokenizer->at++;

                            eat_all_decimaldigits(tokenizer);
                            if(is_floating_suffix(tokenizer->at[0]))
                                tokenizer->at++;
                            ret.text_len = tokenizer->at - ret.text;
                        }
                        if(is_floating_suffix(tokenizer->at[0]))
                            tokenizer->at++;
                            ret.text_len = tokenizer->at - ret.text;
                    }
                    else if(tokenizer->at[0] == 'E' || tokenizer->at[0] == 'e')
                    {
                        tokenizer->at++;
                        if(tokenizer->at[0] == '-')
                            tokenizer->at++;
                        eat_all_decimaldigits(tokenizer);
                        if(is_floating_suffix(tokenizer->at[0]))
                            tokenizer->at++;
                            ret.text_len = tokenizer->at - ret.text;
                    }
                    else
                    {
                            ret.text_len = tokenizer->at - ret.text;
                    }
                }
            }
            else
            {
                ret.type = Token_unknown;
            }
        }break;
    }
    
    return ret;
}
