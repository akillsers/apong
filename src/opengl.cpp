#define MAX_TEXTURE_BATCH_TEXTURES 5000

//TODO instanced rendering
//TODO ascynchronous texture downloads
//SECTION:: Predefined colours
//NOTE:: requires math

global_variable
struct 
{
    GLuint VAO;

    //TODO combine these two guys
    GLuint fetchuvs_VBO;
    GLuint fetchsts_VBO;
    GLuint colors_VBO;
    GLuint modelprojection_VBO;
} opengl_globals;


global_variable u32 framebuffer;
struct OpenGLTexture
{
    u32 width;
    u32 height;
    u32 GPUHandle;
};
global_variable OpenGLTexture tex_colorbuffer;

//internal dependencies

#define opengl_function(type, name) PFNGL##type##PROC   gl##name;
#include "opengl_functions.cpp"
#undef opengl_function

internal void
opengl_load_functions()
{
#define opengl_function(type, name) gl##name = (PFNGL##type##PROC) platform.opengl_func_address("gl" #name);
#include "opengl_functions.cpp"
#undef opengl_function
}

#include "opengl_error_checking.cpp"

#include "opengl_shadergenerated.cpp"

#if 0
glGetError();
glReadBuffer();
glBlitFramebuffer();
glCheckFramebufferStatus();
glGetUniformLocation();
glFinish();
glFlush();
#endif


internal GLuint
opengl_load_shader(char *vert_shadersource, char *frag_shadersource)
{
    //TIMED_BLOCK;
    GLuint retprogram;
    
    GLuint vertshade = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertshade, 1, &vert_shadersource, 0);
    glCompileShader(vertshade);

    i32 success;
    glGetShaderiv(vertshade, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        char infolog[1024];
        glGetShaderInfoLog(vertshade,
                           1024,
                           0, //NOTE::pointer to int that opengl will store length of infolog at
                           &infolog[0]);
        assert(!"Vertex creation not cool");
    }

    
    GLuint fragshade = (GLuint)glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragshade, 1, &frag_shadersource, 0);
    glCompileShader(fragshade);

    glGetShaderiv(fragshade, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        char infolog[1024];
        glGetShaderInfoLog(fragshade,
                           1024,
                           0, //NOTE::pointer to int that opengl will store length of infolog at
                           &infolog[0]);
        assert(!"Fragment creation not cool");
    }

    retprogram = glCreateProgram();
    glAttachShader(retprogram, vertshade);
    glAttachShader(retprogram, fragshade);
    glLinkProgram(retprogram);

    glGetProgramiv(retprogram, GL_LINK_STATUS, &success);
    if(!success)
    {
        char infolog[1024];
        glGetProgramInfoLog(retprogram,
                            1024,
                            0, //NOTE::pointer to int that opengl will store length of infolog at
                            &infolog[0]);
        assert(!"program linkage failed");
    }

    glDeleteShader(vertshade);
    glDeleteShader(fragshade);
    return retprogram;
}


internal void
opengl_regen_tex_colorbuffer(u32 width, u32 height)
{
    //TIMED_BLOCK;
    glBindTexture(GL_TEXTURE_2D, tex_colorbuffer.GPUHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    tex_colorbuffer.width = width;
    tex_colorbuffer.height = height;
}

internal void
opengl_framebuffer_init(u32 width, u32 height)
{
    //TIMED_BLOCK;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    //generate texture
    glGenTextures(1, &tex_colorbuffer.GPUHandle);
    glBindTexture(GL_TEXTURE_2D, tex_colorbuffer.GPUHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    tex_colorbuffer.width = width;
    tex_colorbuffer.height = height;
    
    //attach it to currently bound fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_colorbuffer.GPUHandle, 0);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        platform.display_error("glCheckFramebufferStatus did not return GL_FRAMEBUFFER_COMPLETE during OpenGL initialization");
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

internal void
opengl_init_draw(u32 framebuffer_intial_w, u32 framebuffer_intial_h)
{
    //TIMED_BLOCK;    
    //NOTE:: this is done in the platform layer
    opengl_load_functions();
    
    load_allshaders();
    //vao

    //assert(opengl_texturedrectshader.fetchst_loc >= 0);
    {
        glGenVertexArrays(1, &opengl_globals.VAO);
        GLuint VBO;
        glGenBuffers(1, &VBO);

        glBindVertexArray(opengl_globals.VAO);

        f32 vertices[] =
            {
                //x   //y   //z       //u   //v
                0.0f, 0.0f, 0.0f,     0.0f, 0.0f, 
                0.0f, 1.0f, 0.0f,     0.0f, 1.0f,
                1.0f, 0.0f, 0.0f,     1.0f, 0.0f,
                1.0f, 1.0f, 0.0f,     1.0f, 1.0f
            };
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        i32 stride = 5 * sizeof(f32);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                              stride, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                              stride, (void *)(3 * sizeof(f32)));
        glEnableVertexAttribArray(1);


        glGenBuffers(1, &opengl_globals.fetchuvs_VBO);
        glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchuvs_VBO);
        //TODO should we use STREAM or DYNAMIC DRAW
        //init the buffer to 0, it will be updated later, every frame
        glBufferData(GL_ARRAY_BUFFER, MAX_TEXTURE_BATCH_TEXTURES * sizeof(f32) * 2, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);
        
        glGenBuffers(1, &opengl_globals.fetchsts_VBO);
        glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchsts_VBO);
        //init the buffer to 0, it will be updated later, every frame
        glBufferData(GL_ARRAY_BUFFER, MAX_TEXTURE_BATCH_TEXTURES * sizeof(f32) * 2, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(3);

        glGenBuffers(1, &opengl_globals.colors_VBO);
        glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.colors_VBO);
        //init the buffer to 0, it will be updated later, every frame
        glBufferData(GL_ARRAY_BUFFER, MAX_TEXTURE_BATCH_TEXTURES * sizeof(f32) * 3, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(4);

        glGenBuffers(1, &opengl_globals.modelprojection_VBO);
        glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.modelprojection_VBO);
        //init the buffer to 0, it will be updated later, every frame
        glBufferData(GL_ARRAY_BUFFER, MAX_TEXTURE_BATCH_TEXTURES * sizeof(Mat4), NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * 4 * sizeof(f32), 0);
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * 4 * sizeof(f32), (void *)(1 * 4*sizeof(f32)));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * 4 * sizeof(f32), (void *)(2 * 4*sizeof(f32)));
        glEnableVertexAttribArray(7);
        glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 4 * 4 * sizeof(f32), (void *)(3 * 4*sizeof(f32)));
        glEnableVertexAttribArray(8);


        glVertexAttribDivisor(0, 0);
        glVertexAttribDivisor(1, 0);        
        glVertexAttribDivisor(2, 1);
        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);
        glVertexAttribDivisor(7, 1);
        glVertexAttribDivisor(8, 1);
        //TODO we can do the model * projection matrix calc in the shader
        // glVertexAttribDivisor(8, 1);
        // glVertexAttribDivisor(8, 1);
                                        

        //glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
    }
    
    //We do not need depth testing (WE ARE A SIMPLE 2D GAME!!)
    glDisable(GL_DEPTH_TEST);

    //for testing purposes::
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  

    opengl_framebuffer_init(framebuffer_intial_w, framebuffer_intial_h);
}
internal void
opengl_draw_rectangle(f32 x, f32 y, f32 width, f32 height, Renderer_RGB color, f32 transparency = 1.0f)
{
    //TIMED_BLOCK;
    glUseProgram(opengl_rectshader.ID);
    glBindVertexArray(opengl_globals.VAO);
    
    Mat4 projection_matrix = ortho_mat4(0.0, tex_colorbuffer.width, tex_colorbuffer.height, 0.0f, -1.0f, 1.0f);
    glUniformMatrix4fv(opengl_rectshader.projection_loc, 1, GL_FALSE, &projection_matrix.val[0][0]);
    
    Mat4 model_matrix = translate_mat4(v3(x, y, 0.0f)) * scale_mat4(v3(width, height, 1.0f));
    glUniformMatrix4fv(opengl_rectshader.model_loc, 1, GL_FALSE, &model_matrix.val[0][0]);

    Vec4 color_alpha = v4(color.r, color.g, color.b, transparency);
    glUniform4fv(opengl_rectshader.color_loc, 1, (f32 *)&color_alpha);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}


internal void
opengl_draw_rectangle(f32 x, f32 y, f32 width, f32 height, f32 orientation, Renderer_RGB color, f32 transparency = 1.0f)
{
    //TIMED_BLOCK;
    glUseProgram(opengl_rectshader.ID);
    glBindVertexArray(opengl_globals.VAO);
    
    Mat4 projection_matrix = ortho_mat4(0.0, tex_colorbuffer.width, tex_colorbuffer.height, 0.0f, -1.0f, 1.0f);
    glUniformMatrix4fv(opengl_rectshader.projection_loc, 1, GL_FALSE, &projection_matrix.val[0][0]);
    
    Mat4 model_matrix = translate_mat4(v3(x, y, 0.0f)) * scale_mat4(v3(width, height, 1.0f));

    model_matrix = translate_mat4(v3(-width/2, -height/2, 0.0f)) * model_matrix;
    model_matrix = rotate_mat4(orientation) * model_matrix;
    model_matrix = translate_mat4(v3(width/2, height/2, 0.0f)) * model_matrix;

    glUniformMatrix4fv(opengl_rectshader.model_loc, 1, GL_FALSE, &model_matrix.val[0][0]);

    Vec4 color_alpha = v4(color.r, color.g, color.b, transparency);
    glUniform4fv(opengl_rectshader.color_loc, 1, (f32 *)&color_alpha);


    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

internal void
opengl_draw_rectangle(Rect rectangle, Renderer_RGB color)
{
    opengl_draw_rectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height, color);
}


internal void
opengl_draw_rectangleoutline(f32 x, f32 y, f32 width, f32 height, Renderer_RGB color)
{
    opengl_draw_rectangle(x, y, width, 1, color);
    opengl_draw_rectangle(x, y, 1, height, color);
    opengl_draw_rectangle(x + width, y, 1, height, color);
    opengl_draw_rectangle(x, y + height, width, 1, color);
}
    
internal u32
opengl_create_greyscaletexture(u32 width, u32 height, void* pixels)
{
    //TIMED_BLOCK;
    u32 ret = {};
    glGenTextures(1, &ret);

    glBindTexture(GL_TEXTURE_2D, ret);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

    glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
    //glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D,
                 0, //level
                 GL_RED,
                 width,
                 height,
                 0, //must be 0
                 GL_RED,
                 GL_UNSIGNED_BYTE,
                 pixels);

    
    return ret;
}
internal u32
opengl_create_texture(u32 width, u32 height, void* pixels)
{
    //TIMED_BLOCK;
    u32 ret;
    glGenTextures(1, &ret);

    i32 alignment;
    glGetIntegerv(GL_PACK_ALIGNMENT, &alignment);

    i32 unalignment;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &unalignment);

    glBindTexture(GL_TEXTURE_2D, ret);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

    //glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
    
    glTexImage2D(GL_TEXTURE_2D,
                 0, //level
                 GL_RGB,
                 width,
                 height,
                 0, //must be 0
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 pixels);

    
    return ret;
}
Vec2 location;
Vec2 dimensions;

//only send projection matrix when it changes!!!!
internal void
opengl_draw_texturemasked_instanced(u32 draw_count,
                                    Mat4 *modelprojection_matrices, Vec2 *fetchuvs, Vec2 *fetchsts,
                                    Renderer_RGB *colors, GLuint texID)
{
    //TIMED_BLOCK;
    glUseProgram(opengl_texturedrectshader.ID);
    glBindVertexArray(opengl_globals.VAO);

    //new VBO way
    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchuvs_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*2 * draw_count, 0, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*2*draw_count, (void *) fetchuvs);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchsts_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*2 * draw_count, 0, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*2*draw_count, (void *) fetchsts);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.colors_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*3 * draw_count, 0, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*3*draw_count, (void *) colors);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.modelprojection_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*16 * draw_count, 0, GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*16*draw_count, (void *) &modelprojection_matrices->val);
    
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(opengl_texturedrectshader.tex_loc, 0);
    glBindTexture(GL_TEXTURE_2D, texID);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0 , 4, draw_count);
    glBindVertexArray(0);
    //glFinish();
}

//TODO move this somewhere else
internal Mat4
opengl_calculate_modelprojection_matrix(f32 x, f32 y, f32 w, f32 h)
{
    //TIMED_BLOCK;
    Mat4 projection_matrix;
    Mat4 model_matrix;
    {
        //TIMED_BLOCK;

        projection_matrix = ortho_mat4(0.0f, tex_colorbuffer.width, tex_colorbuffer.height, 0.0f, -1.0f, 1.0f);

        //TODO width and height is for now...

        model_matrix = translate_mat4(v3(x, y, 0.0f)) * scale_mat4(v3(w, h, 1.0f));

        
    }
    Mat4 model_projection_matrix;
    {
        //TIMED_BLOCK;
        model_projection_matrix = projection_matrix * model_matrix;
    }
    return model_projection_matrix;
}

//TODO should we instance with uniforms or VBOS right now we're useing VBOs
internal void
opengl_draw_texturemasked(f32 x, f32 y, f32 w, f32 h,
                          f32 u, f32 v, f32 s, f32 t,
                          Renderer_RGB tint, GLuint texID)
{
    //TIMED_BLOCK;
    glUseProgram(opengl_texturedrectshader.ID);
    glBindVertexArray(opengl_globals.VAO);

    Mat4 projection_matrix;
    projection_matrix = ortho_mat4(0.0f, tex_colorbuffer.width, tex_colorbuffer.height, 0.0f, -1.0f, 1.0f);

    //TODO width and height is for now...
    Mat4 model_matrix;
    model_matrix = translate_mat4(v3(x, y, 0.0f)) * scale_mat4(v3(w, h, 1.0f));

    Mat4 model_projection_matrix;
    model_projection_matrix = projection_matrix * model_matrix;
    
    Vec2 fetchuv = v2(u, v);
    Vec2 fetchst = v2(s ,t);
    // Vec2 fetchuv = v2(0, 0);
    // Vec2 fetchst = v2(1 ,1);
    //new VBO way
    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchuvs_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*2, 0, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*2, (void *) &fetchuv);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.fetchsts_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*2, 0, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*2, (void *) &fetchst);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.colors_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*3, 0, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*3, (void *) &tint);

    glBindBuffer(GL_ARRAY_BUFFER, opengl_globals.modelprojection_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32)*16, 0, GL_STREAM_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(f32)*16, (void *) &model_projection_matrix.val);

    //old uniform way
    //glUniform2fv(opengl_texturedrectshader.fetchuv_loc, 1, (f32 *) &fetchuv);
    //glUniform2fv(opengl_texturedrectshader.fetchst_loc, 1, (f32 *) &fetchst);

    //OLD WAY
    glUniformMatrix4fv(opengl_texturedrectshader.model_loc, 1, GL_FALSE, &model_matrix.val[0][0]);
    glUniformMatrix4fv(opengl_texturedrectshader.projection_loc, 1, GL_FALSE, &projection_matrix.val[0][0]);
    //glUniform3fv(opengl_texturedrectshader.color_loc, 1, (f32 *)&tint);

    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texID);
    glUniform1i(opengl_texturedrectshader.tex_loc, 0);        

    glDrawArrays(GL_TRIANGLE_STRIP, 0 , 4);
    glBindVertexArray(0);
}

internal void
opengl_draw_texture(f32 x, f32 y, f32 w, f32 h,
                    f32 u, f32 v, f32 s, f32 t,
                    Renderer_RGB tint, GLuint texID)
{
    //TIMED_BLOCK;
    glUseProgram(opengl_textureshader.ID);
    glBindVertexArray(opengl_globals.VAO);

    Mat4 projection_matrix = ortho_mat4(0.0f, tex_colorbuffer.width, tex_colorbuffer.height, 0.0f, -1.0f, 1.0f);
    glUniformMatrix4fv(opengl_texturedrectshader.projection_loc, 1, GL_FALSE, &projection_matrix.val[0][0]);

    //TODO width and height is for now...
    Mat4 model_matrix = translate_mat4(v3(x, y, 0.0f)) *
        scale_mat4(v3(w, h, 1.0f));
    
    glUniformMatrix4fv(opengl_textureshader.model_loc, 1, GL_FALSE, &model_matrix.val[0][0]);

    glUniform3fv(opengl_textureshader.color_loc, 1, (f32 *)&tint);

    Vec2 fetchuv = v2(u, v);
    glUniform2fv(opengl_textureshader.fetchuv_loc, 1, (f32 *) &fetchuv);
    Vec2 fetchst = v2(s ,t);
    glUniform2fv(opengl_textureshader.fetchst_loc, 1, (f32 *) &fetchst);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texID);
    glUniform1i(opengl_texturedrectshader.tex_loc, 0);        

    glDrawArrays(GL_TRIANGLE_STRIP, 0 , 4);
    glBindVertexArray(0);
}

// internal void
// opengl_draw_texturemasked(f32 x, f32 y, f32 w, f32 h,
//                           f32 u, f32 v, f32 s, f32 t,
//                           Vec3 tint, u32 opengl_texture_handle)
// {
//     opengl_draw_texturemasked(x, y, w, h,
//                               u, v, s, t,
//                               tint, opengl_texture_handle);

// }
    

internal void
opengl_draw_texturemasked(f32 x, f32 y, 
                          f32 u, f32 v, f32 s, f32 t,
                          Renderer_RGB tint, Renderer_Texture1c *texture)
{
    f32 w = texture->width*(s-u);
    f32 h = texture->height*(t-v);
    opengl_draw_texturemasked(x, y, w, h,
                              u, v, s, t,
                              tint, texture->GPUHandle);
}

// internal void
// dbg_opengl_draw_line(f32 x1, f32 y1, f32 x2, f32 y2, f32 width)
// {
//     GLuint lineVAO;
//     glGenVertexArrays(1, &lineVAO);
//     GLuint lineVBO;
//     glGenBuffers(1, &lineVBO);

//     glBindVertexArray(lineVAO);


//     Vec2 a = v2(x1, y1);
//     Vec2 b = v2(x2, y2);
//     Vec2 d = b - a;
//     //TODO PERPENDICULAR?????
//     glBindVertexArray(0);
    
    
//     glDeleteVertexArrays(1, &lineVAO);
// }


// internal void
// opengl_draw_char(f32 x, f32 y, AkelaFont *fnt, char character)
// {
//     //TIMED_BLOCK;
//     assert(character >= fnt->first_unicode_codepoint && character <= fnt->first_unicode_codepoint + fnt->numglyphs);
//     FontGlyph *glyph = &fnt->glyphs[character-fnt->first_unicode_codepoint];
//     opengl_draw_texturemasked(x, y,
//                               (f32)glyph->x0/fnt->texture.width, (f32)glyph->y0/fnt->texture.height,
//                               (f32)glyph->x1/fnt->texture.width, (f32)glyph->y1/fnt->texture.height,
//                               v3(1.0f, 1.0f, 1.0f), &fnt->texture);
// }

internal void
opengl_set_framebuffersize(u32 width, u32 height)
{
    //TIMED_BLOCK;
    if(width != tex_colorbuffer.width || height != tex_colorbuffer.height)
        opengl_regen_tex_colorbuffer(width, height); 
}

internal void
opengl_beginframe2d(Renderer_RGB clear_color = {0.0f, 0.0f, 0.0f})
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glViewport(0, 0, tex_colorbuffer.width, tex_colorbuffer.height);
    glClearColor(clear_color.r, clear_color.g, clear_color.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

internal void
opengl_blittoscreen()
{
    TIMED_BLOCK;
    //TODO lock the message queue here only

    f32 screeny = platform.w_h/2 - tex_colorbuffer.height/2;
    f32 screenx = platform.w_w/2 - tex_colorbuffer.width/2;
    f32 screenw = tex_colorbuffer.width;
    f32 screenh = tex_colorbuffer.height;

    {
        TIMED_BLOCK;
        glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer);
        glReadBuffer(GL_COLOR_ATTACHMENT0);
    }
    
    {
        TIMED_BLOCK;
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

        //TODO the platform layer shouldn't know about texture constructs like tex_colorbuffer
        //     make this not in the platform layer
        glBlitFramebuffer(0, 0, tex_colorbuffer.width, tex_colorbuffer.height,
                          screenx,
                          platform.w_h - screeny - screenh,
                          screenx + screenw,
                          platform.w_h - screeny,
                          GL_COLOR_BUFFER_BIT,
                          GL_NEAREST);
    }
    // glBlitFramebuffer(0, 0, tex_colorbuffer.width, tex_colorbuffer.height,
    //                   0,
    //                   0,
    //                   platform.w_w,
    //                   platform.w_h,
    //                   GL_COLOR_BUFFER_BIT,
    //                   GL_NEAREST);
    // glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    // glClear(GL_COLOR_BUFFER_BIT);

    {
        TIMED_BLOCK;
        glFlush();
        //TODO put this on another thread
    }
    


    // WaitForSingleObject(win32.vblank, INFINITE);
    {
        TIMED_BLOCK;
        platform.swap_buffers();
    }
    
    
    {
        //TIMED_BLOCK;
        //glFinish();
        //TODO we can also call glFlush() in render.cpp
        //     and then do some cpu work
    }   
}

internal void
swap_memory(u8 *a, u8* b, u32 len)
{
    //TODO optimize make this four wide
    //TIMED_BLOCK;
    for(int i = 0; i < len; i++)
    {
        u8 temp;
        temp = a[i];
        a[i] = b[i];
        b[i] = temp;
    }
}

//NOTE outdata is dword aligned!!!
internal void
opengl_retrieve_framebuffer_data(void *outdata)
{
    //TIMED_BLOCK;
    glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer);
    glReadBuffer(GL_COLOR_ATTACHMENT0);


    glReadPixels(0, 0, tex_colorbuffer.width, tex_colorbuffer.height, GL_RGB,
                 GL_UNSIGNED_BYTE, outdata);

    //NOTE:: 4 byte alignment

    //The following should already be true:: TODO:: somehow!!!
    //glPixelStorei(GL_PACK_ALIGNMENT, 4);

    u8 bytes_per_pixel = 3;
    u32 rowbytes = (tex_colorbuffer.width*bytes_per_pixel + 4 - 1) & ~(4 - 1);
    u8 *top = ((u8*) outdata) + rowbytes * (tex_colorbuffer.height - 1);
    u8 *bottom = ((u8*)outdata);

    for(int i = 0; i < tex_colorbuffer.height/2; ++i)
    {
        swap_memory(top, bottom, rowbytes);
        top-=rowbytes;
        bottom+=rowbytes;
    }

}

internal void
opengl_delete_texture(u64 n, GLuint *textures)
{
    glDeleteTextures(n, textures);
}
