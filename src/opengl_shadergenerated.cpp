//TODO:: FIX LINE ENDINGS and weirdness!!
internal GLuint
opengl_load_shader(char *vert_shadersource, char *frag_shadersource);
 global_variable
struct
{
    GLuint ID;

    GLint model_loc;
    GLint projection_loc;
    GLint color_loc;
}opengl_rectshader;

global_variable
struct
{
    GLuint ID;

    GLint model_loc;
    GLint projection_loc;
    GLint tex_loc;
}opengl_texturedrectshader;

global_variable
struct
{
    GLuint ID;

    GLint tex_rect_loc;
    GLint model_loc;
    GLint projection_loc;
    GLint color_loc;
    GLint tex_loc;
    GLint fetchuv_loc;
    GLint fetchst_loc;
}opengl_textureshader;

char *rectshader_vertexsource = 
R"delimiter(#version 330 core
layout (location = 0) in vec3 pos;

uniform mat4 model;
uniform mat4 projection;

void main()
{
   gl_Position = projection * model * vec4(pos, 1.0f);
}
)delimiter";

char *rectshader_fragmentsource = 
R"delimiter(#version 330 core

uniform vec4 color;
out vec4 frag_color;
void main()
{
   frag_color = color; 
}
)delimiter";

char *texturedrectshader_vertexsource = 
R"delimiter(#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tex_coords;
layout (location = 2) in vec2 fetchuv;
layout (location = 3) in vec2 fetchst;
layout (location = 4) in vec3 color;
layout (location = 5) in mat4 model_attrib;
//layout (location = 9) in mat4 projection_attrib;

//uniform mat4 model;
//uniform mat4 projection;

out vec2 _tex_coords;
out vec2 _fetchuv;
out vec2 _fetchst;
out vec3 _color;
void main()
{

   gl_Position = model_attrib * vec4(pos, 1.0f); //projection * model * blah
   _tex_coords = tex_coords;

   _fetchuv = fetchuv;
   _fetchst = fetchst;
  // mat4 foo = model;
//   mat4 bar = projection;
   _color = color;
}
)delimiter";

char *texturedrectshader_fragmentsource = 
R"delimiter(#version 330 core

uniform sampler2D tex;

in vec2 _tex_coords;
in vec2 _fetchuv;
in vec2 _fetchst;
in vec3 _color;
in vec4 _modeprojection;

out vec4 frag_color;


void main()
{

   frag_color = vec4(_color, texture(tex, _tex_coords * (_fetchst-_fetchuv) + _fetchuv).r); 
}
)delimiter";

char *textureshader_vertexsource = 
R"delimiter(#version 330 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 tex_coords;
uniform vec4 tex_rect;

uniform mat4 model;
uniform mat4 projection;

out vec2 _tex_coords;
void main()
{
   gl_Position = projection * model * vec4(pos, 1.0f);
   _tex_coords = tex_coords;
}
)delimiter";

char *textureshader_fragmentsource = 
R"delimiter(#version 330 core

uniform vec3 color;
uniform sampler2D tex;
uniform vec2 fetchuv;
uniform vec2 fetchst;

out vec4 frag_color;

in vec2 _tex_coords;
void main()
{
   frag_color = vec4(color, 1.0f) * texture(tex, _tex_coords * (fetchst-fetchuv) + fetchuv); 
}
)delimiter";

internal void
load_allshaders()
{
    opengl_rectshader.ID = opengl_load_shader(rectshader_vertexsource, rectshader_fragmentsource);
    opengl_rectshader.model_loc = glGetUniformLocation(opengl_rectshader.ID, "model");
    opengl_rectshader.projection_loc = glGetUniformLocation(opengl_rectshader.ID, "projection");
    opengl_rectshader.color_loc = glGetUniformLocation(opengl_rectshader.ID, "color");

    opengl_texturedrectshader.ID = opengl_load_shader(texturedrectshader_vertexsource, texturedrectshader_fragmentsource);
    opengl_texturedrectshader.model_loc = glGetUniformLocation(opengl_texturedrectshader.ID, "model");
    opengl_texturedrectshader.projection_loc = glGetUniformLocation(opengl_texturedrectshader.ID, "projection");
    opengl_texturedrectshader.tex_loc = glGetUniformLocation(opengl_texturedrectshader.ID, "tex");

    opengl_textureshader.ID = opengl_load_shader(textureshader_vertexsource, textureshader_fragmentsource);
    opengl_textureshader.tex_rect_loc = glGetUniformLocation(opengl_textureshader.ID, "tex_rect");
    opengl_textureshader.model_loc = glGetUniformLocation(opengl_textureshader.ID, "model");
    opengl_textureshader.projection_loc = glGetUniformLocation(opengl_textureshader.ID, "projection");
    opengl_textureshader.color_loc = glGetUniformLocation(opengl_textureshader.ID, "color");
    opengl_textureshader.tex_loc = glGetUniformLocation(opengl_textureshader.ID, "tex");
    opengl_textureshader.fetchuv_loc = glGetUniformLocation(opengl_textureshader.ID, "fetchuv");
    opengl_textureshader.fetchst_loc = glGetUniformLocation(opengl_textureshader.ID, "fetchst");

}