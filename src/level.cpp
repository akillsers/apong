#define BALL_RADIUS 0.12
#define PTS_TO_WIN 7
#define PADDLE_MOVE_SPEED 17
#define PADDLE_WIDTH 0.15f
#define PADDLE_HEIGHT 1.5f
#define PADDLE_HORIZONTAL_MARGIN 0.8f   

internal void
set_ballspawn_timer(GameModeLevel *l)
{
    f32 ballspawn_delay = 2.3f;
    if(l->last_speed > 12.5f)
    {
        ballspawn_delay = 3.3f;
    }

    l->ballspawn_timer = ballspawn_delay;
}

internal f32
random_speed_easy(GameModeLevel *l)
{
    f32 ret = l->ballspeed;
    if(l->ballspeed == l->mode_info[l->current_mode].preset->ball_initial_speed)
    {
        ret += 3.2f;

        if(l->last_speed > 15.0f)
        {
            ret += 3.0f;
        }
    }
    else if(random_unilateral(&l->random) < 0.6)
    {
        ret += 0.6f;
    }
#if 0
    RandomSpeedNiche new_niche;

    f32 random_num = random_unilateral(&l->random);
    if(l->ballspeed < 8.0f)
    {
        if(random_num < 0.15)
        {
            new_niche = way_fast;
        }
        else
        {
            new_niche = slight_increment;
        }
    }
    else if (l->ballspeed > 14.0f)
    {
        if(random_num < 0.1)
            new_niche = way_slow;
        else
            new_niche = slight_increment;
    }
    else
    {
        if(random_num < 0.07)
            new_niche = way_fast;
        else if (random_num < 0.94)
            new_niche = slight_increment;
        else
            new_niche = way_slow;    
    }

    switch(new_niche)
    {
        case RandomSpeedNiche::way_slow:
        {
            ret = l->ballspeed - 2.5f - max(0, l->ballspeed - 7.0f) * 0.5f;
        }break;
        case RandomSpeedNiche::slight_increment:
        {
            if(random_unilateral(&l->random) < 0.6)
                ret = l->ballspeed + 0.30f;
            else
                ret = l->ballspeed + 0.15f;
        }break;
        case RandomSpeedNiche::way_fast:
        {
            ret = l->ballspeed + 2.4f + max(0, 13.0f - l->ballspeed) * 0.35f;
        }break;
    }
    
#endif
    //ret = l->ballspeed;
    return ret;
}

internal void
write_savegame(GameModeLevel *l)
{
    TIMED_BLOCK;

#if ALLOW_SAVEGAME
    PlatformFileHandle file_handle;
    FileError file_error = platform.open_file(path, &file_handle, CREATE_IF_NONEXISTENT | WRITE_ACCESS);

    if(HasFileError(file_error))
        return;

    u64 old_savegame_filesize = platform.get_file_size(file_handle);
    void *old_savegame_file = arena_pushsize(&temp_arena, old_savegame_filesize);
    platform.read_file(file_handle, old_savegame_file, 0, old_savegame_filesize, CREATE_IF_NONEXISTENT | WRITE_ACCESS);

#define pull(type)                             \
    *(type *)at;                               \
    at += sizeof(type);                        

    
    u8 *at = (u8 *) old_savegame_file + SCORE_CHUNK;
    u32 num_record_sets = pull(u32);
    u32 *score_offsets = arena_pushcount(&temp_arena, u32, num_record_sets);
    for(int i = 0; i < num_record_sets; i++)
    {
        score_offsets[i] = pull(u32);
    }
    u32 *old_num_played = arena_pushcount(&temp_arena, u32, num_record_sets);
    
    u32 total_played = 0;
    for(int i = 0; i < num_record_sets; i++)
    {
        old_num_played[i] = pull(u32);
        total_played += old_num_played[i];
    }

    ScoreRecord **old_records = arena_pushcount(&temp_arena, ScoreRecord*, num_record_sets);

    for(int i = 0; i < num_record_sets; i++)
    {
        old_records[i] = arena_pushcount(&temp_arena, ScoreRecord, old_num_played[i]);
    }

    for(int i = 0; i < num_record_sets; i++)
    {
        for(int j = 0; j < old_num_played[i]; j++)
        {
            old_records[i][j] = pull(ScoreRecord);
        }
    }
    
#undef pull    

    u32 total_records = 0;
    for(int i = 0; i < Mode_count; i++)
    {
        total_records += l->mode_info[i].num_played;
    }

    u32 fixed = SAVE_GAME_TOTAL_FIXED_SIZE;
    u32 filesize_in_bytes = SAVE_GAME_TOTAL_FIXED_SIZE + SAVE_GAME_PER_RECORD_SIZE * total_records;
    void *file_contents = arena_pushsize(&temp_arena, filesize_in_bytes);
    ((u8 *)file_contents + filesize_in_bytes - 1)[0] = 'e';

    at = (u8 *)file_contents;
    ((u32 *)at)[0] = 0;
    at += sizeof(u32);
    ((u32 *)at)[0] = 0;
    at += sizeof(u32);

    b32 is___unlocked[15] = {true,
                             true,
                             false, false, false,
                             false, false, false, false, false,
                             false, false, false, false, false};

    //NOTE:: THIS IS SOOO KEWL
    //*(b32(*)[15])at = is___unlocked;
    for(int i = 0; i < arraycount(is___unlocked); i++)
    {
        *(b32 *)at = is___unlocked[i];
        at += sizeof(b32);
    }
    for(int i = 0; i < 2; i++)
    {
        ((b32 *)at)[0] = l->mode_info[i].beat;
        at += sizeof(b32);
        ((u16 *)at)[0] = l->mode_info[i].highscore_left_score;
        at += sizeof(u16);
    }

    //num record sets
    ((u32 *)at)[0] = Mode_count;
    at += sizeof(u32);

    u32 file_offsets[2] = {FIRST_RECORD_SET_OFFSET,
                           FIRST_RECORD_SET_OFFSET + l->mode_info[Mode_normal].num_played * SAVE_GAME_PER_RECORD_SIZE};

    for(int i = 0; i < arraycount(file_offsets); i++)
    {
        *(u32 *)at = file_offsets[i];
        at += sizeof(u32);
    }

    u32 num_records[2] = {l->mode_info[Mode_normal].num_played,
                          l->mode_info[Mode_multiplayer].num_played};

    for(int i = 0; i < arraycount(file_offsets); i++)
    {
        *(u32 *)at = num_records[i];
        at += sizeof(u32);
    }

    for(int i = 0; i < Mode_count; i++)
    {
        //dump the stuff that used to be there
        //(do a memcpy)
        if(i >= num_record_sets)
            continue;
        assert(at - file_contents == file_offsets[i]);
        for(int j = 0; j < old_num_played[i]; j++)
        {
            ((ScoreRecord *)at)[0] = old_records[i][j];
            at += sizeof(ScoreRecord);
        }

    
        //NOW append on the new stuff
        u32 new_records_to_append = l->mode_info[i].num_played - old_num_played[i];
        u32 index =
            (l->mode_info[i].historic_scores_index + MAX_MEM_HISTORIC_SCORES - new_records_to_append) %
            MAX_MEM_HISTORIC_SCORES;
        for(int j = 0; j < new_records_to_append; j++)
        {
            ((u64 *)at)[0] = l->mode_info[i].historic_scores[index].time;//TODO FOR NOW
            at += sizeof(u64);
            ((u32 *)at)[0] = l->mode_info[i].historic_scores[index].left_score;
            at += sizeof(u32);
            ((u32 *)at)[0] = l->mode_info[i].historic_scores[index].right_score;
            at += sizeof(u32);
            index++;
        }
    }
    platform.write_file(file_handle, 0, filesize_in_bytes, (u8 *)file_contents, false);
    platform.close_file(file_handle);
#endif
}

internal void
win_game(GameState *g, GameModeLevel *l, Input *input)
{
    TIMED_BLOCK;
    
    ScoreRecord score_record;
    score_record.time = GetSecondsSince2000();
    score_record.left_score = l->left_player.pts;
    score_record.right_score = l->right_player.pts;

    l->mode_info[l->current_mode].historic_scores[l->mode_info[l->current_mode].historic_scores_index] =
            score_record;
    l->mode_info[l->current_mode].historic_scores_index =
        (l->mode_info[l->current_mode].historic_scores_index + 1) % MAX_MEM_HISTORIC_SCORES;
    
        
    l->mode_info[l->current_mode].num_played++;

    if(l->right_player.pts > l->left_player.pts)
    {
        if(!l->mode_info[l->current_mode].beat)
        {
            l->mode_info[l->current_mode].beat = true;
            l->mode_info[l->current_mode].highscore_left_score = PTS_TO_WIN;
        }
        if(l->left_player.pts < l->mode_info[l->current_mode].highscore_left_score)
        {
            l->mode_info[l->current_mode].highscore_left_score = l->left_player.pts;
        }

        //unlock the next mode if we win a perfect score
        if(l->left_player.pts == 0 && l->current_mode < Mode_count)
        {
            u32 unlock_mode = l->current_mode + 1;
            if(l->mode_info[unlock_mode].unlocked == false)
            {
                l->mode_info[unlock_mode].unlocked = true;
                l->unlocked_new = true;
            }
        }
    }
    write_savegame(l);    
    
    l->wongame = true;
    l->win_game_time = input->dt_from_startup;
}

//NOTE:: this must be called before entering the new game menu
UIFocusGroup menu_focusgroups[Menu_Count];

internal void
prepare_new_game_menu(GameModeLevel *l)
{
    ui_reset_sticky_active(&menu_focusgroups[Menu_NewGame]);
    l->menu.flashing = false;
    ui_state.no_mouse_focusing = false;
}

internal f32
calculate_bally_at_x(GameModeLevel *l, Ball ball, f32 wallx)
{
    TIMED_BLOCK;
    Vec2 velocity = ball.vel;
    f32 dist_to_wallx = ball.b_circle.center.x - wallx;
    f32 HowManyVelocitiesCanFitUntilWall = dist_to_wallx / absolute(velocity.x);

    Vec2 to_wall = velocity * HowManyVelocitiesCanFitUntilWall;
    Vec2 ballpoint = ball.b_circle.center + to_wall;

recheck_in_bounds:
    if(ballpoint.y < l->court_bounds.y1)
    {
        ballpoint.y += ((l->court_bounds.y1 - ballpoint.y) * 2) + 2*l->ball->b_circle.radius;
        goto recheck_in_bounds;
    }
    else if (ballpoint.y > l->court_bounds.y2)
    {
        ballpoint.y -= ((ballpoint.y - l->court_bounds.y2)*2) + 2*l->ball->b_circle.radius;
        goto recheck_in_bounds;
    }
    f32 ret = ballpoint.y;
    return ret;
}

internal void
update_ballys(GameModeLevel *l)
{
    TIMED_BLOCK;
    l->ball_targety = calculate_bally_at_x(l, *l->ball,
                                           l->left_player.player_paddle->bbox.x +
                                           l->left_player.player_paddle->bbox.width +
                                           l->ball->b_circle.radius);
    l->ball_lefty = calculate_bally_at_x(l, *l->ball, l->left_player.player_paddle->bbox.x);
}

internal void
serve_ball(GameState *g, GameModeLevel *l, b32 serveleft)
{
    TIMED_BLOCK;
    l->ball->b_circle.center = AABB_center(l->court_bounds);
    
    //TODO MAKE THIS GO TO THE WINNER OR THE LOSER NOT ALWAYS THE RIGHT PLAYER
    //TODO make this balldir.x go all the way to the paddle
    Vec2 balldir;
    balldir.x = serveleft ? -7 : 7;

    f32 mindiry = -5;
    f32 maxdiry = 5;
    f32 t = random_unilateral(&l->ballserve_random);

    balldir.y = mindiry + t*(maxdiry - mindiry);

    l->ball->vel = balldir;
    l->ball->vel = normalize(l->ball->vel);
    l->ball->vel *= l->mode_info[l->current_mode].preset->ball_initial_speed;
    l->ballspeed = l->mode_info[l->current_mode].preset->ball_initial_speed;
    update_ballys(l);
}

internal void
start_game(GameState *g, GameModeLevel *l, Mode mode)
{
    TIMED_BLOCK;

    if(mode == Mode_multiplayer)
        l->left_player.isAI = false;
    else
    {
        l->left_player.isAI = true;
        *l->left_player.player_paddle->ai = {};
        l->left_player.player_paddle->ai->ai_settings = l->mode_info[mode].preset->ai_settings;
        random_randomize(&l->left_player.player_paddle->ai->random);
    }

    switch(mode)
    {
        case Mode_normal:
        case Mode_multiplayer:
            l->random_speed = random_speed_easy;
            break;
        default:
            stop("no speed func provided");
    }


    l->left_player.player_paddle->bbox.y = (l->court_bounds.y2-l->court_bounds.y1)/2 - PADDLE_HEIGHT/2;
    l->right_player.player_paddle->bbox.y = (l->court_bounds.y2 - l->court_bounds.y1)/2 - PADDLE_HEIGHT / 2;

    l->menu.can_esc_inout = true;
    l->left_player.pts = 0;
    l->right_player.pts = 0;
    l->wongame = false;
    l->serve_to_right = true;
    l->endless = false;

    //NOTE::so we can restart the ball
    l->wonmatch = true; 

    l->current_mode = mode;
    l->ballspeed = l->mode_info[mode].preset->ball_initial_speed;

    l->ballspeed = l->random_speed(l);
    l->ballspawn_delay = l->mode_info[mode].preset->ball_initial_spawndelay;
    l->ballspawn_timer = l->ballspawn_delay;
    l->left_player.player_paddle->ai->targety = l->left_player.player_paddle->bbox.y;
    l->last_speed = 0;
}


internal void
restart_game(GameState *g, GameModeLevel *l)
{
    TIMED_BLOCK;
    start_game(g, l, l->current_mode);
}

internal b32
load_savegame_data(GameState *g, GameModeLevel *l)
{
    TIMED_BLOCK;
    b32 ret = false;

#if ALLOW_SAVEGAME
    PlatformFileHandle handle = 0;
    
    FileError ferror = platform.open_file(g->save_path, &handle, 0);

    if(ferror == FileError_nonexistent)
    {
        // write default savegame file::
    write_default:
        l->mode_info[Mode_normal].unlocked = true;
        l->mode_info[Mode_multiplayer].unlocked = true;

        write_savegame(l, g->save_path);

        goto done;
    }
    else if (ferror == FileError_path_not_found)
    {
        goto write_default;
    }
    else if(ferror == FileError_success)
    {
        //TODO the program can easily crash here if the savegame file is corrupt
        u64 savegame_filesize = platform.get_file_size(handle);
        void *savegame_file = arena_pushsize(&temp_arena, savegame_filesize);
        platform.read_file(handle, savegame_file, 0, savegame_filesize, true);

        u8 *at = (u8 *)savegame_file;

        u32 major_format = *(u32 *)at;
        at += 4;
        u32 minor_format = *(u32 *)at;
        at += 4;

        // char buf[45];
        // sprintf(buf, "maj: %u, min: %u\n", major_format, minor_format);
        // OutputDebugString(buf);
        if(major_format == 0 && minor_format == 0)
        {
	        l->mode_info[Mode_normal].unlocked = *(b32 *)at;
            at +=4;
            l->mode_info[Mode_multiplayer].unlocked = *(b32 *)at;
            at +=4;

            at += 4*12;//reserved locks

            for(int i = 0; i < 2; i++)
            {
                l->mode_info[i].beat = *(b32 *)at;
                at += sizeof(b32);
                l->mode_info[i].highscore_left_score = *(u16 *)at;
                at += sizeof(u16);
            }

            u32 num_record_chunks = *(u32 *)at;
            at += 4;

            u32 *record_offsets = (u32 *)arena_pushsize(&temp_arena, sizeof(u32) * num_record_chunks);
            for(int i = 0; i < num_record_chunks; i++)
            {
                record_offsets[i] = *(u32 *)at;
                at += 4;
            }
            u32 *num_stat_entries = (u32 *)arena_pushsize(&temp_arena, sizeof(u32) * num_record_chunks);
            char buf[453];
            // sprintf(buf, "num records entries!! %u\n", num_record_chunks);
            // OutputDebugString(buf);

            for(int i = 0; i < num_record_chunks; i++)
            {
                num_stat_entries[i] = *(u32 *)at;
                l->mode_info[i].num_played = num_stat_entries[i];
                char buf[453];
                // sprintf(buf, "num stat entries!! %u\n", num_stat_entries[i]);
                // OutputDebugString(buf);
                at += 4;
            }
            for(int i = 0; i < num_record_chunks; i++)
            {
                u32 first_stat_to_load =
                    (num_stat_entries[i] > MAX_MEM_HISTORIC_SCORES) ?
                    num_stat_entries[i] - 2 :
                    0;
                at = (u8 *)savegame_file + record_offsets[i] + (first_stat_to_load * sizeof(ScoreRecord));

                //load in the last two historic scores and set the historic scores index to the next one
                int j = 0;
                for(; j < min(num_stat_entries[i], MAX_MEM_HISTORIC_SCORES); j++)
                {
                    //the chunks correspond to modes
                    //at += sizeof(u64);//FOR NOW
                    
                    l->mode_info[i].historic_scores[j] = *(ScoreRecord *)at;
                    at += sizeof(ScoreRecord);
                }
                l->mode_info[i].historic_scores_index = (j + 1) % MAX_MEM_HISTORIC_SCORES;
            }

            //assert(at[0] == 'e');
            ret = true;
        }
        else
        {
            //OutputDebugString("writing default\n");
            goto write_default; //for now just overwrite the save game file
        }

        
    }

done:
    platform.close_file(handle);
#endif
    return ret;
}


internal GameModeLevel *
init_level(GameState *g, MemoryArena *level_arena)
{
    TIMED_BLOCK;
    GameModeLevel *l = arena_pushtype(level_arena, GameModeLevel);
    
    l->court_bounds.x1 = 0;

    b32 have_savegame = load_savegame_data(g, l);

    for(int i = 0; i < Mode_count; i++)
    {
        l->mode_info[i].preset = &diff_presets[i];
    }
    

    l->mode_info[Mode_normal].unlocked = true;
    l->mode_info[Mode_multiplayer].unlocked = true;

    l->mode_info[Mode_normal].name = normal;
    l->mode_info[Mode_multiplayer].name = multiplayer;

    l->mode_info[Mode_normal].texture_name = normal;
    l->mode_info[Mode_multiplayer].texture_name = multiplayer_tex;


    //COURT is 16 meters by 9 meters
    l->court_bounds.x1 = 0 - BALL_RADIUS*2;
    l->court_bounds.y1 = 0;
    l->court_bounds.x2 = 16 + BALL_RADIUS*2;
    l->court_bounds.y2 = 9;
    l->left_player.player_paddle = arena_pushtype(level_arena, Paddle);
    l->right_player.player_paddle = arena_pushtype(level_arena, Paddle);


    //l->left_player.player_paddle->ai = {}; //needed for gotoball, etc.
    l->left_player.player_paddle->ai = arena_pushtype(level_arena, PaddleAIState);
    random_randomize(&l->random);

    l->ball = arena_pushtype(level_arena, Ball);
    l->ball->b_circle.r = BALL_RADIUS;

    random_randomize(&l->ballserve_random);

    Rect *lpaddle_bbox = &l->left_player.player_paddle->bbox;
    lpaddle_bbox->x = l->court_bounds.x1 + PADDLE_HORIZONTAL_MARGIN;
    lpaddle_bbox->y = (l->court_bounds.y2-l->court_bounds.y1)/2 - PADDLE_HEIGHT/2;
    lpaddle_bbox->width = PADDLE_WIDTH;
    lpaddle_bbox->height = PADDLE_HEIGHT;

    Rect *rpaddle_bbox = &l->right_player.player_paddle->bbox;
    rpaddle_bbox->x = l->court_bounds.x2  - PADDLE_HORIZONTAL_MARGIN - PADDLE_WIDTH;
    rpaddle_bbox->y = (l->court_bounds.y2 - l->court_bounds.y1)/2 - PADDLE_HEIGHT / 2;
    rpaddle_bbox->width = PADDLE_WIDTH;
    rpaddle_bbox->height = PADDLE_HEIGHT;

    
    // get the ball out of the screen a bit of a hack
    // since we should just not draw the ball at all
    l->ball->b_circle.center.x = 4;
    l->ball->b_circle.center.y = -8;
    start_game(g, l, Mode_normal);
    return l;
}

enum BallCollisionResult
{
    BallHitNothing,
    BallHitLeftWall,
    BallHitRightWall,
    BallHitPaddle
};

global_variable Vec2 dbg_collision_last;
global_variable Vec2 dbg_collision_wouldbe;
global_variable b32 dbg_collision_show = false;
global_variable b32 dbg_wouldbecollision_toggler = true;
global_variable b32 dbg_precollision_toggler = true;
global_variable b32 dbg_control_with_mouse = true;

internal void
ball_update(GameState *g, GameModeLevel *l, Input *input,
            AABB *collidable_AABBs, u32 num_collidables)
{
    TIMED_BLOCK;

    Circle *b_circle = &l->ball->b_circle;

    Vec2 lastpos = b_circle->center;
    Vec2 dbg_vel = v2(0,0);
#if INTERNAL && 0
    if(input->key_pressedb(KEY_m))
    {
        dbg_control_with_mouse = !dbg_control_with_mouse;
    }
    
    if(dbg_control_with_mouse)
    {
        Vec2 mousepos;
        mousepos.x = input->mousex;
        mousepos.y = input->mousey;
        b_circle->center = PixelsToMeters(mousepos); //Move ball
    }
    else
    {

        f32 dbg_speed = 5;

        if(input->key[KEY_lCtrl].down)
            dbg_speed *= 10;
        if(input->key[KEY_lShift].down)
            dbg_speed /= 5;

        if(input->key_pressed(KEY_w))
        {
            dbg_vel.y = -dbg_speed;
        }
        else if(input->key_pressed(KEY_s))
        {
            dbg_vel.y = dbg_speed;
        }
        else if(input->key_pressed(KEY_a))
        {
            dbg_vel.x = -dbg_speed;
        }
        else if(input->key_pressed(KEY_d))
        {
            dbg_vel.x = dbg_speed;
        }
        else if (input->key_pressed(KEY_e))
        {
            dbg_vel.x = 1;
            dbg_vel.y = -2;
            dbg_vel = normalize(dbg_vel);

            dbg_vel *= dbg_speed;
        }
        else if (input->key_pressed(KEY_c))
        {
            dbg_vel.x = 2;
            dbg_vel.y = 1;
            dbg_vel = normalize(dbg_vel);

            dbg_vel *= dbg_speed;
        }
        else if (input->key_pressed(KEY_z))
        {
            dbg_vel.x = -1;
            dbg_vel.y = 1;
            dbg_vel = normalize(dbg_vel);

            dbg_vel *= dbg_speed;
        }   
        else if (input->key_pressed(KEY_q))
        {
            dbg_vel.x = -2;
            dbg_vel.y = -1;
            dbg_vel = normalize(dbg_vel);

            dbg_vel *= dbg_speed;
        }
        if(!dbg_vel)
        {
            dbg_collision_show = false;
        }


        // if(b_circle->center.x > l->court_bounds.x2 - 0.5)
        // {
        //     l->ball->vel = v2(-1,0) * l->ballspeed;
        // }
        // else if (b_circle->center.x < l->court_bounds.x2 -2)
        // {
        //     l->ball->vel = v2(1,0) * l->ballspeed;
        // }
        
        b_circle->center += dbg_vel * input->dt; //Move ball
    }
#else
    
    b_circle->center += l->ball->vel * input->dt; //Move ball
#endif

//BLOCKSECITON::Rect / Ball Collision
    {
        AABB thisball_aabb = {};
        thisball_aabb.x1 = b_circle->x - b_circle->radius;
        thisball_aabb.y1 = b_circle->y - b_circle->radius;
        thisball_aabb.x2 = b_circle->x + b_circle->radius;
        thisball_aabb.y2 = b_circle->y + b_circle->radius;

        AABB lastball_aabb = {};
        lastball_aabb.x1 = lastpos.x - b_circle->radius;
        lastball_aabb.y1 = lastpos.y - b_circle->radius;
        lastball_aabb.x2 = lastpos.x + b_circle->radius;
        lastball_aabb.y2 = lastpos.y + b_circle->radius;
        
        AABB travel_aabb = {};
        travel_aabb.x1 = minimum(lastball_aabb.x1, thisball_aabb.x1);
        travel_aabb.y1 = minimum(lastball_aabb.y1, thisball_aabb.y1);
        travel_aabb.x2 = maximum(lastball_aabb.x2, thisball_aabb.x2);
        travel_aabb.y2 = maximum(lastball_aabb.y2, thisball_aabb.y2);
        
        for(int i = 0; i < num_collidables; i++)
        {
            AABB *rect = collidable_AABBs + i;
            
            if(1 || AABB_intersectAABB(collidable_AABBs[i], travel_aabb))
            {
                //We need to check this paddle for a collision
                Vec2 d = b_circle->center - lastpos;
                CollisionResult collision;
                collision =
                    circle_checkcollisionwith_AABB(lastpos, d, b_circle->radius, collidable_AABBs[i]);
                if(collision.collided)
                {
                    Asset *ding_asset = get_asset_by_name(&g->assets, "ding", sound);
                    play_sound(g, ding_asset->sound, 1.0f);

                    //TODO unsolved mystery of CS what is the smallest number that can be subtracted from
                    //     collision.t
                    //     HINT it depends on the length of d (d is the dist from lastpos to nextpos)
                    //     We might want to normalize d so that it is a unit vector so we can
                    //     get a constant answer

                    //we can do a nextafterf to subtract collision.t * d in the direction
                    //of lastpos - d
                    b_circle->center = lastpos + ((collision.t - 0.0001)*d);

                    //Vec2 hitat = lastpos + collision.t*d;
                    // char buf[456];
                    // sprintf(buf, "resolution(%0.12f, %0.12f)\nhit at    (%0.12f, %0.12f)\n",
                    //         b_circle->center.x, b_circle->center.y, hitat.x, hitat.y);
                    // OutputDebugString(buf);
                    
                    f32 intensefactor = 1.2;                    
                    //ret = BallHitPaddle;

                    //TODO put this somewhere else??
                    //l->ballspeed += l->mode_info[l->current_mode].preset->ballspeed_increment;
                    l->ballspeed = l->random_speed(l);
                    
                    //TODO:: random:: make the ia competency change over time??

                    if((collision.contactpoint.x == collidable_AABBs[i].x1 ||
                        collision.contactpoint.x == collidable_AABBs[i].x2))
                    {
                        //we hit the left or right vertical edge::
                        //do paddle physics
                        
                        l->ball->vel.x = 7;
                        if(collision.contactpoint.x == collidable_AABBs[i].x1)
                            l->ball->vel.x *= -1;

                        // TODO make percent range from the ball above and below the paddle
                        //     for that... we cannot collision.contactpoint
                        Vec2 contactpos = lastpos + collision.t*d;
                        f32 miny = collidable_AABBs[i].y1 - b_circle->radius;
                        f32 maxy = collidable_AABBs[i].y2 + b_circle->radius;
                        f32 percent = (contactpos.y-miny)/(maxy-miny);
                        
                        l->ball->vel.y = -7 + (percent * 14);
                        
                        // char buf[324];
                        // sprintf(buf,"contact: %f\n", percent);
                        // OutputDebugString(buf);

                        if(l->ball->vel.x < 0)
                        {
                            //update the target y if the ball is going leftwards
                            //so that the ai works
                            update_ballys(l);
                        }

                        // l->ball->vel.x = (collidable_AABBs[i].y2-collidable_AABBs[i].y1)/intensefactor;
                        // if(collision.contactpoint.x == collidable_AABBs[i].x1)
                        //     l->ball->vel.x *= -1;

                        // f32 miny = collidable_AABBs[i].y1 - b_circle->radius*2;
                        // f32 maxy = collidable_AABBs[i].y2 + b_circle->radius*2;
                        // f32 centery = (miny+maxy)/2;

                        // l->ball->vel.y = collision.contactpoint.y - centery;
                    }
                    else
                    {
                        //we hit a horizontal edge::
                        //do normal physics

                        Vec2 normal = collision.contactpoint - b_circle->center;
                        normal = -normal;
                        normal = normalize(normal);
#if 1
                        l->ball->vel = l->ball->vel - 2*inner(l->ball->vel, normal)*normal;
#else
                        l->ball->vel = dbg_vel - 2*inner(dbg_vel, normal)*normal;
#endif
                    }

                    l->ball->vel = normalize(l->ball->vel);
                    l->ball->vel *= l->ballspeed;

                    b_circle->center = b_circle->center + (1 - collision.t)* l->ball->vel*input->dt;
                }
            }
        }

        f32 minBoundsX = l->court_bounds.x1;
        f32 minBoundsY = l->court_bounds.y1;
        f32 maxBoundsX = l->court_bounds.x2;
        f32 maxBoundsY = l->court_bounds.y2;

        if(b_circle->center.y + b_circle->radius >= maxBoundsY)
        {
            //we have hit bottom wall
            f32 penetration_depth_y = b_circle->center.y + b_circle->radius - maxBoundsY;
            b_circle->center.y = maxBoundsY - b_circle->radius - penetration_depth_y; 

            l->ball->vel.y = -l->ball->vel.y;

            play_sound(g, "wall", 1.0f);
        }
        else if(b_circle->center.y - b_circle->radius <= minBoundsY)
        {
            //we have hit the top wall
            f32 penetration_depth_y = (b_circle->center.y - b_circle->radius) - minBoundsY;
            b_circle->center.y = (minBoundsY + b_circle->radius) - penetration_depth_y ;            

            l->ball->vel.y = -l->ball->vel.y;
            play_sound(g, "wall", 1.0f);
        }

        //NOTE:: this is in a different else if chain because
        //       if we hit the bottom or top wall, there is a
        //       chance we may then resolve the collision
        //       into the left or right wall
        if(b_circle->center.x + b_circle->radius >= maxBoundsX)
        {
            //BALL hit the right wall
            l->left_player.pts++;

            if(l->left_player.pts >= PTS_TO_WIN)
            {
                l->wonmatch = true;
                win_game(g, l, input);
                play_sound(g, "zap", 1.0f);
            }
            else
            {
                play_sound(g, "boop", 0.6f);
                l->wonmatch = true;
                l->last_speed = l->ballspeed;
                l->serve_to_right = true;

                set_ballspawn_timer(l);
            }

            
        }
        else if(b_circle->center.x - b_circle->radius <=  minBoundsX)
        {
            //BallHitLeftWall
            l->right_player.pts++;

            if(l->right_player.pts >= PTS_TO_WIN && !l->endless)
            {
                l->wonmatch = true;
                win_game(g, l, input);
                play_sound(g, "zap", 1.0f);
            }
            else
            {
                play_sound(g, "boop", 0.6f);
                //spawn_ball(l->ballspawn_delay, SERVE_TO_LEFT);
                l->wonmatch = true;
                l->last_speed = l->ballspeed;
                l->serve_to_right = false;

                set_ballspawn_timer(l);
            }

        }

    }

    dbg_markerm(l->left_player.player_paddle->bbox.x +
                l->left_player.player_paddle->bbox.width, l->ball_targety);
    dbg_markercm(l->left_player.player_paddle->bbox.x +
                 l->left_player.player_paddle->bbox.width/2, l->ball_lefty, COLOR_AKGOLDEN);
}



internal void
ball_render(GameState *g, GameModeLevel *l, Circle *b_circle)
{
    Asset *ball = get_asset_by_name(&g->assets, "ball", tex1c);

    push_texture_masked(&g->rlist, RENDER_LAYER_GAME, MetersToPixels(b_circle->x - b_circle->radius),
                        MetersToPixels(b_circle->y - b_circle->radius),
                        MetersToPixels(b_circle->radius * 2),
                        MetersToPixels(b_circle->radius * 2),
                        0, 0, 1, 1,
                        COLOR_AKWHITE, ball);

}

global_variable f32 dbg_offset;
/*
  AI TODO::
multiple move steps (how to determine how many guesses and when to make??)
if(slope is high and we are far enough) == overshots
if(the ball is going up so we want to center with the ball but one press is too much) == overshot

sometimes just randomly even tho its good but the slope is down we think we need to go down and the we end up missing
it + mega recovery (happends when ballspeed fast)

make the guesses more accurate as the ball approaches the duddett

recovery for overshots and understhots (TODO undershots:: start recovery earlier (not when hit the wallx))
*/
#define AI_MOVEMENT_GRANULARITY 0.19f
internal f32
paddle_get_fail_recovery_targety(GameState *g, GameModeLevel *l, Paddle *paddle, Input *input)
{
    PaddleAIState *ai = paddle->ai;
    f32 ret;
    f32 recover_depth = random_range(&ai->random, 0.9f, 3.5f);
    if(ai->targety + paddle->bbox.height/2 > l->ball_targety)
    {
        //recover by moving up
        ret = ai->targety - recover_depth;
    }
    else
    {
        //recover by moving down
        ret = ai->targety + recover_depth;
    }

    return ret;
}



internal f32
paddle_get_humanlike_targety(GameState *g, GameModeLevel *l, Paddle *paddle, Input *input,
                             f32 seconds_until_impact)
{
    //TODO the paddle has some movement granularity and can't move that small steps!
    PaddleAIState *ai = paddle->ai;
    f32 ret;
#define MIN_OVERESTIMATE_AMOUNT l->ball->b_circle.radius * 2.0
#define MIN_UNDERESTIMATE_AMOUNT l->ball->b_circle.radius * 1.8
    b32 underestimate = false;
    b32 overestimate = false;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 ball_slope_sign = sign(ball_slope);
    dbg_var("slope", T_f32, ball_slope);
    dbg_var("slope sign", T_f32, ball_slope_sign);

    f32 center_to_ball = (paddle->bbox.y + paddle->bbox.height/2) > l->ball_targety;
    f32 closest_edge_to_ball = minimum(absolute(paddle->bbox.y - (l->ball_targety + l->ball->b_circle.radius)),
                                   absolute((paddle->bbox.y + paddle->bbox.height) -
                                            l->ball_targety - l->ball->b_circle.radius));
    b32 ball_inside_paddle = (l->ball_targety >= paddle->bbox.y &&
                              l->ball_targety <= paddle->bbox.y + paddle->bbox.height);
    b32 would_overestimate_upwards = (paddle->bbox.y + paddle->bbox.height/2) > l->ball_targety;
    f32 max_overestimate_amount_possible = would_overestimate_upwards ?
        ((l->ball_targety - l->ball->b_circle.radius) - l->court_bounds.y1) - paddle->bbox.height :
        (l->court_bounds.y2 - (l->ball_targety + l->ball->b_circle.radius)) - paddle->bbox.height;

    f32 max_move_distance = PADDLE_MOVE_SPEED * seconds_until_impact;
    f32 max_overestimate_amount_time_constrained =
        would_overestimate_upwards ?
        (l->ball_targety - l->ball->b_circle.radius) - (paddle->bbox.y - max_move_distance) :
        (paddle->bbox.y + max_move_distance) - (l->ball_targety + l->ball->b_circle.radius);
    

    b32 enough_space_to_overestimate =  max_overestimate_amount_possible >= MIN_OVERESTIMATE_AMOUNT;

    dbg_var("max move distance", T_f32, max_move_distance);
    dbg_var("max overestiamte amt time constrained", T_f32, max_overestimate_amount_time_constrained);
    b32 enough_time_to_overestimate = max_overestimate_amount_time_constrained >= MIN_OVERESTIMATE_AMOUNT;
    // b32 enough_space_to_overestimate =  ?
        //     l->court_bounds.y1 + paddle->bbox.height + MIN_OVERESTIMATE_AMOUNT < l->ball_targety : //overestimate upwards
        //     l->court_bounds.y2 - paddle->bbox.height -  MIN_OVERESTIMATE_AMOUNT > l->ball_targety;//overestimate downwards
    
    // if(enough_space_to_overestimate && seconds_until_impact < 0.3f &&
    //    (l->ballspeed > 12.0f && absolute(ball_slope) > 0.35f && center_to_ball > 3.0f) ||
    //    (l->ballspeed > 14.0f && absolute(ball_slope) > 0.1f) && center_to_ball > 0.0f)
    //     overestimate = true;
    // else
    //     overestimate = false;

    // overestimate = false;

    b32 enough_distance_to_underestimate =
        closest_edge_to_ball >=
        MIN_UNDERESTIMATE_AMOUNT + 0.3f;


    //rarely do we underestimate when we are already in ball->targety, but there is a chance especially when
    //the ball is moving fast
    b32 underestimate_anyway = ai->ai_settings.calculate_underestimate_anyway_func(l, paddle);
    b32 underestimate_normal = ai->ai_settings.calculate_underestimate_normal_func(l, paddle);
    b32 underestimate_plausible = (enough_distance_to_underestimate && !ball_inside_paddle);

    b32 overestimate_normal = ai->ai_settings.calculate_overestimate_normal_func(l, paddle);
    //NOTE:: if we are plausible and we can underestimate:: we don't want to underestimate
    //       further away; this can happen becuase we can be plausible without being
    //       the max underestimate distance away from the ball
    b32 underestimate_can_go_further_away = 0;

    if((underestimate_plausible && underestimate_normal))
    {
        underestimate = true;
        underestimate_can_go_further_away = false;
    }else if (underestimate_anyway)
    {
        underestimate = true;
        underestimate_can_go_further_away = true;
    }
    else
    {
        underestimate = false;        
    }

    b32 overestimate_plausible = enough_space_to_overestimate && enough_time_to_overestimate;//&& seconds_until_impact < 2.0f;
    if(overestimate_plausible && overestimate_normal)
    {
        overestimate = true;
    }

    dbg_var("overestimate enough space", T_b32, enough_space_to_overestimate);
    dbg_var("overestimate enough time", T_b32, enough_time_to_overestimate);
    dbg_var("underestimate anyway", T_b32, underestimate_anyway);
    dbg_var("underestimate plausible", T_b32, underestimate_plausible);
    dbg_var("underestimate normal", T_b32, underestimate_normal);
    // if (
    //     absolute(ball_slope) > 0.2 &&
    //     //sign(ball_slope) == sign((paddle->bbox.y + paddle->bbox.height/2) - l->ball_targety) &&
    //     l->ballspeed > 10.0f)
    // {
    //     overestimate = random_unilateral(&ai->random) > 1.0f ? false : true;
    // }
    if(overestimate && underestimate)
    {
        if(random_unilateral(&ai->random) < 0.5f)
            overestimate = false;
        else
            underestimate = false;
    }

    dbg_var("overestimate", T_b32, overestimate);
    dbg_var("underestimate", T_b32, underestimate);

    if(underestimate)
    {
        ai->will_miss = true;
        
        b32 can_underestimate_below = (l->ball_targety + MIN_UNDERESTIMATE_AMOUNT <
                                      l->court_bounds.y2 - paddle->bbox.height);
        b32 can_underestimate_above = (l->ball_targety - MIN_UNDERESTIMATE_AMOUNT - paddle->bbox.height >
                                    l->court_bounds.y1);

        //the closes option is alway preffered
        //is we are closer to underestimating below, we want to underestimate below
        b32 preferred_underestimate_below = paddle->bbox.y + paddle->bbox.height/2 > l->ball_targety;

        //NOTE:: this assumes that the court is big enough to support underestimating at least
        //       one way
        b32 underestimate_below = 0;
        if(preferred_underestimate_below && can_underestimate_below ||
           (preferred_underestimate_below == 0) && can_underestimate_above)
            underestimate_below = preferred_underestimate_below;
        else
        {
            assert(can_underestimate_below || can_underestimate_above);
            assert(can_underestimate_below ^ can_underestimate_above);
            if(can_underestimate_below)
                underestimate_below = true;
            if(can_underestimate_above)
                underestimate_below = false;
        }

        f32 max_underestimate_amount;
        if(underestimate_can_go_further_away)
        {
            max_underestimate_amount = MIN_UNDERESTIMATE_AMOUNT + 0.6f;
        }
        else
        {
            //NOTE:: it is not possbile to not underestimate and go further away if the ball is inside
            //       the paddle
            assert(!ball_inside_paddle);
            max_underestimate_amount = minimum(MIN_UNDERESTIMATE_AMOUNT + 0.6f, closest_edge_to_ball);
        }
        f32 underestimate_amount = random_range(&ai->random, MIN_UNDERESTIMATE_AMOUNT,
                                                max_underestimate_amount);
        // dbg_var("max underestimate", T_f32, max_underestimate_amount);
        // dbg_var("closese edge to ball", T_f32, closest_edge_to_ball);
        // dbg_var("can underestimate below", T_b32, can_underestimate_below);
        // dbg_var("can underestimate above", T_b32, can_underestimate_above);
        // dbg_var("underestimate below", T_b32, underestimate_below);
        // dbg_var("preferred_underestimate below", T_b32, preferred_underestimate_below);
        
        
        if(underestimate_below)
        {
            //underestimate below the target
            ret = l->ball_targety + underestimate_amount;
        }
        else
        {
            //underestimate above the target
            ret = l->ball_targety - underestimate_amount - paddle->bbox.height;
        }
    }
    else if (overestimate)
    {
        ai->will_miss = true;
        f32 max_overestimate_amount = minimum(MIN_OVERESTIMATE_AMOUNT + 0.6f, max_overestimate_amount_possible);
        f32 overestimate_amount = random_range(&ai->random, MIN_OVERESTIMATE_AMOUNT,
                                               max_overestimate_amount);
        if(paddle->bbox.y + paddle->bbox.height/2 > l->ball_targety)
        {
            //paddle needs to move up
            ret = l->ball_targety - paddle->bbox.height - overestimate_amount;
        }
        else
        {
            //paddle needs to move down
            ret = l->ball_targety + overestimate_amount;
        }

    }
    else
    {
        ai->will_miss = false;

        //entire range we could go if we had no min movement step requirements
        //this offset range starts at AI_MOVEMENT_GRANULARITY
        f32 whole_paddle_offset_range = (paddle->bbox.height - AI_MOVEMENT_GRANULARITY) - AI_MOVEMENT_GRANULARITY;
        //this is the absolute world coordinates of where we can go
        f32 whole_world_range_start = l->ball_targety - paddle->bbox.height + AI_MOVEMENT_GRANULARITY;
        f32 whole_world_range_end = whole_world_range_start + whole_paddle_offset_range;

        if(whole_world_range_end + paddle->bbox.height > l->court_bounds.y2)
        {
            //i dont think we need AI_MOVEMENT_GRANULARITY here because it is hard for the ball
            //because the paddle is cut off
            whole_world_range_end = l->court_bounds.y2 - paddle->bbox.height;
        }
        else if (whole_world_range_start < l->court_bounds.y1)
        {
            whole_world_range_start = l->court_bounds.y1;
        }
        
        f32 no_go_half_range = PADDLE_MOVE_SPEED * 0.03;
        f32 no_go_range_start = paddle->bbox.y - no_go_half_range;
        f32 no_go_range_end = paddle->bbox.y + no_go_half_range;

        //DEBUG::
        dbg_rect(MetersToPixels(paddle->bbox.x + paddle->bbox.width),
                 MetersToPixels(whole_world_range_start), MetersToPixels(0.5),
                 MetersToPixels(whole_world_range_end - whole_world_range_start), COLOR_AKVIOLET);

        dbg_rectt(MetersToPixels(paddle->bbox.x + paddle->bbox.width),
                 MetersToPixels(no_go_range_start), MetersToPixels(0.5),
                 MetersToPixels(no_go_range_end - no_go_range_start), COLOR_AKGOLDEN, 0.8f);
        f32 range_of_goability = whole_world_range_end - whole_world_range_start;
        f32 no_go_intersection_start = maximum(whole_world_range_start, no_go_range_start);
        f32 no_go_intersection_end = minimum(whole_world_range_end, no_go_range_end);
        f32 no_go_intersection = no_go_intersection_end - no_go_intersection_start;

        range_of_goability -= no_go_intersection;
        f32 goability_start = (no_go_intersection_start == no_go_range_start) ?
            whole_world_range_start :
            no_go_intersection_end;

        if(range_of_goability <= 0)
        {
            dbg_var("full intersection", T_f32, range_of_goability);
            ret = random_range(&ai->random, whole_world_range_start, whole_world_range_end);
        }
        else
        {
            f32 offset = random_range(&ai->random, 0, range_of_goability);
            if(no_go_intersection_start <= whole_world_range_end &&
               no_go_intersection_end >= whole_world_range_start)
            {
                dbg_var("intersection", T_f32, offset);
                if(no_go_intersection_start > whole_world_range_start)
                {
                    if(no_go_intersection_end < whole_world_range_end)
                    {
                        //completley inside
                        if(offset < no_go_intersection_start - whole_world_range_start)
                        {
                            dbg_var("complete inside top_half", T_f32, offset);
                            goto top_half;
                        }
                        else
                        {
                            dbg_var("complete inside bottom_half", T_f32, offset);
                            goto bottom_half;
                        }
                    }
                    else
                    {
                        //we can go top half
                        dbg_var("top_half", T_f32, offset);

                    top_half:
                        ret = whole_world_range_start + offset;
                    }
                }
                else
                {
                    if(no_go_intersection_end < whole_world_range_end)
                    {
                        //we can go bottom half
                        dbg_var("bottom_half", T_f32, offset);
                    bottom_half:
                        ret = no_go_intersection_end + offset;
                    }
                    else
                    {
                        //we can not go at all
                        stop("should be taken care of by first control path (range_of_goability <=0)");
                    }
                }
                
            }
            else
            {
                dbg_var("no intersection", T_f32, offset);
                ret = random_range(&ai->random, whole_world_range_start, whole_world_range_end);
            }
        }

        dbg_offset = l->ball_targety - ret;
    }

    dbg_var("ret", T_f32, ret);
    return ret;
}

internal PaddleMoveDirection
paddle_get_humanlike_direction(GameState *g, GameModeLevel *l, Paddle *paddle, Input *input)
{
    //NOTE this only works for the paddle ai paddle being on the left side
    PaddleMoveDirection ret;
    ret = nowhere;
    PaddleAIState *ai = paddle->ai;
    
    BallDir bdir = (l->wonmatch) ? BallDir_nowhere : (l->ball->vel.x < 0) ? BallDir_left : BallDir_right;
    b32 first_coming = (bdir == BallDir_left) && (ai->last_bdir != BallDir_left);
    b32 first_going = (bdir == BallDir_right) && (ai->last_bdir != BallDir_right);

    if (!l->wonmatch & ai->last_wonmatch)
    {
        ai->recovering = false;
        ai->recovery_decision_made = false;
    }
    ai->last_wonmatch = l->wonmatch;
    ai->last_bdir = bdir;        

    f32 wallx = paddle->bbox.x + paddle->bbox.width + l->ball->b_circle.radius;
    f32 seconds_until_impact = (l->ball->b_circle.x - wallx) / -l->ball->vel.x;
    f32 middle_wallx = paddle->bbox.x + paddle->bbox.width/2 + l->ball->b_circle.radius;
    f32 seconds_until_middle_of_paddle = (l->ball->b_circle.x - middle_wallx) / -l->ball->vel.x;
    if(first_coming)
    {
        ai->targety = paddle->bbox.y;
        //impact seconds more -> delay guess more
        //impact seconds less -> delay guess less
        f32 min_guess_delay = (seconds_until_impact - 1.55f);
        f32 max_guess_delay = seconds_until_impact - 0.25f;
#define FASTEST_POSSIBLE_REACTION_TIME 0.16f
        if(min_guess_delay < FASTEST_POSSIBLE_REACTION_TIME)
            min_guess_delay = FASTEST_POSSIBLE_REACTION_TIME;
        ai->guess_delay = random_range(&ai->random, min_guess_delay, max_guess_delay);
        
    }

    if(ai->doing_rage)
    {
        ai->rage_seconds_until_other_direction -= input->dt;
        ai->rage_seconds_until_downtime -= input->dt;
        ai->seconds_of_rage -= input->dt;

        if(ai->seconds_of_rage <= 0)
        {
            ai->targety = paddle->bbox.y;
            ai->doing_rage = false;
        }
        else if(ai->rage_seconds_until_other_direction <= 0)
        {
            ai->targety = ai->rage_up ? paddle->bbox.y - 10.0f : paddle->bbox.y + 10.0f;
            ai->rage_seconds_until_downtime = random_range(&ai->random, 0.060, 0.090);
            ai->rage_seconds_until_other_direction = 1000.9f;

            if(paddle->bbox.y < l->court_bounds.y1 + 1.0f)
                ai->rage_up = false;
            else if (paddle->bbox.y + paddle->bbox.height > l->court_bounds.y2 - 1.0f)
                ai->rage_up = true;
            else if(random_unilateral(&ai->random) > 0.15f)
                ai->rage_up = !ai->rage_up;
        }
        else if (ai->rage_seconds_until_downtime <= 0)
        {
            ai->targety = paddle->bbox.y;
            ai->rage_seconds_until_other_direction = random_range(&ai->random, 0.04, 0.12);
            ai->rage_seconds_until_downtime = 1000.0f;
        }
    }

    
    if(ai->recovery_decision_made)
    {
        ai->recovering_delay -= input->dt;
        dbg_var("recovering delay", T_f32, ai->recovering_delay);
        if(ai->recovering_delay <= 0)
        {
            if(ai->recovering)
                ai->targety = ai->fail_recover_targety;
            else
            {
                //we are destined to not recover do a rage dance instead                
                
                ai->rage_seconds_until_other_direction = 0;
                ai->seconds_of_rage = random_range(&ai->random, 1.0f, 2.0f);
                ai->rage_up = random_unilateral(&ai->random) < 0.5f;
                ai->doing_rage = true;
            }
            ai->recovery_decision_made = false;
            ai->will_miss = false;
            ai->recovering_delay = 10000.0f;
            ai->guess_delay = 1000.0f;
        }
    }

    if(bdir == BallDir_left) 
    {
        
        ai->guess_delay -= input->dt;
        if(ai->guess_delay <= 0)
        {
            ai->recovering = false;
            ai->recovery_decision_made = false;
            ai->targety = paddle_get_humanlike_targety(g, l, paddle, input, seconds_until_impact);

            //more guesses when the ball almost hits
            //less guesses when the ball still has a long way to go
            //RESEARCH TODOMAKE THIS a BIASED RANDOM distribution
            //this is a sqrt curve that biases towards high numbers (not perfectly)
            //range but it can also fall as low as 0.1 (TODO and as high as 1.2 if there still is that time)

            f32 min_guess_delay;
            f32 max_guess_delay;
            min_guess_delay = 0.12f;
            max_guess_delay = 1.0f;

            if(l->ballspeed > 16.5f)
            {
                max_guess_delay = 0.7f;
            }

            dbg_var("min guess interval", T_f32, min_guess_delay);
            dbg_var("max guess interval", T_f32, max_guess_delay);

            ai->guess_delay = min_guess_delay +
                sqroot(random_unilateral(&ai->random)) *(max_guess_delay - min_guess_delay);
        }
    }
    if(bdir == BallDir_left || bdir == BallDir_nowhere)
    {
        // ret = paddle_move_granularity_clamped(paddle->bbox.y, ai->targety,
        //                                       input, MovementConstraint_no_constraints);
        if(paddle->bbox.y > ai->targety + AI_MOVEMENT_GRANULARITY)
            ret = up;
            else if (paddle->bbox.y < ai->targety - AI_MOVEMENT_GRANULARITY)
                ret = down;
        f32 newy = paddle->bbox.y;
        
        if(ret == up)
                newy -= input->dt * PADDLE_MOVE_SPEED;
        else if(ret == down)
            newy += input->dt * PADDLE_MOVE_SPEED;
        
        dbg_markercm(paddle->bbox.x+paddle->bbox.width + 0.1, newy + dbg_offset, COLOR_AKBLUE);
    }


    
    //f32 recovery_decision_earliness = 2.0f;
    if(ai->will_miss && !ai->recovery_decision_made)
        //l->ball->b_circle.x < paddle->bbox.x + recovery_earliness)
    {
        //ball out of range
        ai->recovery_decision_made = true;
        //TODO maybe The higher the ballspeed the higher the change of recovering
        ai->recovering =
            l->ballspeed > 14.0f ?
            (random_unilateral(&ai->random) < 0.96) :
            l->ballspeed > 10.3f ?
            (random_unilateral(&ai->random) < 0.70) :
            (random_unilateral(&ai->random) < 0.30);
        //ai->recovering = false;
        //TODO if we don't recover:: do a ragey dance
        if(ai->recovering)
        {
            ai->fail_recover_targety = paddle_get_fail_recovery_targety(g, l, paddle, input);
            //min time we can start our recovery without hitting the ball
            //accurate if assuming that all recoveries are deep enough to
            //eventually hit l->ball_targety
        }

        f32 closest_dist_to_ball = 0;
        if(l->ball_targety < ai->targety)
        {
            //TODO for now we just take the closest sample point for the y the ball will be at above our paddle
            //     the min_recovering delay should think about this??
            /*
              THE PROBLEM::
              an object (the ball) travels along a known vector
              the paddle (whoose speed is known) must decide how much to delay going up (or down) just perfectly 
              so that it avoids colliding with the ball
             */
            closest_dist_to_ball = ai->targety - ((maximum(l->ball_lefty, l->ball_targety) + l->ball->b_circle.radius));
        }
        else
        {
            closest_dist_to_ball = (minimum(l->ball_lefty, l->ball_targety) - l->ball->b_circle.radius) -
                                    (ai->targety + paddle->bbox.height);
        }

        f32 time_to_reach_ball = closest_dist_to_ball / PADDLE_MOVE_SPEED;
        //dbg_var("time to reach ball", T_f32, seconds_until_impact);
        f32 min_recovering_delay = seconds_until_middle_of_paddle - time_to_reach_ball;
        //dbg_var("min recovering delay", T_f32, min_recovering_delay);
#if 1
        ai->recovering_delay = random_range(&ai->random, min_recovering_delay, min_recovering_delay + 0.2f);
#else
        ai->recovering_delay = min_recovering_delay;
#endif
    }
    if(seconds_until_middle_of_paddle < 0)
        ai->guess_delay = 10000.0f;
    
    //NOTE:: we either recover or do a rage dance

    //ai->last_bdir = BallDir_nowhere; //we need to have this statement so that ...

    dbg_struct(PaddleAIState, ai);
    return ret;
}

internal PaddleMoveDirection
paddle_getmovedir(PaddleMoveDirection *last_movedir, KeyState upKey, KeyState downKey, Input *input)
{
    TIMED_BLOCK;
    PaddleMoveDirection ret;

    //debug::
    if(input->key[KEY_lCtrl].down)
    {
        if(input->key_pressed(KEY_upArrow))
        {
            ret = up;
        }
        else if (input->key_pressed(KEY_downArrow))
        {
            ret = down;
        }
        return ret;
    }

    //up key has been released
    if(!upKey.down && *last_movedir == up)
        *last_movedir = nowhere;

    //down key has been released
    if(!downKey.down && *last_movedir == down)
        *last_movedir = nowhere;

    if(upKey.down && downKey.down)
    {
        if(keystate_lastkey_down(upKey) && keystate_lastkey_down(downKey))
        {
            if(*last_movedir == down)
            {
                //now we need to move up
                ret = up;
            }
            else if(*last_movedir == up)
            {
                //now we need to move down
                ret = down;
            }
            else if (*last_movedir == nowhere)
            {
                /*TODO:: be able to send platform input information (e.g. half transition count)
                  so that it is more unlikely that you can reach this code path
                  where the two buttons are pressed simultaneously
                */
                ret = nowhere;
            }
            else
            {
                stop("Invalid paddle direction");
            }
        }
        else if (keystate_lastkey_down(downKey))
        {
            //now we need to move up
            ret = up;
            *last_movedir = down;
        }
        else if (keystate_lastkey_down(upKey))
        {
            //now we need to move down
            ret = down;
            *last_movedir = up;
        }
    }
    //if the key is down or even has been down for some part of the input gathering frame
    else if (downKey.down || keystate_pressedb(downKey))
    {
        ret = down;

    }
    else if (upKey.down || keystate_pressedb(upKey))
    {
        ret = up;
    }
    else
    {
        //no key is down
        ret = nowhere;
    }
    return ret;
}

internal void 
paddle_move(GameState *g, GameModeLevel *l, Input *input, Paddle *paddle, PaddleMoveDirection movedir)
{
    TIMED_BLOCK;
#if INTERNAL
    if(dbg->update_paused)
        return;
#endif
    Vec2 lastpos = paddle->bbox.pos;
    
    if(movedir == up)
        paddle->vel.y = -PADDLE_MOVE_SPEED;
    else if (movedir == down)
        paddle->vel.y = PADDLE_MOVE_SPEED;
    else
        paddle->vel.y = 0;

    //TODO debug
    if(input->key_pressed(KEY_9))
        paddle->bbox.pos.y = 0;
    paddle->bbox.pos += paddle->vel * input->dt;

    Vec2 d = paddle->bbox.pos - lastpos;

    CollisionResult collision =
        paddle_checkcollisionwith_circle(lastpos, d, paddle->bbox, l->ball->b_circle);

    if(collision.collided )
    {
         if(!paddle->last_collided)
            play_sound(g, "ding", 1.0f);
        paddle->last_collided = true;
        if(collision.contactpoint.y == paddle->bbox.y)
        {
            l->ball->b_circle.center.y = collision.contactpoint.y - l->ball->b_circle.radius - 0.000005;
            l->ball->vel += v2(0, -0.45f);
            l->ball->vel = normalize(l->ball->vel);
            l->ball->vel *= l->ballspeed;
        }
        else if (collision.contactpoint.y == paddle->bbox.y + paddle->bbox.height)
        {
            l->ball->b_circle.center.y = collision.contactpoint.y + l->ball->b_circle.radius + 0.000005;
            l->ball->vel += v2(0, 0.45f);
            l->ball->vel = normalize(l->ball->vel);
            l->ball->vel *= l->ballspeed;
        }
    }
    else
    {
        paddle->last_collided = false;
    }

    if(paddle->bbox.pos.y + paddle->bbox.height >= l->court_bounds.y2)
        paddle->bbox.pos.y = l->court_bounds.y2 - paddle->bbox.height;
    else if(paddle->bbox.pos.y <= l->court_bounds.y1)
        paddle->bbox.pos.y = l->court_bounds.y1;

    //   opengl_draw_rectangle(MetersToPixels(paddle->bbox), COLOR_AKWHITE);
    push_rectangle(&g->rlist, RENDER_LAYER_GAME,
                   MetersToPixels(paddle->bbox), COLOR_AKWHITE);
    
}


internal void
update_paddles(GameState *g, GameModeLevel *l, Input *input)
{
#if INTERNAL
    if(dbg->update_paused)
        return;
#endif
    PaddleMoveDirection leftmovedir;
    PaddleMoveDirection rightmovedir;
    
    if(l->showmenu)
    {
        leftmovedir = nowhere;
        rightmovedir = nowhere;
    }
    else
    {
        //TODO remove this wongame switch
        if(!l->wongame)
        {
            if(l->left_player.isAI)
                leftmovedir = paddle_get_humanlike_direction(g, l, l->left_player.player_paddle, input);
            else
                leftmovedir = paddle_getmovedir(&l->left_player.player_paddle->last_movedir,
                                                input->key[KEY_w], input->key[KEY_s], input);
        }
        else
        {
            leftmovedir = nowhere;
        }
         rightmovedir = paddle_getmovedir(&l->right_player.player_paddle->last_movedir,
                                          input->key[KEY_upArrow], input->key[KEY_downArrow], input);
    }

    paddle_move(g, l, input, l->left_player.player_paddle, leftmovedir);
    paddle_move(g, l, input, l->right_player.player_paddle, rightmovedir);
}

internal void
paddle_render(GameState *g, GameModeLevel *l, Rect bbox)
{
    push_rectangle(&g->rlist, RENDER_LAYER_GAME,
                   MetersToPixels(bbox), COLOR_AKWHITE);
}

//TODO:: make locked nightmare instantly reset
internal void
do_menu(GameState *g, GameModeLevel *l, Input *input)
{
    TIMED_BLOCK;
    ui_allocate_storage(&temp_arena);
    ui_state.no_mouse_focusing = false;

restart_menu:
    ui_reset();
    ui_rectangle(GenUIID, RENDER_LAYER_MENUBACK, 0, 0, g->rtarget.w, g->rtarget.h, 0.0f, COLOR_AKBLACK, 0.8f);
    ui_use_focus_group(&menu_focusgroups[l->current_menu]);

    f32 version_fontsize = 13.5f;
    f32 descent = (g->gamefont->descriptor->fontsize - g->gamefont->descriptor->ascent) * version_fontsize / g->gamefont->descriptor->fontsize;
    ui_text(GenUIID, 1280 - 1, 720 - descent - 1, "apong v1.1", version_fontsize, g->gamefont, rgb(0.9f, 0.9f, 0.9f), ALIGN_HORIZONTAL_RIGHT);
    f32 elementwidth = 200;
    f32 elementheight = 40;

    switch(l->current_menu)
    {
        case Menu_About:
        {
            f32 curry = 60;
            f32 fontsize = 22;

            ui_text(GenUIID, 640, curry, "About", fontsize, g->gamefont, COLOR_AKWHITE);
            curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 8;

            if(l->menu_about_page == 0)
            {
                ui_text(GenUIID, 640, curry, "apong is created by Akela Ng and its use is governed by the following license", fontsize, g->gamefont, COLOR_AKWHITE);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 8;
                ui_text(GenUIID, 150, curry, "Copyright (c) 2022, Akela Ng All rights reserved.", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "Redistribution and use in source and binary forms, with or without", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "modification, are permitted provided that the following conditions are met:", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "    1. Redistributions of source code must retain the above copyright", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "       notice, this list of conditions and the following disclaimer.", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "    2. Redistributions in binary form must reproduce the above copyright", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "       notice, this list of conditions and the following disclaimer in the", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "       documentation and/or other materials provided with the", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "THIS SOFTWARE IS PROVIDED BY AKELA NG AS IS AND ANY EXPRESS OR IMPLIED", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "EVENT SHALL AKELA NG BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "POSSIBILITY OF SUCH DAMAGE.", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
            }
            else if(l->menu_about_page == 1)
            {
                ui_text(GenUIID, 640, curry, "The font used in apong is Hack, by the Source Foundry Authors. Its license is reprinted below.", fontsize, g->gamefont, COLOR_AKWHITE);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 8;
                ui_text(GenUIID, 150, curry, "Copyright (c) 2018 Source Foundry Authors", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 9;
                ui_text(GenUIID, 150, curry, "Permission is hereby granted, free of charge, to any person obtaining a copy", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "of this software and associated documentation files (the \"Software\"), to deal", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "copies of the Software, and to permit persons to whom the Software is", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "furnished to do so, subject to the following conditions:", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 8;
                ui_text(GenUIID, 150, curry, "The above copyright notice and this permission notice shall be included in all", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "copies or substantial portions of the Software.", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize + 8;
                ui_text(GenUIID, 150, curry, "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
                curry += g->gamefont->descriptor->yadvance * fontsize / g->gamefont->descriptor->fontsize;
                ui_text(GenUIID, 150, curry, "SOFTWARE.", fontsize, g->gamefont, COLOR_AKWHITE, ALIGN_HORIZONTAL_LEFT);
            }

            ui_focus_with_keyboard(input, 0, 0);

            char *continue_label = (l->menu_about_page == 1) ? "Close" : "Next";
            if(ui_button_traditional(GenUIID, input, 1280/2 - 100, 720-60, elementwidth, elementheight,
                                     COLOR_AKWHITE, continue_label, fontsize, g->gamefont))
            {
                ++l->menu_about_page;
                if(l->menu_about_page == 2)
                {
                    l->showmenu = false;
                    ui_reset();
                    return;
                }
                else
                {
                    goto restart_menu;
                }
                
            }
        } break;

        case Menu_MainMenu:
        {
        mainmenu:
            local_persist u32 boobied_focus = 0;
            
            f32 element_starty = l->wongame ? 285: 295;
            f32 elementy = element_starty;
            ui_title(GenUIID, (1280 / 2), 1280, elementy, "PAUSED", 50, g->gamefont, COLOR_AKWHITE);
            elementy += 30;

#if 0
            if(!l->wongame)
            {
                ui_text(GenUIID, 1280/2, elementy, "v1.0", 20, g->gamefont, COLOR_AKWHITE);
                elementy += 20;
            }
#endif
            //f32 elementmarginy = 0;
            f32 elementx = 1280/2 - 100;
            f32 fontsize = 30.0f;

            //TODO don't calculate this every single frame
            ui_focus_with_keyboard(input, 0, 3);
            if(ui_button_traditional(GenUIID, input, elementx, elementy, elementwidth, elementheight,
                                     COLOR_AKWHITE, "Resume", fontsize, g->gamefont))
            {
                l->showmenu = false;
                ui_reset();
                return;
            }

            elementy += elementheight;

            if(ui_button_traditional(GenUIID, input, elementx, elementy, elementwidth, elementheight,
                                     COLOR_AKWHITE, "New game", fontsize, g->gamefont))
            {
                //for new game menu reset the focus on enter
                prepare_new_game_menu(l);
                l->current_menu = Menu_NewGame;
                //do_menu will call ui_allocate_storage which will reset ui_drawables and nix all the
                //things we drew
                //do_menu(g, l, input);
                //end this menu prematurely

                goto restart_menu;
            }
            
            elementy += elementheight;
#if 1
            if(ui_button_traditional(GenUIID, input, elementx, elementy, elementwidth, elementheight,
                                     COLOR_AKWHITE, "About", fontsize, g->gamefont))
            {
                l->current_menu = Menu_About;
                l->menu_about_page = 0;
            }
            elementy += elementheight;
#endif
            if(ui_button_traditional(GenUIID, input, elementx, elementy, elementwidth, elementheight,
                                     COLOR_AKWHITE, "Quit" , fontsize,g->gamefont))
            {
                ui_state.focus_group->active_id = 0;
                boobied_focus = ui_state.focus_group->focus_index;
                platform.shut_off();
            }
                
        }break;
        case Menu_NewGame:
        {
            //TODO hack can esc inout also mean can backspace inout
            if(input->key_pressed(KEY_backspace) && l->menu.can_esc_inout)
            {
                l->current_menu = Menu_MainMenu;
                //do_menu(g, l, input);

                goto restart_menu; // don't render this menu
            }

            //TODO do we need to check can esc inout?
			#if 0
            if(input->key_pressedb(KEY_s))
            {
                l->menu.showing_info = true;
                l->menu.target_yoffset = -200;
                l->menu.target_stats_transparency = 1.0f;
            }
            else if (input->key_pressedb(KEY_w))
            {
                l->menu.showing_info = false;
                l->menu.target_yoffset = 0;
                l->menu.target_stats_transparency = 0.0f;
            }
			#endif
            
            f32 t = 0.25f;
            f32 offset_movement = t*(l->menu.target_yoffset - l->menu.yoffset);
            l->menu.yoffset = l->menu.yoffset + offset_movement;
            ui_state.arrow_y += offset_movement;

            t = 0.25f;
            f32 transparency_change = t*(l->menu.target_stats_transparency - l->menu.stats_transparency);
            l->menu.stats_transparency += transparency_change;

            
            f32 element_starty = 720/2 - 65 + l->menu.yoffset;
            // f32 element_starty = 720/2 - 100 + l->menu.yoffset;
            f32 elementy = element_starty;
            f32 elementwidth = 200;
            f32 elementx = 1280/2;
            f32 element_yadvance = 40;
            f32 fontsize = 30.0f;

            f32 lightness = 0.7f;
    
            Renderer_RGB mode_draw_colors[Mode_count];
            Renderer_RGB mode_focus_colors[Mode_count];
            Renderer_RGB mode_active_colors[Mode_count];
            
            for(int i = 0; i < Mode_count; i++)
            {
                if(l->mode_info[i].unlocked)
                {
                    mode_draw_colors[i] = COLOR_AKWHITE;
                    mode_active_colors[i] = COLOR_AKGOLDEN;;
                }
                else
                {
                    mode_draw_colors[i] = COLOR_AKGRAY;
                    mode_active_colors[i] = COLOR_AKDARKGRAY;
                }
            }
                
            if(l->menu.flashing)
            {
                l->menu.flashing_t += input->dt;
            }
            u32 start_focus = ui_state.focus_group->focus_index;
            ui_focus_with_keyboard(input, 0, 1);
            if(ui_state.focus_group->focus_index != start_focus && l->menu.flashing)
                ui_state.focus_group->active_id = 0;
  
            //TODO compress mode draw colors
            for(int i = 0; i < Mode_count; i++)
            {
                UIID id = GenUIID + i;
                if(ui_button_spacey(id, input, elementx, elementy, elementwidth, element_yadvance,
                                    mode_draw_colors[i],  mode_active_colors[i],
                                    l->mode_info[i].name, fontsize, g->gamefont, l->mode_info[i].unlocked ? true : false))
                {
                    // if(l->menu.flashing)
                    // {
                    //     if(!l->mode_info[ui_state.focus_group->focus_index].unlocked)
                    //         l->menu.flashing_t = 0;
                    // }
                    // else
                    // {
                    if(id != l->menu.flashing_button || !l->menu.flashing)
                    {
                        // //restart the menu with the right color for flashing
                        // goto restart_menu;
                        l->menu.flashing_t = 0;
                        l->menu.flashing = true;
                        l->menu.flashing_button = id;
                        ui_state.no_mouse_focusing = true;
                        
                        l->menu.starting_mode = (Mode)i;
                        if(!l->mode_info[ui_state.focus_group->focus_index].unlocked)
                        {
                            play_sound(g, "denied", 0.3f);                            
                        }
                        else
                        {
                            play_sound(g, "new_game", 0.9f);
                        }
                    }
                    else if(!l->mode_info[ui_state.focus_group->focus_index].unlocked)
                        l->menu.flashing_t = 0;
                        
                    
                }
                elementy += element_yadvance;
            }

            if(ui_state.focusables[ui_state.focus_group->focus_index].id != l->menu.flashing_button)
                l->menu.flashing = false;
            if(l->menu.flashing && l->menu.flashing_t > 0.5f)
            {
                l->menu.flashing = false;
                ui_state.no_mouse_focusing = false;
                if(l->mode_info[ui_state.focus_group->focus_index].unlocked)
                {
                    l->showmenu = false;
                    g->rtarget.should_show_cursor = false;
                    l->current_menu = Menu_MainMenu;

                    start_game(g, l, l->menu.starting_mode);                        
                }
            }

#if INTERNAL && 0
            f32 statsy = 30;

            u32 count = min(l->mode_info[ui_state.focus_group->focus_index].num_played,
                            MAX_MEM_HISTORIC_SCORES);
            u32 index = (l->mode_info[ui_state.focus_group->focus_index].historic_scores_index + MAX_MEM_HISTORIC_SCORES
                - count) % MAX_MEM_HISTORIC_SCORES;
            for(int i = 0; i < count; i++)
            {
                char buf[3243];
                ScoreRecord *record = &l->mode_info[ui_state.focus_group->focus_index].historic_scores[index];
                sprintf(buf, "%u:%u", record->left_score, record->right_score);
                ui_text(GenUIID, 1280, statsy, buf, 20, g->gamefont, COLOR_AKWHITE,
                        ALIGN_HORIZONTAL_RIGHT);
                index +=1;
                index %= MAX_MEM_HISTORIC_SCORES;
                statsy += 30;
            }
#endif
            char *status_text = 0;
            Renderer_RGB arrow_draw_color;
            Renderer_RGB texture_draw_color;


            if(l->menu.flashing)
            {
                if(l->mode_info[ui_state.focus_group->focus_index].unlocked)
                {
                    if((i32)(l->menu.flashing_t * 10) % 2)
                    {
                        arrow_draw_color = COLOR_AKGOLDEN;
                    }
                    else
                    {
                        arrow_draw_color = COLOR_AKWHITE;
                    }
                    texture_draw_color = COLOR_AKWHITE;
                }
                else
                {
                    l->menu.explain_lock = true;
                    if((i32)(l->menu.flashing_t * 10) % 2)
                    {
                        arrow_draw_color = COLOR_AKRED;
                    }
                    else
                    {
                        arrow_draw_color = COLOR_AKWHITE;
                    }

                    texture_draw_color = COLOR_AKRED;
                }


            }
            else
            {
                arrow_draw_color = COLOR_AKWHITE;
                texture_draw_color = COLOR_AKWHITE;
            }


            f32 diff_image_w = 180;
            f32 image_y = element_starty +(elementy - element_starty)/2 - diff_image_w/2;
            // push_rectangle(&g->rlist, RENDER_LAYER_MENU,
            //                (1280/2-diff_image_w-60),
            //                image_y, diff_image_w, diff_image_w, 0.0f, COLOR_AKVIOLET, 1.0f);
            // push_rectangle(&g->rlist, RENDER_LAYER_MENU,
            //                (1280/2-diff_image_w),
            //                element_starty, diff_image_w, elementy - element_starty, 0.0f, COLOR_AKVIOLET, 1.0f);


            b32 unlocked = l->mode_info[ui_state.focus_group->focus_index].unlocked;             
            char *texture_asset_name = (char *)
                (unlocked ?
                l->mode_info[ui_state.focus_group->focus_index].texture_name :
                "lock");
            Asset *asset = get_asset_by_name(&g->assets, texture_asset_name, tex1c);

            f32 texture_startx = (1280/2-diff_image_w-60);
            ui_texture_masked(GenUIID, texture_startx, image_y, diff_image_w, diff_image_w,
                              asset,
                              texture_draw_color);

            ui_focus_arrow(elementx - 10, arrow_draw_color);


        }break;
        case Menu_Quit:
        {
            ui_title(GenUIID, 1280/2, 1280, 300, "Comfirm quit?", 50, g->gamefont, COLOR_AKWHITE);

            f32 elementy = 720/2-50;
            f32 elementwidth = 200;
            f32 elementx = 1280/2 - 50;


            if(ui_button_spacey(GenUIID, input, elementx, elementy, elementwidth, 50,
                                COLOR_AKWHITE, "Yes", 30.0f, g->gamefont))
            {
                platform.shut_off();
            }
            elementy += 40;
            if(ui_button_spacey(GenUIID, input, elementx, elementy, elementwidth, 50,
                                COLOR_AKWHITE, "No", 30.0f, g->gamefont))
            {
                ui_reset_focus;
                l->current_menu = Menu_MainMenu;
            }
            ui_focus_arrow(elementx - 10, COLOR_AKWHITE);
        }break;
        default:
            break;
    }

    ui_draw_items();
end_menu_premature:;
}

//ADVANTAGES of coupling update and render:: increases cache coherency
//DISADVANTAGES of coupling update and render:: for debuggin we want to pause updating of the game in a mentally
//                                              clear way and go render previous frames
#define DebugString OutputDebugString


#if INTERNAL
internal void
level_render(GameState *g, GameMemory *memory, Rect *left_paddle_rect, Rect *right_paddle_rect, Circle *ball,
             u32 *left_score, u32 *right_score)
{
    TIMED_BLOCK;
    GameModeLevel *l = g->level_data;
    if(left_paddle_rect)
        paddle_render(g, l, left_paddle_rect[0]);
    if(right_paddle_rect)
        paddle_render(g, l, right_paddle_rect[0]);

    if(ball)
        ball_render(g, l, ball);

    if(left_score)
    {
        char left_playerscore[16];
        U32ToASCII(left_playerscore, sizeof(left_playerscore), left_score[0]);




        push_string(&g->rlist, RENDER_LAYER_GAME_FRONT,
                    0.25 * g->rtarget.w, 0.1 * g->rtarget.h,
                    64.0f * ((f32) g->rtarget.w / 1280), g->gamefont, left_playerscore, ALIGN_HORIZONTAL_CENTER_TO_LEFT);
    }
	
    if(right_score)
    {
        char right_playerscore[16];
        U32ToASCII(right_playerscore, sizeof(right_playerscore), right_score[0]);

        push_string(&g->rlist, RENDER_LAYER_GAME_FRONT,
                    0.75 * g->rtarget.w, 0.1 * g->rtarget.h,
                    64.0f * ((f32) g->rtarget.w / 1280), g->gamefont, right_playerscore, ALIGN_HORIZONTAL_CENTER_TO_LEFT);
    }
}
#endif

internal void
level_updateandrender(GameState *g, Input *input, GameMemory *memory)
{
    TIMED_BLOCK;
    GameModeLevel *l = g->level_data;

#if INTERNAL
    //dbg_var("ballspeed", T_f32, l->ballspeed);
    if(dbg->update_paused)
    {
        Rect *left_paddle_rect =  (Rect *)dbg_retrieve_var("left paddle rect");
        Rect *right_paddle_rect =  (Rect *)dbg_retrieve_var("right paddle rect");
        Circle *ball =  (Circle *)dbg_retrieve_var("ball circle");

        u32 *left_score =  (u32 *)dbg_retrieve_var("left score");
        u32 *right_score =  (u32 *)dbg_retrieve_var("right score");

        level_render(g, memory, left_paddle_rect, right_paddle_rect, ball,
                     left_score, right_score);

        return;
    }

    if(input->key_pressed(KEY_end))
        serve_ball(g, l, true);
#endif
    // if(input->key_pressed(KEY_f4))
    //     __debugbreak();
    // if(input->key_pressed(KEY_9))
    //     load_savegame_data(g, l);
    //dbg_dumpstruct(arraycount(),)
//DEBUG::
    //debug::
    //TODO get debut to test this!!!
    f32 expecteddt= 0.0166666;
    f32 length = MetersToPixels(PADDLE_MOVE_SPEED * expecteddt) /2;
    Vec2 location = v2(input->mousex, input->mousey);
    local_persist Vec2 center;
    Vec2 last_center = center;

    center = location;
    f32 radius = v2_length(center - last_center);
    // f32 scale = (radius*2)/l->circle_texture.width;
    // radius +=2 * scale;
    // radius*=2;

    // f32 dot =3;
    // push_texture_masked(&g->rlist, RENDER_LAYER_DEBUGOVERLAY,
    //                     center.x - dot, center.y - dot, dot *2, dot *2,
    //                     0.0f, 0.0f, 1.0f, 1.0f,
    //                     COLOR_AKRED, &l->ball_texture);

    // push_texture_masked(&g->rlist, RENDER_LAYER_DEBUGOVERLAY,
    //                     center.x - radius, center.y - radius, radius*2, radius*2,
    //                     0.0f, 0.0f, 1.0f, 1.0f,
    //                     COLOR_AKWHITE, &l->circle_texture);
    

	// push_rectangle(&g->rlist, RENDER_LAYER_DEBUGOVERLAY,
    //                location.x, location.y, 20, length, 0.0f, COLOR_AKRED, 1.0f);
    // push_rectangle(&g->rlist, RENDER_LAYER_DEBUGOVERLAY,
    //                0, 0,  g->rtarget.w, g->rtarget.h, 0, COLOR_AKBLUE, 1.0f);

    // opengl_draw_rectangleoutline(0, 0,
    //                              gameglobals.display.pixelwidth-1, gameglobals.display.pixelheight-1, COLOR_AKWHITE);

    // push_rectangleoutline(&g->rlist, RENDER_LAYER_DEBUGOVERLAY,
    //                       0, 0, g->rtarget.w-1, g->rtarget.h-1, COLOR_AKYELLOW);

    // if(input->key[KEY_b].is_down)
    // {
    //     //toggle screen blur
    //     render_add_posteffect(RenderPostEffect_Blur);
    // }
    
    if(input->key_pressedb(KEY_esc) && l->menu.can_esc_inout)
    {
        //TODO pock up options menu or comfirm quit to title
        //switch_gamemode(g, GameMode_title, memory);
        l->showmenu = !l->showmenu;            
            

        if(l->showmenu)

        {
            ui_state.arrow_y = 0;
            l->current_menu = Menu_MainMenu;
            g->rtarget.should_show_cursor = true;

        }
        else
        {
            g->rtarget.should_show_cursor = false;
            //for new game menu
        }
    }
#if !INTERNAL || 1
    else if(!input->has_focus)
    {
        l->showmenu = true;
        g->rtarget.should_show_cursor = true;
    }
#endif

    if(l->showmenu)
    {
        do_menu(g, l, input);
    }

    if(!l->showmenu && !l->wongame)
    {
        update_paddles(g, l, input);        
    }

    paddle_render(g, l, l->left_player.player_paddle->bbox);
    paddle_render(g, l, l->right_player.player_paddle->bbox);

    AABB paddle_aabb[2];
    paddle_aabb[0] = rect_toAABB(l->left_player.player_paddle->bbox);
    paddle_aabb[1] = rect_toAABB(l->right_player.player_paddle->bbox);

    dbg_var("left paddle rect", T_Rect, l->left_player.player_paddle->bbox);
    dbg_var("right paddle rect", T_Rect, l->right_player.player_paddle->bbox);
    
    dbg_var("should show cursor?", T_b32, g->rtarget.should_show_cursor);

    if(l->wongame)
    {
        if(!l->showmenu)
        {
            ui_allocate_storage(&temp_arena);
            ui_reset();
            f32 curry = 300;
            char *gameover = "Game over!";
            char *lwins = "Left wins!";
            char *rwins = "Right wins!";
            char *ywin = "You win!";

            char *gameover_msg;
            if(l->current_mode == Mode_multiplayer)
            {
                if(l->left_player.pts > l->right_player.pts)
                    gameover_msg = lwins;
                else
                    gameover_msg = rwins;
            }
            else
            {
                if(l->left_player.pts > l->right_player.pts || (l->endless && l->left_player.pts >= PTS_TO_WIN))
                    gameover_msg = gameover;
                else
                    gameover_msg = ywin;
            }

            ui_title(GenUIID, 640, 1280, curry, gameover_msg, 64, g->gamefont, COLOR_AKWHITE);

            curry += 55;

            if(l->current_mode == Mode_multiplayer)
            {
                ui_text(GenUIID, 640, curry, "press ENTER to start new game", 22, g->gamefont, COLOR_AKWHITE);
                if(input->key_pressed(KEY_enter))
                {
                    // TODO: Let's remove this unlocked_new thing!
                    l->unlocked_new = false;
                    restart_game(g, l);
                }
            }
            else
            {
                if(l->left_player.pts > l->right_player.pts || (l->endless && l->left_player.pts >= PTS_TO_WIN))
                {
                    ui_text(GenUIID, 640, curry, "press ENTER to retry", 22, g->gamefont, COLOR_AKWHITE);
                    if(input->key_pressed(KEY_enter))
                    {
                        // TODO: Let's remove this unlocked_new thing!
                        l->unlocked_new = false;
                        restart_game(g, l);
                    }
                }
                else
                {
                    f64 time_since_win = input->dt_from_startup - l->win_game_time;
                    f64 time_to_endless = 3 - time_since_win;
                    i32 endless_start_time = (i32)time_to_endless + 1;

                    dbg_var("win game time", T_f64, l->win_game_time);
                    dbg_var("time since win", T_f64, time_since_win);
                    dbg_var("time to endless", T_f64, time_to_endless);
                    dbg_var("endless start time", T_i32, endless_start_time);

                    u32 buf_size = 50;
                    char *buf = arena_pushcount(&temp_arena, char, buf_size);
                    snprintf(buf, buf_size, "starting endless in %d", endless_start_time);
                    ui_text(GenUIID, 640, curry, buf, 22, g->gamefont, COLOR_AKWHITE);
                    
                    if(time_to_endless <= 0)
                    {
                        l->endless = true;
                        l->endless_original_points = l->left_player.pts;
                        l->wongame = false;
                        l->showmenu = false;
                        set_ballspawn_timer(l);
                    }
                }
            }

            ui_draw_items();
        }
    }
    else if (l->wonmatch) 
    {
        if(!l->showmenu)
        {
            l->ballspawn_timer -= input->dt;
            if(l->ballspawn_timer <= 0)
            {
                if(l->ballspawn_delay - l->mode_info[l->current_mode].preset->ballspawn_decrement > 0.05)
                    l->ballspawn_delay -= l->mode_info[l->current_mode].preset->ballspawn_decrement;
                serve_ball(g, l, !l->serve_to_right);
                l->wonmatch = false;
            }
        }
    }
    else
    {
        if(!l->showmenu)
        {
            ball_update(g, l, input, paddle_aabb, arraycount(paddle_aabb));                
        }

        ball_render(g, l, &l->ball->b_circle);
    }

    dbg_var("ballspawn_delay", T_f32, l->ballspawn_delay);
    dbg_var("ballspawn_timer", T_f32, l->ballspawn_timer);

    dbg_var("ball circle", T_Circle, l->ball->b_circle);
    f32 magnitude = v2_length(l->ball->vel);
    dbg_var("ball velocity", T_Vec2, l->ball->vel);
    dbg_var("ball velocity magnitue", T_f32, magnitude);
    //draw scores
    //TODO find out how to align this using ui or something (WHERE DO WE PUT THIS TEXT??)
    char left_playerscore[16];
    U32ToASCII(left_playerscore, sizeof(left_playerscore), l->left_player.pts);

    push_string(&g->rlist, RENDER_LAYER_GAME_FRONT,
                0.24 * g->rtarget.w, 0.13 * g->rtarget.h,
                80.0f * ((f32) g->rtarget.w / 1280), g->gamefont, left_playerscore, ALIGN_HORIZONTAL_CENTER_TO_LEFT);

    char right_playerscore[16];
    U32ToASCII(right_playerscore, sizeof(right_playerscore), l->right_player.pts);
    push_string(&g->rlist, RENDER_LAYER_GAME_FRONT,
                0.76 * g->rtarget.w, 0.13 * g->rtarget.h,
                80.0f * ((f32) g->rtarget.w / 1280), g->gamefont, right_playerscore, ALIGN_HORIZONTAL_CENTER_TO_LEFT);
//    DebugString("update end \n");
}
#undef DebugString
