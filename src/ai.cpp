#define OVERRIDE
#if 1
#define DISTANCE_FROM l->ball->b_circle.y
#else
#define DISTANCE_FROM l->ball_targety
#endif
inline b32
calculate_underestimate_anyway_easy(GameModeLevel *l, Paddle *paddle)
{
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 chance;

    if(l->ballspeed > 13.5f)
    {
        if(ball_slope > 0.8f)
        {
            chance = 0.12f * absolute(ball_slope) + 0.02;
        }
        else if(ball_slope > 0.5f)
        {
            chance = 0.06f * absolute(ball_slope) + 0.02;
        }
        else
        {
            chance = 0.04 * absolute(ball_slope) + 0.02;
        }
    }
    else if(l->ballspeed > 9.0f)
    {
        chance = 0.04 * absolute(ball_slope) + 0.03;
    }
    else
    {
        chance = 0.05 * absolute(ball_slope) + 0.02;
    }

    f32 random = random_unilateral(&ai->random);
    b32 ret =  random < chance;
    dbg_var("underestimate anyway chance", T_f32, chance);
    dbg_var("unilateral", T_f32, random);

    OVERRIDE;
    return ret; 
}

inline b32
calculate_underestimate_normal_easy(GameModeLevel *l, Paddle *paddle)
{
    //TODO don't calculate this twice!
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 absolute_slope = absolute(ball_slope);
    f32 dist = absolute(DISTANCE_FROM - (paddle->bbox.y + paddle->bbox.height/2)); 

    f32 chance = 0;
    if(l->ballspeed > 12.5f)
    {
        if(ball_slope > 0.7f)
        {
            chance = 0.36f * absolute(ball_slope) + 0.05;
        }
        else if(ball_slope > 0.5f)
        {
            chance = 0.14f * absolute(ball_slope) + 0.03;
        }
        else
        {
            chance = 0.06 * absolute(ball_slope) + 0.02;
        }
    }
    else if(l->ballspeed > 8.0f)
        chance = 0.32f * absolute(ball_slope) + 0.04f;
    else
        chance = 0.12f * absolute(ball_slope) + 0.02f;

    dbg_var("underestimate normal chance before dist", T_f32, chance);

    chance = chance*0.30 + chance*(dist / 5.5f); 
    
    dbg_var("underestimate normal chance", T_f32, chance);
    dbg_var("DISTANCE_FROM", T_f32, DISTANCE_FROM);
    dbg_var("ballspeed", T_f32, l->ballspeed);
    dbg_var("slope", T_f32, absolute_slope);
    dbg_var("distance", T_f32, dist);
    b32 ret = random_unilateral(&ai->random) < chance;

    OVERRIDE;
    return ret;

}

inline b32
calculate_overestimate_normal_easy(GameModeLevel *l, Paddle *paddle)
{
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 dist = absolute(DISTANCE_FROM - (paddle->bbox.y + paddle->bbox.height/2));
    f32 chance = 0;
    if(l->ballspeed > 12.5f)
    {
        if(ball_slope > 0.7f)
        {
            chance = 0.36f * absolute(ball_slope) + 0.05;
        }
        else if(ball_slope > 0.5f)
        {
            chance = 0.14f * absolute(ball_slope) + 0.03;
        }
        else
        {
            chance = 0.06f * absolute(ball_slope) + 0.02;
        }
    }
    else if(l->ballspeed > 8.0f)
        chance = 0.32f * absolute(ball_slope) + 0.04f;
    else
        chance = 0.12f * absolute(ball_slope) + 0.02f;

    dbg_var("overestimate normal chance before dist", T_f32, chance);


    chance = chance * 0.30 + chance * (dist / 5.5f); 
    dbg_var("overestimate normal chance", T_f32, chance);
    b32 ret = random_unilateral(&ai->random) < chance;

    OVERRIDE;
    return ret;
}


inline b32
calculate_underestimate_anyway_nightmare(GameModeLevel *l, Paddle *paddle)
{
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 chance = (0.005 + (l->ballspeed-8.0)/110) * (absolute(ball_slope)/0.65f + 0.1f);

    f32 random = random_unilateral(&ai->random);
    b32 ret =  random < chance;
    dbg_var("underestimate anyway chance", T_f32, chance);

    OVERRIDE;
    return ret; 
}

inline b32
calculate_underestimate_normal_nightmare(GameModeLevel *l, Paddle *paddle)
{
    //TODO don't calculate this twice!
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 absolute_slope = absolute(ball_slope);
    f32 dist = absolute(DISTANCE_FROM - (paddle->bbox.y + paddle->bbox.height/2)); 

    f32 chance = 0;
    if(l->ballspeed > 12.0f)
        chance = 0.25f * (absolute(ball_slope)/0.5f + 0.1f);
    else if(l->ballspeed > 10.0f)
        chance = 0.1f * (absolute(ball_slope)/0.5f + 0.1f);
    else
        chance = 0.05f * (absolute(ball_slope)/0.5f + 0.1f);
        
    dbg_var("underestimate normal chance before dist", T_f32, chance);

    chance =  chance * 0.04 + chance * (dist / 5.5f); 
    
    dbg_var("underestimate normal chance", T_f32, chance);
    dbg_var("ball targety", T_f32, DISTANCE_FROM);
    dbg_var("ballspeed", T_f32, l->ballspeed);
    dbg_var("slope", T_f32, absolute_slope);
    dbg_var("distance", T_f32, dist);
    b32 ret = random_unilateral(&ai->random) < chance;

    OVERRIDE;
    return ret;
}


inline b32
calculate_overestimate_normal_nightmare(GameModeLevel *l, Paddle *paddle)
{
    PaddleAIState *ai = paddle->ai;
    f32 ball_slope = l->ball->vel.y / l->ball->vel.x;
    f32 dist = absolute(DISTANCE_FROM - (paddle->bbox.y + paddle->bbox.height/2));
    f32 chance = 0;
    if(l->ballspeed > 12.0f)
        chance = 0.25f * (absolute(ball_slope)/0.5f+0.1f);
    else if(l->ballspeed > 10.0f)
        chance = 0.1f * (absolute(ball_slope)/0.5f+0.1f);
    else
        chance = 0.05f * (absolute(ball_slope)/0.6f+0.1f);

    dbg_var("overestimate normal chance before dist", T_f32, chance);


    chance = chance * 0.04 + chance * (dist / 5.5f);
    dbg_var("overestimate normal chance", T_f32, chance);
    b32 ret = random_unilateral(&ai->random) < chance;

    OVERRIDE;
    return ret;
}
