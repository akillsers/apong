#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>

#define Font X11Font
#include <X11/Xlib.h>
#undef Font

#undef internal
#include <X11/XKBlib.h>
#define internal static

#include <X11/keysymdef.h>

#define _NET_WM_STATE_REMOVE 0
#define _NET_WM_STATE_ADD 1

struct
{
    Display *display;
    Window window;
    
    b32 fullscreen;
} g_x11;
struct X11Keymap
{
    Key key;
    const char *name;
};

// TODO: I have no idea if these are right
// TODO: How widely supported is the X11 keyboard extension?
// NOTE: Some sources: 
//    /usr/share/X11/xkb/keycodes/evdev   (this file is probably all that you need)
//    /usr/share/X11/xkb/symbols/us
//    /usr/share/X11/xkb/symbols/pc
//    /usr/share/X11/xkb/symbols/altwin
//    /usr/share/X11/xkb/symbols/keypad
X11Keymap x11_keymap[]
{
    {KEY_tab, "TAB"},
    {KEY_a,   "AC01"},
    {KEY_b,   "AB05"},
    {KEY_c,   "AB03"},
    {KEY_d,   "AC03"},
    {KEY_e,   "AD03"},
    {KEY_f,   "AC04"},
    {KEY_g,   "AC05"},
    {KEY_h,   "AC06"},
    {KEY_i,   "AD08"},
    {KEY_j,   "AC07"},
    {KEY_k,   "AC08"},
    {KEY_l,   "AC09"},
    {KEY_m,   "AB07"},
    {KEY_n,   "AB06"},
    {KEY_o,   "AD09"},
    {KEY_p,   "AD10"},
    {KEY_q,   "AD01"},
    {KEY_r,   "AD04"},
    {KEY_s,   "AC02"},
    {KEY_t,   "AD05"},
    {KEY_u,   "AD07"},
    {KEY_v,   "AB04"},
    {KEY_w,   "AD02"},
    {KEY_x,   "AB02"},
    {KEY_y,   "AD06"},
    {KEY_z,   "AB01"},

    {KEY_comma,           "AB08"},
    {KEY_period,          "AB09"},
    {KEY_forwardslash,    "AB10"},
    {KEY_semicolon,       "AC10"},
    {KEY_singlequotes,    "????"}, // TODO: I don't know which key this is referring to?
    {KEY_leftsqbrackets,  "AD11"},
    {KEY_rightsqbrackets, "AD12"},
    
    {KEY_graveaccent,     "TLDE"},
    {KEY_space,           "SPCE"},
    {KEY_capslock,        "CAPS"},
    {KEY_numlock,         "NMLK"},
    {KEY_scrolllock,      "SCLK"},
    
    {KEY_0,   "AE10"},
    {KEY_1,   "AE01"},
    {KEY_2,   "AE02"},
    {KEY_3,   "AE03"},
    {KEY_4,   "AE04"},
    {KEY_5,   "AE05"},
    {KEY_6,   "AE06"},
    {KEY_7,   "AE07"},
    {KEY_8,   "AE08"},
    {KEY_9,   "AE09"},

    {KEY_minus,      "AE11"},
    {KEY_plus,       "AE12"},
    {KEY_backslash,  "BKSL"},
    
    {KEY_upArrow,     "UP"},
    {KEY_downArrow,   "DOWN"},
    {KEY_leftArrow,   "LEFT"},
    {KEY_rightArrow,  "RGHT"},

    {KEY_enter,      "RTRN"},
    {KEY_backspace,  "BKSP"},
    
    {KEY_lShift,   "LFSH"},
    {KEY_rShift,   "RTSH"},
    {KEY_lCtrl,    "LCTL"},
    {KEY_rCtrl,    "RCTL"},
    {KEY_lAlt,     "LALT"},
    {KEY_rAlt,     "RALT"},
    {KEY_lMenu,    "LWIN"},
    {KEY_rMenu,    "RWIN"},
    {KEY_lContext, "????"}, // TODO: I don't know which key this refers to
    {KEY_rContext, "????"}, // TODO: I don't know which key this refers to
    {KEY_menu,     "MENU"}, // TODO: Is this correct?
    
    {KEY_numpad0,        "KP0"},
    {KEY_numpad1,        "KP1"},
    {KEY_numpad2,        "KP2"},
    {KEY_numpad3,        "KP3"},
    {KEY_numpad4,        "KP4"},
    {KEY_numpad5,        "KP5"},
    {KEY_numpad6,        "KP6"},
    {KEY_numpad7,        "KP7"},
    {KEY_numpad8,        "KP8"},
    {KEY_numpad9,        "KP9"},
    {KEY_numpadslash,    "KPDV"},
    {KEY_numpadasterisk, "KPMU"},
    {KEY_numpadminus,    "KPSU"},
    {KEY_numpadplus,     "KPAD"},
    {KEY_numpadenter,    "KPEQ"},
    {KEY_numpadperiod,   "KPDL"},

    {KEY_esc,  "ESC"},
    {KEY_f1,   "FK01"},
    {KEY_f2,   "FK02"},
    {KEY_f3,   "FK03"},
    {KEY_f4,   "FK04"},
    {KEY_f5,   "FK05"},
    {KEY_f6,   "FK06"},
    {KEY_f7,   "FK07"},
    {KEY_f8,   "FK08"},
    {KEY_f9,   "FK09"},
    {KEY_f10,  "FK10"},
    {KEY_f11,  "FK11"},
    {KEY_f12,  "FK12"},

    {KEY_printscreen,    "PRSC"},
    {KEY_scroll,         "SCLK"},
    {KEY_break,          "PAUS"},
    {KEY_insert,         "INS"},
    {KEY_home,           "HOME"},
    {KEY_end,            "END"},
    {KEY_pageup,         "PGUP"},
    {KEY_pagedown,       "PGDN"},
    {KEY_delete,         "DELE"},

    // TODO: Test these keys?
    {KEY_mediahome,    "I180"},
    {KEY_mediaemail,   "I223"},
    {KEY_mediaplay,    "I172"},
    {KEY_mediastop,    "I174"},
    {KEY_mediaprev,    "I173"},
    {KEY_medianext,    "I171"},
    {KEY_medialouder,  "VOL+"},
    {KEY_mediasofter,  "VOL-"},
    {KEY_mediamute,    "MUTE"},
};

u32 x11_keycode_to_key[256];
