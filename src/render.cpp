
//Text alignment
#define ALIGN_HORIZONTAL_CENTER (1 << 0)
#define ALIGN_HORIZONTAL_LEFT   (1 << 1)
#define ALIGN_HORIZONTAL_RIGHT  (1 << 2)
#define ALIGN_HORIZONTAL_CENTER_TO_LEFT (1 << 3)

#define ALIGN_VERTICAL_CENTER (1 << 16)
#define ALIGN_VERTICAL_CENTER_BYFULL (1 << 17)
//TODO right now we are sending textures every frame when we need them
//     and deletting them at the end of each frame
//     make it so that it only deletes the oldest texture when we don't need them no more
internal void
push_texture_masked(RenderList *rlist, f32 z,
                    f32 x, f32 y, f32 width, f32 height,
                    f32 u, f32 v, f32 s, f32 t,
                    Renderer_RGB color, Renderer_Texture1c *tex1c, f32 transparency = 1.0f)
{
    if(tex1c->width == 0 || tex1c->height == 0)
        return;//null texture

    assert(rlist->num_entries < rlist->max_entries);

    //TODO:: hack we want async tex downloads right now we just generate a texID whenever we need
    //       and never delete it
    if(tex1c->GPUHandle == 0)
    {
#if RENDER_OPENGL
        tex1c->GPUHandle =
            opengl_create_greyscaletexture(tex1c->width, tex1c->height,
                                           tex1c->pixels);
#elif RENDER_D3D
        tex1c->GPUHandle =
            d3d_add_greyscaletexture(tex1c->width, tex1c->height,
                                        tex1c->pixels);
#endif
        
    }

    rlist->entries[rlist->num_entries].type = Render_texture_masked;
    rlist->entries[rlist->num_entries].z = z;

    rlist->entries[rlist->num_entries].transparency = transparency;
    rlist->entries[rlist->num_entries].x = x;
    rlist->entries[rlist->num_entries].y = y;
    rlist->entries[rlist->num_entries].w = width;
    rlist->entries[rlist->num_entries].h = height;
    rlist->entries[rlist->num_entries].u = u;
    rlist->entries[rlist->num_entries].v = v;
    rlist->entries[rlist->num_entries].s = s;
    rlist->entries[rlist->num_entries].t = t;
    rlist->entries[rlist->num_entries].color = color;

    rlist->entries[rlist->num_entries].mask = tex1c;

    rlist->num_entries++;
}


internal void
push_texture(RenderList *rlist, f32 z,
             f32 x, f32 y, f32 width, f32 height,
             f32 u, f32 v, f32 s, f32 t,
             Renderer_RGB color, Renderer_Texture3c *tex)
{
    assert(rlist->num_entries < rlist->max_entries);
    rlist->entries[rlist->num_entries].type = Render_texture;
    rlist->entries[rlist->num_entries].z = z;

    rlist->entries[rlist->num_entries].x = x;
    rlist->entries[rlist->num_entries].y = y;
    rlist->entries[rlist->num_entries].w = width;
    rlist->entries[rlist->num_entries].h = height;
    rlist->entries[rlist->num_entries].u = u;
    rlist->entries[rlist->num_entries].v = v;
    rlist->entries[rlist->num_entries].s = s;
    rlist->entries[rlist->num_entries].t = t;
    rlist->entries[rlist->num_entries].color = color;

    rlist->entries[rlist->num_entries].tex = tex;

    rlist->num_entries++;
}


internal void
push_rectangle(RenderList *rlist, f32 z,
               f32 x, f32 y, f32 width, f32 height, f32 orientation, Renderer_RGB color, f32 transparency = 1.0f)
{
    assert(rlist->num_entries < rlist->max_entries);
    rlist->entries[rlist->num_entries].type = Render_rectangle;
    rlist->entries[rlist->num_entries].z = z;

    rlist->entries[rlist->num_entries].x = x;
    rlist->entries[rlist->num_entries].y = y;
    rlist->entries[rlist->num_entries].w = width;
    rlist->entries[rlist->num_entries].h = height;
    rlist->entries[rlist->num_entries].orientation = orientation;
    rlist->entries[rlist->num_entries].color = color;
    rlist->entries[rlist->num_entries].transparency = transparency;
    
    rlist->num_entries++;    
}

internal void
push_rectangleoutline(RenderList *rlist, f32 z,
                      f32 x, f32 y, f32 width, f32 height, f32 borderwidth, Renderer_RGB color)
{
    push_rectangle(rlist, z, x, y, width, borderwidth, 0.0f, color);
    push_rectangle(rlist, z, x, (y + height)-borderwidth/2, width, borderwidth, 0.0f, color);

    push_rectangle(rlist, z, x-borderwidth/2, y-borderwidth, borderwidth, height+borderwidth+borderwidth/2, 0.0f, color);
    push_rectangle(rlist, z, (x + width)-borderwidth/2, y-borderwidth, borderwidth, height+borderwidth+borderwidth/2, 0.0f, color);


}
internal void
push_rectangleoutline(RenderList *rlist, f32 z,
                      f32 x, f32 y, f32 width, f32 height, Renderer_RGB color)
{
    push_rectangle(rlist, z, x, y, width, 1, 0.0f, color);
    push_rectangle(rlist, z, x, y, 1, height, 0.0f, color);
    push_rectangle(rlist, z, x + width, y, 1, height, 0.0f, color);
    push_rectangle(rlist, z, x, y + height, width, 1, 0.0f, color);
}


//SECTION:: push_... function alliases::
internal void
push_rectangle(RenderList *rlist, f32 z,
               f32 x, f32 y, f32 width, f32 height, Renderer_RGB color)
{
    //TODO any way to optimize this so that it pushes an unrotate rectangle for opengl to use the non rotated
    //function
    push_rectangle(rlist, z,
                   x, y, width, height, 0.0f, color);
    
}

internal void
push_rectangle(RenderList *rlist, f32 z,
               Rect rect, Renderer_RGB color)
{
    push_rectangle(rlist, z,
                   rect.x, rect.y, rect.width, rect.height, 0.0f, color);
}

internal void
push_texture_masked(RenderList *rlist, f32 z,
                    f32 x, f32 y, f32 scale,
                    Renderer_RGB color, Renderer_Texture1c *mask)
{
    push_texture_masked(rlist, z,
                        x, y, mask->width * scale, mask->height * scale,
                        0, 0, 1, 1,
                        color, mask);
}


internal void
push_texture_masked(RenderList *rlist, f32 z,
                    f32 x, f32 y,
                    Renderer_RGB color, Renderer_Texture1c *mask)
{
    push_texture_masked(rlist, z,
                        x, y, mask->width, mask->height,
                        0, 0, 1, 1,
                        color, mask);
}

#if RENDER_OPENGL
struct OpenGLTextureMaskedBatch
{
    b32 pushed;
    GLuint texID;
    u32 draw_count;
    //TODO make this allocated a little smarter
    //dynamic array??
    // Mat4 modelprojection_matrices[MAX_TEXTURE_BATCH_TEXTURES];
    // Vec2 fetchuvs[MAX_TEXTURE_BATCH_TEXTURES];
    // Vec2 fetchsts[MAX_TEXTURE_BATCH_TEXTURES];
    // Renderer_RGB colors[MAX_TEXTURE_BATCH_TEXTURES];
    Mat4 *modelprojection_matrices;
    Vec2 *fetchuvs;
    Vec2 *fetchsts;
    Renderer_RGB *colors;
};

// internal void
// __renderlist_do_post_processing(Rerlist, rtarget)
// {
//     glBindFrameBuffer()
// }


inline void
opengl_push_texmasked_batch(OpenGLTextureMaskedBatch *batch)
{
    
    opengl_draw_texturemasked_instanced(batch->draw_count,
                                        batch->modelprojection_matrices,
                                        batch->fetchuvs,
                                        batch->fetchsts,
                                        batch->colors,
                                        batch->texID);
    batch->pushed = true;
}

#define MAX_OPENGL_TEXMASKED_BATCHES 10
//NOTE:: TO debug the layering and texmasked batch pushing
//       watch rlist->entries, 100
//         and texmasked_batch
//TODO:: optimization:: if it is the same layer but the previous texture masked isn't this texture masked don't push
internal void
__renderlist_to_opengl_framebuffer(RenderList *rlist, RenderTarget *rtarget)
{
    TIMED_BLOCK;
    OpenGLTextureMaskedBatch texmasked_batch[MAX_OPENGL_TEXMASKED_BATCHES];
    u32 num_batches = 0;
    f32 curr_texmasked_z;

    if(rtarget->w == 0 || rtarget->h == 0)
    {
        return;
    }

    //set the framebuffer to rtarget size    
    opengl_set_framebuffersize(rtarget->w, rtarget->h);
    opengl_beginframe2d();
    for(int i = 0; i < rlist->num_entries; i++)
    {

        RenderEntry *entry = &rlist->entries[i];
        if(num_batches > 0 && entry->z != curr_texmasked_z)
        {
            if(!texmasked_batch[num_batches-1].pushed)
                opengl_push_texmasked_batch(&texmasked_batch[num_batches-1]);
        }

        switch(entry->type)
        {
            case Render_rectangle:
                opengl_draw_rectangle(entry->x, entry->y, entry->w, entry->h, entry->orientation, entry->color, entry->transparency);
                break;
            case Render_texture_masked:
            {
                u32 bindex;
                b32 found = false;//opengltexhandle_already_in_an_unpushed_batch(texmasked_batch, num_batches,
                                                                         // entry->mask->GPUHandle,
                                                                         // &bindex);
                if(num_batches == 0)
                    goto CreateNewBatch;

                if(!texmasked_batch[num_batches-1].pushed)
                {
                    if(texmasked_batch[num_batches-1].texID == entry->mask->GPUHandle)
                    {
                        found = true;
                        bindex = num_batches - 1;
                    }
                    else
                    {
                        opengl_push_texmasked_batch(&texmasked_batch[num_batches-1]);
                    }
                }

                if(!found)
                {
                CreateNewBatch:
                    //this is a new texture
                    //TODO entry batches are not z layer sorted with everything else!!!!
                    assert(num_batches < MAX_OPENGL_TEXMASKED_BATCHES);
                    texmasked_batch[num_batches].texID = entry->mask->GPUHandle;
                    texmasked_batch[num_batches].draw_count = 0;
                    //TODO::: just allocate positions!!!

                    u32 alloc_count = MAX_TEXTURE_BATCH_TEXTURES;
                    texmasked_batch[num_batches].modelprojection_matrices =
                        arena_pushcount(&rlist->temp_arena, Mat4, alloc_count);

                    texmasked_batch[num_batches].fetchuvs =
                        arena_pushcount(&rlist->temp_arena, Vec2, alloc_count);

                    texmasked_batch[num_batches].fetchsts =
                        arena_pushcount(&rlist->temp_arena, Vec2, alloc_count);

                    texmasked_batch[num_batches].colors =
                        arena_pushcount(&rlist->temp_arena, Renderer_RGB, alloc_count);


                    texmasked_batch[num_batches].pushed = false;
                    curr_texmasked_z = entry->z;
                    
                    bindex = num_batches;
                    num_batches++;
                }
                u32 index = texmasked_batch[bindex].draw_count;
                if(index > MAX_TEXTURE_BATCH_TEXTURES)
                {
                    //Batch texture oveflow:: push this batch and create a new one
                    opengl_push_texmasked_batch(&texmasked_batch[num_batches-1]);
                    goto CreateNewBatch;   
                }

                texmasked_batch[bindex].modelprojection_matrices[index] =
                    opengl_calculate_modelprojection_matrix(entry->x, entry->y, entry->w, entry->h);
                texmasked_batch[bindex].fetchuvs[index] = v2(entry->u, entry->v);
                texmasked_batch[bindex].fetchsts[index] = v2(entry->s, entry->t);
                texmasked_batch[bindex].colors[index] = entry->color;

                texmasked_batch[bindex].draw_count++;

                // opengl_draw_texturemasked(entry->x, entry->y, entry->w, entry->h,
                //                           entry->u, entry->v, entry->s, entry->t, entry->color,
                //                           entry->mask->GPUHandle);

            }break;
            case Render_texture:
                // entry->tex->GPUHandle =
                //     opengl_create_texture(entry->tex->width, entry->tex->height, entry->tex->pixels);

                opengl_draw_texture(entry->x, entry->y, entry->w, entry->h,
                                    entry->u, entry->v, entry->s, entry->t, entry->color,
                                    entry->tex->GPUHandle);
                // texhandles[num_texhandles] = entry->tex->GPUHandle;
                // num_texhandles++;

                break;
            default:
                stop("unknown entry type");
        }
    }
    if(num_batches > 0 && texmasked_batch[num_batches-1].pushed == false)
    {
        //NOTE:: push any lingering batches!!
        //       (the ones where its the last z layer and didn't get pushed
        //        from a z layer change in the main loop)
        opengl_push_texmasked_batch(&texmasked_batch[num_batches-1]);

    }
}
#elif RENDER_D3D
struct D3DTextureBatch
{
    u32 draw_count;
    Rect *transforms;
    Vec2 *fetchuvs;
    Vec2 *fetchsts;
    Renderer_RGBA *colors;
    u32 texID;
};

internal void
__renderlist_to_d3d(RenderList *rlist, RenderTarget *rtarget)
{
    d3d_set_viewport_size(rtarget->w, rtarget->h);
    d3d_begin();
    // d3d_render_frame();
    for(int i = 0; i < rlist->num_entries; i++)
    {
        RenderEntry *entry = &rlist->entries[i];
        switch(entry->type)
        {
            case Render_rectangle:
            {
                //NOTE:: orientation is ignored
                d3d_draw_rectangle(entry->x, entry->y, entry->w, entry->h, entry->color, entry->transparency);
            }break;
            case Render_texture_masked:
            {
                u32 num_textures = 1;
                int j = i; 
                while(++j < rlist->num_entries)
                {
                    //j++;
                    if(rlist->entries[j].type == Render_texture_masked &&
                       rlist->entries[j].mask->GPUHandle == entry->mask->GPUHandle)
                    {
                        num_textures++;
                    }
                    else
                    {
                        break;
                    }
                }

                D3DTextureBatch batch = {};
                batch.draw_count = num_textures;
                batch.transforms = arena_pushcount(&rlist->temp_arena, Rect, batch.draw_count);
                batch.fetchuvs = arena_pushcount(&rlist->temp_arena, Vec2, batch.draw_count);
                batch.fetchsts = arena_pushcount(&rlist->temp_arena, Vec2, batch.draw_count);
                batch.colors = arena_pushcount(&rlist->temp_arena, Renderer_RGBA, batch.draw_count);
                for(int k = 0; k < num_textures; k++)
                {
                    RenderEntry *this_entry = &rlist->entries[i+k];
                    batch.transforms[k] = {this_entry->x, this_entry->y, this_entry->w, this_entry->h};
                    batch.fetchuvs[k] = v2(this_entry->u, this_entry->v);
                    batch.fetchsts[k] = v2(this_entry->s, this_entry->t);
                    batch.colors[k] = *(Renderer_RGBA*)&this_entry->color;
                    batch.colors[k].a = this_entry->transparency;
                }

                // d3d_draw_texturemasked(entry->x, entry->y, entry->w, entry->h,
                //                        entry->u, entry->v, entry->s, entry->t,
                //                        entry->color, entry->mask->GPUHandle);
                d3d_draw_texturemasked_instanced(batch.draw_count,
                                                 batch.transforms, batch.fetchuvs, batch.fetchsts,
                                                 batch.colors, entry->mask);
                i = j - 1;
            }break;
            case Render_texture:
            {
                stop("no support for textures yet")
            }
            default:
                stop("unknown");
        }
    }
    d3d_end();
}

#endif //if RENDER_D3D
//TODO optimize sorting instead of copying the entire render element
//     maybe just copy a pointer??

#if INTERNAL
inline b32
is_render_element_null(RenderEntry *entry)
{
    if(entry->type == 0 && entry->z == 0 &&
       entry->x == 0 && entry->w ==0 &&
       entry->w == 0) return true;
    return false;
}
#endif

internal void
merge_sort_render_elements_merge(RenderList *rlist, u32 l, u32 m, u32 r)
{
    u32 frontl = l;
    u32 frontr = m+1;

    u32 amt = r-l+1; //NOTE::BUG amount actually!!!
    //RenderEntry temp_entries[20000];
    RenderEntry *temp_entries = arena_pushcount(&rlist->temp_arena, RenderEntry, amt);

    for(int i = 0; i < amt; i++)
    {
        if(frontl > m)
        {
            temp_entries[i] = rlist->entries[frontr];
            frontr++;
        }
        else if(frontr > r)
        {
            temp_entries[i] = rlist->entries[frontl];
            frontl++;
        }
        else if (rlist->entries[frontr].z < rlist->entries[frontl].z)
        {
            temp_entries[i] = rlist->entries[frontr];
            frontr++;
        }
        else
        {
            temp_entries[i] = rlist->entries[frontl];
            frontl++;
        }
        
    }

    for(int i = 0; i < amt; i++)
    {
        rlist->entries[l+i] = temp_entries[i];
    }
}
internal void
merge_sort_render_elements_ranged(RenderList *rlist, u32 l, u32 r)
{
    u32 m = l + (r-l)/2;
    if(l < r)
    {
        merge_sort_render_elements_ranged(rlist, l, m);
        merge_sort_render_elements_ranged(rlist, m+1, r);
        merge_sort_render_elements_merge(rlist, l, m, r);        
    }
}

internal void
merge_sort_render_elements(RenderList *rlist)
{
    TIMED_BLOCK;
    merge_sort_render_elements_ranged(rlist, 0, rlist->num_entries-1);
}

#pragma optimize( "", on )                           
internal void
bubble_sort_render_elements(RenderList *rlist)
{
    for(int i = 0; i < rlist->num_entries-1; i++)
    {
        for(int j = 0; j < rlist->num_entries-i-1; j++)
        {
            //TODO better sorting
            RenderEntry *this_ = &rlist->entries[j];
            RenderEntry *next_ = &rlist->entries[j + 1];

            if(next_->z < this_->z)
            {
                RenderEntry temp;
                temp = next_[0];
                next_[0] = this_[0];
                this_[0] = temp;
                // copy_render_element(&temp, next_);
                // copy_render_element(next_, this_);
                // copy_render_element(this_, &temp);
            }
        }
    }
}
#pragma optimize( "", off )                           

internal RenderList
allocate_render_storage(MemoryArena *alloc, u32 render_storage_size)
{
    RenderList ret;
    //two thirds of the render storage will be used for temporary arena
    //one third will be dedicated to storing render entries
    ret.temp_arena = arena_suballocate(alloc, (render_storage_size/3)*2);
    u32 entries_to_allocate = (render_storage_size/2)/sizeof(RenderEntry);
    ret.entries = arena_pushcount(alloc, RenderEntry, (render_storage_size/3)/sizeof(RenderEntry));
    ret.num_entries = 0;
    ret.max_entries = entries_to_allocate;
    return ret;
}
#if RENDER_OPENGL
internal void
renderlist_to_screen_opengl(RenderList *rlist, RenderTarget *rtarget)
{
    merge_sort_render_elements(rlist);
    // bubble_sort_render_elements(rlist);
    //TODO make opengl directly blit to the screen instead of FBO??
    __renderlist_to_opengl_framebuffer(rlist, rtarget);
    // __renderlist_do_post_processing(rlist, rtarget);
    opengl_blittoscreen();
    rlist->num_entries = 0;    
};
#elif RENDER_D3D
internal void
renderlist_to_screen_d3d(RenderList *rlist, RenderTarget *rtarget)
{
    merge_sort_render_elements(rlist);
    // bubble_sort_render_elements(rlist);
    __renderlist_to_d3d(rlist, rtarget);
    //__renderlist_to_opengl_framebuffer(rlist, rtarget);
    rlist->num_entries = 0;    
};

#endif
