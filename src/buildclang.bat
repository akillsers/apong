@echo off
ctime -begin apong_clang.ctm

IF NOT EXIST ..\build mkdir "..\build"

pushd "..\build"

set CommonCompilerFlags=

set "registryparser="
if defined registryparser (
   cl -nologo -Od -Zi -DSLOW=1 -DINTERNAL=1 -Fo:glregparse -Fe:glregparse "..\src\opengl_registry_parser.cpp" -I "..\headers"       
)

set "meta=34"
if defined meta (
  cl -nologo -Od -Zi -DSLOW=1 -DINTERNAL=1 -Fe:glmeta "..\src\opengl_meta.cpp" -I "..\headers" 
  pushd "..\src"
  ..\build\glmeta.exe opengl_error_checking.cpp opengl_shadergenerated.cpp > opengl_functions.cpp
  popd
) 

set IncludeDirectories= ^
-I"C:\Program Files (x86)\Windows Kits\10\Include\10.0.16299.0\winrt" ^
-I"C:\Program Files (x86)\Windows Kits\10\Include\10.0.16299.0\um" ^
-I"C:\Program Files (x86)\Windows Kits\10\Include\10.0.16299.0\shared" ^
-I"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.13.26128\include" 
clang   -gcodeview -gfull  -Wno-writable-strings -Wno-missing-declarations -Wno-write-strings -ffast-math -fno-exceptions -fno-cxx-exceptions "..\src\windows.cpp"  -Iquote"C:\Program Files\LLVM\lib\clang\6.0.1\include" -I"..\headers" -o"windows.o" -c -v


lld-link.exe windows.o -pdb:windows.pdb -debug -debugtype:cv,FIXUP,PDATA  -out:windows.exe -errorlimit:0 -nodefaultlib ucrt.lib vcruntime.lib kernel32.lib user32.lib opengl32.lib gdi32.lib Winmm.lib

popd "..\build"

ctime -end apong_clang.ctm %ERRORLEVEL%
