#include "aks.h"
#include "windows.h"
#include "stdio.h"
#include "meta.h"
#include "stringutils.cpp"
#include "stringout.cpp"

META_EXCLUDE ____file
inline void
record_filename(char *src, FileInfo *info)
{
    char *at = src;
    char *to;
    if(info->file_count == 0)
    {
        to = info->filenames;
    }
    else
    {
        to = info->filenames + (info->filename_offsets[info->file_count-1] + info->filename_len[info->file_count-1]);
    }
    info->filename_offsets[info->file_count] = to - info->filenames;
    char *from = to;
    while(at[0])
    {
        *to++ = *at++;
    }
    //NOTE:: null terminate this so that it can be passed to functions that require c style strings
    *to++ = 0;
    
    info->filename_len[info->file_count] = to - from;
    info->file_count++;
}

internal void
find_and_record_filenames(char *search_filename[], u32 search_count,
                          char *forbidden_filename[], u32 forbidden_count,  FileInfo *file_info)
{
    for(int i = 0; i < search_count; i++)
    {
        WIN32_FIND_DATA data;
        HANDLE search_handle = FindFirstFile(search_filename[i], &data);
        if(search_handle)
        {
        next:
            b32 forbidden = false;
            for(int i = 0; i < forbidden_count; i++)
            {
                if(string_equal(data.cFileName, forbidden_filename[i]))
                {
                    forbidden = true;
                    break;
                }
            }
            if(!forbidden)
            {
                file_info->file_size[file_info->file_count] = (data.nFileSizeHigh * (MAXDWORD + 1)) + data.nFileSizeLow;            
                record_filename(data.cFileName, file_info);
            }

            BOOL result = FindNextFile(search_handle, &data);
            if(result)
            {
                goto next;
            }

            int err = GetLastError();
            if(err != ERROR_NO_MORE_FILES)
            {
                fprintf(stderr, "FindNextFile has failed!! \n");
            }
        }
        else
        {
            int err = GetLastError();
            if(err != ERROR_FILE_NOT_FOUND)
            {
                fprintf(stderr, "FindFirstFile has failed!! \n");
            }

        }
    }
}


internal Token
next_token(Tokenizer *tokenizer)
{
    for(;;)
    {
        if(tokenizer->at[0] == ' ' || tokenizer->at[0] == '\n'||
           tokenizer->at[0] == '\r')
        {
            tokenizer->at++;
        }
        else if (tokenizer->at[0] == '/' && tokenizer->at[1] == '/')
        {
            do
            {
                tokenizer->at++;                
            }while(!(tokenizer->at[0] == '\r' || tokenizer->at[0] == '\n'));
        }
        else if (tokenizer->at[0] == '/' && tokenizer->at[1] == '*')
        {
            do
            {
                tokenizer->at++;
            }while(!(tokenizer->at[0] == '*' && tokenizer->at[1] == '/'));
        }
        else if (tokenizer->at[0] == '#')
        {
            do
            {
                //NOTE:: skip preprocessor directives!!
                tokenizer->at++;
            }while(!(tokenizer->at[0] == '\n' || tokenizer->at[0] == '\r'));
        }
        else
            break;
    }
    Token ret;
    char c = tokenizer->at[0];
    ret.text = tokenizer->at;
    tokenizer->at++;
    switch(c)
    {
        case '(':{ret.type = Token_openparen;}break;
        case ')':{ret.type = Token_closeparen;}break;
        case '{':{ret.type = Token_openbraces;}break;
        case '}':{ret.type = Token_closebraces;}break;
        case ';':{ret.type = Token_semicolon;}break;
        case '*':{ret.type = Token_asterisk;}break;
        case '~':{ret.type = Token_tilde;}break;
        case '[':{ret.type = Token_openbrackets;}break;
        case ']':{ret.type = Token_closebrackets;}break;
        case ',':{ret.type = Token_comma;}break;
        case '\0':{ret.type = Token_eof;}break;
        case '=':{ret.type = Token_equals;}break;

        case '"':
        {
            ret.type = Token_quoted_text;
            ret.text = tokenizer->at;
            do
            {
                tokenizer->at++;
            }while(tokenizer->at[0] == '"' || tokenizer->at[0] == '\0');
            ret.textlen = tokenizer->at - ret.text;
            tokenizer->at++;
        }break;
            
        default:
        {
            if((c >= 'a' && c <= 'z') ||
               (c >= 'A' && c <= 'Z') ||
               c == '_')
            {
                ret.type = Token_identifier;

                while((tokenizer->at[0]  >= 'a' && tokenizer->at[0] <= 'z') ||
                      (tokenizer->at[0] >= 'A' && tokenizer->at[0] <= 'Z') ||
                      (tokenizer->at[0] >= '0' && tokenizer->at[0] <= '9') ||
                      tokenizer->at[0] == '_' )
                    tokenizer->at++;
                ret.textlen = tokenizer->at - ret.text;
            }
            else
            {
                ret.type = Token_unknown;
            }

        }
    }
    
    return ret;
}

internal Token
predict_token(Tokenizer *tokenizer)
{
    char *prev_at = tokenizer->at;
    Token ret = next_token(tokenizer);
    tokenizer->at = prev_at;
    return ret;
}

internal b32
value_type_already_seen(ValueType *type, ParseData *data)
{
    for(int i = 0; i < data->current_value_type+1; i++)
    {
        if(string_equall(type->name, type->namelen, data->value_types[i].name, data->value_types[i].namelen))
            return true;
    }
    return false;
}

// internal b32
// value_type_is_struct_name(ValueType *type, ParseData *data)
// {
//     for(int i = 0; i < data->current_struct+1; i++)
//     {
//         if(string_equall(type->name, type->namelen, data->structs[i].name, data->structs[i].namelen))
//             return true;
//     }
//     return false;
// }

internal void
find_struct_with_value_type_name(ValueType *type, ParseData *data, Struct **out_struct)
{
    for(int i = 0; i < data->current_struct+1; i++)
    {
        if(string_equall(type->name, type->namelen, data->structs[i].name, data->structs[i].namelen))
        {
            out_struct[0] = &data->structs[i];
            return;
        }
    }
}

internal void
find_enum_with_value_type_name(ValueType *type, ParseData *data, Enum **out_enum)
{
    for(int i = 0; i < data->current_struct+1; i++)
    {
        if(string_equall(type->name, type->namelen, data->enums[i].name, data->enums[i].namelen))
        {
            out_enum[0] = &data->enums[i];
            return;
        }
    }
}

//NOTE:: returns false if not actually a member
internal b32
parse_member(Tokenizer *tokenizer, ParseData *data)
{
    Token token;
    token = next_token(tokenizer);
    b32 isptr = false;

    Token identifier_tokens[20];
    u32 num_identifiers = 0;
    u32 num_commas = 0;
    for (;;)
    {
        if(token.type == Token_identifier)
        {
            identifier_tokens[num_identifiers] = token;
            num_identifiers++;
        }
        else if (token.type == Token_openbrackets)
        {
            do
            {
                token = next_token(tokenizer);
            }while(token.type != Token_closebrackets);
        }
        else if(token.type == Token_asterisk)
            isptr = true;
        else if (token.type == Token_comma)
        {
            num_commas++;
        }
        else if (token.type == Token_equals)
        {
            token = next_token(tokenizer);
            token = next_token(tokenizer);
            if(token.type == Token_semicolon)
                goto end_statement;
            else
                fprintf(stderr, "Expected semicolon after equals designation\n");
        }
        else if(token.type == Token_semicolon)
        {
        end_statement:
            u32 type_index = num_identifiers - num_commas - 2;
            u32 name_index = num_identifiers - num_commas - 1;

            ValueType type;
            type.name = identifier_tokens[type_index].text;
            type.namelen = identifier_tokens[type_index].textlen;

            if(!value_type_already_seen(&type, data))
            {
                data->current_value_type++;
                assert(data->current_value_type < MAX_VALUE_TYPES);
                data->value_types[data->current_value_type] = type;
                //printf("value type: %.*s (atomic? %s)\n", type.namelen, type.name, is_atomic ? "true" : "false");
            }

            for(int i = 0; i < num_commas+1; i++)
            {

                Struct *curr_struct = &data->structs[data->current_struct];
                assert(curr_struct->member_count < MAX_MEMBERS);
                curr_struct->members[curr_struct->member_count].name = identifier_tokens[name_index].text;
                curr_struct->members[curr_struct->member_count].namelen = identifier_tokens[name_index].textlen;
                curr_struct->members[curr_struct->member_count].isptr = isptr;
                curr_struct->members[curr_struct->member_count].type_name = identifier_tokens[type_index].text;
                curr_struct->members[curr_struct->member_count].type_namelen = identifier_tokens[type_index].textlen;

                //printf("member found: %.*s %.*s\n",
                //        identifier_tokens[type_index].textlen,
                //        identifier_tokens[type_index].text,
                //        identifier_tokens[name_index].textlen,
                //        identifier_tokens[name_index].text);

                name_index++;
                curr_struct->member_count++;
            }
            return true;
        }
        else if(token.type == Token_openparen)
        {
            return false;
        }
        token = next_token(tokenizer);
    }

}

//internal void parse_struct(Tokenizer *tokenizer, ParseData *data);



internal void
parse_struct(Tokenizer *tokenizer, ParseData *data, b32 parse_nameless)
{
    Token token;
    token = next_token(tokenizer);
    // if(token.type != Token_identifier)
    //     return;
    u32 member_start = 0;
    if(token.type == Token_identifier)
    {
        data->current_struct++;
        assert(data->current_struct < MAX_STRUCTS);
        data->structs[data->current_struct].name = token.text;
        data->structs[data->current_struct].namelen = token.textlen;
        data->structs[data->current_struct].fromfileindex = data->current_file_index;
        token = next_token(tokenizer);
        //printf("struct found: %.*s\n", data->structs[data->current_struct].namelen, data->structs[data->current_struct].name);
    }
    else if (!parse_nameless)
    {
        u32 unclosed_braces = 0;
        for(;;)
        {
            token = next_token(tokenizer);
            if(token.type == Token_openbraces)
                unclosed_braces++;
            if(token.type == Token_closebraces)
            {
                if(unclosed_braces == 0)
                {
                    token = next_token(tokenizer);

                    //TODO right now it only supports one declarator
                    if(token.type == Token_identifier)
                    {
                        token = next_token(tokenizer);
                    }
                    if(token.type != Token_semicolon)
                        fprintf(stderr, "expected semicolon after struct!\n");
                    return;   
                }
                else
                    unclosed_braces--;
            }
        }
    }
    else
    {
        Struct *curr_struct = &data->structs[data->current_struct];
        member_start = curr_struct->member_count;
    }

    //TODO support dead structs (structs without declaration just like Struct foo;)
    if(token.type != Token_openbraces)
        fprintf(stderr,"error: expected open braces after struct or struct identifer\n");

    for(;;)
    {
        char *prev_tokenat = tokenizer->at;
        token = next_token(tokenizer);
        if(token.type == Token_identifier)
        {
            if(string_equall(token.text, token.textlen, "struct"))
            {
                //NOTE:: no need to rewind just get the next token after struct keyword
                parse_struct(tokenizer, data, true);

            }
            else if (string_equall(token.text, token.textlen, "union"))
            {
                parse_struct(tokenizer, data, true);
            }
            else if (string_equall(token.text, token.textlen, "META_EXCLUDE"))
            {
                do
                {
                    token = next_token(tokenizer);
                }while(token.type != Token_semicolon);
            }
            else
            {
                Token token = next_token(tokenizer);
                if(token.type == Token_openparen)
                {
                    do
                    {
                        token = next_token(tokenizer);
                    }while(token.type != Token_closebraces);
                    continue;
                }

                tokenizer->at = prev_tokenat; //NOTE:: rewind to start of variable declaration

                if(!parse_member(tokenizer, data))
                {
                    //not a member (probably a function in a struct!!!)
                    do
                    {
                        token = next_token(tokenizer);
                    }while(token.type != Token_closebraces);
                    continue;
                }
            }
        }
        else if(token.type == Token_closebraces)
        {
            token = next_token(tokenizer);
            if(token.type == Token_semicolon)
            {
                break;
            }
            else if (token.type == Token_identifier)
            {
                Struct *curr_struct = &data->structs[data->current_struct];
                // curr_struct->members[curr_struct->member_count].name = identifier_tokens[name_index].text;
                // curr_struct->members[curr_struct->member_count].namelen = identifier_tokens[name_index].textlen;
                // curr_struct->members[curr_struct->member_count].isptr = isptr;
                // curr_struct->members[curr_struct->member_count].type_name = identifier_tokens[type_index].text;
                // curr_struct->members[curr_struct->member_count].type_namelen = identifier_tokens[type_index].textlen;

                //printf("member found: %.*s %.*s\n",
                //        identifier_tokens[type_index].textlen,
                //        identifier_tokens[type_index].text,
                //        identifier_tokens[name_index].textlen,
                //        identifier_tokens[name_index].text);
                
                for(int i = member_start; i < curr_struct->member_count; i++)
                {

                    char *new_name = (char *)malloc(50);
                    char *at;
                    at = string_copyl(new_name, token.text, token.textlen);
                    *at++ = '.';
                    at = string_copyl(at, curr_struct->members[i].name,
                                      curr_struct->members[i].namelen);
                    u32 len = at - new_name;

                    curr_struct->members[i].name = new_name;
                    curr_struct->members[i].namelen = len;
                }

                token = next_token(tokenizer);
                if(token.type != Token_semicolon)
                    fprintf(stderr, "expected semicolon after struct!\n");
                break;
            }
        }
        else if (token.type == Token_tilde)
        {
            do
            {
                token = next_token(tokenizer);
            }while(token.type != Token_closebraces);
        }


        // member.name = identifier_tokens[num_identifiers-1].text;
        // member.namelen = identifier_tokens[num_identifiers-1].textlen;
    }
}

internal void
parse_enum(Tokenizer *tokenizer, ParseData *data)
{
    Token token;
    token = next_token(tokenizer);
    // if(token.type != Token_identifier)
    //     return;
    Enum *cenum;
    if(token.type == Token_identifier)
    {
        data->current_enum++;
        assert(data->current_enum < MAX_ENUMS);
        cenum = &data->enums[data->current_enum];

        cenum->name = token.text;
        cenum->namelen = token.textlen;
        token = next_token(tokenizer);
        //printf("enum found: %.*s\n", cenum->namelen, cenum->name);
        if(string_equall(cenum->name, cenum->namelen, "TokenType"))
        {
            stop("hey there");
        }
        if(token.type != Token_openbraces)
        {
            fprintf(stderr,"error: expected open braces after enum or enum identifer\n");
            return;
        }
        
        for(;;)
        {
            char *prev_tokenat = tokenizer->at;
            token = next_token(tokenizer);
            if(token.type == Token_identifier)
            {
                //this is an enum name;
                assert(cenum->enum_name_count < MAX_ENUM_NAMES);
                cenum->enum_names[cenum->enum_name_count] = token.text;
                cenum->enum_name_lengths[cenum->enum_name_count] = token.textlen;
                cenum->enum_name_count++;
            }
            else if(token.type == Token_closebraces)
            {
                token = next_token(tokenizer);
                if(token.type == Token_semicolon)
                {
                    break;
                }
                else if (token.type == Token_identifier)
                {
                    token = next_token(tokenizer);
                    if(token.type != Token_semicolon)
                        fprintf(stderr, "expected semicolon after enum!\n");
                    break;
                }
            }
            // member.name = identifier_tokens[num_identifiers-1].text;
            // member.namelen = identifier_tokens[num_identifiers-1].textlen;
        }

    }

}

internal b32
already_marked(StructMember *_member, char *required_name_array[], u32 *required_namelen_array, u32 current_required)
{
    for(int j = 0; j <= current_required; j++)
    {
        if(string_equall(_member->type_name, _member->type_namelen,
                         required_name_array[j], required_namelen_array[j]))
        {
            return true;
        }
    }
    return false;
}


internal Struct *
find_struct(char *name, u32 namelen, ParseData *parse_data)
{
    for(int i = 0; i <= parse_data->current_struct; i++)
    {
        if(string_equall(name, namelen,
                        parse_data->structs[i].name, parse_data->structs[i].namelen))
        {
            return parse_data->structs + i;
        }
    }
    return false;
}

internal Enum *
find_enum(char *name, u32 namelen, ParseData *parse_data)
{
    for(int i = 0; i <= parse_data->current_enum; i++)
    {
        if(string_equall(name, namelen,
                        parse_data->enums[i].name, parse_data->enums[i].namelen))
        {
            return parse_data->enums + i;
        }
    }
    return false;
}

internal Struct *
find_struct(StructMember *_member, ParseData *parse_data)
{
    return find_struct(_member->type_name, _member->type_namelen, parse_data);
}

internal ValueType *
find_value_type(char *name, u32 namelen, ValueType *database, u32 database_size)
{
    for(int i = 0; i < database_size; i++)
    {
        if(string_equall(name, namelen, database[i].name, database[i].namelen))
        {
            return &database[i];
        }
    }
    return false;
}

internal void
mark_all_structs_necessary(Struct *_struct, ParseData *parse_data)
{
    for(int j = 0; j <= parse_data->current_required_struct; j++)
    {
        if(string_equall(_struct->name, _struct->namelen,
			parse_data->required_struct_names[j], parse_data->required_struct_namelen[j]))
        {
            for(int k = 0; k < _struct->member_count; k++)
            {
                ValueType *value_type =  find_value_type(_struct->members[k].type_name,
                                                         _struct->members[k].type_namelen,
                                                         parse_data->value_types, parse_data->current_value_type + 1);
                if(value_type)
                {
                    if(value_type->kind == BT_vargroup)
                    {
                        if(!already_marked(&_struct->members[k], parse_data->required_struct_names,
                                           parse_data->required_struct_namelen, parse_data->current_required_struct))
                        {
                            //this needs to be marked
                            u32 sindex = ++parse_data->current_required_struct;

                            parse_data->required_struct_names[sindex] = _struct->members[k].type_name;
                            parse_data->required_struct_namelen[sindex] = _struct->members[k].type_namelen;

                            Struct *struct_to_mark = find_struct(&_struct->members[k], parse_data);
                            // fprintf(stderr, "%.*s is now marked!\n", parse_data->required_struct_namelen[sindex],
                            //         parse_data->required_struct_names[sindex]);
                            mark_all_structs_necessary(struct_to_mark, parse_data);
                        }
                    }
                    else if (value_type->kind == BT_enum)
                    {
                        if(parse_data->current_required_enum == U32_MAX ||
                           !already_marked(&_struct->members[k], parse_data->required_enum_names,
                                           parse_data->required_enum_namelen, parse_data->current_required_enum))
                        {
                            //this needs to be marked
                            u32 eindex = ++parse_data->current_required_enum;

                            parse_data->required_enum_names[eindex] = _struct->members[k].type_name;
                            parse_data->required_enum_namelen[eindex] = _struct->members[k].type_namelen;
                        }
                    }
                }
                else
                {
                    fprintf(stderr, "Error:: could not find value type!!\n");
                }
            }
                
        }
        
    }

}

Struct structs[MAX_STRUCTS];
char *required_struct_names[MAX_REQUIRED_STRUCTS];
u32 required_struct_namelen[MAX_REQUIRED_STRUCTS];
char *required_enum_names[MAX_REQUIRED_ENUMS];
u32 required_enum_namelen[MAX_REQUIRED_ENUMS];



ValueType value_types[MAX_VALUE_TYPES];
Enum enums[MAX_ENUMS];

#define MEM_SIZE gigabytes(1)
int main()
{
    FileInfo file_info;
    file_info.file_count = 0;

    ParseData parse_data;
    parse_data.structs = structs;
    parse_data.current_struct = U32_MAX;
    parse_data.value_types = value_types;
    parse_data.current_value_type = U32_MAX;
    parse_data.enums = enums;
    parse_data.current_enum = U32_MAX;

    parse_data.required_struct_namelen = required_struct_namelen;
    parse_data.required_struct_names = required_struct_names;
    parse_data.current_required_struct = U32_MAX;

    parse_data.required_enum_names = required_enum_names;
    parse_data.required_enum_namelen = required_enum_namelen;
    parse_data.current_required_enum = U32_MAX;

    char *file_memory = (char *)VirtualAlloc(0, MEM_SIZE, MEM_COMMIT, PAGE_READWRITE); //TODO NOTE::assume this is enought 
    u32 mem_offset = 0;
    

    find_and_record_filenames(search_filenames, arraycount(search_filenames),
                              forbidden_filenames, arraycount(forbidden_filenames),
                              &file_info);

    printf("//Found %u source code files(*.cpp, *.h)!\n", file_info.file_count);



    for (int i = 0; i < arraycount(extra_required_structs); i++)
    {
        u32 sindex = ++parse_data.current_required_struct;

        parse_data.required_struct_names[sindex] = extra_required_structs[i];
        parse_data.required_struct_namelen[sindex] = string_length(extra_required_structs[i]);
    }
    
    for(int i = 0; i < file_info.file_count; i++)
    {
        printf("//%.*s (%I64u bytes)\n", file_info.filename_len[i], (file_info.filenames + file_info.filename_offsets[i]), file_info.file_size[i]);
    }

    for(int i = 0; i < file_info.file_count; i++)
    {
        HANDLE file_handle = CreateFile(file_info.filenames + file_info.filename_offsets[i],
                                        GENERIC_READ,
                                        FILE_SHARE_READ,
                                        0, //security attributes
                                        OPEN_EXISTING,
                                        FILE_ATTRIBUTE_NORMAL,
                                        0);
        if(file_handle == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Could not open file %s\n", file_info.filenames + file_info.filename_offsets[i]);
            continue;
        }


        if(mem_offset + file_info.file_size[i] > MEM_SIZE)
        {
            fprintf(stderr, "Out of memory! \n");
            break;
        }
        DWORD bytes_read;
        BOOL result = ReadFile(file_handle,
                               file_memory + mem_offset,
                               MEM_SIZE - mem_offset,
                               &bytes_read, 0);

        if(!result)
        {
            int errer = GetLastError();
            fprintf(stderr, "Could not read file %s\n", file_info.filenames + file_info.filename_offsets[i]);
            continue;
        }

        parse_data.current_file_index = i;
        Tokenizer tokenizer;
        tokenizer.at = file_memory + mem_offset;
        Token token = {};
        while(token.type != Token_eof)
        {
            token = next_token(&tokenizer);
            switch(token.type)
            {
                case Token_identifier:
                {
                    // if(string_equall(token.text, token.textlen, "introspect"))
                    // {
                    //     token = next_token(&tokenizer);
                    //     if(string_equall(token.text, token.textlen, "struct"))
                    //     {
                    //         parse_struct(&tokenizer, &parse_data);
                    //     }
                    // }
                    //TODO add support for function pointers
                    //TODO add optimization of reading files from #include directives
                    //     that way we don't have to do this buffering of making a struct and member list
                    //     to see if the type of a member was a previously declared struct
                    if(string_equall(token.text, token.textlen, "META_EXCLUDE"))
                    {
                        token = next_token(&tokenizer);
                        if(string_equall(token.text, token.textlen, "struct"))
                        {
                            token = next_token(&tokenizer);
                            if(token.type == Token_openbraces)
                            {
                                u32 unclosed_braces_count = 0;
                                for(;;)
                                {
                                    token = next_token(&tokenizer);
                                    switch(token.type)
                                    {
                                        case Token_openbraces:
                                            unclosed_braces_count++;
                                            break;
                                        case Token_closebraces:
                                            if(unclosed_braces_count == 0)
                                                goto done;
                                            else
                                            {
                                                unclosed_braces_count--;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            done:;
                            }

                                
                        }
                        else if (string_equall(token.text, token.textlen, "____file"))
                        {
                            goto next_file;
                        }
                        //printf("%u: %.*s\n", token.type, token.len, token.text);
                    }
                    else if (string_equall(token.text, token.textlen, "struct"))
                    {
                        parse_struct(&tokenizer, &parse_data, false);
                    }
                    else if (string_equall(token.text, token.textlen, "union"))
                    {
                        parse_struct(&tokenizer, &parse_data, false);                        
                    }
                    else if (string_equall(token.text, token.textlen, "enum"))
                    {
                        parse_enum(&tokenizer, &parse_data);
                    }
                    else if (string_equall(token.text, token.textlen, "dbg_struct"))
                    {
                        token = next_token(&tokenizer);
                        if(token.type == Token_openparen)
                        {
                            token = next_token(&tokenizer);

                            if(token.type == Token_identifier)
                            {
                                u32 sindex = ++parse_data.current_required_struct;

                                parse_data.required_struct_names[sindex] = token.text;
                                parse_data.required_struct_namelen[sindex] = token.textlen;
                            }
                        }
                    }
                }

            }
        }
    next_file:;        
        mem_offset += bytes_read;

    
    }
    //sd;
    u32 atomic_count = 0;
    u32 enum_count = 0;
    u32 vargroup_count = 0;

    u32 preset_atomic_sizes[200] = {};
    
    for (int i = 0; i < parse_data.current_value_type + 1; i++)
    {
        // if(string_equall(parse_data.value_types[i].name, parse_data.value_types[i].namelen, "s"))
        // {
        //     stop("sdf");
        // }
        Struct *value_type_struct = 0;

        u32 extra_types_index;
        b32 inarray = string_inarray(parse_data.value_types[i].name,
                                     parse_data.value_types[i].namelen,
                                     extra_atomic_types, arraycount(extra_atomic_types),
                                     &extra_types_index);
        if(!inarray)
        {
            find_struct_with_value_type_name(&parse_data.value_types[i], &parse_data, &value_type_struct);            
        }
        else
        {
            preset_atomic_sizes[atomic_count] = extra_atomic_type_sizes[extra_types_index];
        }


        if(value_type_struct)
        {
            //not atomic
            char *first_type_name = value_type_struct->members[0].type_name;
            u32 first_type_name_len = value_type_struct->members[0].type_namelen;

               
            parse_data.value_types[i].kind = BT_vargroup;
            vargroup_count++;
        }
        else
        {
            //pretty deng atomic
            //printf("%.*s is atomic!\n", parse_data.value_types[i].namelen, parse_data.value_types[i].name);

            Enum *value_type_enum = 0;
            find_enum_with_value_type_name(&parse_data.value_types[i], &parse_data, &value_type_enum);
            if(value_type_enum)
            {
                //note atomic is BT_enum
                parse_data.value_types[i].kind = BT_enum;
                enum_count++;
            }
            else
            {
                //pretty deng atomic!!
                parse_data.value_types[i].kind = BT_atomic;
                atomic_count++;
            }
        }
    }


    
    for(int i = 0; i < parse_data.current_value_type; i++ ) //NOTE:: one less than count
    {
        for(int j = 0; j < parse_data.current_value_type - i;j++)
        {
            if(parse_data.value_types[j].kind > parse_data.value_types[j +1].kind)
            {
                ValueType temp;
                temp = parse_data.value_types[j + 1];
                parse_data.value_types[j + 1] = parse_data.value_types[j];
                parse_data.value_types[j] = temp;
            }
        }
    }

    u32 atomic_base = 0;
    u32 vargroup_base = atomic_count;
    u32 enum_base = vargroup_base + vargroup_count;

    for(int i = 0; i <= parse_data.current_struct; i++)
    {
        mark_all_structs_necessary(&parse_data.structs[i], &parse_data);
    }


    u32 new_vargroup_count = vargroup_count;
    u32 index = vargroup_base;
    for(int i = 0; i < vargroup_count; i++)
    {
        b32 marked = false;
        for(int j = 0; j <= parse_data.current_required_struct; j++)
        {
            if(string_equall(parse_data.value_types[index].name,
                             parse_data.value_types[index].namelen,
                             parse_data.required_struct_names[j], parse_data.required_struct_namelen[j]))
            {
                marked = true;
                break;
            }
        }

        if(!marked)
        {
            fprintf(stderr,"%.*s isn't marked!\n", parse_data.value_types[index].namelen,
                    parse_data.value_types[index].name);
            ValueType temp;
            temp = parse_data.value_types[parse_data.current_value_type];
            for(int j = index; j < parse_data.current_value_type - 1; j++)
            {
                ValueType temp;
                temp = parse_data.value_types[j + 1];
                parse_data.value_types[j + 1] = parse_data.value_types[j];
                parse_data.value_types[j] = temp;
            }
            new_vargroup_count--;
            index--;
        }
        else
        {
            fprintf(stderr,"%.*s IS marked!\n", parse_data.value_types[index].namelen,
                    parse_data.value_types[index].name);
        }
        index++;
    }

    vargroup_count = new_vargroup_count;
            enum_base = vargroup_base + vargroup_count;
    
    char buffer[400000];
    DestBuf buf;
    buf.at = buffer;
    buf.remaining_chars = arraycount(buffer);

    outstring(&buf, "META_EXCLUDE ____file\n");

    outstring(&buf, "enum ValueType\n");
    outstring(&buf, "{\n");
    u32 last = enum_base + enum_count;
    for(int i = 0; i < last + 1; i++)
    {
        outstring(&buf, "    T_");
        outstringl(&buf, parse_data.value_types[i].name, parse_data.value_types[i].namelen);
        outstring(&buf, ",\n");
    }
    outstring(&buf, "    T_Count\n");
    outstring(&buf, "};\n\n");
 
    char valuebasetype_info[] = R"delimiter(
enum ValueBaseType
{
    BT_atomic,
    BT_vargroup,
    BT_enum,
    
    BT_Count
};

)delimiter";
    outstring(&buf, valuebasetype_info);
    
    outstring(&buf, "    u32 atomic_base = ");
    outnumber(&buf, atomic_base);
    outstring(&buf, ";\n");

    outstring(&buf, "    u32 vargroup_base = ");
    outnumber(&buf, vargroup_base);
    outstring(&buf, ";\n");

    outstring(&buf, "    u32 enum_base = ");
    outnumber(&buf, enum_base);
    outstring(&buf, ";\n");

    
    outstring(&buf, "\nValueBaseType value_base_types[T_Count] = ");
    outstring(&buf, "{\n");

    for(int i = 0; i < atomic_count; i++)
    {
        outstring(&buf, "    BT_atomic,");
        outstring(&buf, " /*");
        outstringl(&buf, parse_data.value_types[i].name, parse_data.value_types[i].namelen);
        outstring(&buf, "*/\n");
    }


    for(int i = vargroup_base; i < vargroup_base + vargroup_count; i++)
    {
        outstring(&buf, "    BT_vargroup,");            
        outstring(&buf, " /*");
        outstringl(&buf, parse_data.value_types[i].name, parse_data.value_types[i].namelen);
        outstring(&buf, "*/\n");
    }

    for(int i = enum_base; i < enum_base + enum_count; i++)
    {
        if(i + 1 == enum_base + enum_count)
            outstring(&buf, "    BT_enum");
        else
            outstring(&buf, "    BT_enum,");            
        outstring(&buf, " /*");
        outstringl(&buf, parse_data.value_types[i].name, parse_data.value_types[i].namelen);
        outstring(&buf, "*/\n");
    }

    outstring(&buf, "};\n\n");

   
    //TODO enum names;
    for(int i = 0; i <= parse_data.current_required_enum; i++)
    {
        Enum *_enum = find_enum(parse_data.required_enum_names[i],
                                parse_data.required_enum_namelen[i],
                                &parse_data);


        outstring(&buf, "char *");
        outstringl(&buf, _enum->name, _enum->namelen);
        outstring(&buf, "_enum_names[] =\n");
        outstring(&buf, "{\n");
        for(int j = 0; j < _enum->enum_name_count; j++)
        {
            outstring(&buf, "    \"");
            outstringl(&buf, _enum->enum_names[j], _enum->enum_name_lengths[j]);
            if(j + 1 != _enum->enum_name_count)
                outstring(&buf, "\",\n");
            else
                outstring(&buf, "\"\n");
        }
        
        outstring(&buf, "};\n\n");
    }
    outstring(&buf, "u64 value_sizes[] = \n");
    outstring(&buf, "{\n");
    outstring(&buf, "//TODO make these acutal sizes...somehow!!\n");
        
        for(int i = 0; i < atomic_count - 1; i++)
        {
            if(u32 size = preset_atomic_sizes[i])
            {
                outnumber(&buf, size);
                outstring(&buf, ",\n");
            }
            else
            {
                outstring(&buf, "8,\n");                
            }
        }
    // for(int i = multiplie_base; i < vargroup_base; i++)
    // {
    //     Struct *_struct = find_struct(parse_data.value_types[i].name, parse_data.value_types[i].namelen,
    //                                   &parse_data);

        
    //     u32 size_per_count = 8;
    //     u32 size = _struct->member_count * size_per_count;

    //     outnumber(&buf, size);
    //     outstring(&buf, ",\n");
    // }

    for(int i = vargroup_base; i < enum_base; i++)
    {
        outstring(&buf, "0,\n");
    }
    for(int i = enum_base; i < enum_base + enum_count; i++)
    {
        outstring(&buf, "4,\n");
    }
    outstring(&buf, "};\n\n");
    
    char memberdef_info[] = R"delimiter(
struct MemberDefinition
{
    ValueType type;
    char *name;
    u32 offset;
    b32 isptr;
};


//fixed time step will run at BLAH
//when sampling need to resampe
//TODO worry about unions
//TODO worry about pointers

)delimiter";

outstring(&buf, memberdef_info);

//TODO optimize this with less string_equall
//     make a list of pointers to required structs??
for(int j = 0; j <= parse_data.current_required_struct; j++)
{
    Struct *_struct = find_struct(parse_data.required_struct_names[j],
                                  parse_data.required_struct_namelen[j],
                                  &parse_data);
    if(_struct)
    {
           outstring(&buf, "MemberDefinition ");
           outstringl(&buf, _struct->name, _struct->namelen);
           outstring(&buf, "_members[] =");
           outstring(&buf, "//");
           outstring(&buf, &file_info.filenames[file_info.filename_offsets[_struct->fromfileindex]]);
           outstring(&buf, "\n");
           outstring(&buf, "{\n");
           for(int j = 0; j < _struct->member_count; j++)
           {
               outstring(&buf, "    {T_");
               outstringl(&buf, _struct->members[j].type_name,
                          _struct->members[j].type_namelen);
               outstring(&buf, ", \"");
               outstringl(&buf, _struct->members[j].name,
                          _struct->members[j].namelen);
               outstring(&buf, "\",");
            
               outstring(&buf, "offsetof(");
               outstringl(&buf, _struct->name, _struct->namelen);
               outstring(&buf, ", ");
               outstringl(&buf, _struct->members[j].name,
                          _struct->members[j].namelen);

               outstring(&buf, "), ");
               outstring(&buf, _struct->members[j].isptr ? "true" : "false");
               if(j + 1 == _struct->member_count)
                   outstring(&buf, "}\n");
               else
                   outstring(&buf, "},\n");
           }
           outstring(&buf, "};\n\n");

           }
        else
        {
            fprintf(stderr, "Could not find struct when trying to print out memberdefintion members!\n");
        }
    }
    //TODO this is hilarious emacs
    outstring(&buf, "//TODO is there any other way to get a definition from the type at runtime??\n");

outstring(&buf, "MemberDefinition *type_to_definitions[] = \n");
    outstring(&buf, "{\n");
    for(int j = vargroup_base; j < vargroup_base + vargroup_count; j++)
    {
        //TODO optimization we are traversing the list a bunch of times
        Struct *_struct = find_struct(parse_data.value_types[j].name,
                                      parse_data.value_types[j].namelen,
                                      &parse_data);
        
        if(!_struct)
        {
            fprintf(stderr, "Fatal error: Could not find struct!!! with name %.*s\n", parse_data.value_types[j].namelen,
                    parse_data.value_types[j].name);
            return -1;
        }

        outstring(&buf, "    ");
        outstringl(&buf, _struct->name, _struct->namelen);
        outstring(&buf, "_members");
        if(j != parse_data.current_required_struct)
            outstring(&buf, ",\n");
        else
            outstring(&buf, "\n");
                
    }

    outstring(&buf, "};\n\n");
    outstring(&buf, "u32 definition_member_counts[] = \n");
outstring(&buf, "{\n");
    for(int j = vargroup_base; j < vargroup_base + vargroup_count; j++)
    {
        Struct *_struct = find_struct(parse_data.value_types[j].name,
                                      parse_data.value_types[j].namelen,
                                      &parse_data);

        outstring(&buf, "    ");
        outnumber(&buf, _struct->member_count);
        if(j != parse_data.current_required_struct)
            outstring(&buf, ",\n");
        else
            outstring(&buf, "\n");
    }
    outstring(&buf, "};\n");

printf("%.*s", (int)arraycount(buffer) - buf.remaining_chars,buffer);

printf("#define META_GENERATE_ENUM_SWITCHES(...) {\\\n");
    for(int i = 0; i <= parse_data.current_required_enum; i++)
    {
        Enum *_enum = find_enum(parse_data.required_enum_names[i],
                                      parse_data.required_enum_namelen[i],
                                      &parse_data);
        printf("case T_%.*s: \\\n{\\\n", _enum->namelen, _enum->name);
        printf("    u32 num = ((u32 *)(value))[0];\\\n");
        printf("    sprintf(buf, \"%%s: %%u (%%s)\", name, num, (num < arraycount(%.*s_enum_names)) ? %.*s_enum_names[num] : \"enum out of range\");\\\n", _enum->namelen, _enum->name,
               _enum->namelen, _enum->name);
        printf("}break;\\\n");
    }
printf("}\n");

}
