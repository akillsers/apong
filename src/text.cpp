internal void
U32ToASCII(char *buf, u32 buffersize, u32 number)
{
    const u32 base = 10;
    char ASCIIdigits[] = "0123456789";

    u32 numdigits = 0;
    u32 workingnumber = number;

    do
    {
        workingnumber /= base;
        numdigits++;
    }while(workingnumber > 0);


    assert(buffersize >= numdigits+1); //required buffer size INCLUDING null terminator

    workingnumber = number;
    for(int i = 0; i < numdigits; i++)
    {
        u8 outdigit = number % base;
        char ASCIIoutdigit = ASCIIdigits[outdigit];

        *(buf + numdigits-1-i) = ASCIIoutdigit;
        number /= base;
    }

    *(buf + numdigits) = 0;
}
