#!/bin/bash
mkdir -p '../build'
pushd ../build/
g++ '../src/x11.cpp' -lX11 -I"../headers" -Wno-write-strings -lGL -lm -g -fmax-errors=10 -DRENDER_OPENGL=1
popd
