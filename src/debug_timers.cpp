#if INTERNAL

struct TimingRecord {
    u64 line;
    char *file;
    char *func;

    u64 elapsed;
    u32 hitcount;
};

extern const u32 num_records;
extern TimingRecord timing_records[];

//NOTE:: TIMED_BLOCKS are our only use of __COUNTER__


/*
  1.8 ghz
  
  1.8 * 1000 * 1000 * 1000

  1,800,000,000 per second
  
  30 000 000 for 60 fps
  60 000 000 for 30 fps
*/

struct TimedBlock
{
    u32 counter;
    u64 start_cycles;
    char *func_name;

    TimedBlock(char *file_name, char *function_name, u64 linenum, u32 counter)
    {
        this->counter = counter;

        timing_records[this->counter].line = linenum;
        timing_records[this->counter].file = file_name;
        timing_records[this->counter].func = function_name;
        timing_records[this->counter].hitcount++;
        this->start_cycles = __rdtsc();
    }

    ~TimedBlock()
    {
        u64 end_cycles = __rdtsc();
        u64 elapsed = end_cycles - this->start_cycles;
        timing_records[this->counter].elapsed += elapsed;
    }
};


#define TIMED_BLOCK TimedBlock timed_block##__LINE____FILE__(__FILE__, __FUNCTION__, __LINE__, __COUNTER__)
#else
#define TIMED_BLOCK
#endif
