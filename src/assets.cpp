internal b32
open_assets_file(Assets *assets)
{
    TIMED_BLOCK;

    // TODO allow for asset files with different names
    // First, try to find assets in the data folder
    FileError ferror = FileError_success;
    ferror = platform.open_file(FileID_GameAssetsFile, &assets->file);

    if(ferror)
    {
        // Unable to open assets file
		return false;
    }
	
	u64 file_size = platform.get_file_size(assets->file);
	u8 footer_sig[4] = {};
	platform.read_file(assets->file, footer_sig, file_size - 4, 4, true); 
	
    // TODO: Make this _not_ crash the game
    assert(footer_sig[0] == 'A' &&
           footer_sig[1] == 'P' &&
           footer_sig[2] == 'A' &&
           footer_sig[3] == 'F');

    u8 buf[4] = {};
    platform.read_file(assets->file, buf, file_size - 8, 4, true);
	u32 asset_file_size = (buf[0] << 0 | buf[1]<<8 | buf[2]<<16 | buf[3]<<24);
	
	assets->file_offset = file_size - asset_file_size;
	
    return true;
}

inline b32
check_magic(APAAssetFileHeader *hdr)
{
    if(hdr->magic[0] == 'A' &&
       hdr->magic[1] == 'K' &&
       hdr->magic[2] == 'P' &&
       hdr->magic[3] == 'A')
    {
        return true;
    }
    return false;
    
}

internal void
load_asset_file(Assets *assets)
{
    TIMED_BLOCK;
    if(open_assets_file(assets))
    {
        //Read the file header
        APAAssetFileHeader header;
        platform.read_file(assets->file, (u8 *)&header, assets->file_offset, sizeof(APAAssetFileHeader), true);
        if(check_magic(&header))
        {
            //NOTE:: we can do version checking here too
            assets->num_assets = header.num_assets;
            assets->headers = arena_pushcount(&assets->memory_arena, APAAssetHeader, header.num_assets);
            platform.read_file(assets->file, (u8 *)assets->headers, assets->file_offset + sizeof(APAAssetFileHeader),
                               sizeof(APAAssetHeader) * header.num_assets, true);

            u32 name_chunk_size;
            u32 name_chunk_offset = sizeof(APAAssetFileHeader) + sizeof(APAAssetHeader) * header.num_assets;
            if(header.num_assets == 0)
                name_chunk_size = 0;
            else
            {
                //data chunk - location of last header
                name_chunk_size = assets->headers[0].data_offset_in_bytes - name_chunk_offset; 
            }

            assets->name_chunk = (char *) arena_pushsize(&assets->memory_arena, name_chunk_size);
            platform.read_file(assets->file, (u8 *)assets->name_chunk, assets->file_offset + name_chunk_offset, name_chunk_size, true);
        }
        else
        {
            // TODO Somehow communicate that the file is corrupt?
        }
    }

#define LARGEST_ASSET_TYPE_SIZE 30
    assets->null_memory = arena_pushsize(&assets->memory_arena, LARGEST_ASSET_TYPE_SIZE);
    assets->empty_font_memory = arena_pushtype(&assets->memory_arena, Font);
    assets->empty_font_memory->tex1c = (Texture1c *)assets->null_memory;
    assets->empty_font_memory->descriptor = (FontDescriptor *)assets->null_memory;
}

inline Asset *
next_asset(Assets *assets, u32 *index)
{
    TIMED_BLOCK;
    index[0] += 1;
    index[0] %= MAX_APA_NAME_LENGTH;
    return &assets->assets[*index];
}

//0 1 2 3 4 5 6 7 8 9 10 11 12
internal b32
load_asset(Assets *assets, Asset *asset, AssetType type, char *requested_name)
{
    TIMED_BLOCK;
    for(int i = 0; i < assets->num_assets; i++)
    {
        char *name = (char *)assets->name_chunk + assets->headers[i].name_offset_in_bytes;
        if(string_equal(requested_name, name) && type == assets->headers[i].asset_type)
        {
            string_copy(asset->name, requested_name);
            
            //we found the asset..start loading this asset but don't block
            u32 additional_bytes = additional_required_bytes[assets->headers[i].asset_type];
            asset->data_start = arena_pushsize(&assets->memory_arena,
                                               assets->headers[i].size_of_data_in_bytes +
                                               additional_bytes);
            asset->loaded_raw_data = (u8 *) asset->data_start + additional_bytes;
            platform.read_file(assets->file, (u8 *)asset->loaded_raw_data,
                               assets->file_offset + assets->headers[i].data_offset_in_bytes,
                               assets->headers[i].size_of_data_in_bytes, true);


            //TODO what the heck is a non discriminated union
            switch(assets->headers[i].asset_type)
            {
                case AssetType::font:
                {
                    asset->font->tex1c = (Texture1c *)((u8*)asset->loaded_raw_data - sizeof(u32));
                    u32 width = ((asset->font->tex1c->width+3)/4)*4;
                    u32 height = asset->font->tex1c->height;
                    u32 font_texture_size = TEX1C_HEADER_SIZE + width * height;
                    
                    asset->font->descriptor = (FontDescriptor *)((u8 *)asset->loaded_raw_data + font_texture_size);
                }break;
                case AssetType::tex1c:
                {
                    
                }break;
                case AssetType::sound:
                {
                    
                }break;
                default:
                    break;
            }
            return true;
        }
    }

    //we have not found the asset
    //so let's just clear the data_start chunk to 0
    //so if anyone tries to access anything they get a certain 0
    switch(type)
    {
        case AssetType::font:
        {
            asset->font = assets->empty_font_memory;
        }break;
        default:
        {
            asset->data_start = assets->null_memory;
        }break;
    }

    
    //TODO::ensure that this memory is cleared
    string_copy(asset->name, requested_name);
            
    return false;
}

//TODO IS there any way we don't have to pass in the type
//     rn we do that because otherwise we can't provide a null font asset etc... properly
internal Asset *
get_asset_by_name(Assets *assets, char *requested_name, AssetType type)
{

    TIMED_BLOCK;
    // u32 l = 0;
    // u32 r = assets->num_assets - 1;
    // u32 mid = (l+r)/2;
    //NOTE:: sort the assets so we can binary search for them!!!
    //NOTE:: the assets stuff must already be loaed
    Asset ret = {};

    //first use this hash function to search in memory;
    char *at = requested_name;
    u32 hash_key = 0;
    u32 pos = 0;
    while(at[0])
    {
        pos++;
        hash_key += at[0] * pos;
        at++;
    }

    u32 index = hash_key % MAX_IN_MEMORY_ASSETS;
    Asset *potential_asset = &assets->assets[index];
    //we found it!!! BUT this may not even be the right asset
    //two assets may have wound up with the same key!

    //ONE way store the name in the asset that way we will know its really use
    //another way is to check if the down one is empty
    //if it is RESOLVE collision
    while(!string_equal(potential_asset->name, requested_name))
    {
        //this is really what we want
        if(potential_asset->data_start == 0)
        {
            if(load_asset(assets, potential_asset, type, requested_name))
            {
                //asset not found
            }
            else
            {
                //asset found!
            }
            return potential_asset;
        }

        potential_asset = next_asset(assets, &index);
    }
    return potential_asset;
}
