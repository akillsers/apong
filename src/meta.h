#define LIST_FILES 1

#define MAX_REQUIRED_ENUMS 1500
#define MAX_REQUIRED_STRUCTS 1500
#define MAX_ENUMS 1000
#define MAX_ENUM_NAMES 1000
#define MAX_STRUCTS 1500
#define MAX_MEMBERS 500
#define MAX_VALUE_TYPES 1000

char *search_filenames[] = {"*.cpp", "*.h"};
char *forbidden_filenames[] = {"introspect_gen.cpp", "introspect_gen - Copy.cpp", "meta - Copy.h"};
char *extra_required_structs[] = {"Circle"};
char *extra_atomic_types[] = {"Rect", "Vec2", "Vec3", "Circle", "AABB"};
u32 extra_atomic_type_sizes[] =
{
    16,
    8,
    12,
    12,
    16
};

struct Tokenizer
{
    char *at;
};

enum TokenKind
{
    Token_unknown,

    Token_openparen,
    Token_closeparen,
    Token_openbraces,
    Token_closebraces,
    Token_semicolon,
    Token_identifier,
    Token_asterisk,
    Token_openbrackets,
    Token_closebrackets,
    Token_quoted_text,
    Token_tilde,
    Token_comma,
    Token_equals,
    
    Token_eof,

    Token_count
};

struct Token
{
    TokenKind type;
    char *text;
    u32 textlen;
};


struct FileInfo
{
    char filenames[3000];
    u32 file_count;

    u32 filename_offsets[900];
    u32 filename_len[900];
    
    u64 file_size[900];
};
struct StructMember
{
    char *name;
    u32 namelen;
    b32 isptr;

    char *type_name;
    u32 type_namelen;
};


struct Struct
{
    u32 fromfileindex;

    char *name;
    u32 namelen;

    StructMember members[MAX_MEMBERS];
    u32 member_count;
};

struct Enum
{
    char *name;
    u32 namelen;

    char *enum_names[MAX_ENUM_NAMES];
    u32 enum_name_lengths[MAX_ENUM_NAMES];
    u32 enum_name_count;
};

enum BaseType
{
    BT_atomic,
    BT_vargroup,
    BT_enum,
    
    BT_count
};

struct ValueType
{
    char *name;
    u32 namelen;

    BaseType kind;
};

struct ParseData
{
    u32 current_file_index;

    Struct *structs;
    u32 current_struct;
    
    Enum *enums;
    u32 current_enum;
    
    ValueType *value_types;
    u32 current_value_type;

    char **required_struct_names;
    u32 *required_struct_namelen;
    u32 current_required_struct;

    char **required_enum_names;
    u32 *required_enum_namelen;
    u32 current_required_enum;
};
