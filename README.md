# apong
An open-source clone of the game Pong for Windows. (Linux version is coming soon!)

## Download
Get the latest version of apong on [itch.io](https://akillsers.itch.io/apong).

## Building

Prerequisite: C++ Build Tools for Visual Studio 2019.

1. Clone this git repository.
1. Open the Developer Command Prompt for VS 2019.
1. Change into the src directory.
1. Run build.bat from the Developer Command Prompt for VS 2019. Use argument release to generate a release build. Your binary will be in the build directory.

## Debugging

Debugging features are only available with a debug build of the game.

To access these features, use the following keyboard shortcuts:

* **F7** - Toggle debug overlay
* **F8** - Toggle profiler in debug overlay
* **F9** - Toggle debug shapes
* **F10** - Toggle vsync test

In the debug overlay, the following additional hotkeys are available.

* **Ctrl + P** - Pause/unpause game simulation
* **Left Arrow** - Previous snapshot
* **Right Arrow** - Next snapshot
* **Ctrl + End** - Jump to current frame
* **Ctrl + S** - Toggle slow/fast debug overlay frequency

A screenshot of the debug overlay is shown below.

![Screenshot of debug overlay.](debug.png)

## Asset tools used
The graphics of apong was created in [paint.NET](https://www.getpaint.net/). The sound effects was created in [rFXGen](https://raylibtech.itch.io/rfxgen).

## License
apong is released under the BSD 2-Clause "Simplified" License. See LICENSE for details.
