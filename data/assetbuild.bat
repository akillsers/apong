@echo off
del apong_assets.apa
set apa="../apa/build/apa.exe"

%apa% apong_assets.apa /create
%apa% apong_assets.apa /add font fonts/hack.bmp fonts/hack.aft hack
%apa% apong_assets.apa /add sound soundfx/wallhigher.wav ding
%apa% apong_assets.apa /add texture1c images/ball.bmp ball
%apa% apong_assets.apa /add texture1c images/normal.bmp normal
%apa% apong_assets.apa /add texture1c images/nightmare.bmp nightmare
%apa% apong_assets.apa /add texture1c images/sword.bmp sword
%apa% apong_assets.apa /add texture1c images/lock.bmp lock
%apa% apong_assets.apa /add texture1c images/ui_arrow.bmp focus_arw
%apa% apong_assets.apa /add sound soundfx/endmatchcomposed2.wav boop
%apa% apong_assets.apa /add sound soundfx/seeyerealler.wav zap
%apa% apong_assets.apa /add sound soundfx/ui_move.wav new_game
%apa% apong_assets.apa /add sound soundfx/denied.wav denied
%apa% apong_assets.apa /add sound soundfx/wallrealler.wav wall
