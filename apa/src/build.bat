@echo off
IF NOT EXIST ..\build mkdir "..\build"
pushd "..\build"

set CompilerFlags=-nologo -Od -EHa- -Zi -DSLOW=1 -DINTERNAL=1 -fp:fast -fp:except- -GR- -Gm- -GS- 
set LinkerFlags=-incremental:no

cl %CompilerFlags% -Fe:apa.exe "..\src\asset_packer.cpp" -I"..\headers" /link %LinkerFlags%  
popd "..\build"
