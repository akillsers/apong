#define MAX_SUPPORTED_VERSION 0
#define MAX_NAME_LENGTH 10
#define pull(type)                             \
    *(type *)at;                               \
    at += sizeof(type);                        
#define counted_advance(type, count) (at += sizeof(type) * count;)
#define push(type, value) {                    \
        *((type *)to) = value;               \
        to+=sizeof(type);                      \
    }

#define TEX1C_HEADER_SIZE 8
#pragma pack(push, 1)
struct AFTHdr
{
    u32 first_uni_codepoint;
    u32 num_chars;
    u32 font_size;
    f32 yadvance;
    f32 ascent;
};

struct FontGlyph
{
    u16 w;
    u16 h;
    f32 xoff;
    f32 yoff;
    f32 xadvance;
    f32 xoff2;
    f32 yoff2;
    f32 u, v, s, t;
};

struct RIFFHeader
{
    char ChunkID[4];
    u32 ChunkSize;                             
    char Format[4];
};
struct Subchunk1
{
    u32 Subchunk1ID;      //offset 12
    u32 Subchunk1Size; 
    u16 AudioFormat;
    u16 NumChannels;
    u32 SampleRate; 
    u32 ByteRate;     
    u16 BlockAlign;
    u16 BitsPerSample;
};
struct Subchunk2
{
    u32 Subchunk2ID;      //ofdfset 36 or 38
    u32 Subchunk2Size;
    u8  Data;
    
};


struct AssetFileHeader
{
    u8 magic[4];
    u16 version;
    u32 num_assets;
};

struct AssetHeader
{
    u16 asset_type;
    u32 data_offset_in_bytes;
    u32 size_of_data_in_bytes;
    u32 name_offset_in_bytes;
};
#pragma pack(pop)

struct APAfile
{
    u8 *file_contents;
    u32 filesize;
};

struct APALocs
{
    u8 *data_chunk;
    u8 *name_chunk;
};

enum CLOperation
{
    CLOperation_noswitch,
    CLOperation_create,
    CLOperation_list,
    CLOperation_remove,
    CLOperation_add,

    CLOperation_count
};

enum AssetType
{
    unknown,
    texture1c,
    texture3c,
    sound,
    font,
    invalid,

    AssetType_count
};

struct CommandLineContext
{
    CLOperation cloperation;
    char *file_name;

    char *item_filename;
    char *second_item_filename;
    char *item_name;
    AssetType type;
};
