//@header
struct Text
{
    char *from;
    u32 len;
};
    
//@implementation
internal u32
string_length(char *a)
{
    u32 index = 0;
    while(*a++)
        index++;
    return index;
}

internal void
to_upperl(char *dest, int destsize, char* source, int sourcesize)
{
    int atIndex = 0;
    while(atIndex < sourcesize)
    {
        assert(atIndex < destsize);
        if((source[atIndex] >= 'a' && source[atIndex] <= 'z'))
        {
            dest[atIndex] = source[atIndex] - 0x20;
        }
        else
            dest[atIndex] = source[atIndex];
        atIndex++;
    }
}

internal void
to_lowerl(char *dest, int destsize, char* source, int sourcesize)
{
    int atIndex = 0;
    while(atIndex < sourcesize)
    {
        assert(atIndex < destsize);
        if((source[atIndex] >= 'A' && source[atIndex] <= 'Z'))
        {
            dest[atIndex] = source[atIndex] + 0x20;
        }
        else
            dest[atIndex] = source[atIndex];
        atIndex++;
    }
}

internal char*
string_copyl(char *dest, char* src, int length)
{
    while(length > 0)
    {
        *dest++ = *src++;
        length--;
    }
    return dest;
}

//this is legacy
internal char*
string_copy(char *dest, char* src)
{
    while(*dest++ = *src++);

    return dest;
}


internal char*
string_cat(char *dest, char* src)
{
    while(*dest++ = *src++);

    return --dest;
}

internal char*
string_catl(char *dest, char* src, int length)
{
    while(length > 0)
    {
        *dest++ = *src++;
        length--;
    }
    return dest;
}

internal u32
numlen(u32 number)
{
    u32 base = 10;

    u32 numdigits = 0;
    do
    {
        number /= base;
        numdigits++;
    }while(number > 0);

    return numdigits;
}

internal char*
U32ToASCII(char *buf, u32 number)
{
    const u32 base = 10;
    char ASCIIdigits[] = "0123456789";

    u32 numdigits = 0;
    u32 workingnumber = number;

    do
    {
        workingnumber /= base;
        numdigits++;
    }while(workingnumber > 0);


    // assert(buffersize >= numdigits+1); //required buffer size INCLUDING null terminator

    workingnumber = number;
    for(int i = 0; i < numdigits; i++)
    {
        u8 outdigit = number % base;
        char ASCIIoutdigit = ASCIIdigits[outdigit];

        *(buf + numdigits-1-i) = ASCIIoutdigit;
        number /= base;
    }

    *(buf + numdigits) = 0;
    return buf+numdigits;
}

inline bool
is_upper(char character)
{
    if(character >= 'A' && character <= 'Z')
        return true;
    return false;
}

internal b32
string_equal(char *a, char *b)
{
    while(*a++==*b++ && *a && *b);
    return(*a == 0 && *b == 0);
}

//NOTE:: this version says that the length of both a and b is length
//       (they are both NOT null terminated)
internal b32
string_equall(char *a, char *b, u32 length)
{
    u32 index = 0;
    while(index < length)
    {
        if(!(a[index] == b[index]))
            break;
        index++;
    }

    return(index == length);
}
//NOTE:: this version says that the length of  a and b is lengtha and lengthb respectively
//       (they are both NOT null terminated)
internal b32
string_equall(char *a, u32 lengtha, char *b, u32 lengthb)
{
    if(lengtha != lengthb) return false;
    u32 index = 0;
    while(index < lengtha)
    {
        if(!(a[index] == b[index]))
            break;
        index++;
    }

    return(index == lengtha);
}
//NOTE:: this version expects b to be a nullterminated string
internal b32
string_equall(char *a, char *b)
{
    u32 index = 0;
    while(b[index])
    {
        if(!(a[index] == b[index]))
            break;
        index++;
    }

    return(!b[index]); //b is at her null terminator
}

//NOTE:: this version says that the length of a is length
//       and b is a null terminated string
internal b32
string_equall(char *a, u32 length, char *b)
{
    u32 index = 0;
    while(index < length)
    {
        if(!(a[index] == b[index]))
            break;
        index++;
    }

    return(index == length && b[index] == 0);//NOTE b is at her null-terminator
}

internal b32
string_inarray(char *str, u32 strlength, char **stringarray, u32 *arrayoflengths, u32 stringarraylength,
               u32 *out_index)
{
    //Linear search for string in the array
    for(int i = 0; i < stringarraylength; i++)
    {
        if(string_equall(str, strlength, stringarray[i], arrayoflengths[i]))
        {
            out_index[0] = i;
            return true;            
        }

    }
    return false;
}

internal b32
string_inarray(char *str, u32 strlength, char **stringarray, u32 stringarraylength, u32 *out_index)
{
    //Linear search for string in the array
    for(int i = 0; i < stringarraylength; i++)
    {
        if(string_equall(str, strlength, stringarray[i]))
        {
            out_index[0] = i;
            return true;
        }
    }
    return false;
}


inline bool
is_whitespace(char character)
{
    if(character == ' ' ||
       character == '\t' ||
       character == '\r' ||
        character == '\n')
        return true;
    return false;
}

inline bool
is_endofline(char character)
{
    if(character == '\r' ||
       character == '\n')
        return true;
    return false;
}

inline bool
is_alpha(char character)
{
    if((character >= 'a' && character <= 'z' ) ||
       (character >= 'A' && character <= 'Z'))
        return true;
    return false;
}

inline bool
is_numeric(char character)
{
    if(character >= '0' && character <= '9')
        return true;
    return false;
}

inline bool
is_octal(char character)
{
    if(character >= '0' && character <= '7')
        return true;
    return false;
}

inline bool
is_hexdigit(char character)
{
    if(is_numeric(character) || (character >= 'a' && character <= 'f') || character >= 'A' && character <= 'F')
        return true;
    return false;
}


//NOTE there is no error checking in this function the string could be not a valid numver
internal u32
string_tonum(char *str)
{
    u32 index = 0;
    u32 outnum = 0;
    while(str[index])
    {
        outnum = outnum * 10 + str[index] - '0';
        index++;
    }
    return outnum;
}
