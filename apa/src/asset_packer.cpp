//we now have two copies of these
//how about we make these guys shared!!!
#include "aks.h" 
#include "stringutils.cpp"
#include "asset_packer.h"
#include <io.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/stat.h>
#include <memory.h>

struct MemoryArena
{
    u8 *base;
    u64 used;
    u64 size;
};

#define arena_pushtype(memory_arena, type) (type *)arena_pushsize(memory_arena, sizeof(type)) 
#define arena_pushcount(memory_arena, type, count) (type *)arena_pushsize(memory_arena, sizeof(type) * count) 

internal void *
arena_pushsize(MemoryArena *arena, u64 size)
{
    if(arena->used + size > arena->size)
        return 0;
    
    u8 *ret = arena->base + arena->used;
    arena->used += size;
    return ret;
}

internal void
arena_reset(MemoryArena *arena)
{
    arena->used = 0;
    return;
}

internal MemoryArena
arena_suballocate(MemoryArena *srcarena, u64 size)
{
    MemoryArena ret;
    ret.base = (u8 *)arena_pushsize(srcarena, size);
    ret.size = size;
    ret.used = 0;
    return ret;
}

MemoryArena arena;

internal void
parse_cl(int argc, char **argv, CommandLineContext *context)
{
    for(int i = 1; i < argc; i++)
    {
        char *at = argv[i];
        if(at[0] == '/')
        {
            //parsing a swithc
            at++;
            if(string_equal(at, "create"))
            {
                context->cloperation = CLOperation_create;
            }
            else if (string_equal(at, "list"))
            {
                context->cloperation = CLOperation_list;
            }
            else if (string_equal(at, "remove"))
            {
                context->cloperation = CLOperation_remove;
            }
            else if (string_equal(at, "add"))
            {
                context->cloperation = CLOperation_add;
            }
        }
        else 
        {
            //parsing an argument
            if(context->cloperation == CLOperation_noswitch)
            {
                if(context->file_name == 0)
                {
                    context->file_name = at;
                }
                else
                {
                    fprintf(stderr, "ignored argument %s\n", at);
                }
            }
            else if (context->cloperation == CLOperation_create)
            {
                fprintf(stderr, "ignored argument %s\n", at);
            }
            else if (context->cloperation == CLOperation_list)
            {
                fprintf(stderr, "ignored argument %s\n", at);
            }
            else if (context->cloperation == CLOperation_add)
            {

                if(context->type == 0)
                {
                    if(string_equal(at, "texture1c"))
                        context->type = texture1c;
                    else if (string_equal(at, "texture3c"))
                        context->type = texture3c;
                    else if (string_equal(at, "sound"))
                        context->type = sound;
                    else if (string_equal(at, "font"))
                        context->type = font;
                    else if (string_equal(at, "help"))
                        printf("%s (.apa filename) /add type item_filename (optional item_name)\n", argv[0]);
                    else
                    {
                        context->type = invalid;
                        fprintf(stderr, "unknown type of asset to create\n");
                    }
                }
                else if (context->item_filename == 0)
                {
                    context->item_filename = at;
                }
                else if (context->second_item_filename == 0)
                {
                    context->second_item_filename = at;
                }
                else if (context->item_name == 0)
                {
                    context->item_name = at;
                }
                else
                {
                    fprintf(stderr, "ignored argument %s\n", at);                    
                }
            }
            else if (context->cloperation == CLOperation_remove)
            {
                if(context->item_name == 0)
                {
                    context->item_name = at;
                }
                else
                {
                    fprintf(stderr, "ignored argument %s\n", at);
                }
            }
        }
    }

    if(!context->item_name && context->second_item_filename)
    {
        context->item_name = context->second_item_filename;
        context->second_item_filename = 0;
    }
}

internal void
CreateAPA(char *filename)
{
    int file_handle = _open(filename, _O_CREAT | _O_EXCL | _O_BINARY | _O_WRONLY, _S_IWRITE);  
    if(file_handle < 0)
    {
        if(errno == EEXIST)
        {
            fprintf(stderr, "The file already exists!! (Erase the contents of this guy???) y for yes, any other key for no.\n", filename);
            char answer = getchar();
            if(answer == 'y')
            {
                file_handle = _open(filename, _O_CREAT | _O_BINARY | _O_WRONLY, _S_IWRITE);
                if(file_handle < 0)
                    goto open_err;
                else
                    goto continue_execution;
            }

        }
        else
        {
        open_err:
            fprintf(stderr, "Could not open %s for writing.\n", filename);
        }
        return;      
    }
continue_execution:
    void *file_contents_buf = arena_pushsize(&arena, megabytes(1));
    char *to = (char *)file_contents_buf;
    AssetFileHeader hdr;
    hdr.magic[0] = 'A';
    hdr.magic[1] = 'K';
    hdr.magic[2] = 'P';
    hdr.magic[3] = 'A';
    hdr.version = 0;
    hdr.num_assets = 0;

    push(AssetFileHeader, hdr);

    //TODO how do i terminate this file!!!
    // to[0] = '^Z';
    // to++;
    u32 file_size = to - file_contents_buf;
    _chsize(file_handle, file_size);
    if(_write(file_handle, file_contents_buf, file_size) < 0)
    {
        fprintf(stderr, "Could not write the file %s", filename);
        goto close_and_done;
    }
close_and_done:
    _close(file_handle);
}

inline b32
CheckMagic(AssetFileHeader *hdr)
{
    if(hdr->magic[0] == 'A' &&
       hdr->magic[1] == 'K' &&
       hdr->magic[2] == 'P' &&
       hdr->magic[3] == 'A')
    {
        return true;
    }
    return false;
}

internal b32
LoadExistingFileAndPrintErrors(int *outfd, char *filename, b32 writable)
{
    int oflags = _O_BINARY;
    int pmode = _S_IREAD;
    if(writable)
    {
        pmode |= _S_IWRITE;
        oflags |= _O_RDWR;
    }
    else
    {
        oflags |= _O_RDONLY;
    }
        
    int file_handle = _open(filename, oflags, pmode);
    if(file_handle < 0)
    {
        if(errno == ENOENT)
        {
            fprintf(stderr, "%s is non existent!!.\n", filename);
        }
        else
        {
            fprintf(stderr, "Could not open %s for reading!\n", filename);
        }
        return false;
    }
    outfd[0] = file_handle;
    return true;
}

//NOTE:: for now this cannot read a file larger than 1 megabyte
//TODO remove
internal u32
ReadFileIntoMemory(int fd, void **buffer_out)
{
    u64 file_contents_size = _filelengthi64(fd);
    buffer_out[0] = arena_pushsize(&arena, file_contents_size);
    int bytes_read = _read(fd, buffer_out[0], file_contents_size);
    if(bytes_read < 0)
    {
        return false;
    }
    return bytes_read;
}

internal u32
ReadFileIntoBuffer(int fd, void *buffer_out, u64 buffer_size)
{
    //u32 file_contents_size = megabytes(1);
    //buffer_out = arena_pushsize(&arena, file_contents_size);
    int bytes_read = _read(fd, buffer_out, buffer_size);
    if(bytes_read < 0)
    {
        int errno;
        if(errno == EBADF)
            int y =0;
        else if (errno == EINVAL)
            int k = 0;
        return false;
    }
    return bytes_read;
}

//TODO how is this better than allocating an array in the data block??
internal char *
AssetTypeToName(AssetType type)
{
    switch(type)
    {
        case texture1c: return "texture1c";
        case texture3c: return "texture3c";
        case sound: return "sound";
        case font: return "font";
        case invalid: return "invalid";
        default: return "unknown";
    }
}

//TODO difference between using a u8 vs void *
internal void
PrintAdditionalInfoForType(char *buf, u32 bufsize, AssetType type, u8 *data)
{
    u8 *at = data;
    switch(type)
    {
        case texture1c:
        {
            u32 width = pull(u32)
            u32 height = pull(u32);
            sprintf_s(buf, bufsize, "Width: %u Height:%u", width, height);
        }break;
        case sound:
        {
            u8 num_channels = pull(u16);
            u32 total_audio_frames = pull(u32);
            sprintf_s(buf, bufsize, "Num channels: %hhu Total frames:%u", num_channels, total_audio_frames);
        }break;
        case font:
        {
            u32 width = pull(u32);
            u32 height = pull(u32);

            at = data + TEX1C_HEADER_SIZE + ((width+3)/4)*4 * height;
            u32 first_unicode_codepoint = pull(u32);
            u32 num_glyphs = pull(u32);
            u32 fontsize = pull(u32);
            f32 yadvance = pull(f32);
            f32 ascent = pull(f32);
            sprintf_s(buf, bufsize, "Rasterized bmp w: %u h: %u. first codepoint %u, num_glyphs: %u, fontsize: %u, yadvance: %0.3f, ascent: %0.3f", width, height, first_unicode_codepoint, num_glyphs, fontsize, yadvance, ascent);
            
        }break;
        default:
            sprintf_s(buf, bufsize, "No additional data");
    }
}

internal void
ListAPA(char *filename)
{
    int file_handle = 0;
    if(!LoadExistingFileAndPrintErrors(&file_handle, filename, false))
        return;

    void *file_contents_buf = 0;
    if(!ReadFileIntoMemory(file_handle, &file_contents_buf))
    {
        fprintf(stderr, "Could not read the file %s", filename);
        goto close_and_done;
    }

    u8 *at = (u8 *)file_contents_buf;

    AssetFileHeader *hdr;
    hdr = (AssetFileHeader *)at;
    at += sizeof(AssetFileHeader);
    u8 *first_asset_header = at;
    if(!CheckMagic(hdr))
    {
        fprintf(stderr, "Invalid or corrupt .apa file!\n");
        goto close_and_done;
    }
    else if(!(hdr->version <= MAX_SUPPORTED_VERSION))
    {
        fprintf(stderr, "Unsupported .apa file version: %hu!\n", hdr->version);
        goto close_and_done;
    }
    u64 apa_filesize =  _filelengthi64(file_handle);

    printf("File size: %I64u\n", apa_filesize);
    printf("Asset file version: %hu\n", hdr->version);
    printf("Number of assets in file: %u\n", hdr->num_assets);


    u8 *name_chunk = first_asset_header + sizeof(AssetHeader)*hdr->num_assets;
    for(int i = 0; i < hdr->num_assets; i++)
    {
        AssetHeader *ahdr = (AssetHeader *)at;
        at += sizeof(AssetHeader);

        char *asset_type_name = AssetTypeToName((AssetType)ahdr->asset_type);
        char *asset_name = (char *)name_chunk + ahdr->name_offset_in_bytes;
        char additional_info[512];
        PrintAdditionalInfoForType(additional_info, arraycount(additional_info), (AssetType)ahdr->asset_type,
                                   (u8 *)file_contents_buf + ahdr->data_offset_in_bytes);
        printf("%s: (type:%s) (size:%u) %s\n", asset_name, asset_type_name, ahdr->size_of_data_in_bytes,
               additional_info);
    }

close_and_done:
    _close(file_handle);
}

internal char *
GetFileExtension(char *file)
{
    //extension starts after the last dot
    char *at = file;
    char *ext = 0;
    while(at[0])
    {
        if(at[0] == '.' && at[1])
        {
            ext = &at[1];
        }
        at++;
    }
    return ext;
}

internal u8 *
StartOfData(u8 *name_chunk, u8 *asset_header_location, u32 numassets)
{
    AssetHeader *asset_headers = (AssetHeader *)asset_header_location;
    AssetHeader *last_header;
    if(numassets == 0)
    {
        return asset_header_location;
    }
    else
    {
        u32 header_index = numassets-1;
        last_header = &asset_headers[header_index];

        u8 *start_of_data = name_chunk + last_header->name_offset_in_bytes;
        while(start_of_data[0])
        {
            start_of_data++;
        }
        start_of_data++;
        return start_of_data;
    }
}

internal void
InsertNewAsset(APAfile *apa, char *item_name, AssetType type,
               APALocs *old_locs, u8 **new_data, AssetHeader **newahdr)
{
    //NOTE:: let's insert this data entry at the start of the data chunk
    //       for this: we need to copy away the old data and then append that to what
    //       what we add here
    //       the start of the data chunk will have to be changed since the name chunk and asset header
    //       chunks are growing by 1
    
    //for space for another asset header we must move the name chunk and what follows (the data chunk)
    //away
    //TODO make sure this buffer is big enough
    u8 *old_name_chunk_buf = (u8 *) arena_pushsize(&arena, megabytes(1));
    u32 old_name_chunk_size = old_locs->data_chunk - old_locs->name_chunk;
    memcpy(old_name_chunk_buf, old_locs->name_chunk, old_name_chunk_size);

    //TODO what is the EOF marker for files??
    //cuz then we can just copy until EOF?? (we can make our own EOF)
    u8 *old_data_chunk_buf = (u8 *)arena_pushsize(&arena, megabytes(1));
    u32 old_data_chunk_size = apa->filesize - (old_locs->data_chunk - apa->file_contents);
    memcpy(old_data_chunk_buf, old_locs->data_chunk, old_data_chunk_size);

    u8 *new_asset_hdr_loc = old_locs->name_chunk;
    u8 *new_name_chunk_loc = old_locs->name_chunk + sizeof(AssetHeader);
    u8 *new_name_loc = old_locs->data_chunk + sizeof(AssetHeader);
    memcpy(new_name_chunk_loc, old_name_chunk_buf, old_name_chunk_size);

    u8 *end_of_new_name = (u8 *) string_copy(((char *)new_name_loc), item_name);
    
    memcpy(end_of_new_name, old_data_chunk_buf, old_data_chunk_size);
    u8 *new_data_loc = end_of_new_name + old_data_chunk_size;

    AssetHeader *newhdr = (AssetHeader *)new_asset_hdr_loc;
    newhdr->asset_type = type;
    newhdr->data_offset_in_bytes = new_data_loc - apa->file_contents;
    newhdr->name_offset_in_bytes = new_name_loc - new_name_chunk_loc;

    u32 num_assets = ((AssetFileHeader *)apa->file_contents)[0].num_assets;
    for(int i = 0; i < num_assets; i++)
    {
        newhdr[-1 - i].data_offset_in_bytes += sizeof(AssetHeader) + (end_of_new_name - new_name_loc);
    }
    //we don't need to update the name offsets for any of the previous assets
    //but we need to update all the previous data offsets since
    //the name buffer, between the start of the file and the data chunk, has grouwn
    
    newahdr[0] = newhdr;    
    new_data[0] = new_data_loc;
}

internal b32
LoadFont_aft(u8 *item_contents_buf, u8 *new_data_loc, u32 *out_bytes_processed)
{
    u8 *at = item_contents_buf;
    u8 *to = new_data_loc;

    u8 check_chars[4];
    check_chars[0] = pull(u8);
    check_chars[1] = pull(u8);
    check_chars[2] = pull(u8);
    check_chars[3] = pull(u8);

    if(!(check_chars[0] == 'A' && check_chars[1] == 'F' &&
         check_chars[2] == 'N' && check_chars[3] == 'T'))
    {
        fprintf(stderr, "This seems to be a corrupt aft file (beginning check characters are incorrect)!\n");
        return false;
    }

    
    // *(AFTHdr *)to = *(AFTHdr *)at;
    // to += sizeof(AFTHdr);
    // at += sizeof(AFTHdr);

    push(AFTHdr, pull(AFTHdr))
    u32 num_chars = ((AFTHdr *)new_data_loc)->num_chars;
    for(int i = 0; i < num_chars; i++)
    {
        push(FontGlyph, pull(FontGlyph));
    }

    out_bytes_processed[0] += to - new_data_loc;
    return true;
}


internal b32
LoadTex1c_bmp(u8 *item_contents_buf, u8 *new_data_loc, u32 *out_bytes_processed)
{
    u8 *at = item_contents_buf;
    u8 *to = new_data_loc;
    u8 check_chars[2];
    check_chars[0] = pull(u8);
    check_chars[1] = pull(u8);

    //TODO check the cost of using this not operator!
    //     is it better to omit the not operator and use nesting
    //     also is it good to use something like <
    //     RESEARCH grab the intels arcitecture guide thingy
    if(!(check_chars[0] == 'B' && check_chars[1] == 'M'))
    {
        fprintf(stderr, "This seems to be a corrupt bitmap file (beginning check characters are incorrect)!\n");
        return false;
    }

    pull(i32); //size of file in bytes
    pull(u16); //reserved and MUST be 0
    pull(u16); //reserved and MUST be 0
    u32 bmp_data_offset = pull(u32); //offset to start of pixeldata

    pull(u32); //header size MUST be at least 40
    u32 bmp_width = pull(u32);
    u32 bmp_height = pull(u32);
    pull(u16); //image planes MUST be 1
    u16 bitsPerPix = pull(u16);

    if(bitsPerPix != 24)
    {
        fprintf(stderr, "This bmp has a bit depth of %u, currently only 24 bit depth bmp files are supported\n",
                bitsPerPix);
        return false;
    }

    push(u32, bmp_width);
    push(u32, bmp_height);
                
    //NOTE:: this is always true since we only support 24 bit depth
    u32 bmp_bytes_per_pixel = 3; 
    u32 bmp_data_bytes_per_row = bmp_bytes_per_pixel * bmp_width;

    //TODO what is the best way to round up to the next word with x86-64
    //total scanline bytes = data bytes rounded to nearest word
    //TODO what is the big deal with word alignment
    //     we're doing word alignment just cuz opengl defaults to that
    u32 bmp_pitch = ((bmp_data_bytes_per_row + 3)/4)*4;

    u32 dest_padding = ((bmp_width + 3)/4)*4 - bmp_width;
                
    //layout in mem: BGR BGR
    u8 *row = ((u8*)item_contents_buf + bmp_data_offset) + (bmp_height - 1)*bmp_pitch + 2;
    for(int y = 0; y < bmp_height; y++)
    {
        at = row;
        for(int x = 0; x < bmp_width; x++)
        {
            push(u8, at[0]);
            at += 3; //go to the next red pixel
        }
        row -= bmp_pitch;
        to += dest_padding;
    }
    out_bytes_processed[0] += to - new_data_loc;
    return true;
}

internal b32
LoadSound_wav(u8 *wav_file, u8 *new_data_loc, u32 *out_bytes_processed)
{
    u8 *at = wav_file;
    u8 *to = new_data_loc;

    RIFFHeader *hdr = (RIFFHeader *)wav_file;
    Subchunk1 *subchunk1 = (Subchunk1 *)(wav_file + sizeof(RIFFHeader));
    u32 subchunk2_offset = subchunk1->Subchunk1Size + 12 + 8;
    Subchunk2 *subchunk2 = (Subchunk2 *)(wav_file + subchunk2_offset);

    if(!(hdr->ChunkID[0] == 'R' &&
         hdr->ChunkID[1] == 'I' &&
         hdr->ChunkID[2] == 'F' &&
         hdr->ChunkID[3] == 'F' &&
         hdr->Format[0] == 'W' &&
         hdr->Format[1] == 'A' &&
         hdr->Format[2] == 'V' &&
         hdr->Format[3] == 'E'))

    {
        fprintf(stderr, "Doesn't look like its a (cananoical) wav file.\n");
        return false;
    }

    if(!(subchunk1->Subchunk1Size == 16 || subchunk1->Subchunk1Size == 18) || subchunk1->AudioFormat != 1)
    {
        fprintf(stderr, "Doesn't look like PCM data.\n");
        return false;
    }

    //NOTE:: we require some things about the wav file to be
    if(!(subchunk1->NumChannels == 1 || subchunk1->NumChannels == 2))
    {
        fprintf(stderr, "Currently only 1 or 2 channel .wav file is supported. \n");
        return false;
    }
    if(!(subchunk1->SampleRate == 44100))
    {
        fprintf(stderr, "Currently only 44100 sample rate is supported. \n");
        return false;
    }
    if(!((subchunk1->BitsPerSample == 16)))
    {
        fprintf(stderr, "Currently only 16 bit per sample .wav file is supported. \n");
        return false;
    }

    u32 frames_count = subchunk2->Subchunk2Size / (subchunk1->NumChannels * subchunk1->BitsPerSample/8);

    push(u16, subchunk1->NumChannels);
    push(u32, frames_count);

    u8 *end = to;
    if(subchunk1->NumChannels == 1)
    {
        u16 *at = (u16 *) &subchunk2->Data;
        u16 *dest = (u16 *)to;
        for(int i = 0; i < frames_count; i++)
        {
            *dest++ = *at++;
        }

        end = (u8 *)dest;
    }
    else if (subchunk1->NumChannels == 2)
    {
        u32 *at = (u32 *) &subchunk2->Data;
        u32 *dest = (u32 *)to;
        for(int i = 0; i < frames_count; i++)
        {
            *dest++ = *at++;
        }
        end = (u8 *)dest;
    }
    out_bytes_processed[0] += end - new_data_loc;
    return true;
}

internal void
AddAPA(char *apa_filename, AssetType type, char *item_filename, char *second_item_filename, char *item_name)
{
    int apa_file_handle = 0;
    if(!LoadExistingFileAndPrintErrors(&apa_file_handle, apa_filename, true))
        return;

    u32 file_contents_buf_size = megabytes(10);
    void *file_contents_buf = arena_pushsize(&arena, file_contents_buf_size);
    u32 file_size = ReadFileIntoBuffer(apa_file_handle, file_contents_buf, file_contents_buf_size);
    if(!file_size)
    {
        fprintf(stderr, "Could not read the file %s", apa_filename);
        goto close_and_done;        
    }
    u8 *at = (u8 *)file_contents_buf;

    AssetFileHeader *hdr;
    hdr = (AssetFileHeader *)at;
    at += sizeof(AssetFileHeader);
    
    if(!(CheckMagic(hdr)))
    {
        fprintf(stderr, "Invalid or corrupt .apa file!\n");
        goto close_and_done;
    }
    else if(!(hdr->version <= MAX_SUPPORTED_VERSION))
    {
        fprintf(stderr, "Unsupported .apa file version: %hu!\n", hdr->version);
        goto close_and_done;
    }

    int item_file_handle = 0;
    if(!LoadExistingFileAndPrintErrors(&item_file_handle, item_filename, false))
        goto close_and_done;

    int second_item_file_handle = 0;
    if(second_item_filename)
    {
        if(!LoadExistingFileAndPrintErrors(&second_item_file_handle, second_item_filename, false))
            goto close_and_done;

    }

    //NOTE we can also use seeking to determine the filesizes
    u64 first_filesize = _filelengthi64(item_file_handle);
    u64 second_filesize = second_item_file_handle ? _filelengthi64(second_item_file_handle) : 0;
    u64 total_filesize = first_filesize + second_filesize;
    void *item_contents_buf = arena_pushsize(&arena, total_filesize);
    u64 second_item_offset = first_filesize;
    
    if(!ReadFileIntoBuffer(item_file_handle, item_contents_buf, first_filesize))
    {
        fprintf(stderr, "Could not read the file %s", item_filename);
        goto close_and_done;        
    }
    if(second_item_filename && !ReadFileIntoBuffer(second_item_file_handle,
                                                   (u8 *)item_contents_buf + second_item_offset, second_filesize))
    {
        fprintf(stderr, "Could not read the file %s", item_filename);
        goto close_and_done;        
    }

    char *item_extension = GetFileExtension(item_filename);
    u8 *asset_header_loc = at;
    at = asset_header_loc + sizeof(AssetHeader) * hdr->num_assets;
    u8 *old_name_chunk_loc = at;
    u8 *old_data_chunk_loc = StartOfData((u8 *)old_name_chunk_loc, asset_header_loc, hdr->num_assets);    

    u8 *new_data_loc = 0;
    AssetHeader *new_ahdr;

    APAfile apa;
    apa.file_contents = (u8 *)file_contents_buf;
    apa.filesize = file_size;

    APALocs old_locs;
    old_locs.data_chunk = old_data_chunk_loc;
    old_locs.name_chunk = old_name_chunk_loc;


    InsertNewAsset(&apa, item_name, type, &old_locs, &new_data_loc, &new_ahdr);
    u32 total_data_bytes_created = 0;
    switch(type)
    {
        case sound:
        {
            if(string_equal(item_extension, "wav"))
            {
                if(!LoadSound_wav((u8 *)item_contents_buf, new_data_loc, &total_data_bytes_created))
                    goto close_and_done;
            }
            else
            {
                fprintf(stderr, "Currently, .wav files are supported for sound!\n");
                goto close_and_done;
            }
        }break;
        case texture1c:
        {
            if(string_equal(item_extension, "bmp"))
            {
                if(!LoadTex1c_bmp((u8 *)item_contents_buf, new_data_loc, &total_data_bytes_created))
                    goto close_and_done;
            }
            else
            {
                fprintf(stderr, "Currently, only 24 bit depth *.BMP files are supported for texture1c!\n");
                goto close_and_done;
            }
        }break;
        case font:
        {
            //the item_contents_buf contains the .bmp (first file)
            //let's load as if we are loading a texture1c first
            if(string_equal(item_extension, "bmp"))
            {
                if(!LoadTex1c_bmp((u8 *)item_contents_buf, new_data_loc, &total_data_bytes_created))
                    goto close_and_done;
            }
            else
            {
                fprintf(stderr, "Currently, only 24 bit depth *.BMP files are supported for rasterized fonts!\n");
                goto close_and_done;
            }
			
			u32 width = ((u32 *)new_data_loc)[0];
            u32 height = ((u32 *)new_data_loc + 1)[0];
            u32 pixels_size = ((width+3)/4)*4 * height;
            u8 *font_descriptor_loc = new_data_loc + TEX1C_HEADER_SIZE + pixels_size;
            
            if(!LoadFont_aft((u8 *)item_contents_buf + second_item_offset, font_descriptor_loc,
                             &total_data_bytes_created))
                goto close_and_done;

        }break;
        default:
                fprintf(stderr, "That asset type is currently not supported!\n");
            break;
    }
    
    new_ahdr->size_of_data_in_bytes = total_data_bytes_created;
    hdr->num_assets++;

    u32 total_size = total_data_bytes_created + (new_data_loc - file_contents_buf);
    _lseek(apa_file_handle, SEEK_SET, 0);
    if(_write(apa_file_handle, file_contents_buf, total_size) < 0)
    {
        fprintf(stderr, "The file could not be written to!\n");
    }
	
	// HACK we haphazardly append the file footer here, and ONLY here.
	//      so its not appended in other CL operations
	long new_file_size = _tell(apa_file_handle);
	new_file_size += sizeof(long)+ (sizeof("APAF") - 1);
	
	u8 lolo = (new_file_size >> 0) & 0xff;
	u8 lohi = (new_file_size >> 8) & 0xff;
	u8 hilo = (new_file_size >> 16) & 0xff;
	u8 hihi = (new_file_size >> 24) & 0xff;
	
	if(_write(apa_file_handle, &lolo, 1) < 0  ||
	   _write(apa_file_handle, &lohi, 1) < 0 ||
	   _write(apa_file_handle, &hilo, 1) < 0 ||
	   _write(apa_file_handle, &hihi, 1) < 0 ||
	   _write(apa_file_handle, "APAF", sizeof("APAF") - 1) < 0)
	{
		fprintf(stderr, "The file could not be written to!\n");
	}
	
close_and_done:
    if(apa_file_handle)
        _close(apa_file_handle);
    if(item_file_handle)
        _close(item_file_handle);
    if(second_item_file_handle)
        _close(second_item_file_handle);
            
}

internal u32
CutData(u8 *file_contents_buf, u8 *cut_start, u32 cut_size, u32 old_filesize)
{
    u8 *cut_end = cut_start + cut_size;
    memmove(cut_start, cut_end, (file_contents_buf + old_filesize) - cut_end);

    u32 new_file_size = old_filesize - (cut_end - cut_start);
    return new_file_size;
}

internal void
RemoveAPA(char *filename, char *item)
{
    int apa_file_handle = 0;
    if(!LoadExistingFileAndPrintErrors(&apa_file_handle, filename, true))
        return;

    //NOTE::because we are shrinking the file...we do not need extra buffer space let's just load into mem
    void *file_contents_buf;
    u32 old_file_size = ReadFileIntoMemory(apa_file_handle, &file_contents_buf);
    if(!old_file_size)
    {
        fprintf(stderr, "Could not read the file %s", filename);
        goto close_and_done;        
    }

    u8 *at = (u8 *) file_contents_buf;
    AssetFileHeader *header = (AssetFileHeader *)file_contents_buf;

    if(!(CheckMagic(header)))
    {
        fprintf(stderr, "Invalid or corrupt .apa file!\n");
        goto close_and_done;
    }
    else if(!(header->version <= MAX_SUPPORTED_VERSION))
    {
        fprintf(stderr, "Unsupported .apa file version: %hu!\n", header->version);
        goto close_and_done;
    }

    at += sizeof(AssetFileHeader);
    u8 *asset_header_chunk_base = at;
    AssetHeader *remove_target_ahdr = 0;

    char *name_chunk_base = (char *)asset_header_chunk_base + sizeof(AssetHeader) * header->num_assets;
    
    int remove_asset_index = 0;
    for(remove_asset_index;
        remove_asset_index < header->num_assets;
        remove_asset_index++)
    {
        AssetHeader *ahdr = (AssetHeader *)asset_header_chunk_base + remove_asset_index;

        if(string_equal(name_chunk_base + ahdr->name_offset_in_bytes, item))
        {
            remove_target_ahdr = ahdr;
            break;
        }
    }

    if(!remove_target_ahdr)
    {
        fprintf(stderr, "Could not find %s in %s\n", item, filename);
        goto close_and_done;
    }


    u32 new_file_size = old_file_size;
    u8 *data_cut_start = (u8 *)file_contents_buf + remove_target_ahdr->data_offset_in_bytes;
    new_file_size = CutData((u8 *)file_contents_buf, data_cut_start,
                            remove_target_ahdr->size_of_data_in_bytes, new_file_size);

    u8 *name_cut_start = (u8 *)name_chunk_base + remove_target_ahdr->name_offset_in_bytes;

    u32 remove_name_string_len = string_length((char *)name_cut_start) + 1; //add the null terminator!
    new_file_size = CutData((u8 *)file_contents_buf, name_cut_start,
                            remove_name_string_len, new_file_size);

    new_file_size = CutData((u8 *)file_contents_buf, (u8 *)remove_target_ahdr,
                            sizeof(AssetHeader), new_file_size);
    
    header->num_assets--;
    _chsize(apa_file_handle, new_file_size);
    _lseek(apa_file_handle, 0, SEEK_SET);

    for(int i = 0; i < header->num_assets; i++)
    {
        AssetHeader *ahdr = (AssetHeader *)asset_header_chunk_base + i;

        ahdr->data_offset_in_bytes -= remove_target_ahdr->size_of_data_in_bytes + remove_name_string_len +
            sizeof(AssetHeader);
        ahdr->name_offset_in_bytes -= remove_name_string_len;
    }

    if(_write(apa_file_handle, file_contents_buf, new_file_size) < 0)
    {
        fprintf(stderr, "Could not write the file %s", filename);
        goto close_and_done;
    }

close_and_done:
    if(apa_file_handle)
        _close(apa_file_handle);
}

int main(int argc, char **argv)
{
    CommandLineContext cl_context = {};
    parse_cl(argc, argv, &cl_context);

    u32 arenasize = megabytes(60);
    arena.base = (u8 *)malloc(arenasize);
    arena.size = arenasize;
    
    switch(cl_context.cloperation)
    {
        case CLOperation_create:
        {
            if(cl_context.file_name)
            {
                b32 has_extension = false;
                char *at = cl_context.file_name;
                while(at[0])
                {
                    if(at[0] == '.')
                    {
                        has_extension = true;
                        break;
                    }
                    at++;
                }

                char *real_file_name = cl_context.file_name;
                if(!has_extension)
                {
                    u32 raw_file_namelen = at - cl_context.file_name;
                    real_file_name = (char *)arena_pushsize(&arena, raw_file_namelen + 5); //".apa + nullterminator"
                    char *at = string_cat(real_file_name, cl_context.file_name);
                    string_cat(at, ".apa");
                }
                else
                {
                    real_file_name = cl_context.file_name;
                }
                CreateAPA(cl_context.file_name);                
            }
            else
            {
                fprintf(stderr, "Please specify a filename!\n");
            }
        }break;
        case CLOperation_list:
        {
            if(cl_context.file_name)
            {
                ListAPA(cl_context.file_name);                
            }
            else
            {
                fprintf(stderr, "Please specify a filename!\n");
            }
        }break;
        case CLOperation_remove:
        {
            if(!cl_context.file_name)
            {
                fprintf(stderr, "Please specify a filename!\n");
                goto done;
            }
            if (!cl_context.item_name)
            {
                fprintf(stderr, "Please specify an item name!\n");                
                goto done;
            }

            RemoveAPA(cl_context.file_name, cl_context.item_name);
        }break;
        case CLOperation_add:
        {
            if(!cl_context.type)
            {
                fprintf(stderr, "Please specify an asset type (texture1c, texture3c, sound, font)!\n");
                goto done;
            }
            else if(cl_context.type == invalid)
            {
                fprintf(stderr, "Asset type is not valid (texture1c, texture3c, sound, font)!\n");
                goto done;
            }
            if(!cl_context.file_name)
            {
                fprintf(stderr, "Please specify a filename!\n");
                goto done;
            }
            if (!cl_context.item_filename)
            {
                fprintf(stderr, "Please specify an item filename!\n");
                goto done;
            }
            if(cl_context.type == font && !cl_context.second_item_filename)
            {
                fprintf(stderr, "Please specify an a second item filename (of the .aft font file!)!\n");
                goto done;
            }
            char *item_name;
            if(cl_context.item_name)
            {
                item_name = cl_context.item_name;
                u32 len = string_length(item_name);
                if(len > MAX_NAME_LENGTH)
                {
                    goto name_too_long;
                }
            }
            else
            {
                char item_name_buf[200];
                char *at = string_cat(item_name_buf, cl_context.item_filename);
                u32 len = at - item_name_buf;
                //TODO is this good numerics??
                while(len-- > 0)
                {
                    if(at[0] == '.')
                    {
                        at[0] = 0;
                    }
                    at--;
                }
                item_name = item_name_buf;
                if(len > MAX_NAME_LENGTH)
                {
                name_too_long:
                    fprintf(stderr, "APA item names can only be a maximum of %u in length!\n", MAX_NAME_LENGTH);
                    return -1;
                }
            }
            AddAPA(cl_context.file_name, cl_context.type, cl_context.item_filename,
                   cl_context.second_item_filename, item_name);
        }break;
        default:
        {
            fprintf(stderr, "Please specify an operation!\n");
        }
    }
done:;
}
