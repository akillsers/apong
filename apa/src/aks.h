#if SLOW
#define assert(expression) if(!(expression)) {__debugbreak();}
#else
#define assert(Expression)
#endif

#define invalidcodepath assert(!"Invalid code path!")
#define stop(stopmessage) assert(false) 

#if INTERNAL
#endif

#define internal static
#define global_variable static
#define local_persist static

#include <stdint.h>
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef i32 b32;
typedef float f32;
typedef double f64;

#define kilobytes(Value) (Value * 1024)
#define megabytes(Value) (kilobytes(Value) * 1024)
#define gigabytes(Value) (megabytes(Value) * 1024)

#define min(A, B) (A < B ? A : B)
#define max(A, B) (A > B ? A : B)

#define arraycount(array) (sizeof(array)/sizeof(array[0]))

#define true 1
#define U32_MAX 4294967295
#define I16_MAX 32767
#define I16_MIN -32768
#define I32_MAX 2147483647
#define I32_MIN -2147483648

#define META_EXCLUDE
#define META_EXCLUDE_FILE
#define ____file
